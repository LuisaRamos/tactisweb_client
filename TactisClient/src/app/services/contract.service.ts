import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Contract } from '../model/contract';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ContractChanges } from '../model/contractchanges';
import { NewClientAndContract } from '../util/view/newclientandcontract';
import { Installation } from '../model/installation';
import { ContractoParaVigor } from '../util/view/ContratoParaVigor';
import { ClientContractDividaView } from '../util/view/ClientContractDividaView';
import { ClientContracts } from '../util/view/clientcontracts';
import { AssociateData } from '../util/view/associatedata';

@Injectable({
  providedIn: 'root'
})
export class ContractService extends GenericService {
  constructor(httpClient: HttpClient, private http: HttpClient, auth: AuthService) {
    super("protected/contracts", httpClient, auth);
  }

  createContract(contract: Contract): Observable<Contract> {
    return super.create(contract);
  }

  updateContract(contractId:number, contract:Contract) : Observable<Contract> {
    return super.update(contractId,contract);
  }

  createContract2(contract: NewClientAndContract): Observable<any> {
    return this.http.post(`${this.url}/client`, contract,{ headers: this.generateHeaders() });
  }

  createContractChanges(id:number, cc: ContractChanges): Observable<any> {
    return this.http.post(`${this.url}/${id}/changes`, cc,{ headers: this.generateHeaders() });
  }

  getChangesOfContract(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}/changes`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicsOfContract(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}/clinics`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getContractCodes(): Observable<any> {
    return this.http.get(`${this.url}/new/codes`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getContractAllCodes(): Observable<any> {
    return this.http.get(`${this.url}/allcodes`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getContractInstallations(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/installations`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getLastContractNumber(type:string): Observable<any> {
    return this.http.get(`${this.url}/last/${type}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }
  
  getUnfinishedContracts(): Observable<any> {
    return this.http.get(`${this.url}/unfinished`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  checkIfContractNumberExists(cn:string): Observable<any> {
    return this.http.get(`${this.url}/${cn}/repeated`, { headers: this.generateHeaders() });
  }

  updateContractChanges(contract:number, changes:ContractChanges[]) : Observable<any> {
    return this.http.put(`${this.url}/${contract}/changes`, changes,{ headers: this.generateHeaders() });
  }

  forceUpdateContractChanges(contract:number, changes:ContractChanges[]) : Observable<any> {
    return this.http.put(`${this.url}/${contract}/changes/force`, changes,{ headers: this.generateHeaders() });
  }

  addInstallationToContract(contract:number, installation:Installation): Observable<any> {
    return this.http.put(`${this.url}/${contract}/installation`, installation,{ headers: this.generateHeaders() });
  }

  deleteContractChange(cc:number): Observable<any> {
    return this.http.delete(`${this.url}/changes/${cc}`,{ headers: this.generateHeaders() });
  }

  updateContractAndPutInVigor(id:number, contract:ContractoParaVigor) : Observable<any> {
    return this.http.put(`${this.url}/${id}/vigor`, contract,{ headers: this.generateHeaders() });
  }

  getAllContracts():Observable<ClientContracts[]> {
    return super.getAll();
  }

  getListRenovationsAluguer(): Observable<any> {
    return this.http.get(`${this.url}/renovations/aluguer`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getListRenovationsClienteInicial(): Observable<any> {
    return this.http.get(`${this.url}/renovations/iniciais`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getListRenovationsclientFidRenReint(): Observable<any> {
    return this.http.get(`${this.url}/renovations/outros`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getListagemClientesIniciais(datainicio:string, datafim:string): Observable<any> {
    let params = new HttpParams()
    .set('datainicio', datainicio)
    .set('datafim', datafim);
    return this.http.get(`${this.url}/clientesiniciais`, { headers: this.generateHeaders(), params:params }).pipe(map(this.extractData));
  }

  getListagemCirculares(cm:boolean, cminicial:boolean, aluguerano:boolean, aluguermes:boolean, potenciais:boolean, desiste:boolean, scm:boolean, fechou:boolean): Observable<any> {
    let params = new HttpParams()
    .set('cm', "" + cm)
    .set('cminicial', "" + cminicial)
    .set('aluguerano', "" + aluguerano)
    .set('aluguermes', "" + aluguermes)
    .set('potenciais', "" + potenciais)
    .set('desiste', "" + desiste)
    .set('fechou', "" + fechou)
    .set('scm', "" + scm);
    return this.http.get(`${this.url}/circulares`, { headers: this.generateHeaders(), params:params }).pipe(map(this.extractData));
  }
  
  createContractAssociation(pp:number, pa:number, proposta:string, type:string, notes:string, othercontract:number, date:number, associateData:AssociateData): Observable<any> {
    let params = new HttpParams()
    .set('pp', "" + pp)
    .set('pa', "" + pa)
    .set('proposta', "" + proposta)
    .set('type', "" + type)
    .set('notes', "" + notes)
    .set('date', "" + date)
    .set('othercontract', "" + othercontract);
    return this.http.post(`${this.url}/association`, associateData ,{ headers: this.generateHeaders(), params:params });
  }

}
