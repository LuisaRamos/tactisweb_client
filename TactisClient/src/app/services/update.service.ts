import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Installation } from '../model/installation';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UpdateService extends GenericService {
  constructor(httpClient: HttpClient, private http: HttpClient, auth: AuthService) {
    super("protected/updates", httpClient, auth);
  }

 changeAllowUpdate(bd:string, installations:Installation[]):Observable<any> {
    return this.http.put(`${this.url}/updatedallowed/${bd}`, installations,{ headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

}
