import { TestBed } from '@angular/core/testing';

import { AditamentoService } from './aditamento.service';

describe('AditamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AditamentoService = TestBed.get(AditamentoService);
    expect(service).toBeTruthy();
  });
});
