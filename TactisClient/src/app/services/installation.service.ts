import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Client } from '../model/client';
import { map } from 'rxjs/operators';
import { Installation } from '../model/installation';
import { ContractAndInstallation } from '../util/view/contractandinstallation';
import { PemSliderDb } from '../model/pemSliderDb';
import { NovigestSliderDb } from '../model/novigestSliderDb';

@Injectable({
  providedIn: 'root'
})
export class InstallationService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/installations", httpClient, auth);
  }

  getInstallations(): Observable<Installation[]> {
    return super.getAll();
  }

  getInstallationsUnassigned(): Observable<any> {
    return this.http.get(`${this.url}/unassigned`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  sendXMLFile(file): Observable<any> {
    return this.http.post(`${this.url}/xml/ng`, file, { headers: this.generateHeaders() });
  }

  getInstallation(id:number): Observable<Installation> {
    return super.getById(id);
  }

  updateInstallation(id: number, installation: Installation): Observable<any> {
    return super.update(id, installation);
  }

  getInstallationBetweenClientClinic(client:number, clinic:number): Observable<any> {
    return this.http.get(`${this.url}/client/${client}/clinic/${clinic}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getInstallationOfClinic( clinic:number): Observable<any> {
    return this.http.get(`${this.url}/clinic/${clinic}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getInstallationClient(installation:number): Observable<any> {
    return this.http.get(`${this.url}/${installation}/client`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getInstallationsAssignedToUserNotDone(userId:number): Observable<any> {
    return this.http.get(`${this.url}/notdone/assigned/${userId}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getAllInstallationsAssignedAndNotDone(): Observable<any> {
    return this.http.get(`${this.url}/notdone/assigned`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getAllInstallationsAndItsClinics(db:string): Observable<any> {
    return this.http.get(`${this.url}/${db}/clinics`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getInstallationsAndItsClinicsWithKeyword(key:string, db:string): Observable<any> {
    return this.http.get(`${this.url}/${db}/search/${key}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  readInstallationMonitor(name:string): Observable<any> {
    return this.http.get(`${this.url}/monitor/${name}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  checkInstallationNameExists(name:string, id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/exists/${name}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getNovigestSliders(): Observable<any> {
    return this.http.get(`${this.url}/ng/activations`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getPemSliders(): Observable<any> {
    return this.http.get(`${this.url}/pem/activations`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  addPemToInstallation(id:number, pem:PemSliderDb): Observable<any> {
    return this.http.put(`${this.url}/${id}/activations/pem`,  pem,{ headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  addNovigestToInstallation(id:number, ng:NovigestSliderDb): Observable<any> {
    return this.http.put(`${this.url}/${id}/activations/ng`,  ng,{ headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  deletePemActivation(id:number, pemid:number): Observable<any> {
    return this.http.delete(`${this.url}/${id}/activations/ng/${pemid}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  deleteNovigestActivation(id:number, ngid:number): Observable<any> {
    return this.http.delete(`${this.url}/${id}/activations/ng/${ngid}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  deleteInstallation(id:number): Observable<any> {
    return super.delete(id);
  }

  getInstallationsWithoutNovigestSlider(): Observable<any> {
    return this.http.get(`${this.url}/available/novigest`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getInstallationsWithoutNovipemSlider(): Observable<any> {
    return this.http.get(`${this.url}/available/novipem`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getInstallationContract(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/contract`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }
}
