import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Intervention } from '../model/intervention';
import { InterventionData } from '../util/view/interventiondata';

@Injectable({
  providedIn: 'root'
})
export class InterventionService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/interventions", httpClient, auth);
  }

  getIntervention(id: number): Observable<Intervention> {
    return super.getById(id);
  }

  createIntervention(intervention:InterventionData): Observable<Intervention> {
    return super.create(intervention);
  }

  getInterventionTypes(): Observable<any>{
    return this.http.get(`${this.url}/types`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getInterventionOfClinic(id:number): Observable<any>{
    return this.http.get(`${this.url}/clinic/${id}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getProductActivities(): Observable<any>{
    return this.http.get(`${this.url}/productactivities`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

}
