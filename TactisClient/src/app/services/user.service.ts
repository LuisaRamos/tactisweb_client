import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TactisUser } from '../model/tactisuser';
import { UserRole } from '../model/userrole';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/users", httpClient, auth);
  }

  getTactisUsers(): Observable<TactisUser[]> {
    return super.getAll();
  }

  getTactisUser(id: number): Observable<TactisUser> {
    return super.getById(id);
  }

  createTactisUser(tactisUser:TactisUser): Observable<TactisUser> {
    return super.create(tactisUser);
  }

  updateTactisUser(id: number, tactisUser: TactisUser): Observable<any> {
    return super.update(id, tactisUser);
  }

  deleteTactisUser(id: number): Observable<any> {
    return super.delete(id);
  }

  getTactisUserByUsername(username:string): Observable<any>{
    return this.http.get(`${this.url}/${username}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getActiveUsers(): Observable<any>{
    return this.http.get(`${this.url}/active`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getAssignedEventsToUser(username:string): Observable<any> {
    return this.http.get(`${this.url}/${username}/events`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
 
  }

  checkIfEmailExists(email:string): Observable<any> {
    return this.http.get(`${this.url}/repeated/email/${email}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  checkIfUsernameExists(username:string): Observable<any> {
    return this.http.get(`${this.url}/repeated/username/${username}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }
  
}