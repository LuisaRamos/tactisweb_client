import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Training } from '../model/training';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TrainingService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/trainings", httpClient, auth);
  }

  getTrainings(): Observable<Training[]> {
    return super.getAll();
  }

  getTraining(id:number): Observable<Training> {
    return super.getById(id);
  }

  createTraining(training:Training): Observable<Training> {
    return super.create(training);
  }

  getTrainingsAssignedToUserNotDone(userId:number): Observable<any> {
    return this.http.get(`${this.url}/notdone/assigned/${userId}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getAllTrainingsAssignedAndNotDone(): Observable<any> {
    return this.http.get(`${this.url}/notdone/assigned`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getTrainingOfClinic(id:number): Observable<any> {
    return this.http.get(`${this.url}/clinic/${id}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

}
