import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CalendarEvent } from '../model/calendarEvent';
import {Event} from '../model/event';
import { map } from 'rxjs/operators';
import { EventAndUsers } from '../util/view/eventAndUsers';

@Injectable({
  providedIn: 'root'
})
export class CalendarEventsService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/events", httpClient, auth);
  }
  getCalendarEvents(): Observable<CalendarEvent[]> {
    return super.getAll();
  }

  createEvent(event:Event):Observable<CalendarEvent> {
    return super.create(event);
  }

  getEventByDescription(descr:string): Observable<any> { 
    return this.http.get(`${this.url}/${descr}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  updateEvent(id:number, event:Event): Observable<any> {
    return super.update(id, event);
  }

  getAllTrainingsAssignedAndNotDone(): Observable<any> {
    return this.http.get(`${this.url}/trainings/notdone/assigned`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }
  getAllTrainingsAssignedAndNotDoneToUser(userId:number): Observable<any> {
    return this.http.get(`${this.url}/trainings/notdone/assigned/user/${userId}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }
  getAllOtherEventsAssignedAndNotDone(): Observable<any> {
    return this.http.get(`${this.url}/others/notdone/assigned`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }
  getAllOtherEventsAssignedAndNotDoneToUser(userId:number): Observable<any> {
    return this.http.get(`${this.url}/others/notdone/assigned/user/${userId}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  updateTrainingEvent(eventId:number, obj:EventAndUsers): Observable<any> {
    return this.http.put(`${this.url}/trainings/${eventId}`, obj,{ headers: this.generateHeaders() });
  }
  updateOtherEvent(eventId:number, obj:EventAndUsers): Observable<any> {
    return this.http.put(`${this.url}/others/${eventId}`, obj,{ headers: this.generateHeaders() });
  }
  updateTrainingEventDone(eventId:number, obj:Event): Observable<any> {
    return this.http.put(`${this.url}/trainings/${eventId}/done`, obj,{ headers: this.generateHeaders() });
  }

  updateOtherEventDone(eventId:number, obj:Event): Observable<any> {
    return this.http.put(`${this.url}/others/${eventId}/done`, obj,{ headers: this.generateHeaders() });
  }

  updateInstallationEvent(eventId:number, obj:EventAndUsers): Observable<any> {
    return this.http.put(`${this.url}/installations/${eventId}`, obj,{ headers: this.generateHeaders() });
  }

  getEventAndUsersByDescription(descr:string): Observable<any> {
    return this.http.get(`${this.url}/users/${descr}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  
}
