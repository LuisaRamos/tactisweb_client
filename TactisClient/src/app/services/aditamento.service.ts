import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Aditamento } from '../model/aditamento';
import { Observable } from 'rxjs';
import { AditamentoCreate } from '../util/view/aditamentocreate';
import { Client } from '../model/client';
import { map } from 'rxjs/operators';
import { ContractoParaVigor } from '../util/view/ContratoParaVigor';

@Injectable({
  providedIn: 'root'
})
export class AditamentoService extends GenericService {
  constructor(httpClient: HttpClient, private http: HttpClient, auth: AuthService) {
    super("protected/aditamentos", httpClient, auth);
  }

  getAditamentos(): Observable<Aditamento[]> {
    return super.getAll();
  }

  createAditamento(adit: AditamentoCreate): Promise<any> {
    return this.http.post(this.url, adit, { headers: this.generateHeaders() }).toPromise();
  }

  getAditamentoClient(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}/client`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getAditamento(id: number): Observable<Aditamento> {
    return super.getById(id);
  }

  updateAditamento(id: number, aditamento: Aditamento): Observable<any> {
    return super.update(id, aditamento);
  }

  updateAditamentoAndPutInVigor(id:number, aditamento:Aditamento) : Observable<any> {
    return this.http.put(`${this.url}/${id}/vigor`, aditamento,{ headers: this.generateHeaders() });
  }
}
