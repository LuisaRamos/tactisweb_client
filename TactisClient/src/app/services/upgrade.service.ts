import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Upgrade } from '../model/upgrade';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UpgradeService extends GenericService {
  constructor(httpClient: HttpClient, private http: HttpClient, auth: AuthService) {
    super("protected/upgrades", httpClient, auth);
  }

  createUpgrade(upgrade: Upgrade): Observable<Upgrade> {
    return super.create(upgrade);
  }

  updateUpgrade(upgrade:Upgrade): Observable<Upgrade> {
    return super.update(upgrade.upgradeId, upgrade);
  }
  getUpgrades(): Observable<Upgrade[]> {
    return super.getAll();
  }

  getDBUpgrades(db:string): Observable<any> {
    return this.http.get(`${this.url}/${db}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  deleteUpgrade(id:number): Observable<Upgrade[]> {
    return super.delete(id);
  }

}
