import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserRole } from '../model/userrole';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends GenericService {
  constructor(httpClient: HttpClient, auth: AuthService) {
    super("protected/users/roles", httpClient, auth);
  }

  getUserRoles(): Observable<UserRole[]> {
    return super.getAll();
  }

  getUserRole(id: number): Observable<UserRole> {
    return super.getById(id);
  }

  updateUserRole(id: number, role: UserRole): Observable<any> {
    return super.update(id, role);
  }

  deleteUserRole(id: number): Observable<any> {
    return super.delete(id);
  }

}
