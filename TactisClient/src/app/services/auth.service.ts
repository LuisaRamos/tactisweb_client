import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';

export const TOKEN_LS_LABEL: string = 'token';
export const CURRENT_USER_LS_LABEL: string = 'currentUser';
export const CURRENT_ROLE_LS_LABEL: string = 'currentUserRole';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //private headers = new Headers({ 'Content-Type': 'application/json' });

  //private readonly MANAGER_AUTH_URL: string = "http://localhost:4567";
  //private readonly MANAGER_AUTH_URL: string = 'http://192.168.70.93:4567';
  private readonly MANAGER_AUTH_URL: string = 'https://tactiscloud.no-ip.net:4568';
  expiredSession:boolean = false;

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Login.
   * It makes a POST request to server/login with username and password.
   * @param credential
   * @param password 
   */
  login(credential: string, password: string): Observable<any> {
    return this.httpClient.post(`${this.MANAGER_AUTH_URL}/auth/login`, { credential: credential, password: password })
      .pipe(map(data => {
        let user = <any>data;
        this.expiredSession = false;

        // login successful if there's a jwt token in the response
        if (user != null && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          //localStorage.setItem('currentUser', JSON.stringify(user.username));
          this.setToken(user.token);
          this.setUser(user.token);
          this.setRole(user.token);
        }

        return user;
      }));
  }

  logout() {
    localStorage.removeItem(TOKEN_LS_LABEL);
    localStorage.removeItem(CURRENT_USER_LS_LABEL);
    localStorage.removeItem(CURRENT_ROLE_LS_LABEL);
  }

  getToken(): string {
    return localStorage.getItem(TOKEN_LS_LABEL);
  }

   getRole(): string {
    return localStorage.getItem(CURRENT_ROLE_LS_LABEL);
  }

  private getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);
    if (decoded.expireTime === undefined) return null;

    const date = new Date(0);
    date.setUTCSeconds(decoded.expireTime);
    return date;
  }

  private getTokenUser(token: string): string {
    const decoded = jwt_decode(token);
    if (decoded.username === undefined) return null;

    return decoded.username;
  }

  private getTokenRole(token: string): string {
    const decoded = jwt_decode(token);
    if (decoded.userRole === undefined) return null;

    return decoded.userRole;
  }

  private setToken(token: string) {
    localStorage.setItem(TOKEN_LS_LABEL, token);
  }

  private setUser(token: string) {
    let user = this.getTokenUser(token);
    localStorage.setItem(CURRENT_USER_LS_LABEL, user);
  }

  private setRole(token: string) {
    let role = this.getTokenRole(token);
    localStorage.setItem(CURRENT_ROLE_LS_LABEL, role);
  }

  isTokenExpired(token?: string): boolean {
    if (!token) token = this.getToken();
    if (!token) return true;

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }

  public getActiveUser(): string {
    return localStorage.getItem(CURRENT_USER_LS_LABEL);
  }

  public sessionExpired():boolean {
    return this.expiredSession;
  }

  public setSessionExpired(){
    this.expiredSession = true;
  }




}
