import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { GenericService } from './generic.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Clinic } from '../model/clinic';
import { Training } from '../model/training';
import { ClinicInstallationAccess } from '../util/view/ClinicInstallationAccess';

@Injectable({
  providedIn: 'root'
})
export class ClinicService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/clinics", httpClient, auth);
  }

  createClinic(clinic:Clinic): Observable<Clinic> {
    return super.create(clinic);
  }
  createClinic2(installation:number, clinic:Clinic): Observable<any> {
    return this.http.post(`${this.url}/new/installation/${installation}`, clinic,{ headers: this.generateHeaders() });
  }

  getClinicOfClient2(cid:number): Observable<any> {
    return this.http.get(`${this.url}/${cid}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinics(): Observable<Clinic[]> {
    return super.getAll();
  }

  getPotencialClinics():Observable<any> {
    return this.http.get(`${this.url}/potencial`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicsWithKeyword(keyword: string): Observable<any> {
    return this.http.get(`${this.url}/key/${keyword}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicContract(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/contracts`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicAditamentos(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/aditamentos`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicAVL(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/avl`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicCM(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/cm`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  updateAccess(clinicId:number, installationId:number, access:ClinicInstallationAccess ): Observable<any> {
    return this.http.put(`${this.url}/${clinicId}/installation/${installationId}/access`, access, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  updateClinic(clinicId:number, clinic:Clinic): Observable<any> {
    return this.http.put(`${this.url}/${clinicId}`, clinic, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicProductType(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/producttype`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  deletePotencialClinics(obj:Object){
    return this.http.put(`${this.url}/potencial`, obj, { headers: this.generateHeaders() });
  }

  addTrainingToClinic(clinicId:number, training:Training): Observable<any> {
    return this.http.put(`${this.url}/${clinicId}/training`, training,{ headers: this.generateHeaders() });
  }

  checkIfNameExists(name: string): Observable<any> {
    return this.http.get(`${this.url}/exists/name/${name}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  checkClinicHasDebt(clinicId:number): Observable<any> {
    return this.http.get(`${this.url}/${clinicId}/hasDebt`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClientOfClinic(clinicId:number): Observable<any> {
    return this.http.get(`${this.url}/${clinicId}/client`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClinicLastChange(clinicId:number): Observable<any> {
    return this.http.get(`${this.url}/${clinicId}/lastchange`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }
}
