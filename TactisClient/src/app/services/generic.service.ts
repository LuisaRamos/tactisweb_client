import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export abstract class GenericService {
  private headers : HttpHeaders;
  url: string;

  constructor(url: string, private httpClient: HttpClient, private authSrv : AuthService) {
    //this.url = "http://192.168.70.93:4567/" + url;
    this.url = "https://tactiscloud.no-ip.net:4568/" + url;
    //this.url = "http://localhost:4567/" + url;
  }

  getById(id: any): Observable<any> {
    
    return this.httpClient.get(`${this.url}/${id}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getAll(): Observable<any> {
    return this.httpClient.get(this.url, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  create(obj: Object) : Observable<any> {
    return this.httpClient.post(this.url, obj,{ headers: this.generateHeaders() });
  }

  update(id: any, obj: Object) : Observable<any> {
    return this.httpClient.put(`${this.url}/${id}`, obj,{ headers: this.generateHeaders() });
  }

  delete(id: any) : Observable<any> {
    return this.httpClient.delete(`${this.url}/${id}`, { headers: this.generateHeaders() });
  }

   extractData(res: Response) { return res || {}; }

   generateHeaders(): HttpHeaders{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-TOKEN' : `${this.authSrv.getToken()}`,
      'Authorization': `Bearer ${this.authSrv.getToken()}`
    });

    return headers;
  }
}
