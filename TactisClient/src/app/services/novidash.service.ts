import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Kpi } from '../model/kpi';
import { map } from 'rxjs/operators';
import { Grafico } from '../model/grafico';

@Injectable({
  providedIn: 'root'
})
export class NovidashService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/novidash", httpClient, auth);
  }

  createKpi(kpi:Kpi): Observable<any> {
    return this.http.post(`${this.url}/kpi`, kpi,{ headers: this.generateHeaders() });
   }

  getAllKpi(): Observable<any>{
    return this.http.get(`${this.url}/kpi`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  updateKpi(kpi:Kpi, id:number): Observable<any>{
    return this.http.put(`${this.url}/kpi/${id}`, kpi, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  createGrafico(grafico:Grafico): Observable<any> {
    return this.http.post(`${this.url}/grafico`, grafico,{ headers: this.generateHeaders() });
   }

  getAllGraficos(): Observable<any>{
    return this.http.get(`${this.url}/grafico`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  updateGrafico(grafico:Grafico, id:number): Observable<any>{
    return this.http.put(`${this.url}/grafico/${id}`, grafico, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

}
