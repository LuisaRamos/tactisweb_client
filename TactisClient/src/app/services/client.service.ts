import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';

import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Client } from '../model/client';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { QuickSearchMultipleResult } from '../util/view/quickSearchMultipleResult';

@Injectable({
  providedIn: 'root'
})
export class ClientService extends GenericService {
  constructor(httpClient: HttpClient, private http:HttpClient, auth: AuthService) {
    super("protected/clients", httpClient, auth);
  }

  getClients(): Observable<Client[]> {
    return super.getAll();
  }

  getClinicOfClient(id:number, cid:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/clinic/${cid}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClientContracts(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/contracts`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClientContracts2(id:number): Observable<any> {
    return this.http.get(`${this.url}/${id}/contractslist`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClientContactReasons(): Observable<any> {
    return this.http.get(`${this.url}/contactreasons`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClientandItsClinics(): Observable<any> {
    return this.http.get(`${this.url}/clinics`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  getClientsWithKeyword(keyword: string): Observable<any> {
    return this.http.get(`${this.url}/key/${keyword}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  checkIfNIFExists(nif: string): Observable<any> {
    return this.http.get(`${this.url}/exists/nif/${nif}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  checkIfNameExists(name: string): Observable<any> {
    return this.http.get(`${this.url}/exists/name/${name}`, { headers: this.generateHeaders() }).pipe(map(this.extractData));
  }

  updateClinicOfClient(id:number, object:QuickSearchMultipleResult): Observable<any> {
    return this.http.put(`${this.url}/${id}/clinic`, object, { headers: this.generateHeaders()});
  }

  forceUpdateClinicOfClient(id:number, object:QuickSearchMultipleResult): Observable<any> {
    return this.http.put(`${this.url}/${id}/clinic/force`, object, { headers: this.generateHeaders()});
  }

  getClient(id: number): Observable<Client> {
    return super.getById(id);
  }
  getClient111(id: number): Observable<Client> {
    //https://stackoverflow.com/questions/29758765/json-to-typescript-class-instance/29759472#29759472
    let client:Client;
    this.http.get(`${this.url}/${id}`, { headers: this.generateHeaders() }).pipe(map(this.extractData)).subscribe((json:Object) => {
      
    });
    return super.getById(id);
  }

  createClient(client:Client): Observable<Client> {
    return super.create(client);
  }

  updateClient(id: number, client: Client): Observable<any> {
    return super.update(id, client);
  }
}
