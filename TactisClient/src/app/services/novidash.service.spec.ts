import { TestBed } from '@angular/core/testing';

import { NovidashService } from './novidash.service';

describe('NovidashService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NovidashService = TestBed.get(NovidashService);
    expect(service).toBeTruthy();
  });
});
