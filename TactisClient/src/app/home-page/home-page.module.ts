import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageRoutingModule } from './home-page-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MenubarModule } from 'primeng/menubar';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { InputMaskModule } from 'primeng/inputmask';
import { CalendarModule } from 'primeng/calendar';
import {FullCalendarModule} from 'primeng/fullcalendar';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PasswordModule } from 'primeng/password';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { HomeMenuComponent } from './home-menu/home-menu.component';
import { MessageService } from 'primeng/api';
import {TreeTableModule} from 'primeng/treetable';
import { HandleErrorComponent } from './home-menu/handle-error/handle-error.component';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [
    HomeMenuComponent,
  ],
  imports: [
    CommonModule,
    HomePageRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
   // routing,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    InputTextModule,
    ButtonModule,
    TreeTableModule,
    ConfirmDialogModule,
    MenubarModule,
    FormsModule,
    CommonModule,
    DialogModule,
    TooltipModule,
    MessagesModule,
    MessageModule,
    InputMaskModule,
    CalendarModule,
    FullCalendarModule,
    CardModule,
    ToastModule,
    ScrollPanelModule,
    PasswordModule,
    KeyFilterModule,
    ProgressSpinnerModule
  ],
  providers: [
    MessageService,
    HandleErrorComponent
],
})
export class HomePageModule { }
