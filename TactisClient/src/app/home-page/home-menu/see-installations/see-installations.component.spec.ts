import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeInstallationsComponent } from './see-installations.component';

describe('SeeInstallationsComponent', () => {
  let component: SeeInstallationsComponent;
  let fixture: ComponentFixture<SeeInstallationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeInstallationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeInstallationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
