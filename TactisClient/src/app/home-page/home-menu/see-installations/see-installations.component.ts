import { Component, OnInit } from '@angular/core';
import { ContractAndInstallation } from 'src/app/util/view/contractandinstallation';
import { TactisUser } from 'src/app/model/tactisuser';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService } from 'primeng/api';
import { Event } from 'src/app/model/event';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { EventAndUsers } from 'src/app/util/view/eventAndUsers';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Aditamento } from 'src/app/model/aditamento';
import { AditamentoService } from 'src/app/services/aditamento.service';

@Component({
  selector: 'app-see-installations',
  templateUrl: './see-installations.component.html',
  styleUrls: ['./see-installations.component.css']
})
export class SeeInstallationsComponent implements OnInit {

  users: TactisUser[] = [];
  data: ContractAndInstallation[];
  activeUser: TactisUser;
  display: boolean = false;
  selectedInstallation: Installation;
  selectedAditamento: Aditamento;
  newInstDate: Date = new Date();
  values: TactisUser[] = [];
  selectedEvent: Event;
  selectedEAU: EventAndUsers;
  dataLoaded: boolean = true;

  loading:boolean = true;
  openHelp:boolean = false;

  constructor(
    private installationService: InstallationService,
    private userService: UserService,
    private calendarService: CalendarEventsService,
    private router: Router,
    private messageService: MessageService,
    private errorService: HandleErrorComponent,
    private aditamentoService:AditamentoService
  ) {
  }

  ngOnInit() {
    this.getAllNotDoneInstallations();
    this.getActiveUsers();
  }

  getAllNotDoneInstallations() {
    this.installationService.getAllInstallationsAssignedAndNotDone().subscribe(data => {

      this.data = <ContractAndInstallation[]>data;
      this.dataLoaded = true;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações.", error);
    });
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.users = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores.", error);
    });
  }

  install(installation: Installation) {
    this.router.navigate(["home/install/pp/" + installation.installationId]);
  }

  confirmInstallationAlteration() {

    if (this.values.length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção!',
        detail: 'Tem de atribuir a pelo menos uma pessoa.',
        sticky: false,
        life: 1000
      });
      return;
    }

    if(this.selectedInstallation == null){
      this.updateAditamento();
    } else {
      this.updateInstallation();
    }
  }

  updateAditamento(){
    this.selectedAditamento.data = this.newInstDate;
    this.aditamentoService.updateAditamento(this.selectedAditamento.aditamentoId, this.selectedAditamento).subscribe(res => {

      this.selectedEvent.beginDate = this.newInstDate;
      let end2: Date = new Date(this.selectedEvent.beginDate);
      end2.setHours(end2.getHours() + 1);
      let instEndDate: Date = new Date(end2);
      this.selectedEvent.endDate = instEndDate;

      let eau: EventAndUsers = new EventAndUsers(this.selectedEvent, this.values);

      this.calendarService.updateInstallationEvent(this.selectedEvent.eventId, eau).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Dados da instalação alterados',
          sticky: false,
          life: 1000
        });
        this.selectedEAU = undefined;
        this.values = [];
        this.display = false;
        this.getAllNotDoneInstallations();

      }, error => {
        this.errorService.handleError("Não foi possível guardar as alterações.", error);
      });
    });
  }

  updateInstallation(){
    this.selectedInstallation.installationDate = this.newInstDate;
    this.installationService.updateInstallation(this.selectedInstallation.installationId, this.selectedInstallation).subscribe(res => {

      this.selectedEvent.beginDate = this.newInstDate;
      let end2: Date = new Date(this.selectedEvent.beginDate);
      end2.setHours(end2.getHours() + 1);
      let instEndDate: Date = new Date(end2);
      this.selectedEvent.endDate = instEndDate;

      let eau: EventAndUsers = new EventAndUsers(this.selectedEvent, this.values);

      this.calendarService.updateInstallationEvent(this.selectedEvent.eventId, eau).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Dados da instalação alterados',
          sticky: false,
          life: 1000
        });
        this.selectedEAU = undefined;
        this.values = [];
        this.display = false;
        this.getAllNotDoneInstallations();

      }, error => {
        this.errorService.handleError("Não foi possível guardar as alterações.", error);
      });
    });
  }

  editSchedule(data: ContractAndInstallation) {
    this.values = [];
    if (data.installation != undefined) {
      this.calendarService.getEventAndUsersByDescription(data.installation.description).subscribe(res => {
        this.selectedEAU = <EventAndUsers>res;
        this.selectedEvent = this.selectedEAU.event;
        this.selectedEAU.users.forEach(element1 => {
          this.users.forEach(element2 => {
            if (element1.username == element2.username) {
              this.values.push(element2);
              return;
            }
          });
        })

        this.display = true;
        this.selectedInstallation = data.installation;
        this.selectedAditamento = null;
        this.newInstDate = new Date(data.installation.installationDate);
      })
    } else {
      this.calendarService.getEventAndUsersByDescription(data.aditamento.description).subscribe(res => {
        this.selectedEAU = <EventAndUsers>res;
        this.selectedEvent = this.selectedEAU.event;
        this.selectedEAU.users.forEach(element1 => {
          this.users.forEach(element2 => {
            if (element1.username == element2.username) {
              this.values.push(element2);
              return;
            }
          });
        })

        this.display = true;
        this.selectedInstallation = null;
        this.selectedAditamento = data.aditamento;
        this.newInstDate = new Date(data.aditamento.data);
      })
    }

  }

  assignedTo(data: ContractAndInstallation) {
    if (data.installation != undefined) {
      this.calendarService.getEventAndUsersByDescription(data.installation.description).subscribe(res => {
        this.selectedEAU = <EventAndUsers>res;
      })
    } else {
      this.calendarService.getEventAndUsersByDescription(data.aditamento.description).subscribe(res => {
        this.selectedEAU = <EventAndUsers>res;
      })
    }
  }

  back() {
    this.router.navigate(['home']);
  }
}
