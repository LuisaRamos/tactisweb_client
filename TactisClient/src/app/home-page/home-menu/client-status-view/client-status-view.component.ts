import { Component, OnInit, Input } from '@angular/core';
import { ContractAndChanges } from 'src/app/util/view/ContractAndChanges';

@Component({
  selector: 'app-client-status-view',
  templateUrl: './client-status-view.component.html',
  styleUrls: ['./client-status-view.component.css']
})
export class ClientStatusViewComponent implements OnInit {

  debttime: string;

  @Input() contract:ContractAndChanges;

  constructor() { }

  ngOnInit() {
    this.calculateDivida(this.contract.cmPagamentoDivida == "Sim");
  }

  /**
   * Calcula se está com pagamentos em atraso em relação à última linha do contract changes
   * @param normalized 
   */
  calculateDivida(divida: boolean) {
    if (divida) {
      this.calculateDebtTime(this.contract.cmPagamentoData);
    }
  }

  calculateDebtTime(date: Date) {
    let time = this.monthDiff(date);
    let timedays;
    if (time < 1) {
      timedays = this.daysDiff(date);
      if (timedays == 1) {
        this.debttime = "" + timedays + " dia";
      } else {
        this.debttime = "" + timedays + " dias";
      }
    } else if (time == 1) {
      this.debttime = "" + time + " mês";
    } else {
      this.debttime = "" + time + " meses";
    }
  }

  monthDiff(d3: Date) {
    let d1: Date = new Date(d3);
    let d2: Date = new Date();
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? months : months + 1;
  }

  daysDiff(d3: Date) {
    let d1: Date = new Date(d3);
    let d2: Date = new Date();
    var diff = Math.abs(d1.getTime() - d2.getTime());
    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
    return diffDays;
  }

}
