import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientStatusViewComponent } from './client-status-view.component';

describe('ClientStatusViewComponent', () => {
  let component: ClientStatusViewComponent;
  let fixture: ComponentFixture<ClientStatusViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientStatusViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientStatusViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
