import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeTrainingsComponent } from './see-trainings.component';

describe('SeeTrainingsComponent', () => {
  let component: SeeTrainingsComponent;
  let fixture: ComponentFixture<SeeTrainingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeTrainingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeTrainingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
