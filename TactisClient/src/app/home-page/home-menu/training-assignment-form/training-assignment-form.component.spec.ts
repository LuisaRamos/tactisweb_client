import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingAssignmentFormComponent } from './training-assignment-form.component';

describe('TrainingAssignmentFormComponent', () => {
  let component: TrainingAssignmentFormComponent;
  let fixture: ComponentFixture<TrainingAssignmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingAssignmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingAssignmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
