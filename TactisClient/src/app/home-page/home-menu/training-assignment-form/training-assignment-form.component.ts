import { Component, OnInit, Input } from '@angular/core';
import { TactisUser } from 'src/app/model/tactisuser';
import { UserService } from 'src/app/services/user.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Training } from 'src/app/model/training';

@Component({
  selector: 'app-training-assignment-form',
  templateUrl: './training-assignment-form.component.html',
  styleUrls: ['./training-assignment-form.component.css']
})
export class TrainingAssignmentFormComponent implements OnInit {

  trainingUsers: TactisUser[] = [];
  valuesT: TactisUser[] = [];

  @Input() training:Training;

  constructor(
    private userService:UserService,
    private errorService:HandleErrorComponent
  ) { }

  ngOnInit() {
    this.getActiveUsers();
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.trainingUsers = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores", error);
    });
  }

}
