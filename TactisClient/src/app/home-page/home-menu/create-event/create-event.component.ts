import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, SelectItem, MessageService } from 'primeng/api';
import { TactisUser } from 'src/app/model/tactisuser';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Clinic } from 'src/app/model/clinic';
import { Client } from 'src/app/model/client';
import { Event } from 'src/app/model/event';
import { ClinicService } from 'src/app/services/clinic.service';
import { ContractChanges } from 'src/app/model/contractchanges';
import { ClientService } from 'src/app/services/client.service';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { EventTactisUser } from 'src/app/model/eventTactisUser';
import { TrainingService } from 'src/app/services/training.service';
import { Training } from 'src/app/model/training';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ClientViewComponent } from '../client-view/client-view.component';
import { ClinicViewComponent } from '../clinic-view/clinic-view.component';
import { ContractchangesViewComponent } from '../contractchanges-view/contractchanges-view.component';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  beginDate: Date = new Date();
  endDate: Date = new Date();
  notes: string;
  eventName: string;
  eventDescription: string;
  eventTypes: SelectItem[] = [ { label: 'Outro', value: 'Outro' }];
  selectedEventType: string;
  items: MenuItem[];
  activeIndex: number = 0;
  users: TactisUser[] = [];
  values: TactisUser[] = [];
  clinics: Clinic[] = [];
  selectedClinic: Clinic;
  clients: Client[] = [];
  selectedClient: Client;
  selectedClinicClient: Client;
  changecols: any[];
  changes: ContractChanges[];
  toClinic: boolean = false;
  toClient: boolean = false;
  hexColorDiv: string = "grey";

  loading:boolean = true;

  @ViewChild("client") client:ClientViewComponent;
  @ViewChild("clinic") clinic:ClinicViewComponent;
  @ViewChild("changes") contract:ContractchangesViewComponent;
  
  constructor(
    private userService: UserService,
    private auth: AuthService,
    private messageService: MessageService,
    private router: Router,
    private clinicService: ClinicService,
    private clientService: ClientService,
    private calendarService: CalendarEventsService,
    private errorService:HandleErrorComponent
  ) { }

  ngOnInit() {
    this.items = [{
      label: 'Dados',
      command: (event: any) => {
        this.activeIndex = 0;
      }
    },
    {
      label: 'Atribuição',
      command: (event: any) => {
        this.activeIndex = 1;
      }
    },
    {
      label: 'Clínica',
      command: (event: any) => {
        this.activeIndex = 2;
      }
    },
    {
      label: 'Cliente',
      command: (event: any) => {
        this.activeIndex = 3;
      }
    }
    ];
    this.changecols = [
      { field: 'changeDatadoc', header: 'Data' },
      { field: 'changeCode', header: 'Código' },
      { field: 'changeDescr', header: 'Descrição' },
      { field: 'changeDate', header: 'Até' }
    ];
    this.getActiveUsers();
    this.getAllClinics();
    this.getAllClients();
  }

  getAllClinics() {
    // get potencial clinics
    this.clinicService.getClinics().subscribe(res => {
      this.clinics = <Clinic[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter as clínicas.", error);
    });
  }

  getAllClients() {
    // get potencial clinics
    this.clientService.getClients().subscribe(res => {
      this.clients = <Client[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os clientes.", error);
    });
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.users = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores", error);
    });
  }

  next() {
    if (this.activeIndex < 3) {
      this.activeIndex += 1;
      return;
    }
  }

  event(event, tipo: string) {
    if (tipo == "CLN") {
      this.selectedClinic = null;
      this.selectedClinic = event.value;
      this.clinicService.checkClinicHasDebt(this.selectedClinic.clinicId).subscribe(res => {
        if (res == true) {
          this.hexColorDiv = "red";
        } else {
          this.hexColorDiv = "green";
        }
      })
    } else if (tipo == "CLT") {
      this.selectedClient = null;
      this.selectedClient = event.value;
    }
  }

  previous() {
    if (this.activeIndex > 0) {
      this.activeIndex -= 1;
      return;
    }
  }

  save() {
    if (this.beginDate == null || this.beginDate == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher a data de início',
        sticky: false,
        life: 2000
      });
      return;
    }

    if (this.endDate == null || this.endDate == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher a data de fim',
        sticky: false,
        life: 2000
      });
      return;
    }

    if (this.selectedEventType == null || this.selectedEventType == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher um tipo de evento',
        sticky: false,
        life: 2000
      });
      return;
    }

    if (this.selectedEventType == "Outro") {
      if (this.eventName == null || this.eventName == undefined || this.eventName.trim().length < 1) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Atenção',
          detail: 'Como escolheu o tipo "Outro", deve definir um nome/título para o evento',
          sticky: false,
          life: 2000
        });
        return;
      }
    }

    if(this.selectedEventType == "Formação"){
      this.eventName="Formação";
      if (this.selectedClinic == undefined || this.selectedClinic == null) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Atenção',
          detail: 'Os eventos "Formação" devem estar associados a uma clínica.',
          sticky: false,
          life: 2000
        });
        return;
      }
    }

    if (this.values.length < 1) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve atribuir o evento a si ou a outro(s) utilizador(es)',
        sticky: false,
        life: 2000
      });
      return;
    }

    this.eventDescription = "event_" + new Date().getTime();

    let client: Client = null;
    let clinic: Clinic = null;
    if (this.toClient) {
      if (this.selectedClient == undefined || this.selectedClient == null) {
        this.messageService.add({
          severity: 'error',
          summary: 'Atenção',
          detail: 'Indicou que pretende associar este evento a um cliente. Por favor, indique-o.',
          sticky: false,
          life: 2000
        });
        return;
      }
      client = this.selectedClient;
    }

    if (this.toClinic) {
      if (this.selectedClinic == undefined || this.selectedClinic == null) {
        this.messageService.add({
          severity: 'error',
          summary: 'Atenção',
          detail: 'Indicou que pretende associar este evento a uma clínica. Por favor, indique-a.',
          sticky: false,
          life: 2000
        });
        return;
      }
      clinic = this.selectedClinic;
    }

    let userlist: EventTactisUser[] = [];
    this.values.forEach(element => {
      userlist.push(new EventTactisUser(element));
    })

    let event: Event = new Event(this.eventName, this.eventDescription, this.notes, "",
      false, this.beginDate, this.endDate, client, clinic, userlist);

    this.calendarService.createEvent(event).subscribe(res => {
      if(this.selectedEventType == "Formação"){
        let train : Training = new Training(this.beginDate,this.eventDescription,this.notes,clinic,false,true);
        
        this.clinicService.addTrainingToClinic(clinic.clinicId, train).subscribe(res => {
          this.messageService.add({
            severity: 'success',
            summary: 'Sucesso',
            detail: 'O evento foi registado.',
            sticky: false,
            life: 2000
          });
          setTimeout(res => {
            this.back();
          }, 1200)
          return;
        }, error => {
          this.errorService.handleError("Não gardou a formação", error);
        })
      }
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'O evento foi registado.',
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.back();
      }, 1200)
      
    }, error => {
      this.errorService.handleError("Nao foi possível guardar o evento", error);
    })

  }

  validField(field: string, mandatory: boolean): boolean {
    if (mandatory && this.nullOrUndefinedField(field)) {
      console.log("mandatory && this.nullOrUndefinedField");
      //exemplo '', true
      return false;
    } else if (!mandatory && this.nullOrUndefinedField(field)) {
      console.log("!mandatory && this.nullOrUndefinedField");
      //exemplo '', false
      return true;
    } else {
      console.log("else");
      return field.trim().length > 0;
    }
  }

  private nullOrUndefinedField(field: string) {
    if (field == null || field == undefined) {
      return true;
    }
    return false;
  }

  back() {
    this.router.navigate(["home/schedules"]);
  }

}
