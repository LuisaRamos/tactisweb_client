import { Component, OnInit, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction'; // for dateClick
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { AuthService } from 'src/app/services/auth.service';
import { InstallationService } from 'src/app/services/installation.service';
import { TrainingService } from 'src/app/services/training.service';
import { Installation } from 'src/app/model/installation';
import { Training } from 'src/app/model/training';
import ptLocale from '@fullcalendar/core/locales/pt';
import * as _moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDatepicker } from '@angular/material';
import { OWL_DATE_TIME_LOCALE, DateTimeAdapter } from 'ng-pick-datetime';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import { Moment } from 'moment';
import { Router } from '@angular/router';

const moment =  _moment;
// export const MY_FORMATS = {
//   parse: {
//     dateInput: 'YYYY-MM-DD',
//   },
//   display: {
//     dateInput: 'YYYY-MM-DD',
//     monthYearLabel: 'YYYY',
//     dateA11yLabel: 'L',
//     monthYearA11yLabel:  'YYYY',
//   },
// };

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
  providers: [
  {provide: OWL_DATE_TIME_LOCALE, useValue: 'fr'},
  ]
})
export class CalendarComponent implements OnInit {

  events: any[] = [];
  options: any;
  selectedInstallation: Installation;
  displayInstallation: boolean = false;
  selectedTraining: Training;
  displayTraining: boolean = false;
  infoLoaded: boolean = false;
  eventName: string;

  //@ViewChild('calendar') calendarComponent: FullCalendarComponent;
  
  date1: Date;
  date6: Date;
  date7: Date;
  date8: Date;
  date9: Date;
  date12: Date;
  date = new FormControl(moment());
  selectedMoment = new Date();
  
  constructor(
    private calendarService: CalendarEventsService,
    private authService: AuthService,
    private installationService: InstallationService,
    private trainingService: TrainingService,
    private router:Router){
      // moment.locale('pt');
  }

  ngOnInit() {
    // this.calendarService.getCalendarEvents().subscribe(events => { 
    //   console.log(events); 
    //  // this.events = events; 
    // });
    this.options = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      defaultDate: this.today(),
      locale: ptLocale,
      eventLimit: true,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      },
      editable: true,
      selectable: true,
      businessHours: {
        // days of week. an array of zero-based day of week integers (0=Sunday)
        daysOfWeek: [ 1, 2, 3, 4,5 ], // Monday - Thursday

        startTime: '09:00', // a start time (10am in this example)
        endTime: '19:00', // an end time (6pm in this example)
      },
      dateClick: (e) => {
        this.handleDateClick(e);
      },
      eventClick: (e) => {
        console.log('clicked')
        this.showInfoOfEvent(e.event.id);
      },
      datesRender: (e) => {
        console.log(e);
      }
    };
  }

  back() {
    this.router.navigate(["home"]);
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  today(): Date {
    return new Date();
  }

  handleDateClick(arg) {
    if (confirm('Quer adicionar um evento para o dia ' + arg.dateStr + ' ?')) {
      this.events =[...this.events, {
        title: 'Evento',
        start: arg.date,
        allDay: arg.allDay
      }];
      //save to repository
      //this.calendarService.createEvent
    }

  }

  showInfoOfEvent(idString: string) {
    let id: number = +idString.substring(4);
    let type: string = idString.substring(0, 4);
    switch (type) {
      case 'INST':
        this.installationService.getInstallation(id).subscribe(res => {

          this.eventName = "Instalação";
          this.selectedInstallation = res;
          this.selectedTraining = null;
          this.displayInstallation = true;
          this.displayTraining = false;
          this.infoLoaded = true;
        });
        break;
      case 'TRNG':
        this.trainingService.getTraining(id).subscribe(res => {

          this.eventName = "Formação";
          this.selectedTraining = res;
          this.selectedInstallation = null;
          this.displayTraining = true;
          this.displayInstallation = false;
          this.infoLoaded = true;
        });
        break;
      case 'EVNT':
        console.log("get event by id");
        break;
    }
  }

}
