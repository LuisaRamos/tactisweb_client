import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractAssociationComponent } from './contract-association.component';

describe('ContractAssociationComponent', () => {
  let component: ContractAssociationComponent;
  let fixture: ComponentFixture<ContractAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
