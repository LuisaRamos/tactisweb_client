import { Component, OnInit, Input } from '@angular/core';
import { Contract } from 'src/app/model/contract';
import { ContractService } from 'src/app/services/contract.service';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ActivatedRoute, Router } from '@angular/router';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { MessageService } from 'primeng/api';
import { Client } from 'src/app/model/client';
import { ClientService } from 'src/app/services/client.service';
import { Installation } from 'src/app/model/installation';
import { Clinic } from 'src/app/model/clinic';
import { Address } from 'src/app/model/address';
import { Contact } from 'src/app/model/contact';
import { ClinicService } from 'src/app/services/clinic.service';
import { AssociateData } from 'src/app/util/view/associatedata';

@Component({
  selector: 'app-contract-association',
  templateUrl: './contract-association.component.html',
  styleUrls: ['./contract-association.component.css']
})
export class ContractAssociationComponent implements OnInit {

  pp: number = 0;
  pa: number;
  proposta: string = "";
  idContract: number;
  notas: string = "";
  selectedType: CodeDescription;
  codes: CodeDescription[] = [];
  typesLoaded: boolean = false;
  clientsLoaded:boolean = false;
  selectedClient:Client;
  selectedInstallation:Installation;
  installations:Installation[];
  installationsLoaded:boolean = false;
  associatePlease:boolean = false;
  clinics: Clinic[];
  clinicLoading: boolean = true;
  clinicsCopy: Clinic[] = [];
  previousChanges: Clinic;
  apagar: [number, string][] = []; //index da clinicsCopy
  selectedClinic: Clinic;
  clients:Client[];
  numbReg: RegExp = /[0-9]+$/;
  contract: Contract;

  constructor(
    private contractService: ContractService,
    private route: ActivatedRoute,
    private previousRouteService: PreviousRouteService,
    private router: Router,
    private clinicService:ClinicService,
    private messageService: MessageService,
    private clientService:ClientService,
    private errorService: HandleErrorComponent) { }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.idContract = <number>res.id;
      this.findInstallationsOfSelectedContract();
    });
    this.getCodes();
    this.getClients();
    this.getPotencialClinics();
  }

  getCodes() {
    this.contractService.getContractCodes().subscribe(res => {
      this.codes = <CodeDescription[]>res;
      this.typesLoaded = true;
    }, error => {
      this.errorService.handleError("Não foi possível obter os códigos de contrato.", error);
    })
  }

  getPotencialClinics() {
    // get potencial clinics
    this.clinicService.getPotencialClinics().subscribe(res => {
      this.clinics = <Clinic[]>res;
      this.clinicLoading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter os potenciais clientes.", error);
    });
    this.clinicService.getPotencialClinics().subscribe(res => {
      this.clinicsCopy = <Clinic[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os potenciais clientes.", error);
    });
  }

  associate() {

    let contact: Contact = new Contact(this.selectedClinic.contact.phone1, this.selectedClinic.contact.phone2, this.selectedClinic.contact.phone3,
      this.selectedClinic.contact.email, this.selectedClinic.contact.other);

    let address: Address = new Address(this.selectedClinic.address.address, this.selectedClinic.address.postcode1, this.selectedClinic.address.postcode2,
      this.selectedClinic.address.addressLocation);

    this.selectedClinic.contact = contact;
    this.selectedClinic.address = address;
    this.selectedClinic.clientSince = /*new Date(this.clientSince)*/ null;

    this.selectedInstallation.clinicList.push(this.selectedClinic);
    let i = this.clinicsCopy.findIndex(c => c == this.previousChanges);
    this.apagar.push([i, this.selectedInstallation.installationName]);


    let index: number = this.clinics.findIndex(clinica => (clinica == this.selectedClinic));
    this.deleteIndexOfClinicsArray(index, this.clinics);

    this.selectedClinic = undefined;
    this.associatePlease = false;

  }
  deleteIndexOfClinicsArray(index: number, array: Clinic[]) {
    array.splice(index, 1);
  }

  findInstallationsOfSelectedContract() {
    this.contractService.getContractInstallations(this.idContract).subscribe(res => {
      this.installations = <Installation[]>res;
      this.installations.forEach(installation => {
        installation.clinicList.forEach(clinic => {
          clinic.mainPostos = 0;
          clinic.addPostos = 0;
          //isto é feito apenas nas clinicas existentes para caso se verifique que têm estes números alterados, juntar ao aditamento
        });
      });
      this.installationsLoaded = true;
      if (this.installations.length == 1) {
        this.selectedInstallation = this.installations[0];
      }
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações do contrato.", error);
    })
  }

  event(event) {
    let index = this.clinicsCopy.findIndex(clinic => clinic.name == (<Clinic>event.value).name);
    if (index == -1) {
      //this.getPotencialClinics();

      //e apagar as que nao tem interesse
    }
    this.previousChanges = this.clinicsCopy[index];
  }

  addClinic() {
    //coloca um id temporario, de acordo com o index da lista
    let c: Clinic = new Clinic("", /*this.clientSince*/ null, "", "", new Address("", "", "", ""), new Contact("", "", "", "", ""), 0, 0, new Date(), null);
    this.selectedInstallation.clinicList.push(c);
  }

  getClients(){
    this.clientService.getClients().subscribe(res => {
      this.clients = <Client[]> res;
      this.clientsLoaded = true;
    })
  }

  guardar() {
    // this.messageService.add({
    //   severity: 'warn',
    //   summary: 'Ups',
    //   detail: 'Ainda não está disponível!',
    //   sticky: false,
    //   life: 2000
    // });
    //descomentar
    let clinicsToAdd :Clinic[] = [];

    this.selectedInstallation.clinicList.forEach(element => {
      if(element.clinicId == undefined || element.clinicId == null){
        clinicsToAdd.push(element);
      }
    });
    let assoc: AssociateData = new AssociateData(this.selectedInstallation, clinicsToAdd);
    this.contractService.createContractAssociation(this.pp,this.pa,this.proposta,this.selectedType.descr, this.notas,this.idContract, new Date().getTime(),assoc).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'Contrato registado!',
        sticky: false,
        life: 2000
      });
      this.back();
    }, error => {
      this.errorService.handleError("Não foi possivel guardar.", error);
    })
  }

  associateClinicToInstallation() {
    this.associatePlease = true;
  }

  back() {
    let prevUrl = this.previousRouteService.getPreviousUrl();
    if (prevUrl == null || prevUrl == undefined) {
      this.router.navigate(["home/byclient"]);
      return;
    }
    if (prevUrl == this.router.url) {
      this.router.navigate(["home/byclient"]);
      return;
    }
    this.router.navigate([prevUrl]);
  }

}
