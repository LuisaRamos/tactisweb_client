import { Component, OnInit, ViewChild } from '@angular/core';
import { QuickSearchMultipleResult } from 'src/app/util/view/quickSearchMultipleResult';
import { Installation } from 'src/app/model/installation';
import { Contract } from 'src/app/model/contract';
import { ContractAndChanges } from 'src/app/util/view/ContractAndChanges';
import { Clinic } from 'src/app/model/clinic';
import { TactisUser } from 'src/app/model/tactisuser';
import { InterventionsViewComponent } from '../interventions-view/interventions-view.component';
import { InterventionNewViewComponent } from '../intervention-new-view/intervention-new-view.component';
import { ClientViewComponent } from '../client-view/client-view.component';
import { ClinicViewComponent } from '../clinic-view/clinic-view.component';
import { AccessViewComponent } from '../access-view/access-view.component';
import { ClinicStatusViewComponent } from '../clinic-status-view/clinic-status-view.component';
import { ClientStatusViewComponent } from '../client-status-view/client-status-view.component';
import { MonitorViewComponent } from '../monitor-view/monitor-view.component';
import { TrainingsViewComponent } from '../trainings-view/trainings-view.component';
import { ClientService } from 'src/app/services/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ContractService } from 'src/app/services/contract.service';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { MessageService, ConfirmationService } from 'primeng/api';
import { ClinicService } from 'src/app/services/clinic.service';
import { InstallationService } from 'src/app/services/installation.service';
import { UserService } from 'src/app/services/user.service';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { AuthService } from 'src/app/services/auth.service';
import { Training } from 'src/app/model/training';
import { EventTactisUser } from 'src/app/model/eventTactisUser';
import { Address } from 'src/app/model/address';
import { Contact } from 'src/app/model/contact';
import { ClinicDetailsView } from 'src/app/util/view/ClinicDetailsView';
import { Event } from 'src/app/model/event';
import { Client } from 'src/app/model/client';
import { Aditamento } from 'src/app/model/aditamento';
import { ContractAssociationComponent } from '../contract-association/contract-association.component';

@Component({
  selector: 'app-client-details-v2',
  templateUrl: './client-details-v2.component.html',
  styleUrls: ['./client-details-v2.component.css']
})
export class ClientDetailsV2Component implements OnInit {

  //------- user logged
  active: string;
  isComercial: boolean = false;
  isHelpdesk: boolean = false;
  isImplementation: boolean = false;
  isAdministrative: boolean = false;

  //-------- params from route
  id: number;
  cid: number;
  accessOn: boolean = false;
  newIntervention: boolean = false;

  //-------- loaded data
  loading: boolean = true;
  result: ClinicDetailsView;

  //-------- clinic view accordion
  hasClinic: boolean = false;
  showClinic: boolean = true;
  nIntrv: number;
  nTrain: number;
  productType: string;

  //-------- aditamentos
  aditamentos: Aditamento[] = [];
  aditamentosLoaded: boolean = false;
  aditCols: any[] = [{ field: 'conclusionDate', header: 'Data' },
  { field: 'proposta', header: 'Proposta' },
  { field: 'mainPostos', header: 'Principais' },
  { field: 'addPostos', header: 'Adicionais' },
  { field: 'notas', header: 'Notas' }];

  //-------- notes attention
  knockDialogVisible: boolean = false;
  showDiv: boolean = false;
  alertaCliente: boolean = false;
  //-------- renew and edit contract
  selectedContract: Contract;
  renewContract: boolean;
  editContract: boolean;

  //-------- do new training event
  newEvent: Event;
  displayNewTraining: boolean;
  users: TactisUser[] = [];
  values: TactisUser[] = [];
  redoTrain: boolean = false; //to apply ngonchanges

  //-------- contracts for client view
  clientContracts2: ContractAndChanges[];
  contractLoaded: boolean = false;
  clinics: Clinic[];

  // ------- installation and clinics
  instcliSelected: boolean = false;
  renSelected: boolean = true;

  //--------- adicionar clinica a instalaçao
  //--------- adicionar nova instalaçao
  newClinic: Clinic;
  displayNewClinic: boolean;
  selectedInstallation: Installation;
  editInstallationView: boolean = false;

  //-------- editar
  selectedClient: Client;
  selectedClinic: Clinic;
  editOnlyClient: boolean = false;
  editOnlyClinic: boolean = false;
  editAccess: boolean = false;
  editInst: boolean = false;

  //--------- fab button
  private _fixed = true;
  public open = true;
  public spin = true;
  public direction = 'down';
  public animationMode = 'fling';
  get fixed(): boolean {
    return this._fixed;
  }
  set fixed(fixed: boolean) {
    this._fixed = fixed;
    if (this._fixed) {
      this.open = true;
    }
  }

  //------- CHILDREN
  @ViewChild("intervention") intervention: InterventionsViewComponent;
  @ViewChild("newintervention") newintervention: InterventionNewViewComponent;
  @ViewChild("client") client: ClientViewComponent;
  @ViewChild("clinic") clinic: ClinicViewComponent;
  @ViewChild("access") access: AccessViewComponent;
  @ViewChild("clinicstatus") clinicstatus: ClinicStatusViewComponent;
  @ViewChild("clientstatus") clientstatus: ClientStatusViewComponent;
  @ViewChild("monitor") monitor: MonitorViewComponent;
  @ViewChild("intTab") intTab: HTMLElement;
  @ViewChild("trainview") trainTab: TrainingsViewComponent;


  //---------------------

  constructor(
    private clientService: ClientService,
    private route: ActivatedRoute,
    private router: Router,
    private previousRouteService: PreviousRouteService,
    private errorService: HandleErrorComponent,
    private messageService: MessageService,
    private authService: AuthService,
    private clinicService: ClinicService,
    private calendarService: CalendarEventsService,
    private userService: UserService,
    private contractService: ContractService,
    private confirmationService: ConfirmationService,
    private installationService: InstallationService
  ) {

  }

  ngOnInit() {
    this.checkRoutes();
    this.getClientAndAllItsInformation();
    this.getLoggedUser();

  }

  checkRoutes() {
    if (this.router.url.match("access") != null) {
      this.accessOn = true;
    } else if (this.router.url.match("/intervention/new") != null) {
      this.doNewIntervention();
    }
    else {
      this.accessOn = false;
    }
  }

  // ------- GET LOGGED USER

  getLoggedUser() {
    this.active = this.authService.getRole();
    if (this.active == "ADMINISTRATIVE") {
      this.isAdministrative = true;
    } else if (this.active == "HELPDESK") {
      this.isHelpdesk = true;
    } else if (this.active == "COMERCIAL") {
      this.isComercial = true;
    } else if (this.active == "IMPLEMENTATION") {
      this.isImplementation = true;
    } else {
      this.isAdministrative = true;
      this.isHelpdesk = true;
      this.isComercial = true;
      this.isImplementation = true;
    }
  }

  // ------- GET CLIENT INFO

  getClientAndAllItsInformation() {
    let id: number;//client
    let cid: number;//clinic
    this.route.params.subscribe(res => {
      id = <number>res.id;
      this.id = id;
      cid = <number>res.cid;
      this.cid = <number>res.cid;
    });

    //so tem cliente,
    if (cid == undefined) {
      this.hasClinic = false;

      this.clientService.getClient(id).subscribe(res => {
        this.result = new ClinicDetailsView(res);
        this.loading = true;
        this.getClientContracts2(id);

        this.loading = false;
      }, error => {
        this.errorService.handleError("Não foi possível obter o cliente.", error);
      });
    } else {
      this.hasClinic = true;
      this.clinicService.getClinicOfClient2(cid).subscribe(res => {
        this.result = <ClinicDetailsView>res;
        this.loading = false;
        let opennote: boolean = false;
        let openclient: boolean = false;
        setTimeout(res => {


          this.result.clientcontract.forEach(element => {
            if (element.divida != null) {
              if (element.divida.divida == ", em dívida") {
                document.getElementById(element.contract.contractNumber).classList.add("redTab");
              }
            }
            if (element.contract.notes != undefined && element.contract.notes.trim().length > 1) {
              opennote = true;
            }
            if (element.client != undefined && element.client.notes.trim().length > 1) {
              openclient = true;
            }
          });

          this.knockDialogVisible = opennote;
          this.alertaCliente = openclient;
          if (opennote) {
            this.showDiv = true;
          }
        }, 400);

      }, error => {
        this.errorService.handleError("Não foi possível obter o cliente e clinica", error);
      });
      this.clinicService.getClinicAditamentos(cid).subscribe(res => {
        this.aditamentos = <Aditamento[]>res;
        this.aditamentosLoaded = true;
      }, error => {
        this.errorService.handleError("Não foi possível obter o cliente e clinica", error);
      });
    }
  }

  //----- GET DATA FROM CHILDREN: INT, TRAIN, PROD TYPE

  readProductType(event) {
    this.productType = event;
  }

  showNrIntervention(nr: number) {
    this.nIntrv = nr;
  }

  showNrTraining(nr: number) {
    this.nTrain = nr;
  }

  // ----- GET Contracts if client view
  getClientContracts2(id: number) {
    this.clientService.getClientContracts2(id).subscribe(res => {
      this.clientContracts2 = <ContractAndChanges[]>res;
      this.contractLoaded = true;
      if (this.clientContracts2.length > 0) {
        this.getClinics(this.clientContracts2[0].contract.contractId);
      }
    })
  }

  getClinics(id: number) {
    this.contractService.getClinicsOfContract(id).subscribe(res => {
      this.clinics = <Clinic[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível saber as clínicas do contrato.", error);
    });
  }

  editClient(client: Client) {
    this.selectedClient = client;
    this.editOnlyClient = true;
  }

  editClinic() {
    this.selectedClinic = this.result.clinic;
    this.editOnlyClinic = true;
  }

  /**
   * check error https://github.com/primefaces/primeng/issues/8044
   * @param event 
   */
  closeEditDialogs(event) {
    this.editOnlyClinic = false;
    this.editOnlyClient = false;
    this.editInst = false;
    this.editAccess = false;
    this.getClientAndAllItsInformation();
  }

  //------- DO RENEW AND EDIT CONTRACT
  renovarContract(contract: Contract) {
    this.selectedContract = contract;
    this.renewContract = true;
  }

  editarContract(contract: Contract) {
    this.selectedContract = contract;
    this.editContract = true;
  }

  associarContrato(contract: Contract) {
    // this.messageService.add({
    //   severity: 'info',
    //   summary: 'Ups',
    //   detail: 'Ainda não está implementado! Aqui poderá partilhar esta clínica com outro contrato',
    //   sticky: false,
    //   life: 6000
    // });
    this.router.navigate(['home/contract/' + contract.contractId + "/association"]);
  }

  editarAcessos() {
    this.selectedInstallation = this.result.installation;
    this.selectedClinic = this.result.clinic;
    this.editAccess = true;
    console.log("quer editar acessos")
  }

  listenCloseRenovar(event) {
    if (event == false) {
      this.renewContract = false;
      this.editContract = false;
    } else {
      this.getClientAndAllItsInformation();
      this.renewContract = false;
      this.editContract = false;
    }
  }

  listenCloseEditar(event) {
    console.log(event)
    this.getClientAndAllItsInformation();
    this.renewContract = false;
    this.editContract = event;
  }

  // ------- DO NEW INTERVENTION 
  doNewIntervention() {
    this.showClinic = false;
    this.newIntervention = true;
  }

  interventionDone(event) {
    if (event) {
      this.newIntervention = false;
      this.showClinic = true;
      this.intervention.getInterventionsOfClinic();
    } else {
      this.showClinic = true;
      this.newIntervention = false;
    }
  }

  // ------- DO NEW TRAINING
  doNewTraining() {
    this.getActiveUsers();
    this.newEvent = new Event("", "", "", "", false, null, null, null, this.result.clinic, []);
    this.displayNewTraining = true;
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.users = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores", error);
    });
  }

  saveNewTraining() {

    if (!this.checkTrainingInfo()) {
      return;
    }

    let trainingName: string = "train_" + new Date().getTime();


    let training: Training = new Training(this.newEvent.beginDate, trainingName, this.newEvent.notes, this.result.clinic, false, true);
    this.clinicService.addTrainingToClinic(this.result.clinic.clinicId, training).subscribe(res => {

      let userlist2: EventTactisUser[] = [];
      this.values.forEach(element => {
        userlist2.push(new EventTactisUser(element));
      })

      let myevent2: Event = new Event("Formação", trainingName, this.newEvent.notes, "", false, this.newEvent.beginDate, this.newEvent.endDate, null, this.result.clinic, userlist2);

      this.calendarService.createEvent(myevent2).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Formação agendada!',
          sticky: false,
          life: 2000
        });
        setTimeout(() => {
          this.displayNewTraining = false;
          this.newEvent = undefined;
          this.redoTrain = !this.redoTrain;
          this.values = [];
        }, 1000);
      }, error => {
        this.errorService.handleError("Não foi possível criar o evento", error);
      });

    }, error => {
      this.errorService.handleError("Não foi possível criar a formação", error);
    });
  }

  checkTrainingInfo(): boolean {
    if (this.newEvent.beginDate == null || this.newEvent.beginDate == undefined) {
      this.showWarnMessage("Deve preencher o campo 'Data início'");
      return false;
    }
    if (this.newEvent.endDate == null || this.newEvent.endDate == undefined) {
      this.showWarnMessage("Deve preencher o campo 'Data fim'");
      return false;
    }
    if (this.values.length == 0) {
      this.showWarnMessage("Deve escolher pelo menos um utilizador a atribuir a formação");
      return false;
    }
    return true;
  }

  // -------- adicionar/remover instalação ao contrato
  addInstallation(contract: Contract) {
    let now = new Date();
    let installation_name = "dummy_" + contract.contractNumber;
    let last: number;
    let installation_name2 = installation_name + "_" + now.getTime();
    let description: string = "inst_" + new Date().getTime();
    let i: Installation = new Installation(null, "", description, [], [], [], installation_name2, "", false, false, false, false, false, false, false, false, false, false);

    this.contractService.addInstallationToContract(contract.contractId, i).subscribe(res => {
      this.getClientContracts2(this.id);
      this.instcliSelected = true;
      this.renSelected = false;
    }, error => {
      this.errorService.handleError("Não foi possível adicionar a instalação ao contrato.", error);
    })
  }

  deleteInstallation(installation: Installation) {
    this.confirmationService.confirm({
      message: 'Tem a certeza que quer apagar? As clínicas associadas também serão apagadas.',
      header: 'Confirmação',
      key: 'deleteinst',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        // this.installationService.deleteInstallation(installation.installationId).subscribe(res => {
        this.messageService.add({
          severity: 'warn',
          summary: 'Atenção',
          detail: 'Não é assim tão simples. O que acontece ao PemSlider e NovigestSlider? O que acontece às clínicas e seus contactos e moradas?.',
          sticky: false,
          life: 7000
        });
        //   this.getClientContracts2(this.id);
        // }, error => {
        //   this.errorService.handleError("Por acaso até nem foi possível apagar a instalação", error);
        // });
      }
    });
  }

  getContractAgain() {
    this.clinicService.getClinicOfClient2(this.cid).subscribe(res => {
      this.result = <ClinicDetailsView>res;
    });
  }

  // adicionar clinica à instalação
  addNewClinicToInstallation(inst: Installation) {
    this.newClinic = new Clinic("", null, "", "", new Address("", "", "", ""), new Contact("", "", "", "", ""), 0, 0, new Date(), inst);
    this.displayNewClinic = true;
    this.selectedInstallation = inst;
    //this.router.navigate(['home/installation/' + inst.installationId + "/clinic/new"]);
  }

  saveNewClinic(): boolean {

    console.log(this.newClinic)
    if (!this.validarCamposClinica()) {
      return false;
    }

    //TODO - validar se nome da clinica existe

    this.clinicService.createClinic2(this.selectedInstallation.installationId, this.newClinic).subscribe(data => {

      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'Clínica Registada!',
        sticky: false,
        life: 2000
      });

      setTimeout(() => {
        this.displayNewClinic = false;
        this.getClientContracts2(this.id);
        this.newClinic = undefined;
        this.renSelected = false;
        this.instcliSelected = true;

      }, 2000);
      return true;
    }, error => {
      this.errorService.handleError("Não foi possível guardar a clinica.", error);
    });
    return false;
  }

  private validField(field: string, mandatory: boolean): boolean {
    if (mandatory && this.nullOrUndefinedField(field)) {
      //exemplo '', true
      return false;
    } else if (!mandatory && this.nullOrUndefinedField(field)) {
      //exemplo '', false
      return true;
    } else {
      return field.trim().length > 0;
    }
  }

  private nullOrUndefinedField(field: string) {
    if (field == null || field == undefined) {
      return true;
    }
    return false;
  }

  private validarCamposClinica(): boolean {
    if (!this.validField(this.newClinic.name, true)) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher o nome',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!(this.validField(this.newClinic.contact.email, true) || this.validField(this.newClinic.contact.phone1, true))) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher um contacto ou email',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!(this.validField(this.newClinic.address.address, true) || this.validField(this.newClinic.address.addressLocation, true))) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher a morada ou localização',
        sticky: false,
        life: 2000
      });
      // mensagem preencher morada ou localização
      return false;
    }

    if (this.newClinic.addPostos < 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'O número de postos adicionais não pode ser negativo',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (this.newClinic.mainPostos < 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'O número de postos principais não pode ser negativo',
        sticky: false,
        life: 2000
      });
      return false;
    }

    return true;
  }

  // -------- editar instalaçao

  editarInst() {
    this.selectedInstallation = this.result.installation;
    this.editInst = true;
  }


  // --------- adicionar instalaçao nova
  editInstallation(i: Installation) {
    this.selectedInstallation = i;
    this.editInstallationView = true;
  }

  saveInstallation() {
    if (!this.checkInstallationFields()) {
      return;
    }

    this.installationService.updateInstallation(this.selectedInstallation.installationId, this.selectedInstallation).subscribe(res => {

      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'A instalação foi atualizada. No entanto é preciso ter atenção ao pem e ng slider',
        sticky: false,
        life: 2000
      });
    })
    this.messageService.add({
      severity: 'warn',
      summary: 'Ups',
      detail: 'Ainda não está implementado!',
      sticky: false,
      life: 2000
    });
  }
  checkInstallationFields() {
    if (this.selectedInstallation.installationName.trim().length == 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'O nome da instalação não pode estar vazio',
        sticky: false,
        life: 2000
      });
    }

    if (this.selectedInstallation.password.trim().length == 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'A password não pode estar vazio',
        sticky: false,
        life: 2000
      });
    }
    if (!this.checkInstallationName()) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: "O nome de instalação já existe. Deve escolher outro.",
        sticky: false,
        life: 2000
      });
      return false;
    }
  }

  checkInstallationName(): boolean {
    let nameOK = false;
    this.installationService.checkInstallationNameExists(this.selectedInstallation.installationName, this.selectedInstallation.installationId).subscribe(res => {
      if (res == true) {
        nameOK = true;
      }
    })
    return nameOK;
  }

  // -------- BACK
  back() {
    let prevUrl = this.previousRouteService.getPreviousUrl();
    console.log(prevUrl)
    if (prevUrl == null || prevUrl == undefined) {
      this.router.navigate(["home/byclient"]);
      return;
    }
    if (prevUrl == this.router.url) {
      this.router.navigate(["home/byclient"]);
      return;
    }

    if (prevUrl.match("edit") != null) {
      this.router.navigate(["home/byclient"]);
      return;
    }
    if (prevUrl.match("contracts/see") != null) {
      this.router.navigate([prevUrl]);
      return;
    }
    let urlsplited: string[] = prevUrl.split('/');
    let keyword: string;

    if (urlsplited.length == 4) {
      keyword = urlsplited[3].replace('%20', ' ');
      while (keyword.match("%20")) {
        keyword = keyword.replace('%20', ' ');
      }
      this.router.navigate(["home/byclient/" + keyword])
      return;
    }
    this.router.navigate([prevUrl]);
  }


  //--------- MESSAGE HANDLER
  showWarnMessage(message: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Atenção!',
      detail: message,
      sticky: false,
      life: 2000
    });
  }
}