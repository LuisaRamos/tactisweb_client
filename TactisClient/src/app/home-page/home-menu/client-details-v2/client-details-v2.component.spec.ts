import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDetailsV2Component } from './client-details-v2.component';

describe('ClientDetailsV2Component', () => {
  let component: ClientDetailsV2Component;
  let fixture: ComponentFixture<ClientDetailsV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDetailsV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDetailsV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
