import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractNewFormComponent } from './contract-new-form.component';

describe('ContractNewFormComponent', () => {
  let component: ContractNewFormComponent;
  let fixture: ComponentFixture<ContractNewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractNewFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractNewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
