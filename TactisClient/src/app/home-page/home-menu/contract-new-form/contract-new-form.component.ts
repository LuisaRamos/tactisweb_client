import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ContractService } from 'src/app/services/contract.service';
import { Contract } from 'src/app/model/contract';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-contract-new-form',
  templateUrl: './contract-new-form.component.html',
  styleUrls: ['./contract-new-form.component.css']
})
export class ContractNewFormComponent implements OnInit {
 
  codes: CodeDescription[] = [];
  typesLoaded: boolean = false;
 
  selectedType:CodeDescription;

  numbReg: RegExp = /[0-9]+$/;

  @Input() contract:Contract;

  @Output() code: EventEmitter<CodeDescription> = new EventEmitter<CodeDescription>();

  constructor(
    private contractService:ContractService,
    private errorService:HandleErrorComponent
  ) { }

  ngOnInit() {
    this.getCodes();
  }

  tellParent(){
    this.code.emit(this.selectedType);
  }

  getCodes() {
    this.contractService.getContractCodes().subscribe(res => {
      this.codes = <CodeDescription[]>res;
      this.typesLoaded = true;
    }, error => {
      this.errorService.handleError("Não foi possível obter os códigos de contrato.", error);
    })
  }
}
