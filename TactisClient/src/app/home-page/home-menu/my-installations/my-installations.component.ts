import { Component, OnInit } from '@angular/core';
import { Installation } from 'src/app/model/installation';
import { ContractAndInstallation } from 'src/app/util/view/contractandinstallation';
import { TactisUser } from 'src/app/model/tactisuser';
import { InstallationService } from 'src/app/services/installation.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService } from 'primeng/api';
import { UserService } from 'src/app/services/user.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Aditamento } from 'src/app/model/aditamento';
import { AditamentoService } from 'src/app/services/aditamento.service';

@Component({
  selector: 'app-my-installations',
  templateUrl: './my-installations.component.html',
  styleUrls: ['./my-installations.component.css']
})
export class MyInstallationsComponent implements OnInit {

  data: ContractAndInstallation[];
  activeUser: TactisUser;
  display: boolean = false;
  selectedInstallation: Installation;
  selectedAditamento: Aditamento;
  newInstDate: Date = new Date();
  dataLoaded: boolean = false;

  loading:boolean = true;
  openHelp:boolean = false;

  constructor(
    private installationService: InstallationService,
    private userService: UserService,
    private router: Router,
    private auth: AuthService,
    private messageService: MessageService,
    private errorService: HandleErrorComponent,
    private aditamentoService: AditamentoService
  ) {
  }

  ngOnInit() {
    this.getActiveUser();
  }

  getActiveUser() {
    let user = this.auth.getActiveUser();
    this.userService.getTactisUserByUsername(user).subscribe(res => {
      this.activeUser = <TactisUser>res;
      this.getInst();
    }, error => {
      this.errorService.handleError("Não foi possível obter o utilizador.", error);
    });
  }

  getInst() {
    if (this.activeUser.userRoleId.userRole == 'ADMIN') {
      this.getAllNotDoneInstallations();
    } else {
      this.getInstallations(this.activeUser.tactisUserId);
    }
  }

  getAllNotDoneInstallations() {
    this.installationService.getAllInstallationsAssignedAndNotDone().subscribe(data => {
      this.data = <ContractAndInstallation[]>data;

      this.dataLoaded = true;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações.", error);
    });
  }

  getInstallations(id: number) {
    this.installationService.getInstallationsAssignedToUserNotDone(id).subscribe(data => {

      this.data = <ContractAndInstallation[]>data;
      this.dataLoaded = true;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações.", error);
    });
  }


  install(installation: Installation) {
    this.router.navigate(["home/install/pp/" + installation.installationId]);
  }

  installAd(aditamento: Aditamento) {
    this.router.navigate(["home/install/pa/" + aditamento.aditamentoId]);
  }

  dividir(aditamento: Aditamento) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Ups!',
      detail: 'Ainda não está implementado. Aqui deve poder dizer que já concluiu parte da instalação.',
      sticky: false,
      life: 5000
    })
  }

  finishAD(aditamento: Aditamento) {
    aditamento.instalado = true;
    aditamento.conclusionDate = new Date();
    this.aditamentoService.updateAditamento(aditamento.aditamentoId, aditamento).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'A instalação foi concluída.',
        sticky: false,
        life: 1000
      })
      this.getInst();
    }, error => {
      this.errorService.handleError("Não foi possível dar a instalação como concluída", error);
    })
  }

  finish(installation: Installation) {
    installation.done = true;
    installation.conclusionDate = new Date();
    this.installationService.updateInstallation(installation.installationId, installation).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'A instalação foi concluída.',
        sticky: false,
        life: 1000
      })
      this.getInst();
    }, error => {
      this.errorService.handleError("Não foi possível dar a instalação como concluída", error);
    })
  }

  confirmInstallationDateAlteration() {
    if(this.selectedInstallation != null){
      this.selectedInstallation.installationDate = this.newInstDate;
      this.installationService.updateInstallation(this.selectedInstallation.installationId, this.selectedInstallation).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Data da instalação alterada',
          sticky: false,
          life: 1000
        })
        this.display = false;
        this.getInst();
  
      }, error => {
        this.errorService.handleError("Não foi possível atualizar.", error);
      })
    } else {
      this.selectedAditamento.data = this.newInstDate;
      this.aditamentoService.updateAditamento(this.selectedAditamento.aditamentoId, this.selectedAditamento).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Data da instalação alterada',
          sticky: false,
          life: 1000
        })
        this.display = false;
        this.getInst();
  
      }, error => {
        this.errorService.handleError("Não foi possível atualizar.", error);
      })
    }
  }

  editSchedule(data: ContractAndInstallation) {
    this.display = true;
    if (data.installation != undefined) {
      this.selectedInstallation = data.installation;
      this.selectedAditamento = null;
    } else {
      this.selectedInstallation = null;
      this.selectedAditamento = data.aditamento;
    }
  }

  back() {
    this.router.navigate(['home']);
  }

  todayInstall(date: Date) {
    let today = new Date();
    let instDate = new Date(date);

    //se a data for a mesma
    if (today.getUTCFullYear() == instDate.getUTCFullYear() && today.getMonth() == instDate.getMonth() && today.getDate() == instDate.getDate()) {
      return 1;
    }
    //se a data for de amanha
    if (today.getUTCFullYear() == instDate.getUTCFullYear() && today.getMonth() == instDate.getMonth() && today.getDate() == instDate.getDate() - 1) {
      return 2;
    }
    if (today > instDate) {
      return 3;
    }
    return 4;
  }
}
