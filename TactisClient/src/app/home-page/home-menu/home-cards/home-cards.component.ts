import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MessageService, SelectItem } from 'primeng/api';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { InstallationService } from 'src/app/services/installation.service';
import { Installation } from 'src/app/model/installation';
import { TactisUser } from 'src/app/model/tactisuser';
import { UserService } from 'src/app/services/user.service';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { Training } from 'src/app/model/training';
import { Event } from 'src/app/model/event';
import { EventAndUsers } from 'src/app/util/view/eventAndUsers';
import { ContractUnfinishedView } from 'src/app/util/view/ContractUnfinishedView';
import { ContractService } from 'src/app/services/contract.service';
import { ContractAndInstallation } from 'src/app/util/view/contractandinstallation';
import { Subscription, Observable, interval } from 'rxjs';

@Component({
  selector: 'app-home-cards',
  templateUrl: './home-cards.component.html',
  styleUrls: ['./home-cards.component.css']
})
export class HomeCardsComponent implements OnInit {

  sub: Subscription;

  actual_role: string;
  activeUser: TactisUser;

  client_search: boolean;
  client_new: boolean;
  schedules: boolean;
  users_new: boolean;
  contract_new: boolean;
  contract_see: boolean;
  users_search: boolean;
  updates: boolean;
  calendar: boolean;
  definitions: boolean;
  doInstallations: boolean;
  doTrainings: boolean;
  statistics:boolean;
  alerts:boolean;
  listagens:boolean;
  seeContracts:boolean;

  quick_search: boolean;
  activations: boolean;

  userInst: number;
  userInstLoaded: boolean = false;

  userTrain: number;
  userTrainLoaded: boolean = false;

  userOthers: number;
  userOthersLoaded: boolean = false;

  nrCont: number;
  nrContLoaded: boolean = false;

  number: number;
  numberloaded: boolean = false;

  instAg: number;
  instAgLoaded: boolean = false;

  trainAg: number;
  trainAgLoaded: boolean = false;

  otherAg: number;
  otherAgLoaded: boolean = false;

  nrAlerts: number;
  nrAlertsLoaded: boolean = false;


  appsAtiv: SelectItem[] = [
    { label: "Novigest", value: "ng" },
    { label: "Novipem", value: "pem" }
  ];

  apps: SelectItem[] = [
    { label: "Tactis Updater", value: "tu" },
    { label: "Novigest", value: "ng" },
    { label: "Novipem", value: "pem" },
    { label: "Infarmed", value: "inf" }
  ];

  prefs:SelectItem[] = [
    { label: "KPIs", value: "kpi" },
    { label: "Gráficos", value: "grafico" },
  ];

  lists:SelectItem[] = [
    { label: "Renovações", value: "renovacoes" },
    { label: "Novos Clientes", value: "novosclientes" },
    { label: "Circulares", value: "circulares" },
    { label: "Dívidas", value: "dividas" }
  ]

  selectedDB: string;
  selectedGDB: string;
  selectedPref:string;
  selectedListagem:string;
  quickSearchForm: FormGroup;


  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private installationService: InstallationService,
    private messageService: MessageService,
    private userService: UserService,
    private calendarService: CalendarEventsService,
    private contractService: ContractService,
    private previousRouteService: PreviousRouteService) {
    this.actual_role = this.authService.getRole();
    this.quick_search = true;
    this.definitions = true;
    this.client_search = true;
    this.calendar = true;
    this.users_search = this.actual_role == "ADMIN";
    this.client_new = this.actual_role == "ADMIN" || this.actual_role == "COMERCIAL" || this.actual_role == "ADMINISTRATIVE";
    this.contract_new = this.actual_role == "ADMIN" || this.actual_role == "ADMINISTRATIVE" || this.actual_role == "COMERCIAL";
    this.contract_see = this.actual_role == "ADMIN" || this.actual_role == "ADMINISTRATIVE" || this.actual_role == "COMERCIAL";
    this.schedules = this.actual_role == "ADMIN" || this.actual_role == "ADMINISTRATIVE" || this.actual_role == "COMERCIAL";
    this.doInstallations = this.actual_role == "ADMIN" || this.actual_role == "HELPDESK" || this.actual_role == "IMPLEMENTATION";
    this.doTrainings = this.actual_role == "ADMIN" || this.actual_role == "HELPDESK" || this.actual_role == "COMERCIAL";
    this.updates = this.actual_role == "ADMIN" || this.actual_role == "IMPLEMENTATION" || this.actual_role == "HELPDESK";
    this.activations = this.actual_role == "ADMIN" || this.actual_role == "IMPLEMENTATION" || this.actual_role == "HELPDESK";
    this.users_new = this.actual_role == "ADMIN";
    this.alerts = this.actual_role == "ADMIN" || this.actual_role == "ADMINISTRATIVE" ;
    this.listagens = this.actual_role == "ADMIN" || this.actual_role == "ADMINISTRATIVE" ; 
    this.seeContracts = this.actual_role == "ADMIN"; 
    this.statistics = this.actual_role == "ADMIN" || this.actual_role == "COMERCIAL" || this.actual_role == "ADMINISTRATIVE";
  }

  ngOnInit() {
    this.quickSearchForm = this.formBuilder.group({
      _toSearch: ['',]
    });

    this.getActiveUser();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['']);
  }

  createNewUser() {
    this.router.navigate(['home/users/new']);
  }

  createClient() {
    this.router.navigate(['home/clients/new']);
  }

  createFullContract() {
    this.router.navigate(['home/contract/new']);
  }

  doSchedules() {
    this.router.navigate(['home/schedules']);
  }

  tools() {
    this.router.navigate(['home/myprofile']);
  }

  searchUsers() {
    this.router.navigate(['home/users']);
  }

  seeCalendar() {
    this.router.navigate(['home/calendar']);
  }

  seeDashboard(){
    this.router.navigate(['home/dashboard']);
  }

  novidash(){
    if (this.selectedPref == undefined || this.selectedPref == null) {
      return;
    }
    this.router.navigate(['home/novidash/' + this.selectedPref]);
  }

  seeSchedules() {
    this.router.navigate(['home/scheduled/installations']);
  }

  seeOtherSchedules() {
    this.router.navigate(['home/scheduled/others']);
  }

  seeTSchedules() {
    this.router.navigate(['home/scheduled/trainings']);
  }

  manageUpdates() {
    if (this.selectedGDB == undefined || this.selectedGDB == null) {
      return;
    }
    this.router.navigate(['home/manage/updates/' + this.selectedGDB]);
  }

  doUpdates() {
    if (this.selectedDB == undefined || this.selectedDB == null) {
      return;
    }
    this.router.navigate(['home/updates/' + this.selectedDB]);
  }

  doActivations() {
    if (this.selectedDB == undefined || this.selectedDB == null) {
      return;
    }
    this.router.navigate(['home/activations/' + this.selectedDB]);
  }

  doSomething() {
    this.router.navigate(['home/byclient']);
  }

  userInstallationsToDo() {
    this.router.navigate(['home/schedule/installations']);
  }

  userInstallationsAditamentos(){
    this.router.navigate(['home/schedule/installations/adit']);
  }

  userTrainingsToDo() {
    this.router.navigate(['home/schedule/trainings']);
  }

  userOtherEventsToDo() {
    this.router.navigate(['home/schedule/others']);
  }

  seeContractsUnfinished() {
    this.router.navigate(['home/contracts/unfinished/see']);
  }

  seeListagens(){

    if (this.selectedListagem == undefined || this.selectedListagem == null) {
      return;
    }
    if(this.selectedListagem == 'renovacoes'){
      this.router.navigate(['home/listagem/' + this.selectedListagem]);
    } if(this.selectedListagem == 'novosclientes'){
      this.router.navigate(['home/listagem/' + this.selectedListagem]);
    }  if(this.selectedListagem == 'circulares'){
      this.router.navigate(['home/listagem/' + this.selectedListagem]);
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Ups',
        detail: 'Ainda não está implementado.',
        sticky: false,
        life: 1000
      });
  
    }
  }

  seeAlerts(){
    this.messageService.add({
      severity: 'warn',
      summary: 'Ups',
      detail: 'Ainda não está implementado.',
      sticky: false,
      life: 1000
    });
    //this.router.navigate(['home/alerts/see']);
  }

  getNumberOfUnassignedInstallations() {
    this.installationService.getInstallationsUnassigned().subscribe(res => {
      let inst: Installation[] = <Installation[]>res;
      this.number = inst.length;
      this.numberloaded = true;
    }, error => {
      console.log("erro a getNumberOfUnassignedInstallations");
    });
  }

  getNumberOfInstallationsAssignedToUser() {
    if (this.activeUser.userRoleId.userRole == 'ADMIN') {
      this.installationService.getAllInstallationsAssignedAndNotDone().subscribe(res => {
        let inst: Installation[] = <Installation[]>res;
        this.userInst = inst.length;
        this.userInstLoaded = true;
      })
    } else {
      this.installationService.getInstallationsAssignedToUserNotDone(this.activeUser.tactisUserId).subscribe(res => {
        let inst: Installation[] = <Installation[]>res;
        this.userInst = inst.length;
        this.userInstLoaded = true;
      }, error => {
        console.log("erro a getNumberOfInstallationsAssignedToUser");
      });
    }
  }

  getNumberOfTrainingsAssignedToUser() {
    if (this.activeUser.userRoleId.userRole == 'ADMIN') {
      this.calendarService.getAllTrainingsAssignedAndNotDone().subscribe(res => {
        let f: EventAndUsers[] = <EventAndUsers[]>res;
        this.userTrain = f.length;
        this.userTrainLoaded = true;
      })
    } else {
      this.calendarService.getAllTrainingsAssignedAndNotDoneToUser(this.activeUser.tactisUserId).subscribe(res => {
        let f: EventAndUsers[] = <EventAndUsers[]>res;
        this.userTrain = f.length;
        this.userTrainLoaded = true;
      }, error => {
        console.log("erro a getNumberOfTrainingsAssignedToUser");
      });
    }
  }

  getNumberOfOtherEventsAssignedToUser() {
    if (this.activeUser.userRoleId.userRole == 'ADMIN') {
      this.calendarService.getAllOtherEventsAssignedAndNotDone().subscribe(res => {
        let inst: EventAndUsers[] = <EventAndUsers[]>res;
        this.userOthers = inst.length;
        this.userOthersLoaded = true;
      })
    } else {
      this.calendarService.getAllOtherEventsAssignedAndNotDoneToUser(this.activeUser.tactisUserId).subscribe(res => {
        let inst: EventAndUsers[] = <EventAndUsers[]>res;
        this.userOthers = inst.length;
        this.userOthersLoaded = true;
      }, error => {
        console.log("erro a getNumberOfOtherEventsAssignedToUser");
      });
    }
  }

  getNumberOfAllTrainings() {
    this.calendarService.getAllTrainingsAssignedAndNotDone().subscribe(res => {
      let inst: EventAndUsers[] = <EventAndUsers[]>res;
      this.trainAg = inst.length;
      this.trainAgLoaded = true;
    }, error => {
      console.log("erro a getNumberOfAllTrainings");
    });
  }

  getNumberOfAllOtherEvents() {
    this.calendarService.getAllOtherEventsAssignedAndNotDone().subscribe(res => {
      let inst: EventAndUsers[] = <EventAndUsers[]>res;
      this.otherAg = inst.length;
      this.otherAgLoaded = true;
    }, error => {
      console.log("erro a getNumberOfAllOtherEvents");
    });
  }

  getNumberOfAllInstallations() {
    this.installationService.getAllInstallationsAssignedAndNotDone().subscribe(res => {
      let inst: Installation[] = <Installation[]>res;
      this.instAg = inst.length;
      this.instAgLoaded = true;
    }, error => {
      console.log("erro a getNumberOfAllInstallations");
    });
  }

  getNumberOfContratosPorConcluir() {
    this.contractService.getUnfinishedContracts().subscribe(res => {
      let contracts = <ContractUnfinishedView[]>res;
      this.nrCont = contracts.length;
      this.nrContLoaded = true;
    }, error => {
      console.log("erro a getNumberOfContratosPorConcluir");
    });
  }

  getActiveUser() {
    let user = this.authService.getActiveUser();
    this.userService.getTactisUserByUsername(user).subscribe(res => {
      this.activeUser = <TactisUser>res;
      
      this.getNotifications();

      const source = interval(10000);

      this.sub = source
        .subscribe((val) => {
          if (this.authService.getActiveUser() == null || this.authService.getActiveUser() == undefined) {
            if(this.sub != undefined){
              this.sub.unsubscribe();
            }
          }
          this.getNotifications();
        });


    }, error => {
      if (error.status == 401 /*&& error.error == "SessionExpired" or "Unauthorized"*/) {
        this.authService.setSessionExpired();
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Sessão expirada. Faça novamente o login.',
          sticky: false,
          life: 1000
        });
        setTimeout(() => {
          this.authService.logout();
          this.router.navigate(["/login"]);
        }, 1000);
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Não foi possível obter o utilizador logged in.',
          sticky: false,
          life: 1000
        });
      }
    });
  }

  ngOnDestroy() {
    if(this.sub != undefined){
      this.sub.unsubscribe();
    }

  }

  getNotifications() {
    if (this.schedules) {
      this.getNumberOfUnassignedInstallations();
      this.getNumberOfAllInstallations();
      this.getNumberOfAllOtherEvents();
      this.getNumberOfAllTrainings();
    }

    if (this.contract_see) {
      this.getNumberOfContratosPorConcluir();
    }

    if (this.doInstallations) {
      this.getNumberOfInstallationsAssignedToUser();
    }
    if (this.doTrainings) {
      this.getNumberOfTrainingsAssignedToUser();
    }

    this.getNumberOfOtherEventsAssignedToUser();

  }

}
