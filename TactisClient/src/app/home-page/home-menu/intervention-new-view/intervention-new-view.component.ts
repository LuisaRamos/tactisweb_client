import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { InterventionService } from 'src/app/services/intervention.service';
import { ProductActivity } from 'src/app/model/productActivity';
import { SelectItem, MessageService } from 'primeng/api';
import { InterventionType } from 'src/app/model/interventionType';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { TactisUser } from 'src/app/model/tactisuser';
import { Intervention } from 'src/app/model/intervention';
import { Clinic } from 'src/app/model/clinic';
import { UserService } from 'src/app/services/user.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { InterventionData } from 'src/app/util/view/interventiondata';

@Component({
  selector: 'app-intervention-new-view',
  templateUrl: './intervention-new-view.component.html',
  styleUrls: ['./intervention-new-view.component.css']
})
export class InterventionNewViewComponent implements OnInit {

  productActivities: ProductActivity[];
  paItems: SelectItem[] = [];
  selectedProductActivities: ProductActivity;
  loadingPA: boolean;
  loadingTypes: boolean;
  activeUser: TactisUser;
  interventionTypes: InterventionType[];
  selectedType: InterventionType;
  interventionDate: Date = new Date();
  pt: any;
  notes: string = "";
  interlocutor: string = "";
  duration: number;

  numbReg: RegExp = /[0-9]+$/;

  @Input() clinic: Clinic;
  @Input() status: string;
  @Input() produto: string;

  @Output() closeTab: EventEmitter<boolean> = new EventEmitter<boolean>();


  constructor(
    private interventionService: InterventionService,
    private auth: AuthService,
    private router: Router,
    private messageService: MessageService,
    private userService: UserService,
    private errorService: HandleErrorComponent

  ) { }

  ngOnInit() {
    this.doCalendarPt();
    this.getActiveUser();
    this.getThingsForIntervention();

  }

  doCalendarPt() {
    this.pt = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
      dayNamesShort: ["Dom", "2ª", "3ª", "4ª", "5ª", "6ª", "Sáb"],
      dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S"],
      monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      today: 'Hoje',
      clear: 'Clear',
      dateFormat: 'yy-mm-dd'
    };
  }

  getThingsForIntervention() {
    this.interventionService.getProductActivities().subscribe(res => {
      this.productActivities = <ProductActivity[]>res;
      this.productActivities.forEach(pa => {
        this.paItems.push({ label: pa.product + " - " + pa.activity, value: pa });
      });
      this.loadingPA = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter os produtos-atividades.", error);
    });

    //obter os tipos de intervençao
    this.interventionService.getInterventionTypes().subscribe(res => {
      this.interventionTypes = <InterventionType[]>res;
      this.loadingTypes = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter os tipos de intervenção.", error);
    });
  }

  getActiveUser() {
    let user = this.auth.getActiveUser();
    this.userService.getTactisUserByUsername(user).subscribe(res => {
      this.activeUser = <TactisUser>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter o utilizador.", error);
    });
  }

  saveIntervention() {
    let user: TactisUser = this.activeUser;


    if (this.selectedProductActivities == null || this.selectedProductActivities == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve selecionar um produto-atividade.',
        sticky: false,
        life: 1000
      });
      return;
    }

    if (this.selectedType == null || this.selectedType == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve selecionar um tipo de intervenção.',
        sticky: false,
        life: 1000
      });
      return;
    }

    if (this.duration == null || this.duration == undefined || this.duration < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'A duração deve ser superior a 0.',
        sticky: false,
        life: 1000
      });
      return;
    }

    if (this.notes.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve colocar uma descrição da intervenção nas notas.',
        sticky: false,
        life: 1000
      });
      return;
    }

    let intervention = new Intervention(this.interventionDate, this.notes, this.duration, this.interlocutor, this.selectedType,
      user, this.selectedProductActivities, this.clinic);

    let interventionData: InterventionData = new InterventionData(intervention, this.produto, this.status);

    this.interventionService.createIntervention(interventionData).subscribe(res => {

      this.messageService.add({
        severity: 'success',
        summary: 'Guardada',
        detail: 'A intervenção foi registada',
        sticky: false,
        life: 1000
      });
      this.tellParent(true);
    }, error => {
      this.errorService.handleError("Aconteceu um erro, não foi possível registar esta intervenção.", error);
    });
  }

  tellParent(saved: boolean) {
    this.closeTab.emit(saved);
  }

  cancelIntervention() {
    this.tellParent(false);
  }
}
