import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterventionNewViewComponent } from './intervention-new-view.component';

describe('InterventionNewViewComponent', () => {
  let component: InterventionNewViewComponent;
  let fixture: ComponentFixture<InterventionNewViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterventionNewViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterventionNewViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
