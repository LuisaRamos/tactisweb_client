import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovigestTypeViewComponent } from './novigest-type-view.component';

describe('NovigestTypeViewComponent', () => {
  let component: NovigestTypeViewComponent;
  let fixture: ComponentFixture<NovigestTypeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovigestTypeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovigestTypeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
