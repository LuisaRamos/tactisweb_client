import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClinicService } from 'src/app/services/clinic.service';
import { Clinic } from 'src/app/model/clinic';
import { Client } from 'src/app/model/client';
import { ContractChanges } from 'src/app/model/contractchanges';

@Component({
  selector: 'app-novigest-type-view',
  templateUrl: './novigest-type-view.component.html',
  styleUrls: ['./novigest-type-view.component.css']
})
export class NovigestTypeViewComponent implements OnInit {

  type: string;
  adquirido_em: Date;
  product_divida: string;

  @Input() clinic:Clinic;
  @Input() client:Client;

  @Output() producttype: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private clinicService:ClinicService
  ) { }

  ngOnInit() {
    this.getProductType();
  }

  getProductType() {
    this.clinicService.getClinicProductType(this.clinic.clinicId).subscribe(res => {
      let cc: ContractChanges = <ContractChanges>res;
      let code: string = cc.changeCode;

      this.adquirido_em = cc.changeDatadoc;
      if (cc.changeNormalized) {
        this.product_divida = "Não";
      } else {
        this.product_divida = "Sim";
      }
      switch (code) {
        case 'NG_F_VENDA':
          this.type = "Novigest Profissional";
          this.tellParent();
          return;
        case 'NG_S_VENDA':
          this.type = "Novigest Starter";
          this.tellParent();
          return;
        case 'NG_F_A_RENT':
          this.type = "Novigest Profissional (RA_ANUAL)";
          this.tellParent();
          break;
        case 'NG_S_A_RENT':
          this.type = "Novigest Starter (RA_ANUAL)";
          this.tellParent();
          break;
        case 'NG_F_M_RENT':
          this.type = "Novigest Profissional (RA_MENSAL)";
          this.tellParent();
          break;
        case 'NG_S_M_RENT':
          this.type = "Novigest Starter (RA_MENSAL)";
          this.tellParent();
          break;
      }
      
    });
    
  }

  tellParent(){
    this.producttype.emit(this.type);
  }

}
