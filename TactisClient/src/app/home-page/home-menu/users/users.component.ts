import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { TactisUser } from 'src/app/model/tactisuser';
import { Router } from '@angular/router';
import { MessageService, SelectItemGroup, ConfirmationService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { UserRole } from 'src/app/model/userrole';
import { RoleService } from 'src/app/services/role.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [ConfirmationService]
})
export class UsersComponent implements OnInit {

  loading: boolean = true;
  users: TactisUser[];
  groupedUsers: SelectItemGroup[] = [];
  adminlst: TactisUser[] = [];
  administrativelst: TactisUser[] = [];
  comerciallst: TactisUser[] = [];
  helpdesklst: TactisUser[] = [];
  devlst: TactisUser[] = [];
  selectedUser:TactisUser;

  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private roleService: RoleService,
    private confirmationService:ConfirmationService,
    private router: Router,
    private errorService:HandleErrorComponent
    ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    //get user roles
    let roles: UserRole[] = [];
    let userslst: TactisUser[] = [];
    this.groupedUsers = [];

    this.roleService.getUserRoles().subscribe(res => {
      roles = <UserRole[]>res;
      roles.forEach(r => {
        let role = r.userRole;
        this.groupedUsers.push({ label: role, value: role, items: [] });
      });
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores.", error);
    });

    this.userService.getTactisUsers().subscribe(res => {
      userslst = <TactisUser[]>res;
      userslst.forEach(u => {
        let role: string = u.userRoleId.userRole;
        let n: SelectItemGroup = this.groupedUsers.filter(function (x) { return x.label == role })[0];
        setTimeout(() => {
          n.items.push({ label: u.username, value: u });
        });
        if (role == 'ADMIN') {
          this.adminlst.push(u)
        } else if (role == 'ADMINISTRATIVE') {
          this.administrativelst.push(u)
        }
        else if (role == 'COMERCIAL') {
          this.comerciallst.push(u)
        }
        else if (role == 'HELPDESK') {
          this.helpdesklst.push(u)
        }
        else if (role == 'IMPLEMENTATION') {
          this.devlst.push(u)
        }
      });
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores.", error);
    });
  }

  back() {
    this.router.navigate(['/home']);
  }

  edit(){
    this.router.navigate(['/home/profile/' + this.selectedUser.username]);
  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Tem a certeza que pretende apagar o utilizador?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        //this.msgs = [{ severity: 'info', summary: 'Confirmed', detail: 'You have accepted' }];
        this.userService.deleteTactisUser(this.selectedUser.tactisUserId).subscribe(res => {
          this.messageService.add({
            severity: 'success',
            summary: 'Apagado',
            detail: 'O utilizador foi apagado.',
            sticky: false,
            life: 1000
          });
          this.adminlst = [];
          this.administrativelst = [];
          this.comerciallst = [];
          this.helpdesklst = [];
          this.devlst = [];
          this.getUsers();
        }, error => {
          if (error.status == 400 && error.error == "DataIntegrityViolationException") {
            this.messageService.add({
              severity: 'error',
              summary: 'Atenção',
              detail: 'Não é possível apagar o utilizador. ',
              sticky: false,
              life: 1000
            });
          } else {
            this.errorService.handleError("Não é possível apagar o utilizador.", error);
          }
        });
      },
      reject: () => {
        //this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

  addNewUser(){
    this.router.navigate(['home/users/new']);
  }
}