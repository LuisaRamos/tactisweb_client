import { Component, OnInit, Input } from '@angular/core';
import { ClinicService } from 'src/app/services/clinic.service';
import { ContractChanges } from 'src/app/model/contractchanges';
import { Clinic } from 'src/app/model/clinic';
import { ContractAndChanges } from 'src/app/util/view/ContractAndChanges';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Contract } from 'src/app/model/contract';
import { ContractService } from 'src/app/services/contract.service';

@Component({
  selector: 'app-contractchanges-view',
  templateUrl: './contractchanges-view.component.html',
  styleUrls: ['./contractchanges-view.component.css']
})
export class ContractchangesViewComponent implements OnInit {

  changecols: any[];
  changes: ContractChanges[] = [];

  @Input() contract:ContractAndChanges;

  @Input() clinic:Clinic;
  @Input() contract2:Contract;
  
  constructor(
    private errorService:HandleErrorComponent,
    private clinicService:ClinicService,
    private contractService:ContractService) { }

  ngOnInit() {
    this.changecols = [
      { field: 'changeDatadoc', header: 'Data' },
      { field: 'changeCode', header: 'Código' },
      { field: 'changeDescr', header: 'Descrição' },
      { field: 'changeDate', header: 'Até' }
    ];   
    this.getContractChanges();
  }

  getContractChanges() {
     if(this.contract2 != undefined && this.contract2 != null){
      this.contractService.getChangesOfContract(this.contract2.contractId).subscribe(res => {
        this.changes = <ContractChanges[]>res;
        return;
       })
       return;
      }

    // if(this.contract == undefined || this.contract == null){
    //    this.clinicService.getClinicContract(this.clinic.clinicId).subscribe(res => {
    //     this.changes = <ContractChanges[]>res;
    //   }, error => {
    //     this.errorService.handleError("Não foi possível obter as mudanças de contrato.", error);
    //   });
    // } else {
    //   console.log(this.contract);
    //   this.changes = this.contract.changes;
    // }
  }
}
