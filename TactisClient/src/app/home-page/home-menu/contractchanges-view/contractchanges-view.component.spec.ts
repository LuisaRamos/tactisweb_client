import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractchangesViewComponent } from './contractchanges-view.component';

describe('ContractchangesViewComponent', () => {
  let component: ContractchangesViewComponent;
  let fixture: ComponentFixture<ContractchangesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractchangesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractchangesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
