import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemClientesSeeComponent } from './listagem-clientes-see.component';

describe('ListagemClientesSeeComponent', () => {
  let component: ListagemClientesSeeComponent;
  let fixture: ComponentFixture<ListagemClientesSeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemClientesSeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemClientesSeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
