import { Component, OnInit } from '@angular/core';
import { ContractService } from 'src/app/services/contract.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { formatDate, registerLocaleData } from '@angular/common';
import localePT from "@angular/common/locales/pt";
import { MessageService } from 'primeng/api';
import { ClientesIniciaisView } from 'src/app/util/view/ClientesIniciaisView';
import { DateTimeFormatPipe } from 'src/app/util/DateTimeFormatPipe';
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Router } from '@angular/router';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
registerLocaleData(localePT, "pt");

@Component({
  selector: 'app-listagem-clientes-see',
  templateUrl: './listagem-clientes-see.component.html',
  styleUrls: ['./listagem-clientes-see.component.css']
})
export class ListagemClientesSeeComponent implements OnInit {

  cols:any[];
  clientesIniciais:ClientesIniciaisView[] = [];
  datainicio:Date;
  datafim:Date;
  openHelp:boolean = false;

  constructor(
    private contractService:ContractService,
    private errorService:HandleErrorComponent,
    private messageService:MessageService,
    private router:Router
  ) { }

  ngOnInit() {
    
    this.cols = [
      { field: 'clientSince', header: 'Cliente Desde' },
      { field: 'clinic', header: 'Clínica' },
      { field: 'contractNumber', header: 'Nº Contrato' }
    ];
  }

  getListagemClientesIniciais(){
    if(this.datainicio == undefined){
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher uma data de início.',
        sticky: false,
        life: 2000
      });
      return;
    }
    if(this.datafim == undefined){
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher uma data de fim.',
        sticky: false,
        life: 2000
      });
      return;
    }
    if(this.datafim < this.datainicio){
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'A data de fim não pode ser antes da data de início.',
        sticky: false,
        life: 2000
      });
      return;
    }

    let di = formatDate(this.datainicio, 'dd/MM/yyyy', 'pt');
    let df = formatDate(this.datafim, 'dd/MM/yyyy', 'pt');
    this.contractService.getListagemClientesIniciais(di, df).subscribe(res => {
      this.clientesIniciais = <ClientesIniciaisView[]> res;
    }, error => {
      this.errorService.handleError("Não foi possível obter a listagem", error);
    })
  }

  dataToPDFArray(): any[] {
    let rows = [];
    this.clientesIniciais.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.clientSince, "YYYY-MM-DD");
      rows.push([date, element.clinic, element.contractNumber]);
    });
    return rows;
  }

  exportPdf() {
    let rows = this.dataToPDFArray();
    let doc = new jsPDF('l', 'pt');
    doc.autoTable(this.cols, rows);
    doc.save('novos_clientes.pdf');
  }

  dataToExcelFormat(): any[] {
    let rows = [];
    this.clientesIniciais.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.clientSince, "YYYY-MM-DD");
      rows.push({ 'Cliente Desde': date, 'Clínica': element.clinic, 'Nº Contrato': element.contractNumber});
    });
    return rows;
  }

  exportExcel() {
    const worksheet = XLSX.utils.json_to_sheet(this.dataToExcelFormat());
    const workbook = { Sheets: { 'Novos': worksheet}, SheetNames: ['Novos'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, "Novos Clientes");
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  back() {
    this.router.navigate(["home"]);
  }

}
