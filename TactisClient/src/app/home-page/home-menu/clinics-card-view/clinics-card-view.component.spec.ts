import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicsCardViewComponent } from './clinics-card-view.component';

describe('ClinicsCardViewComponent', () => {
  let component: ClinicsCardViewComponent;
  let fixture: ComponentFixture<ClinicsCardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicsCardViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicsCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
