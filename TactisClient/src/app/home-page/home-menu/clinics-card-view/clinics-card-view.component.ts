import { Component, OnInit, Input } from '@angular/core';
import { Clinic } from 'src/app/model/clinic';
import { Aditamento } from 'src/app/model/aditamento';

@Component({
  selector: 'app-clinics-card-view',
  templateUrl: './clinics-card-view.component.html',
  styleUrls: ['./clinics-card-view.component.css']
})
export class ClinicsCardViewComponent implements OnInit {

  @Input() clinicList:Clinic[];
  @Input() aditamento:Aditamento;
  
  constructor() { }

  ngOnInit() {
  }

}
