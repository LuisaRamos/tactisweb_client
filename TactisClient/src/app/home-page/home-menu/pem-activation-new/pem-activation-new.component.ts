import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
import { PemSliderDb } from 'src/app/model/pemSliderDb';

@Component({
  selector: 'app-pem-activation-new',
  templateUrl: './pem-activation-new.component.html',
  styleUrls: ['./pem-activation-new.component.css']
})
export class PemActivationNewComponent implements OnInit {

  @Input() viewOnly:boolean;
  @Input() pem:PemSliderDb;

  constructor() { }

  ngOnInit() {
  }

}
