import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PemActivationNewComponent } from './pem-activation-new.component';

describe('PemActivationNewComponent', () => {
  let component: PemActivationNewComponent;
  let fixture: ComponentFixture<PemActivationNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PemActivationNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PemActivationNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
