import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Kpi } from 'src/app/model/kpi';
import { NovidashService } from 'src/app/services/novidash.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-novidash-kpi-gest',
  templateUrl: './novidash-kpi-gest.component.html',
  styleUrls: ['./novidash-kpi-gest.component.css']
})
export class NovidashKpiGestComponent implements OnInit {


  categorias: any[];
  selectedCategoria: string;
  kpis: Kpi[];
  cols: any[];
  selectedKpi: Kpi;
  displayDialog: boolean = false;
  edit: boolean = false;
  constructor(
    private route: Router,
    private novidashService: NovidashService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.categorias = [
      { label: 'Agenda', value: 'agenda' },
      { label: 'Pacientes', value: 'pacientes' },
      { label: 'Financeiro', value: 'financeiro' },
      { label: 'Orçamentos', value: 'orçamentos' },
      { label: 'Clínica', value: 'clinica' },
      { label: 'Resumo', value: 'resumo' },
    ];

    this.cols = [
      { field: 'titulo', header: 'Título' },
      { field: 'icon', header: 'Icon' },
      { field: 'routerlink', header: 'Router Link' },
      { field: 'categoriaBase', header: 'Categoria Base' },
      { field: 'codigo', header: 'Código' },
      { field: 'sufixo', header: 'Sufixo' }
    ];
    this.seeAllKpi();
  }

  saveKpi() {
    if (this.selectedKpi.titulo == null || this.selectedKpi.titulo == undefined) {
      console.log("titulo nao definido")
      return;
    }
    if (this.selectedKpi.titulo.trim().length < 1) {
      return;
    }

    if (this.selectedKpi.icon == null || this.selectedKpi.icon == undefined) {
      console.log("titulo nao definido")
      return;
    }
    if (this.selectedKpi.icon.trim().length < 1) {
      return;
    }

    if (this.selectedKpi.routerlink == null || this.selectedKpi.routerlink == undefined) {
      console.log("titulo nao definido")
      return;
    }
    if (this.selectedKpi.routerlink.trim().length < 1) {
      return;
    }

    if (this.selectedKpi.categoriaBase == null || this.selectedKpi.categoriaBase == undefined) {
      console.log("titulo nao definido")
      return;
    }
    if (this.selectedKpi.categoriaBase.trim().length < 1) {
      return;
    }

    let kpi: Kpi = new Kpi(this.selectedKpi.titulo, this.selectedKpi.icon, this.selectedKpi.routerlink, this.selectedKpi.categoriaBase, this.selectedKpi.codigo, this.selectedKpi.sufixo);
    this.novidashService.createKpi(kpi).subscribe(res => {
      console.log("guardou com sucesso");
      this.displayDialog = false;
      this.edit = false;
      this.seeAllKpi();
    }, error => {
      console.log("deu erro 1");
    })
  }

  onRowSelect(event) {
    console.log(event);
    this.selectedKpi = event.data;
    // this.displayDialog = true;
  }

  showDialogToAdd() {
    this.selectedKpi = new Kpi("", "", "", "", null, "");
    this.displayDialog = true;
    this.edit = false;
    // this.newCar = true;
    // this.car = {};
    // this.displayDialog = true;
  }

  showDialogToEdit() {
    this.displayDialog = true;
    this.edit = true;
    // this.newCar = true;
    // this.car = {};
    // this.displayDialog = true;
  }

  seeAllKpi() {
    this.novidashService.getAllKpi().subscribe(res => {
      console.log(res);
      this.kpis = <Kpi[]>res;
    }, error => {
      console.log("deu erro 2");
    })
  }

  updateKpi() {
    this.novidashService.updateKpi(this.selectedKpi, this.selectedKpi.id).subscribe(res => {
      this.seeAllKpi();
      this.displayDialog = false;
      this.edit = false;
    }, error => {
      console.log("deu erro 3");
    })
  }

  back() {
    this.route.navigate(['home']);
  }
}
