import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovidashKpiGestComponent } from './novidash-kpi-gest.component';

describe('NovidashKpiGestComponent', () => {
  let component: NovidashKpiGestComponent;
  let fixture: ComponentFixture<NovidashKpiGestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovidashKpiGestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovidashKpiGestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
