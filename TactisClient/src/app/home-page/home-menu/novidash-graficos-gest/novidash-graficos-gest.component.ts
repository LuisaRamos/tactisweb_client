import { Component, OnInit } from '@angular/core';
import { Grafico } from 'src/app/model/grafico';
import { Router } from '@angular/router';
import { NovidashService } from 'src/app/services/novidash.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-novidash-graficos-gest',
  templateUrl: './novidash-graficos-gest.component.html',
  styleUrls: ['./novidash-graficos-gest.component.css']
})
export class NovidashGraficosGestComponent implements OnInit {

  categorias: any[];
  tipos: any[];
  selectedCategoria: string;
  graficos: Grafico[];
  cols: any[];
  selectedGrafico: Grafico;
  displayDialog: boolean = false;
  edit: boolean = false;
  constructor(
    private route: Router,
    private novidashService: NovidashService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.tipos = [
      { label: 'Linear', value: 'line' },
      { label: 'Barra', value: 'bar' },
      { label: 'Barra Horizontal', value: 'horizontalBar' },
      { label: 'Pie', value: 'pie' },
      { label: 'Radar', value: 'radar' }
    ];

    this.categorias = [
      { label: 'Agenda', value: 'agenda' },
      { label: 'Pacientes', value: 'pacientes' },
      { label: 'Financeiro', value: 'financeiro' },
      { label: 'Orçamentos', value: 'orçamentos' },
      { label: 'Clínica', value: 'clinica' },
      { label: 'Resumo', value: 'resumo' },
    ];
    this.cols = [
      { field: 'titulo', header: 'Título' },
      { field: 'tipo', header: 'Tipo' },
      { field: 'categoriaBase', header: 'Categoria Base' },
      { field: 'codigo', header: 'Código' }
    ];
    this.seeAllGraficos();
  }

  saveGrafico() {
    if (this.selectedGrafico.titulo == null || this.selectedGrafico.titulo == undefined) {
      console.log("titulo nao definido")
      return;
    }
    if (this.selectedGrafico.titulo.trim().length < 1) {
      return;
    }


    if (this.selectedGrafico.categoriaBase == null || this.selectedGrafico.categoriaBase == undefined) {
      console.log("titulo nao definido")
      return;
    }
    if (this.selectedGrafico.categoriaBase.trim().length < 1) {
      return;
    }

    let grafico: Grafico = new Grafico(this.selectedGrafico.titulo, this.selectedGrafico.tipo, this.selectedGrafico.options, this.selectedGrafico.categoriaBase, this.selectedGrafico.codigo);
    this.novidashService.createGrafico(grafico).subscribe(res => {
      console.log("guardou com sucesso");
      this.displayDialog = false;
      this.edit = false;
      this.seeAllGraficos();
    }, error => {
      console.log("deu erro 1");
    })
  }

  onRowSelect(event) {
    console.log(event);
    this.selectedGrafico = event.data;
  }

  showDialogToAdd() {
    this.selectedGrafico = new Grafico("", "", "", "",0);
    this.displayDialog = true;
    this.edit = false;
  }

  showDialogToEdit() {
    this.displayDialog = true;
    this.edit = true;
  }

  seeAllGraficos() {
    this.novidashService.getAllGraficos().subscribe(res => {
      console.log(res);
      this.graficos = <Grafico[]>res;
    }, error => {
      console.log("deu erro 2");
    })
  }

  updateGrafico() {
    this.novidashService.updateGrafico(this.selectedGrafico, this.selectedGrafico.id).subscribe(res => {
      this.seeAllGraficos();
      this.displayDialog = false;
      this.edit = false;
    }, error => {
      console.log("deu erro 3");
    })
  }

  back() {
    this.route.navigate(['home']);
  }
}
