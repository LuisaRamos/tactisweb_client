import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovidashGraficosGestComponent } from './novidash-graficos-gest.component';

describe('NovidashGraficosGestComponent', () => {
  let component: NovidashGraficosGestComponent;
  let fixture: ComponentFixture<NovidashGraficosGestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovidashGraficosGestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovidashGraficosGestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
