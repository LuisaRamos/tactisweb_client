import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractsUnfinishedComponent } from './contracts-unfinished.component';

describe('ContractsUnfinishedComponent', () => {
  let component: ContractsUnfinishedComponent;
  let fixture: ComponentFixture<ContractsUnfinishedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractsUnfinishedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractsUnfinishedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
