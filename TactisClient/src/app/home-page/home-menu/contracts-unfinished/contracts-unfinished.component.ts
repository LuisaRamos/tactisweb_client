import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContractService } from 'src/app/services/contract.service';
import { Contract } from 'src/app/model/contract';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { MessageService, SelectItem } from 'primeng/api';
import { ContractUnfinishedView } from 'src/app/util/view/ContractUnfinishedView';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { ContractoParaVigor } from 'src/app/util/view/ContratoParaVigor';
import { AditamentoService } from 'src/app/services/aditamento.service';

@Component({
  selector: 'app-contracts-unfinished',
  templateUrl: './contracts-unfinished.component.html',
  styleUrls: ['./contracts-unfinished.component.css']
})
export class ContractsUnfinishedComponent implements OnInit {

  level:number = 2;
  contracts: ContractUnfinishedView[] = [];
  selectedContract: ContractUnfinishedView;
  editContract: boolean = false;
  clientSince: Date = null;
  contractDate: Date = null;
  contractAte: Date = null;

  isCAL: boolean = false;
  isCM: boolean = false;
  isDoacao:boolean = false;
  timesCM: SelectItem[];
  timesCAL: SelectItem[];


  codes: CodeDescription[] = [];
  typesLoaded: boolean = false;
  icon: string = "pi pi-question";
  hexColor: string = "grey";
  
  selectedTime: string;
  selectedType: CodeDescription;

  constructor(
    private router: Router,
    private contractService: ContractService,
    private errorService: HandleErrorComponent,
    private messageService: MessageService,
    private aditamentoService:AditamentoService
  ) { }

  ngOnInit() {
    this.getContracts();
    
    this.timesCM = [
      { label: '', value: null },
      { label: 'Imediato', value: "im" },
      { label: '1 mês', value: "1M" },
      { label: '6 meses', value: "6M" },
      { label: '1 ano', value: "1A" }];
    this.timesCAL = [
      { label: '', value: null },
      { label: '1 mês', value: "1M" },
      { label: '6 meses', value: "6M" },
      { label: '1 ano', value: "1A" }];
     
  }

  getContracts() {
    this.contractService.getUnfinishedContracts().subscribe(res => {
      this.contracts = [];
      this.contracts = <ContractUnfinishedView[]>res;
      console.log(this.contracts)
    }, error => {
      this.errorService.handleError("Não foi possivel obter os contratos", error);
    })
  }

  getCodes() {
    this.contractService.getContractCodes().subscribe(res => {
      this.codes = <CodeDescription[]>res;
      let index: number = this.codes.findIndex((code) => code.descr == this.selectedContract.contract.contractType);
      this.selectedType = this.codes[index];
      this.typesLoaded = true;
      if(this.selectedType != undefined){
        if (this.selectedType.code.match("RENT")) {
          this.isCAL = true;
          this.isCM = false;
        } else {
          this.isCAL = false;
          this.isCM = true;
        }
        if(this.selectedContract.contract.proposta == "doação" || this.selectedContract.contract.proposta == "Doação"){
          this.isDoacao = true;
          this.selectedContract.contract.contractNumber = "DONATIVO";
        }
      }
     
    }, error => {
      this.errorService.handleError("Não foi possível obter os códigos de contrato.", error);
    })
  }

  calculateTime(){
    if(this.clientSince!= null && this.clientSince != undefined){
      this.contractAte = new Date(this.clientSince);
      if(this.selectedTime == "1M"){
        this.contractAte.setMonth(this.clientSince.getMonth() + 1);
      } else if ( this.selectedTime == "6M"){
        this.contractAte.setMonth(this.clientSince.getMonth() + 6);
      } else if (this.selectedTime == "1A"){
        this.contractAte.setMonth(this.clientSince.getMonth() + 12);
      } else if (this.selectedTime == "im"){
        this.contractAte = new Date(this.clientSince);
      }
    }
    else {
      if(this.isCAL){
        this.selectedTime = this.timesCAL[0].value;
      }
      if(this.isCM){
        console.log(this.timesCM[0].value)
        this.selectedTime = this.timesCM[0].value;
      }
      
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Para calcular automaticamente o periodo, deve preencher o "Cliente Desde".',
        sticky: false,
        life: 3000
      });
    }
  }

  sugestNextCode() {
    if(this.isDoacao){
      this.selectedContract.contract.contractNumber = "DONATIVO";
    } else {
      this.contractService.getLastContractNumber(this.selectedType.code).subscribe(res => {
        let t: string = res;
        let tcut;
        let numbers: number;
        let code: string;
        if (this.selectedType.code.match("RENT")) {
          tcut = t.slice(3, 7);
          code = "CAL0";
          this.isCAL = true;
          this.isCM = false;
        } else {
          code = "CM0";
          this.isCAL = false;
          this.isCM = true;
          if (t.length > 5) {
            tcut = t.slice(2, 6);
          } else {
            tcut = t.slice(2, 5);
          }
        }
        numbers = +tcut;
        numbers += 1;
        code = code + numbers;
        this.selectedContract.contract.contractNumber = code + "/0/00";
      });
    }
  }


  clearCNbutton() {
    this.icon = "pi pi-question";
    this.hexColor = "grey"
  }


  clearContractNumber(event) {
    this.selectedType = event.value;
    this.selectedContract.contract.contractNumber = "";
    this.icon = "pi pi-question";
    this.hexColor = "grey"
    this.isCAL = false;
    this.isCM = false;
  }

  checkValidAndExistsContractNumber(): boolean {
    if (!this.checkValidContractNumber()) {
      this.icon = "pi pi-times";
      this.hexColor = "red";
      return false;
    }

    let contractnr = this.selectedContract.contract.contractNumber.split('/')[0];
    this.contractService.checkIfContractNumberExists(contractnr).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result) {
        this.icon = "pi pi-times";
        this.hexColor = "red";
        return false;
      }
      else {
        this.icon = "pi pi-check";
        this.hexColor = "green";
        return true;
      }
    })
  }

  checkValidContractNumber(): boolean {
    let tcut;
    let codeCut;
    let t = this.selectedContract.contract.contractNumber;
    if (this.selectedType.code.match("RENT")) {
      codeCut = t.slice(0, 3);
      tcut = t.slice(4, this.selectedContract.contract.contractNumber.length);
      let num: number = <number>tcut.length;
      if (codeCut == "CAL") {
        if (num > 0) {
          return true;
        }
      }
      return false;

    } else {
      codeCut = t.slice(0, 2);
      tcut = t.slice(3, this.selectedContract.contract.contractNumber.length);
      let num: number = <number>tcut.length;
      if (codeCut == "CM") {
        if (num > 0) {
          return true;
        }
      }
      return false;
    }
  }


  back() {
    this.router.navigate(['home']);
  }

  finish(contract: ContractUnfinishedView) {
    this.selectedContract = contract;
    console.log("finish contract");
    console.log(contract);
    this.getCodes();
    this.editContract = true;
  }

  hearType(event) {
    this.selectedType = event;
  }

  hearContractAte(event) {
    this.contractAte = event;
  }

  hearContractDate(event) {
    this.contractDate = event;
  }

  hearClientSince(event) {
    this.clientSince = event;
  }


  entrarEmVigor() {
    this.contractDate = new Date();

    if(this.selectedContract.contract.proposta == "doação" || this.selectedContract.contract.proposta == "Doação" ){
      if (this.contractAte == null || this.contractAte == undefined) {
        this.showWarnMessage("Deve preencher o campo 'Contrato Até'");
        return;
      }
      if (this.clientSince == null || this.clientSince == undefined) {
        this.showWarnMessage("Deve preencher o campo 'Cliente Desde'");
        return;
      }
      if (this.contractDate == null || this.contractDate == undefined) {
        this.showWarnMessage("Deve preencher o campo 'Data do Contrato'");
        return;
      }
    } else {
      if (this.checkContractInput() == false) {
        return;
      }
    }
  

    if(this.selectedContract.aditamento == undefined){
      let c:ContractoParaVigor = new ContractoParaVigor(this.selectedContract.contract, this.selectedContract.clinics, this.clientSince, this.contractDate, this.contractAte);
      this.contractService.updateContractAndPutInVigor(this.selectedContract.contract.contractId, c).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'O contrato foi atualizado e está em vigor.',
          sticky: false,
          life: 2000
        });
  
        
        setTimeout(res =>{
          this.clientSince= null;
          this.contractDate= null;
          this.contractAte = null;
          this.editContract = false;
          this.selectedContract = undefined;
          this.getContracts();
        }, 400)
      }, error => {
        this.errorService.handleError("Não foi possível atualizar o contrato", error);
      })
    } else {
      
      this.selectedContract.aditamento.faturado = true;
      this.selectedContract.aditamento.conclusionDate = new Date();
      this.selectedContract.aditamento.contractId = this.selectedContract.contract;
      this.aditamentoService.updateAditamentoAndPutInVigor(this.selectedContract.aditamento.aditamentoId, this.selectedContract.aditamento).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'O contrato/aditamento foi registado. Se se trata de uma associação, por favor adicione manualmente os documentos.',
          sticky: false,
          life: 5000
        });
        setTimeout(res =>{
          this.clientSince= null;
          this.contractDate= null;
          this.contractAte = null;
          this.editContract = false;
          this.selectedContract = undefined;
          this.getContracts();
        }, 100)
      }, error => {
        this.errorService.handleError("Não foi possível atualizar o contrato", error);
      });
    }
    
  }

  checkContractInput(): boolean {
    console.log(this.selectedContract.contract.contractNumber);
    if(this.selectedContract.aditamento == undefined){
      if (this.contractAte == null || this.contractAte == undefined) {
        this.showWarnMessage("Deve preencher o campo 'Contrato Até'");
        return false;
      }
      if (this.clientSince == null || this.clientSince == undefined) {
        this.showWarnMessage("Deve preencher o campo 'Cliente Desde'");
        return false;
      }
      if (this.contractDate == null || this.contractDate == undefined) {
        this.showWarnMessage("Deve preencher o campo 'Data do Contrato'");
        return false;
      }
      if (this.selectedContract.contract.contractNumber.match("/0/00")) {
        this.showWarnMessage("Deve preencher um 'Número de contrato' válido. Não pode ficar /0/00");
        return false;
      }
      let first :number =<number>(<any> this.selectedContract.contract.contractNumber.split("/")[1]);
      if (  first > 2 ) {
        this.showWarnMessage("Deve preencher um 'Número de contrato' válido. No meio deve ter indicação 1 ou 2");
        return false;
      }
      let second =(this.selectedContract.contract.contractNumber.split("/")[2]);
      if (  second != ("" + new Date().getFullYear()).substring(2,4) ) {
        this.showWarnMessage("Deve preencher um 'Número de contrato' válido");
        return false;
      }
    }
   
    if (this.selectedContract.contract.contractNumber == null ||
      this.selectedContract.contract.contractNumber == undefined ||
      this.selectedContract.contract.contractNumber.trim().length == 0) {
      this.showWarnMessage("Deve preencher o campo 'Número de contrato'");
      return false;
    }
   
    return true;
  }

  showWarnMessage(message: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Atenção!',
      detail: message,
      sticky: false,
      life: 2000
    });
  }
}
