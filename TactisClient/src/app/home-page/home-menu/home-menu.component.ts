import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService, ConfirmationService, MenuItem } from 'primeng/api';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';



@Component({
  selector: 'app-home-menu',
  templateUrl: './home-menu.component.html',
  styleUrls: ['./home-menu.component.css'],
  providers: [ConfirmationService]
})
export class HomeMenuComponent implements OnInit {

  ticks =0;
  items:MenuItem[]=[];
  activeUser:string = "";
  toSearch:string;

  number:number=0;
  
  constructor(
    private router: Router,
    private authService: AuthService,
    private installationService:InstallationService,
    private messageService:MessageService,
    private confirmationService:ConfirmationService) { 
  }

  ngOnInit() {
    this.activeUser = this.authService.getActiveUser();

    //está em comentario para nao estar sempre a fazer pedidos desnecessarios enquanto trato do resto
    //const source = timer(1000, 20000);
    //const subscribe = source.subscribe(val => this.findNotifications());

  }
  doLogout() {
    this.confirmationService.confirm({
      message: 'Tem a certeza que quer terminar sessão?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.authService.logout();
        this.router.navigate(["/login"]);
      }
    });
  }

  goHome(){
    this.router.navigate(["home"]);
    return;
  }

  search() {
    this.router.navigate(["home/byclient/" + this.toSearch]);
    return;
  }

  findNotifications(){
    this.installationService.getInstallationsUnassigned().subscribe(data => {
      let ins:Installation[] = <Installation[] > data;
      let many:number = ins.length;
      this.number = many;
    })
  }

  openNotifications(){
    this.messageService.add({
      severity: 'warn',
      summary: 'Ups!',
      detail: 'Ainda não está implementado.',
      sticky: false,
      life: 2000
    });
  }

}
