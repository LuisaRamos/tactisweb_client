import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { TactisUser } from 'src/app/model/tactisuser';
import { UserRole } from 'src/app/model/userrole';
import { RoleService } from 'src/app/services/role.service';
import { AuthService } from 'src/app/services/auth.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-user-new',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.css']
})
export class UserNewComponent implements OnInit {

  _password: string = "";
  roles: UserRole[];
  selectedRole: UserRole;

  //regex
  blockSpace: RegExp = /[^\s]/;

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  iconUN: string = "pi pi-question";
  iconEM: string = "pi pi-question";
  hexColorUN: string = "grey";
  hexColorEM: string = "grey";

  constructor(
    private roleSrv: RoleService,
    private userSrv: UserService,
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private errorService:HandleErrorComponent) { }

  ngOnInit() {
    this.getUserRoles();
    this.registerForm = this.formBuilder.group({
      _name: ['', [Validators.required, Validators.minLength(1)]],
      _selectedRole: ['', [Validators.required]],
      _username: ['', [Validators.required, Validators.minLength(1)]],
      _email: ['', [Validators.required, Validators.minLength(1)]],
      _password: ['', [Validators.required, Validators.minLength(6)]],
      _retypePassword: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  getUserRoles(): void {
    this.roleSrv.getUserRoles().subscribe(data => {
      this.roles = <UserRole[]>data;
    }, error => {
      this.errorService.handleError("Aconteceu um erro, não foi possível registar obter os tipos de utilizador.", error);
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  createNew(): void {
    this.submitted = true;
    let nameFormControl = this.f._name.value;
    let usernameFormControl = this.f._username.value;
    let emailFormControl = this.f._email.value;
    let passwordFormControl = this.f._password.value;
    let rpasswordFormControl = this.f._retypePassword.value;
    let roleFormControl = this.f._selectedRole.value;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Verifique os dados preenchidos',
        sticky: false,
        life: 1000
      });
      return;
    }

    //stop if user role is not defined
    if (roleFormControl == undefined || roleFormControl == null) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Verifique os dados preenchidos',
        sticky: false,
        life: 1000
      });
      return;
    }

    //stop if password is not strong enough
    if (!this.validatePassword(passwordFormControl)) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Introduza uma password com pelo menos 1 número, letra maiscula e minuscula',
        sticky: false,
        life: 1000
      });
      return;
    }

    //stop if passwords dont match
    if (rpasswordFormControl != passwordFormControl) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'As passwords não coincidem!',
        sticky: false,
        life: 1000
      });
      return;
    }

    let user = new TactisUser(nameFormControl, usernameFormControl, emailFormControl, passwordFormControl, roleFormControl, true);
    this.userSrv.createTactisUser(user)
      .subscribe(data => {

        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Utilizador Registado!',
          sticky: false,
          life: 1000
        });

        this.submitted = false;
        //espera 1000 milisegundos para mudar para o home, se mudar logo a toast nao aparece
        //e nao entendo o porque. gostava que ela ficasse mesmo alterando a pagina
        setTimeout(() => {
          this.router.navigate(["/home"]);
        }, 1000);

      }, error => {
        if (error.status == 400 && error.error == "DataIntegrityViolationException") {
          this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: 'Existe um utilizador igual a este.',
            sticky: false,
            life: 1000
          });
        } else if (error.status == 400 && error.error == "UsernameNotAvailable") {
          this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: 'Existe um utilizador com esse username.',
            sticky: false,
            life: 1000
          });
        } else {
          this.errorService.handleError("Aconteceu um erro, não foi possível criar um novo utilizador.", error);
        }
        this.submitted = false;
      });
  }

  /**
   * Para que seja válida, neste momento só é requerido que tenha pelo menos:
   * 1 letra maiscula
   * 1 letra minuscula
   * 1 numero
   * e comprimento igual ou superior a 6 (validado no form)
   */
  private validatePassword(password: string): boolean {
    let minuscula = password.match(/[a-z]/);
    let maiscula = password.match(/[A-Z]/);
    let numeros = password.match(/\d/);

    return minuscula != null && maiscula != null && numeros != null;

  }
  back() {
    this.router.navigate(["/home"]);
  }

  checkUsername(){
    if(this.f._username == undefined){
      return;
    }
    let usernameFormControl:string = this.f._username.value;
    if(usernameFormControl == undefined){
      return ;
    }
    if(usernameFormControl.trim().length == 0 ){
      return ;
    }
    this.userSrv.checkIfUsernameExists(usernameFormControl).subscribe(res => {
      console.log(res);
      let result: boolean = <boolean>res;
      console.log(result)
      if (result == true) {
        
        this.iconUN = "pi pi-times";
        this.hexColorUN = "red";
      }
      else {
        this.iconUN = "pi pi-check";
        this.hexColorUN = "green";
      }
    })
  }

  clearUsername(){
    this.hexColorUN = "grey";
    this.iconUN= "pi pi-question";
  }

  clearEmail(){
    this.hexColorEM = "grey";
    this.iconEM= "pi pi-question";
  }

  checkEmail(){
    if(this.f._email == undefined){
      return;
    }
    let emailFormControl = this.f._email.value;
    if(emailFormControl == undefined){
      return ;
    }
    if(emailFormControl.trim().length == 0 ){
      return ;
    }
    
    this.userSrv.checkIfEmailExists(emailFormControl).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result == true) {
        this.iconEM = "pi pi-times";
        this.hexColorEM = "red";
      }
      else {
        this.iconEM = "pi pi-check";
        this.hexColorEM = "green";
      }
    })
  }
}