import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-handle-error',
  templateUrl: './handle-error.component.html',
  styleUrls: ['./handle-error.component.css']
})
export class HandleErrorComponent implements OnInit {

  constructor(
    private auth:AuthService,
    private messageService:MessageService,
    private router:Router) { }

  ngOnInit() {
  }

  handleError(message:string, error){
    if (error.status == 401 /*&& error.error == "SessionExpired" or "Unauthorized"*/) {
      this.auth.setSessionExpired();
      this.messageService.add({
        severity: 'error',
        summary: 'Erro',
        detail: 'Sessão expirada. Faça novamente o login.',
        sticky: false,
        life: 2000
      });
      setTimeout(() => {
        this.auth.logout();
        this.router.navigate(["/login"]);
      }, 1000);
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'Erro',
        detail: message,
        sticky: false,
        life: 2000
      });
    }
  }

}
