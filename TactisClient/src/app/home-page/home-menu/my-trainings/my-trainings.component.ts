import { Component, OnInit, ViewChild } from '@angular/core';
import { ContractAndInstallation } from 'src/app/util/view/contractandinstallation';
import { TactisUser } from 'src/app/model/tactisuser';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService } from 'primeng/api';
import { EventAndUsers } from 'src/app/util/view/eventAndUsers';
import { Event } from 'src/app/model/event';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { EventResumeViewComponent } from '../event-resume-view/event-resume-view.component';

@Component({
  selector: 'app-my-trainings',
  templateUrl: './my-trainings.component.html',
  styleUrls: ['./my-trainings.component.css']
})
export class MyTrainingsComponent implements OnInit {


  data: EventAndUsers[];
  activeUser: TactisUser;
  display: boolean = false;
  selectedEvent: Event;
  newInstDate: Date = new Date();
  dataLoaded: boolean = false;
  loading: boolean = true;
  usersT: TactisUser[] = [];
  values: TactisUser[] = [];

  @ViewChild("resume") resume: EventResumeViewComponent;

  constructor(
    private userService: UserService,
    private router: Router,
    private calendarService: CalendarEventsService,
    private auth: AuthService,
    private messageService: MessageService,
    private errorService: HandleErrorComponent
  ) {
  }

  ngOnInit() {
    this.getActiveUser();
    this.getActiveUsers();
  }

  getActiveUser() {
    let user = this.auth.getActiveUser();
    this.userService.getTactisUserByUsername(user).subscribe(res => {
      this.activeUser = <TactisUser>res;
      this.getTrainings();
    }, error => {
      this.errorService.handleError("Não foi possível obter o utilizador.", error);
    });
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.usersT = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores.", error);
    });
  }

  getTrainings() {
    if (this.activeUser.userRoleId.userRole == 'ADMIN') {
      this.getAllNotDoneTrainings();
    } else {
      this.getTrainingsOfUser(this.activeUser.tactisUserId);
    }
  }

  getAllNotDoneTrainings() {
    this.calendarService.getAllTrainingsAssignedAndNotDone().subscribe(data => {
      this.handleData(data);
    }, error => {
      this.errorService.handleError("Não foi possível obter as formações.", error);
    });
  }

  getTrainingsOfUser(id: number) {
    this.calendarService.getAllTrainingsAssignedAndNotDoneToUser(id).subscribe(data => {
      this.handleData(data)
    }, error => {
      this.errorService.handleError("Não foi possível obter as formações.", error);
    });
  }

  handleData(data) {
    this.data = <EventAndUsers[]>data;
    this.dataLoaded = true;
    this.loading = false;
  }

  todayInstall(date: Date) {
    let today = new Date();
    let instDate = new Date(date);

    //se a data for a mesma
    if (today.getUTCFullYear() == instDate.getUTCFullYear() && today.getMonth() == instDate.getMonth() && today.getDate() == instDate.getDate()) {
      return 1;
    }
    //se a data for de amanha
    if (today.getUTCFullYear() == instDate.getUTCFullYear() && today.getMonth() == instDate.getMonth() && today.getDate() == instDate.getDate() - 1) {
      return 2;
    }
    if (today > instDate) {
      return 3;
    }
    return 4;
  }

  install(e: Event) {
    e.done = true;
    this.calendarService.updateTrainingEventDone(e.eventId, e).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'A formação foi dada como concluída.',
        sticky: false,
        life: 1500
      })
      this.display = false;
      this.getTrainings();

    }, error => {
      this.errorService.handleError("Não foi possível concluir a formação", error);
    })
  }

  editSchedule(data: EventAndUsers) {
    this.values = [];

    data.users.forEach(element1 => {
      this.usersT.forEach(element2 => {
        if (element1.username == element2.username) {
          this.values.push(element2);
          return;
        }
      });
    })


    this.selectedEvent = data.event;
    this.newInstDate = new Date(data.event.beginDate);
    this.display = true;
  }

  confirmEditedTraining() {

    if (this.values.length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção!',
        detail: 'Tem de atribuir a pelo menos uma pessoa.',
        sticky: false,
        life: 1000
      });
      return;
    }

    this.selectedEvent.beginDate = this.newInstDate;
    let end2: Date = new Date(this.selectedEvent.beginDate);
    end2.setHours(end2.getHours() + 1);

    let trainingEndDate: Date = new Date(end2);
    this.selectedEvent.endDate = trainingEndDate;

    let eau: EventAndUsers = new EventAndUsers(this.selectedEvent, this.values);

    this.calendarService.updateTrainingEvent(this.selectedEvent.eventId, eau).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'Dados da formação alterados',
        sticky: false,
        life: 1000
      })
      this.display = false;
      this.getAllNotDoneTrainings();
    });
  }


  back() {
    this.router.navigate(['home']);
  }
}