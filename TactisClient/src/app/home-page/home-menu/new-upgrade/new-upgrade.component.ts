import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { Upgrade } from 'src/app/model/upgrade';
import { UpgradeService } from 'src/app/services/upgrade.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-new-upgrade',
  templateUrl: './new-upgrade.component.html',
  styleUrls: ['./new-upgrade.component.css']
})
export class NewUpgradeComponent implements OnInit {

  ativo: boolean = false;
  versao: string = "";
  checksum: string = "";
  url_db: string = "";
  url_app: string= "";
  password: string = "";

  submitted: boolean = false;

  @Input() app:string;

  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private messageService: MessageService,
    private upgradeService: UpgradeService,
    private router: Router,
    private errorService: HandleErrorComponent
  ) { }

  ngOnInit() {

  }


  createNew(): void {

    if (this.app.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher o nome',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }

    if (this.versao.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher a versão',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }

    if (this.checksum.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher o checksum',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }

    // if (this.url_db.trim().length < 1) {
    //   this.messageService.add({
    //     severity: 'warn',
    //     summary: 'Atenção',
    //     detail: 'Deve preencher o URL DB',
    //     sticky: false,
    //     life: 2000
    //   });
    //   //mensagem preencher nome
    //   return;
    // }

    if (this.url_app.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher o URL App',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }

    let upgrade: Upgrade = new Upgrade(this.app, this.ativo, this.versao, this.checksum, this.url_db, this.url_app, this.password);

    console.log(upgrade);
    this.upgradeService.createUpgrade(upgrade).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'A versão foi criada',
        sticky: false,
        life: 1200
      });
      setTimeout(res => {
        this.clearFields();
        this.tellParent();
      }, 1200)
    }, error => {
      this.errorService.handleError("Não foi possível guardar a versão", error);
    });

  }

  clearFields(){
    this.versao = "";
    this.checksum = "";
    this.url_db = "";
    this.url_app= "";
    this.password = "";
  }

  tellParent(){
    this.close.emit(false);
  }

  back() {
    this.router.navigate(["/home"]);
  }

}
