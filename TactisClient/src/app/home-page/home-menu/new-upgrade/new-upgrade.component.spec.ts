import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewUpgradeComponent } from './new-upgrade.component';

describe('NewUpgradeComponent', () => {
  let component: NewUpgradeComponent;
  let fixture: ComponentFixture<NewUpgradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewUpgradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewUpgradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
