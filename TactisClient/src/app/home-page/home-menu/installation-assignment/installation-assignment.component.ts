import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { TactisUser } from 'src/app/model/tactisuser';
import { Router, ActivatedRoute, ChildActivationEnd } from '@angular/router';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService, MenuItem, } from 'primeng/api';
import { TrainingService } from 'src/app/services/training.service';
import { Client } from 'src/app/model/client';
import { Event } from 'src/app/model/event';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { EventTactisUser } from 'src/app/model/eventTactisUser';
import { Training } from 'src/app/model/training';
import { ClinicService } from 'src/app/services/clinic.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ClientViewComponent } from '../client-view/client-view.component';
import { ClinicsCardViewComponent } from '../clinics-card-view/clinics-card-view.component';
import { Clinic } from 'src/app/model/clinic';
import { TrainingAssignmentFormComponent } from '../training-assignment-form/training-assignment-form.component';

@Component({
  selector: 'app-installation-assignment',
  templateUrl: './installation-assignment.component.html',

  styles: [`
        .custombar1 .ui-scrollpanel-wrapper {
            border-right: 9px solid #E9ECEF;
        }
            
        .custombar1 .ui-scrollpanel-bar {
            background-color: #1976d2;
            opacity: 1;
            transition: background-color .3s;
        }
            
        .custombar1 .ui-scrollpanel-bar:hover {
            background-color: #135ba1;
        }
         
    `],
  encapsulation: ViewEncapsulation.None
})
export class InstallationAssignmentComponent implements OnInit {

  users: TactisUser[] = [];

  loading: boolean = true;
  installation: Installation;
  pt: any;
  installationDate: Date = new Date();

  id: number;
  instReady: boolean = false;

  cli: Client;
  clientLoaded: boolean = false;


  values: TactisUser[] = [];
  items: MenuItem[];

  activeIndex: number = 0;

  selectedClinicTraining: Clinic;
  allpushed: boolean = false;

  selectedClinics: Clinic[] = [];
  // --- formaçao
  trainOpt: string = "val1";
  trainings: Training[] = [];
  trainingDate: Date = new Date();
  trainingNotes: string = "";
  valuesT: TactisUser[];
  trainingUsers: TactisUser[];

  openHelp:boolean = false;

  @ViewChild("client") client: ClientViewComponent;
  @ViewChild("clinicList") clinicList: ClinicsCardViewComponent;
  @ViewChildren(TrainingAssignmentFormComponent) tabs: QueryList<TrainingAssignmentFormComponent>;

  constructor(
    private userService: UserService,
    private installationService: InstallationService,
    private calendarService: CalendarEventsService,
    private route: ActivatedRoute,
    private clinicService: ClinicService,
    private messageService: MessageService,
    private router: Router,
    private errorService: HandleErrorComponent) { }

  ngOnInit() {
    this.items = [{
      label: 'Cliente',
      command: (event: any) => {
        this.activeIndex = 0;
      }
    },
    {
      label: 'Clínicas',
      command: (event: any) => {
        this.activeIndex = 1;
      }
    },
    {
      label: 'Instalação',
      command: (event: any) => {
        this.activeIndex = 2;
      }
    },
    {
      label: 'Formação',
      command: (event: any) => {
        this.activeIndex = 3;
      }
    },
    {
      label: 'Contrato',
      command: (event: any) => {
        this.activeIndex = 4;
      }
    }
    ];
    this.getActiveUsers();
    this.getInstallationInformation();


    this.pt = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
      dayNamesShort: ["Dom", "2ª", "3ª", "4ª", "5ª", "6ª", "Sáb"],
      dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S"],
      monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      today: 'Hoje',
      clear: 'Clear',
      dateFormat: 'yy-mm-dd'
    };
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.users = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores", error);
    });
    this.userService.getActiveUsers().subscribe(res => {
      this.trainingUsers = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores", error);
    });
  }

  getInstallationInformation() {
    this.route.params.subscribe(res => {
      this.id = <number>res.id;
    });
    this.installationService.getInstallation(this.id).subscribe(res => {
      this.installation = <Installation>res;
      this.getInstallationClient();
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações", error);
    });
  }

  getInstallationClient() {
    this.installationService.getInstallationClient(this.installation.installationId).subscribe(res => {
      this.cli = <Client>res;
      this.clientLoaded = true;
    }, error => {
      this.errorService.handleError("Não foi possível obter o cliente", error);
    })
  }

  allSame() {
    this.installation.clinicList.forEach(element => {
      setTimeout(res => {
        let trainingName: string = "train_" + new Date().getTime();
        let training: Training = new Training(new Date(), trainingName, "", element, false, true);
        this.trainings.push(training);
      }, 20);
      setTimeout(res => {
        this.allpushed = true;
      }, 100);
    });
    // let training: Training = new Training(this.trainingDate, trainingName, this.trainingNotes, this.selectedClinicTraining, false, true);
  }

  allDiff() {
    this.installation.clinicList.forEach(element => {
      // let trainingEndDate: Date = new Date(end2);
      setTimeout(res => {
        let trainingName: string = "train_" + new Date().getTime();
        let training: Training = new Training(new Date(), trainingName, "", element, false, true);
        this.trainings.push(training);
      }, 20);
      setTimeout(res => {
        this.allpushed = true;
      }, 100);
    });
  }

  back() {
    this.router.navigate(["home/schedules"]);
  }

  save2() {
    this.doSaveTraining();
  }

  save() {
    if (this.values.length < 1) {
      this.messageService.add({
        severity: 'error',
        summary: 'Erro',
        detail: 'Tem de escolher utilizadores para realizar a instalação. Pode alterá-los no futuro.',
        sticky: false,
        life: 2000
      });
      return;
    }
    // if (true) {
    //   // if (this.valuesT.length < 1) {
    //   //   this.messageService.add({
    //   //     severity: 'error',
    //   //     summary: 'Erro',
    //   //     detail: 'Tem de escolher utilizadores para realizar a formação. Pode alterá-los no futuro.',
    //   //     sticky: false,
    //   //     life: 2000
    //   //   });
    //   //   return;
    //   // }
    //   if (this.selectedClinicTraining == undefined || this.selectedClinicTraining == null) {
    //     this.messageService.add({
    //       severity: 'warn',
    //       summary: 'Atenção',
    //       detail: 'Tem de associar a formação a uma clínica',
    //       sticky: false,
    //       life: 2000
    //     });
    //     return;
    //   }
    // }

    this.installation.installationDate = this.installationDate;
    this.installation.scheduled = true;

    let end: Date = new Date(this.installationDate);
    end.setHours(end.getHours() + 1);

    let endDate: Date = new Date(end);

    this.installationService.updateInstallation(this.id, this.installation).subscribe(res => {
      this.instReady = true;


      //fez update, entao criar os eventos
      let userlist: EventTactisUser[] = [];
      this.values.forEach(element => {
        userlist.push(new EventTactisUser(element));
      })

      let myevent: Event = new Event("Instalação", this.installation.description, this.installation.notes, "", false, this.installationDate, endDate, this.cli, null, userlist);
      this.calendarService.createEvent(myevent).subscribe(res => {
        if (this.trainOpt == 'val1') {
          this.messageService.add({
            severity: 'success',
            summary: 'Sucesso',
            detail: 'Agendamento de instalação guardado!',
            sticky: false,
            life: 2000
          });
          setTimeout(() => {
            this.router.navigate(["/home"]);
          }, 1000);
          return;
        }
        // ----------------- TRAINING ---------------------
        this.doSaveTraining();

      }, error => {
        this.errorService.handleError("Não foi possível criar o evento de instalação", error);
      });

    }, error => {
      this.errorService.handleError("Não foi possível guardar a instalação", error);
    });



  }

  doSaveTraining() {
    console.log(this.trainings.length);
    for (let i = 0; i < this.trainings.length; i++) {
      let element = this.trainings[i];

      setTimeout(res => {
        // se for igual para todas, é preciso atualizar cada training com os valores gerais
        if (this.trainOpt == "val2") {
          element.trainingDate = this.trainingDate;
          element.notes = this.trainingNotes;
        }

        let end2: Date = new Date(element.trainingDate);
        end2.setHours(end2.getHours() + 1);
        let trainingEndDate: Date = new Date(end2);

        if ((this.trainOpt == "val3" && this.selectedClinics.includes(element.clinic)) || this.trainOpt == "val2" ) {
          this.clinicService.addTrainingToClinic(element.clinic.clinicId, element).subscribe(res => {

            let userlist2: EventTactisUser[] = [];
            //se for diferente, tem de ir buscar os values de cada tab
            if (this.trainOpt == "val3") {
              let valuesTr: TactisUser[] = [];
              console.log("i = " + i);
              valuesTr = this.tabs.toArray()[i].valuesT;
              valuesTr.forEach(element => {
                userlist2.push(new EventTactisUser(element));
              })
            } else if (this.trainOpt == "val2") {
              //se for igual para todos, tem de ir buscar o valueT geral
              this.valuesT.forEach(element => {
                userlist2.push(new EventTactisUser(element));
              })
            }

            let myevent2: Event = new Event("Formação", element.description, element.notes, "", false, element.trainingDate, trainingEndDate, null, element.clinic, userlist2);

            this.calendarService.createEvent(myevent2).subscribe(res => {
              if (i == this.trainings.length - 1) {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Sucesso',
                  detail: 'Agendamentos guardados!',
                  sticky: false,
                  life: 2000
                });
              }
              setTimeout(() => {
                this.router.navigate(["/home"]);
              }, 1000);
            }, error => {
              this.errorService.handleError("Não foi possível criar o evento", error);
            });

          }, error => {
            this.errorService.handleError("Não foi possível criar a formação", error);
          });
        }


      }, 20)
    }
  }

  next() {
    if (this.activeIndex < 3) {
      this.activeIndex += 1;
      return;
    }
  }

  previous() {
    if (this.activeIndex > 0) {
      this.activeIndex -= 1;
      return;
    }
  }
}
