import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallationAssignmentComponent } from './installation-assignment.component';

describe('InstallationAssignmentComponent', () => {
  let component: InstallationAssignmentComponent;
  let fixture: ComponentFixture<InstallationAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstallationAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallationAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
