import { Component, OnInit } from '@angular/core';
import { InstallationService } from 'src/app/services/installation.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ActivatedRoute, Router } from '@angular/router';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { InstallationAndNovigestSlider } from 'src/app/util/view/InstallationAndNovigestSlider';
import { NovigestSliderDb } from 'src/app/model/novigestSliderDb';
import { Installation } from 'src/app/model/installation';
import { MessageService, ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-novigest-activations-view',
  templateUrl: './novigest-activations-view.component.html',
  styleUrls: ['./novigest-activations-view.component.css'],
  providers: [ConfirmationService]
})
export class NovigestActivationsViewComponent implements OnInit {

  data: InstallationAndNovigestSlider[] = [];
  loading: boolean = true;
  allNg: InstallationAndNovigestSlider[] = [];
  selectedNgs: InstallationAndNovigestSlider[] = [];
  selectedNg: NovigestSliderDb = undefined;
  selectedInstallation: Installation;
  selectedInstallationToAdd: Installation;
  installations: Installation[];

  displaySeeInstallation: boolean;
  displayEditInstallation: boolean;
  displaySeeNg: boolean;
  displayAddNewNg: boolean;
  displayEditNg: boolean;
  newNg: NovigestSliderDb = new NovigestSliderDb("", "", "", "", "", "", "", "", "", null, null, null, "", null, null, null, null, null, null, null, null, null, "");
  xmlURL: string = "https://tactiscloud.no-ip.net:4568/protected/installations/xml/ng"

  // --- btn 
  private _fixed = false;

  public open = false;
  public spin = true;
  public direction = 'down';
  public animationMode = 'fling';

  get fixed(): boolean {
    return this._fixed;
  }

  set fixed(fixed: boolean) {
    this._fixed = fixed;
    if (this._fixed) {
      this.open = true;
    }
  }

  public stopPropagation(event: Event): void {
    // Prevent the click to propagate to document and trigger
    // the FAB to be closed automatically right before we toggle it ourselves
    event.stopPropagation();
  }

  // --- btn 

  constructor(
    private installationService: InstallationService,
    private errorService: HandleErrorComponent,
    private route: ActivatedRoute,
    private previousRouteService: PreviousRouteService,
    private router: Router,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.getData();
    this.getUnactivatedInstallations();
  }

  getUnactivatedInstallations() {
    this.installations = [];
    this.installationService.getInstallationsWithoutNovigestSlider().subscribe(res => {
      this.installations = <Installation[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações", error);
    });
  }

  getData() {
    this.installationService.getNovigestSliders().subscribe(res => {
      this.data = <InstallationAndNovigestSlider[]>res;
      console.log(this.data);

    }, error => {
      this.errorService.handleError("Não foi possível obter as ativações", error);
    })
    this.installationService.getNovigestSliders().subscribe(res => {
      this.allNg = <InstallationAndNovigestSlider[]>res;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as ativações", error);
    })
  }

  select(row) {
    this.selectedNg = row.novigest;
    this.selectedInstallation = row.installation;
  }

  seeInstallation() {
    this.displaySeeInstallation = true;
  }
  seeNovigest() {
    this.displaySeeNg = true;
  }
  addInstallationNgActivation() {
    this.newNg = new NovigestSliderDb("", "", "", "", "", "", "", "", "", 0, 0, null, "", null, 0, 0, null, null, null, null, null, null, "");
    this.displayAddNewNg = true;
  }
  editInstallation() {
    this.displayEditInstallation = true;
  }
  editNgSlider() {
    this.displayEditNg = true;
  }

  hide(event) {
    console.log("onHide")
    console.log(this.selectedNg)
    this.selectedInstallation = undefined;
    this.selectedNg = undefined;
    this.displaySeeInstallation = false;
    this.displayEditInstallation = false;
    this.displaySeeNg = false;
    this.displayAddNewNg = false;
    this.displayEditNg = false;
    this.newNg = undefined;
    console.log(this.selectedNg)
  }

  saveNewNgActivation() {
    if (this.selectedInstallationToAdd == null || this.selectedInstallationToAdd == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: "Tem de escolher uma instalação para ativar o novigest slider",
        sticky: false,
        life: 2000
      });
      return;
    }
    console.log(this.selectedInstallationToAdd)
    this.installationService.addNovigestToInstallation(this.selectedInstallationToAdd.installationId, this.newNg).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "Foi adicionada uma ativação à instalação selecionada",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.displayAddNewNg = false;
        this.newNg = new NovigestSliderDb("", "", "", "", "", "", "", "", "", 0, 0, null, "", null, 0, 0, null, null, null, null, null, null, "");
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível adicionar uma nova ativação", error);
    })
  }

  saveEditInstallation() {
    this.installationService.updateInstallation(this.selectedInstallation.installationId, this.selectedInstallation).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "Os dados da instalação foram guardados.",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.displayEditInstallation = false;
        this.selectedNg = undefined;
        this.selectedInstallation = undefined;
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível guardar as alterações da instalação", error);
    })
  }

  saveEditNg() {
    this.installationService.addNovigestToInstallation(this.selectedInstallation.installationId, this.selectedNg).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "A ativação foi editada",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.displayEditNg = false;
        this.selectedNg = undefined;
        this.selectedInstallation = undefined;
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível guardar a edição da ativação", error);
    })
  }

  deleteNgSlider() {
    this.confirmationService.confirm({
      message: 'Tem a certeza que quer apagar?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.forceDeleteNg();
      }
    });
  }

  forceDeleteNg() {
    this.installationService.deleteNovigestActivation(this.selectedInstallation.installationId, this.selectedNg.id).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "A ativação foi apagada.",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.selectedInstallation = undefined;
        this.selectedNg = undefined;
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível eliminar a ativação", error);
    })
  }

  back() {
    let route: string = this.previousRouteService.getPreviousUrl();
    let now: string = this.router.url;
    if (route == null || route == undefined) {
      this.router.navigate(['home']);
    } else if (route == now) {
      this.router.navigate(['home']);
    }
    else {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    }

  }

  onUpload(event) {
    this.newNg = <NovigestSliderDb>event.originalEvent.body;
  }

  doAtivarAlterations(){
    this.messageService.add({
      severity: 'warn',
      summary: 'Atenção',
      detail: "Ups. ainda não está implementado. é suposto alterar o estado da ativação dos selecionados (ativado -> desativado ou desativado-> ativado)",
      sticky: false,
      life: 4000
    });
  }

}
