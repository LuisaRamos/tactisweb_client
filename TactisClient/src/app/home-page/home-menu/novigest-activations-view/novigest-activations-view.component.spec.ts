import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovigestActivationsViewComponent } from './novigest-activations-view.component';

describe('NovigestActivationsViewComponent', () => {
  let component: NovigestActivationsViewComponent;
  let fixture: ComponentFixture<NovigestActivationsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovigestActivationsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovigestActivationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
