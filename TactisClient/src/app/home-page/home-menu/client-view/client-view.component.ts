import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Client } from 'src/app/model/client';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-client-view',
  templateUrl: './client-view.component.html',
  styleUrls: ['./client-view.component.css']
})
export class ClientViewComponent implements OnInit {

  //------- user logged
  active: string;
  isComercial: boolean = false;
  isHelpdesk: boolean = false;
  isImplementation: boolean = false;
  isAdministrative: boolean = false;

  client_postcode: string = "";
  notesRed: boolean = false;
  
  knockDialogVisible: boolean = false;

  @Input() client:Client;

  @Output() notesToRed: EventEmitter<boolean> = new EventEmitter<boolean>();
  

  constructor(private authService:AuthService) { }

  ngOnInit() {
    this.getLoggedUser();
  }

  getLoggedUser() {
    this.active = this.authService.getRole();
    if (this.active == "ADMINISTRATIVE") {
      this.isAdministrative = true;
    } else if (this.active == "HELPDESK") {
      this.isHelpdesk = true;
    } else if (this.active == "COMERCIAL") {
      this.isComercial = true;
    } else if (this.active == "IMPLEMENTATION") {
      this.isImplementation = true;
    } else {
      this.isAdministrative = true;
      this.isHelpdesk = true;
      this.isComercial = true;
      this.isImplementation = true;
    }
  }
  
  checkClientNotes(){
    if (this.client.notes != null && this.client.notes != undefined && this.client.notes.trim().length != 0) {
      this.knockDialogVisible = true;
      this.notesRed = true;
    } else {
      this.notesRed = false;
    }
    this.tellParent();
  }

  tellParent(){
    this.notesToRed.emit(this.notesRed);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.client_postcode = this.client.address.postcode1 + " - " + this.client.address.postcode2;
    this.checkClientNotes();
  }

}
