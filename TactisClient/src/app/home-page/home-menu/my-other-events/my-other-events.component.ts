import { Component, OnInit } from '@angular/core';
import { EventAndUsers } from 'src/app/util/view/eventAndUsers';
import { Event} from 'src/app/model/event';
import { TactisUser } from 'src/app/model/tactisuser';
import { InstallationService } from 'src/app/services/installation.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService } from 'primeng/api';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-my-other-events',
  templateUrl: './my-other-events.component.html',
  styleUrls: ['./my-other-events.component.css']
})
export class MyOtherEventsComponent implements OnInit {

  data: EventAndUsers[];
  activeUser: TactisUser;
  afterCalculated: boolean = false;
  display: boolean = false;
  selectedEvent: Event;
  newInstDate: Date = new Date();

  dataLoaded:boolean = false;
  loading: boolean = true;

  constructor(
    private userService: UserService,
    private router: Router,
    private calendarService: CalendarEventsService,
    private auth: AuthService,
    private messageService: MessageService,
    private errorService:HandleErrorComponent
  ) {
  }

  ngOnInit() {
    this.getActiveUser();
  }

  getActiveUser() {
    let user = this.auth.getActiveUser();
    this.userService.getTactisUserByUsername(user).subscribe(res => {
      this.activeUser = <TactisUser>res;
      this.getOtherEvents();
      this.dataLoaded=true;
    }, error => {
      this.errorService.handleError("Não foi possível obter o utilizador.", error);
    });
  }

  getOtherEvents() {
    if (this.activeUser.userRoleId.userRole == 'ADMIN') {
      this.getAllNotDoneOtherEvents();
    } else {
      this.getOtherEventsOfUser(this.activeUser.tactisUserId);
    }
  }

  getAllNotDoneOtherEvents() {
    this.calendarService.getAllOtherEventsAssignedAndNotDone().subscribe(data => {
      this.handleData(data);
    }, error => {
      this.errorService.handleError("Não foi possível obter os eventos", error);
    });
  }

  getOtherEventsOfUser(id: number) {
    this.calendarService.getAllOtherEventsAssignedAndNotDoneToUser(id).subscribe(data => {
      this.handleData(data);
    }, error => {
      this.errorService.handleError("Não foi possível obter os eventos.", error);
    });
  }

  handleData(data){
    this.data = <EventAndUsers[]>data;
    this.loading = false;
  }

  todayInstall(date: Date) {
    let today = new Date();
    let instDate = new Date(date);

    //se a data for a mesma
    if (today.getUTCFullYear() == instDate.getUTCFullYear() && today.getMonth() == instDate.getMonth() && today.getDate() == instDate.getDate()) {
      return 1;
    }
    //se a data for de amanha
    if (today.getUTCFullYear() == instDate.getUTCFullYear() && today.getMonth() == instDate.getMonth() && today.getDate() == instDate.getDate() - 1) {
      return 2;
    }
    if (today > instDate) {
      return 3;
    }
    return 4;
  }

  install(e: Event) {
    e.done = true;
    this.calendarService.updateOtherEventDone(e.eventId, e).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'O evento foi dado como concluido.',
        sticky: false,
        life: 1000
      })
      this.display = false;
      this.getOtherEvents();

    }, error => {
      this.errorService.handleError("Não foi possível guardar as alterações.", error);
    })
  }

  editSchedule(e: Event) {
    this.display = true;
    this.selectedEvent = e;
    this.newInstDate = new Date(e.beginDate);
  }

  back() {
    this.router.navigate(['home']);
  }
}
