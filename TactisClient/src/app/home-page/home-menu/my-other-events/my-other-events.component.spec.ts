import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyOtherEventsComponent } from './my-other-events.component';

describe('MyOtherEventsComponent', () => {
  let component: MyOtherEventsComponent;
  let fixture: ComponentFixture<MyOtherEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyOtherEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyOtherEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
