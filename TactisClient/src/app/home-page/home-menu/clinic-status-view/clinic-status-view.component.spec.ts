import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicStatusViewComponent } from './clinic-status-view.component';

describe('ClinicStatusViewComponent', () => {
  let component: ClinicStatusViewComponent;
  let fixture: ComponentFixture<ClinicStatusViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicStatusViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicStatusViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
