import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClinicService } from 'src/app/services/clinic.service';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Clinic } from 'src/app/model/clinic';
import { ContractChanges } from 'src/app/model/contractchanges';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ClientContractDividaView } from 'src/app/util/view/ClientContractDividaView';
import { INPUTMASK_VALUE_ACCESSOR } from 'primeng/inputmask';

@Component({
  selector: 'app-clinic-status-view',
  templateUrl: './clinic-status-view.component.html',
  styleUrls: ['./clinic-status-view.component.css']
})
export class ClinicStatusViewComponent implements OnInit {


  today:Date=new Date();

  @Input() contractchange:ContractChanges;
  @Input() user:string;
  @Input() detail:ClientContractDividaView;
  @Input() clinic:Clinic;

  cmdocate:Date 

  constructor(
  ) { }

  ngOnInit(): void {
    if(this.detail.divida != undefined){
      this.cmdocate = new Date(this.detail.divida.cmdocate);
    }
    
  }

  tellParent(){
  }
}