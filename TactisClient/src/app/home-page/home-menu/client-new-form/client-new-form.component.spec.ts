import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientNewFormComponent } from './client-new-form.component';

describe('ClientNewFormComponent', () => {
  let component: ClientNewFormComponent;
  let fixture: ComponentFixture<ClientNewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientNewFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientNewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
