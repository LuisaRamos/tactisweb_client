import { Component, OnInit, Input } from '@angular/core';
import { Client } from 'src/app/model/client';
import { ClientService } from 'src/app/services/client.service';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-client-new-form',
  templateUrl: './client-new-form.component.html',
  styleUrls: ['./client-new-form.component.css']
})
export class ClientNewFormComponent implements OnInit {

  @Input() client:Client;

  iconNAME: string = "pi pi-question";
  iconNIF: string = "pi pi-question";
  hexColorNAME: string = "grey";
  hexColorNIF: string = "grey";
  numbReg: RegExp = /[0-9]+$/;
  motivos:SelectItem[] = [];
  
  blockSpecial : RegExp = /^[^<>$%&(),*!^\s]+$/ 
  constructor(
    private clientService:ClientService
  ) { }

  ngOnInit() {
    this.getContactReasons();
  }
  
  getContactReasons(){
    this.clientService.getClientContactReasons().subscribe(res => {
      let reasons:string[] = <string[]> res;
      reasons.forEach(element => {
        this.motivos.push({label:element, value:element});
      });
    })
  }

  checkName() {
    if (this.client.name == undefined) {
      return;
    }
    if (this.client.name == "") {
      return;
    }
    if (this.client.name.trim().length == 0) {
      return;
    }
    this.clientService.checkIfNameExists(this.client.name).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result == true) {

        this.iconNAME = "pi pi-times";
        this.hexColorNAME = "red";
      }
      else {
        this.iconNAME = "pi pi-check";
        this.hexColorNAME = "green";
      }
    })
  }

  clearName() {
    this.hexColorNAME = "grey";
    this.iconNAME = "pi pi-question";
  }

  clearNIF() {
    this.hexColorNIF = "grey";
    this.iconNIF = "pi pi-question";
  }

  checkNIF() {
    if (this.client.nif == undefined) {
      return;
    }
    if (this.client.nif == "") {
      return;
    }
    if (this.client.nif.trim().length == 0) {
      return;
    }

    this.clientService.checkIfNIFExists(this.client.nif).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result == true) {
        this.iconNIF = "pi pi-times";
        this.hexColorNIF = "red";
      }
      else {
        this.iconNIF = "pi pi-check";
        this.hexColorNIF = "green";
      }
    })
  }


}
