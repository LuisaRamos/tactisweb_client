import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertsSeeComponent } from './alerts-see.component';

describe('AlertsSeeComponent', () => {
  let component: AlertsSeeComponent;
  let fixture: ComponentFixture<AlertsSeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertsSeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertsSeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
