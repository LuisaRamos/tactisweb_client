import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { InstallationService } from 'src/app/services/installation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService, MenuItem, ConfirmationService } from 'primeng/api';
import { Installation } from 'src/app/model/installation';
import { Client } from 'src/app/model/client';
import { Event } from 'src/app/model/event';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ClientViewComponent } from '../client-view/client-view.component';
import { ClinicsCardViewComponent } from '../clinics-card-view/clinics-card-view.component';
import { PemActivationNewComponent } from '../pem-activation-new/pem-activation-new.component';
import { PemSliderDb } from 'src/app/model/pemSliderDb';
import { EditInstallationComponent } from '../edit-installation/edit-installation.component';
import { NovigestSliderDb } from 'src/app/model/novigestSliderDb';
import xml2js from 'xml2js';
import { HttpHeaders } from '@angular/common/http';
import { ContractService } from 'src/app/services/contract.service';
import { Contract } from 'src/app/model/contract';
import { SourceListMap } from 'source-list-map';
@Component({
  selector: 'app-installation-update',
  templateUrl: './installation-update.component.html',
  styleUrls: ['./installation-update.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [ConfirmationService]
})
export class InstallationUpdateComponent implements OnInit {

  installation: Installation;
  contract: Contract;
  contractLoaded: boolean = false;
  loading: boolean = true;
  items: MenuItem[];
  activeIndex: number = 0;
  id: number;
  cli: Client;
  clientLoaded: boolean = false;
  selectedPrograms: string[] = [];

  openHelp: boolean = false;

  oldName: string;
  event: Event;
  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    'X-API-TOKEN': `${this.authSrv.getToken()}`,
  });
  registado: boolean = false;
  pemobj: PemSliderDb = new PemSliderDb(0, "", "", null, null, false);
  ngobj: NovigestSliderDb = new NovigestSliderDb("", "", "", "", "", "", "", "", "", null, null, null, "", null, null, null, null, null, null, null, null, null, "");
  //xmlURL: string = "http://localhost:4568/protected/installations/xml/ng"
  xmlURL: string = "https://tactiscloud.no-ip.net:4568/protected/installations/xml/ng"
  ngId: number = null;
  @ViewChild("client") client: ClientViewComponent;
  @ViewChild("cliniclst") clinicList: ClinicsCardViewComponent;
  @ViewChild("pem") pem: PemActivationNewComponent;
  @ViewChild("editinst") editinst: EditInstallationComponent;

  constructor(
    private installationService: InstallationService,
    private route: ActivatedRoute,
    private calendarService: CalendarEventsService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private authSrv: AuthService,
    private errorService: HandleErrorComponent) { }

  ngOnInit() {
    this.loading = true;
    this.items = [
      {
        label: 'Contrato',
        command: (event: any) => {
          this.activeIndex = 0;
        }
      },
      {
        label: 'Cliente',
        command: (event: any) => {
          this.activeIndex = 1;
        }
      },
      {
        label: 'Clínicas',
        command: (event: any) => {
          this.activeIndex = 2;
        }
      },
      {
        label: 'Dados',
        command: (event: any) => {
          this.activeIndex = 3;
        }
      },
      {
        label: 'PEM',
        command: (event: any) => {
          this.activeIndex = 4;
        }
      },
      {
        label: 'NG',
        command: (event: any) => {
          this.activeIndex = 5;
        }
      }];
    this.getInstallationInformation();
  }

  contains(type: string): boolean {
    this.selectedPrograms.forEach(element => {
      if (element == type) {
        console.log("return true")
        return true;
      }
    });
    console.log("return false")
    return false;
  }

  getInstallationInformation() {
    this.route.params.subscribe(res => {
      this.id = <number>res.id;
    });
    this.installationService.getInstallation(this.id).subscribe(res => {
      this.installation = <Installation>res;
      if (this.installation.installpem == true) {
        this.pemobj = this.installation.pemSliderDbList[0];
      }
      if (this.installation.installng == true) {
        console.log(this.installation.novigestSliderDbList.length);
        this.ngobj = this.installation.novigestSliderDbList[0];
        this.ngId = this.installation.novigestSliderDbList[0].id;
      }
      this.getInstallationClient();
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter a instalação", error);
    });
    this.installationService.getInstallation(this.id).subscribe(res => {
      let i: Installation = <Installation>res;
      this.oldName = i.installationName;

      this.calendarService.getEventByDescription(i.description).subscribe(res => {
        this.event = <Event>res;
      }, error => {
        this.errorService.handleError("Não foi possível obter o evento.", error);
      });
    });
  }

  getInstallationClient() {
    this.installationService.getInstallationClient(this.installation.installationId).subscribe(res => {
      this.cli = <Client>res;
      this.clientLoaded = true;
    })
    this.installationService.getInstallationContract(this.installation.installationId).subscribe(res => {
      this.contract = <Contract>res;
      this.contractLoaded = true;
    })
  }

  back() {
    this.router.navigate(["home/schedule/installations"]);
  }

  checkPemSlider(): boolean {
    if (this.pemobj.codreceitas.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Cod Receitas'");
      return false;
    }
    if (this.pemobj.localprescricao.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Local Prescrição'");
      return false;
    }
    // if (this.pemobj.activacaoinicio == undefined || this.pemobj.activacaoinicio == null) {
    //   this.showInputError("Deve preenchar o campo 'Ativação Início'");
    //   return false;
    // }
    // if (this.pemobj.activacaofim == undefined || this.pemobj.activacaofim == null) {
    //   this.showInputError("Deve preenchar o campo 'Ativação Fim'");
    //   return false;
    // }
    if (this.pemobj.maxusers == 0 || this.pemobj.maxusers == null) {
      this.showInputError("'Max users' deve ser superior a 0");
      return false;
    }
  }

  checkNgSlider(): boolean {
    if (this.ngobj.nomea.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nomea'");
      return false;
    }
    if (this.ngobj.nomeb.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nomeb'");
      return false;
    }
    if (this.ngobj.nomec.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nomec'");
      return false;
    }
    if (this.ngobj.nomed.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nomed'");
      return false;
    }
    if (this.ngobj.nifa.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nifa'");
      return false;
    }
    if (this.ngobj.nifb.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nifb'");
      return false;
    }
    if (this.ngobj.nifc.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nifc'");
      return false;
    }
    if (this.ngobj.nifd.trim().length < 1) {
      this.showInputError("Deve preenchar o campo 'Nifd'");
      return false;
    }
  }

  checkInstallationThings(): boolean {
    if (this.installation.installationName.trim().length == 0) {
      this.showInputError("Tem de escrever um nome para a instalação");
      return false;
    }

    if (this.installation.password.trim().length == 0) {
      this.showInputError("Tem de definir uma password para a instalação");
      return false;
    }

    // if(!this.checkInstallationName()){
    //   this.showInputError("O nome de instalação já existe. Deve escolher outro.");
    //   return false;
    // }
  }

  checkInstallationName(): boolean {
    let nameExists = false;
    if (this.installation.installationName.trim().length == 0) {
      console.log("length é 0")
      nameExists = true;
      return nameExists;
    }
    if (this.installation.installationName == undefined || this.installation.installationName == null) {
      nameExists = true;
      console.log("é undefined ou null")
      return nameExists;
    }
    this.installationService.checkInstallationNameExists(this.installation.installationName, this.installation.installationId).subscribe(res => {
      console.log("service res é : " + res)
      if (res == true) {
        console.log("service res é true")
        nameExists = true;
      }
      console.log("name exists: " + nameExists)
      return nameExists;
    })

  }

  save() {
    let pemOK: boolean = true;
    let ngOK: boolean = true;
    let instOK: boolean = true;
    if (this.installation.installpem) {
      if (this.pemobj.codreceitas.trim().length < 1) {
        pemOK = false;
      }
      if (this.pemobj.localprescricao.trim().length < 1) {
        pemOK = false;
      }
      if (this.pemobj.maxusers == 0 || this.pemobj.maxusers == null) {
        pemOK = false;
      }
    }

    if (this.installation.installng) {

      if (this.ngobj.nomea.trim().length < 1) {
        ngOK = false;
      }
      if (this.ngobj.nomeb.trim().length < 1) {
        ngOK = false;
      }
      if (this.ngobj.nomec.trim().length < 1) {
        ngOK = false;
      }
      if (this.ngobj.nomed.trim().length < 1) {
        ngOK = false;
      }
      if (this.ngobj.nifa.trim().length < 1) {
        ngOK = false;
      }
      if (this.ngobj.nifb.trim().length < 1) {
        ngOK = false;
      }
      if (this.ngobj.nifc.trim().length < 1) {
        ngOK = false;
      }
      if (this.ngobj.nifd.trim().length < 1) {
        ngOK = false;
      }
    }

    if (this.installation.installationName.trim().length == 0) {
      instOK = false;
    }

    if (this.installation.password.trim().length == 0) {
      instOK = false;
    }

    if (!ngOK || !pemOK || !instOK) {
      this.confirmationService.confirm({
        message: 'Alguns dados não estão preenchidos. Pode terminar de preenchê-los agora ou mais tarde.',
        header: 'Confirmação',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          //quero apenas guardar
          if (this.installation.installpem) {
            this.installation.pemSliderDbList.push(this.pemobj);
          }
          if (this.installation.installng) {
            this.installation.novigestSliderDbList.push(this.ngobj);
          }

          let notok = false;
          if (this.installation.installationName.trim().length == 0) {
            notok = true;
          }
          if (this.installation.installationName == undefined || this.installation.installationName == null) {
            notok = true;
          }
          if (!notok) {
            this.installationService.checkInstallationNameExists(this.installation.installationName, this.installation.installationId).subscribe(res => {
              if (res == true) {
                this.messageService.add({
                  severity: 'warn',
                  summary: 'Atenção',
                  detail: 'O login da instalação já existe. Escolha outro antes de guardar.',
                  sticky: false,
                  life: 2500
                });
              } else {
                this.installationService.updateInstallation(this.installation.installationId, this.installation).subscribe(res => {

                  this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'A instalação foi guardada.',
                    sticky: false,
                    life: 1500
                  });
                  setTimeout(res => {
                    this.back();
                  }, 1500);

                })
              }
            })
          }

        },
        reject: () => {
          // quero concluir
          this.saveAndReady();
        }
      });
    } else {
      this.saveAndReady();
    }

  }

  saveAndReady() {
    if (this.installation.installpem) {
      if (this.checkPemSlider() == false) {
        return;
      }
    }

    if (this.installation.installng) {
      if (this.checkNgSlider() == false) {
        return;
      }
    }

    if (this.checkInstallationThings() == false) {
      return;
    }

    this.installation.ready = true;
    //this.installation.done = true;
    if (this.installation.installpem) {
      this.installation.pemSliderDbList.push(this.pemobj);
    }
    if (this.installation.installng) {
      this.installation.novigestSliderDbList.push(this.ngobj);
    }

    let notok = false;
    if (this.installation.installationName.trim().length == 0) {
      notok = true;
    }
    if (this.installation.installationName == undefined || this.installation.installationName == null) {
      notok = true;
    }
    if (!notok) {
      this.installationService.checkInstallationNameExists(this.installation.installationName, this.installation.installationId).subscribe(res => {
        if (res == true) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Atenção',
            detail: 'O login da instalação já existe. Escolha outro antes de guardar.',
            sticky: false,
            life: 2500
          });
        } else {
          this.installationService.updateInstallation(this.installation.installationId, this.installation).subscribe(res => {

            // this.event.done = true;
            // this.calendarService.updateEvent(this.event.eventId, this.event).subscribe(res => {
            this.messageService.add({
              severity: 'success',
              summary: 'Sucesso',
              detail: 'A instalação foi registada.',
              sticky: false,
              life: 1500
            });
            setTimeout(res => {
              this.back();
            }, 1500);
            // }, error => {
            //   this.errorService.handleError("Não foi possível guardar as novas informações.", error);
            // });
          });
        }
      });
    }
  }

  showInputError(mensagem: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Atenção',
      detail: mensagem,
      sticky: false,
      life: 1500
    });
  }

  onUpload(event) {
    this.ngobj = <NovigestSliderDb>event.originalEvent.body;
    this.ngobj.id = this.ngId;
  }
}
