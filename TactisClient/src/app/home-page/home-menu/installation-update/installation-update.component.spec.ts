import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstallationUpdateComponent } from './installation-update.component';

describe('InstallationUpdateComponent', () => {
  let component: InstallationUpdateComponent;
  let fixture: ComponentFixture<InstallationUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstallationUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstallationUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
