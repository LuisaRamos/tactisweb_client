import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Client } from 'src/app/model/client';
import { QuickSearchMultipleResult } from 'src/app/util/view/quickSearchMultipleResult';
import { ClientService } from 'src/app/services/client.service';
import { MessageService, SelectItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { ClinicService } from 'src/app/services/clinic.service';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';
import { Clinic } from 'src/app/model/clinic';
import { Router } from '@angular/router';
import { ClinicInstallationAccess } from 'src/app/util/view/ClinicInstallationAccess';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})
export class ClientEditComponent implements OnInit {

  //------- user logged
  active: string;
  isComercial: boolean = false;
  isHelpdesk: boolean = false;
  isImplementation: boolean = false;
  isAdministrative: boolean = false;

  motivos:SelectItem[] = [];
  selectedMotivo:string ;
  
  loading: boolean = true;
  fechou: boolean = false;

  force: boolean = false;

  iconNAME: string = "pi pi-question";
  hexColorNAME: string = "grey";
  numbReg: RegExp = /[0-9]+$/;
  installationReady: boolean = false;
  blockSpecial : RegExp = /^[^<>$%&(),*!^\s]+$/ 
  // ---------- INPUT
  updateClient: boolean = false;
  updateClinic: boolean = false;
  updateInstallation: boolean = false;

  @Input() client: Client;
  @Input() clinic: Clinic;
  @Input() installation: Installation;
  @Input() updateAccess: boolean;

  @Output() closeEdit: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private clientService: ClientService,
    private auth: AuthService,
    private router: Router,
    private previousRouteService: PreviousRouteService,
    private messageService: MessageService,
    private installationService: InstallationService,
    private errorService: HandleErrorComponent,
    private clinicService: ClinicService,
    private authService:AuthService) { }

  ngOnInit() {
    if (this.client != null && this.client != undefined) {
      this.updateClient = true;
    }
    if (this.installation != null && this.installation != undefined) {
      this.updateInstallation = true;
    }
    if (this.clinic != null && this.clinic != undefined) {
      this.updateClinic = true;
    }

    if(this.updateAccess){
      this.updateClinic = false;
      this.updateInstallation = false;
    }
    this.getLoggedUser();
    if(this.updateClient){
      this.getContactReasons();
    }
 
  }

  getContactReasons(){
    this.clientService.getClientContactReasons().subscribe(res => {
      let reasons:string[] = <string[]> res;
      reasons.forEach(element => {
        this.motivos.push({label:element, value:element});
      });
      this.selectedMotivo = this.client.motivo;
    })
  }

  getLoggedUser() {
    this.active = this.authService.getRole();
    if (this.active == "ADMINISTRATIVE") {
      this.isAdministrative = true;
    } else if (this.active == "HELPDESK") {
      this.isHelpdesk = true;
    } else if (this.active == "COMERCIAL") {
      this.isComercial = true;
    } else if (this.active == "IMPLEMENTATION") {
      this.isImplementation = true;
    } else {
      this.isAdministrative = true;
      this.isHelpdesk = true;
      this.isComercial = true;
      this.isImplementation = true;
    }
  }

  validField(field: string, mandatory: boolean): boolean {
    if (mandatory && this.nullOrUndefinedField(field)) {
      //exemplo '', true
      return false;
    } else if (!mandatory && this.nullOrUndefinedField(field)) {
      //exemplo '', false
      return true;
    } else {
      return field.trim().length > 0;
    }
  }

  private nullOrUndefinedField(field: string) {
    if (field == null || field == undefined) {
      return true;
    }
    return false;
  }

  checkClientFields() {
    if (!this.validField(this.client.name, true)) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher o nome do cliente',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return false;
    }

    if (!(this.validField(this.client.contact.email, true) || this.validField(this.client.contact.phone1, true))) {
      // mensagem preencher contacto ou email
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher um contacto ou email do cliente',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!(this.validField(this.client.address.address, true) || this.validField(this.client.address.addressLocation, true))) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher a morada ou localização do cliente',
        sticky: false,
        life: 2000
      });
      // mensagem preencher morada ou localização
      return false;
    }
    return true;
  }

  checkClinicFields(): boolean {
    if (!this.validField(this.clinic.name, true)) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher o nome da clínica',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return false;
    }

    if (!(this.validField(this.clinic.contact.email, true) || this.validField(this.clinic.contact.phone1, true))) {
      // mensagem preencher contacto ou email
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher um contacto ou email da clínica',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!(this.validField(this.clinic.address.address, true) || this.validField(this.clinic.address.addressLocation, true))) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher a morada ou localização da clínica',
        sticky: false,
        life: 2000
      });
      // mensagem preencher morada ou localização
      return false;
    }
    return true;
  }

  save() {
    if (this.updateClient && !this.updateClinic) {
      console.log("vai fazer updateClient !updateClinic")
      this.doUpdateClient();
      return;
    }

    if (this.updateInstallation && !this.updateClinic) {
      this.doUpdateInstallation();
      console.log("vai fazer updateInstallation !updateClinic")
      return;
    }

    if (!this.updateClient && this.updateClinic) {
      this.doUpdateClinic();
      console.log("vai fazer updateClinic")
      return;
    }

    if (this.updateAccess) {
      console.log("vai fazer updateAccess")
      this.doUpdateClinic();
      //this.doUpdateInstallation();
      return;
    }
  }

  doUpdateClinic() {

    if (!this.checkClinicFields()) {
      return;
    }

    if (this.updateAccess) {
      console.log(this.clinic.accessData)
      console.log(this.installation.accessData)
      let access:ClinicInstallationAccess = new ClinicInstallationAccess(this.clinic.accessData, this.installation.accessData);
      console.log(access)
      this.clinicService.updateAccess(this.clinic.clinicId, this.installation.installationId, access).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Alterado com sucesso.',
          sticky: false,
          life: 2000
        });
        setTimeout(() => {
          this.closeEdit.emit(true);
          console.log("this is a known issue, please follow https://github.com/primefaces/primeng/issues/8044 for more details")
          return;
        }, 1000);
        return;
      }, error => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Não foi possível atualizar.',
          sticky: false,
          life: 1000
        });
      })
    } else {
      let result: QuickSearchMultipleResult = new QuickSearchMultipleResult(this.clinic, this.client, null);
      this.clinicService.updateClinic(this.clinic.clinicId, this.clinic).subscribe(res => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Alterado com sucesso.',
          sticky: false,
          life: 2000
        });
        setTimeout(() => {
          this.closeEdit.emit(true);
          // console.log("this is a known issue, please follow https://github.com/primefaces/primeng/issues/8044 for more details")
          return;
        }, 1000);
        return;
      }, error => {
        if (error.status == 401 /*&& error.error == "SessionExpired" or "Unauthorized"*/) {
          this.auth.setSessionExpired();
          this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: 'Sessão expirada. Faça novamente o login',
            sticky: false,
            life: 1000
          });
          setTimeout(() => {
            this.auth.logout();
            this.router.navigate(["/login"]);
          }, 1000);
        } else if (error.status == 400) {

          // console.log('reparou que o cliente já existe e vai unir as fichas');
          this.clientService.forceUpdateClinicOfClient(result.clientId.clientId, result).subscribe(res => {
            this.force = true;
            this.messageService.add({
              severity: 'success',
              summary: 'Sucesso',
              detail: 'Reparou que o cliente já existe e vai uniu as fichas.',
              sticky: false,
              life: 2000
            });
          });

        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: 'Não foi possível atualizar.',
            sticky: false,
            life: 1000
          });
        }
      });
    }
  }

  doUpdateInstallation() {

    // if (!this.checkInstallationFiels()) {
    //   return;
    // }

    this.installationService.updateInstallation(this.installation.installationId, this.installation).subscribe(res => {
      this.messageService.add({ severity: 'success', summary: 'Sucesso', detail: 'Alterado com sucesso.', sticky: false, life: 2000 });
      setTimeout(() => {
        this.closeEdit.emit(true);
        return;
      }, 1000);
    }, error => {
      this.errorService.handleError("Não foi possivel guardar a instalação", error);
    })

  }

  doUpdateClient() {
    if (!this.checkClientFields()) {
      return;
    }
    this.client.motivo = this.selectedMotivo;
    this.clientService.updateClient(this.client.clientId, this.client).subscribe(res => {
      this.messageService.add({ severity: 'success', summary: 'Sucesso', detail: 'Alterado com sucesso.', sticky: false, life: 2000 });
      setTimeout(() => {
        this.closeEdit.emit(true);
        return;
      }, 1000);
    }, error => {
      this.errorService.handleError("Não foi possivel guardar a instalação", error);
    });
  }

  checkName() {
    this.installationService.checkInstallationNameExists(this.installation.installationName, this.installation.installationId).subscribe(res => {
      if (res == true) {
        this.iconNAME = "pi pi-times";
        this.hexColorNAME = "red";
      }
      else {
        this.iconNAME = "pi pi-check";
        this.hexColorNAME = "green";
      }
    })
  }

  clearName() {
    this.hexColorNAME = "grey";
    this.iconNAME = "pi pi-question";
  }

  back() {
    // let prevUrl = this.previousRouteService.getPreviousUrl();
    // if (prevUrl == null || prevUrl == undefined) {
    //   this.router.navigate(["home/byclient"]);
    //   return;
    // }
    // if (prevUrl == this.router.url) {
    //   this.router.navigate(["home/byclient"]);
    //   return;
    // } else if (this.force) {
    //   this.router.navigate(['home/byclient']);
    //   return;
    // }
    // this.router.navigate([prevUrl]);
    this.closeEdit.emit(true);
  }
}