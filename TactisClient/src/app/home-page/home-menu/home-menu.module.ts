import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeMenuRoutingModule } from './home-menu-routing.module';
import { HomeCardsComponent } from './home-cards/home-cards.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MenubarModule } from 'primeng/menubar';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { InputMaskModule } from 'primeng/inputmask';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PasswordModule } from 'primeng/password';
import { KeyFilterModule } from 'primeng/keyfilter';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { UserNewComponent } from './user-new/user-new.component';
import { ClientNewComponent } from './client-new/client-new.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { MessageService } from 'primeng/api';
import { DataViewModule } from 'primeng/dataview';
import { PanelModule } from 'primeng/panel';
import { ContractNewComponent } from './contract-new/contract-new.component';
import { CheckboxModule } from 'primeng/checkbox';
import { TooltipModule } from 'primeng/tooltip';
import { ClientEditComponent } from './client-edit/client-edit.component';
import { UsersComponent } from './users/users.component';
import { InstallationAssignmentComponent } from './installation-assignment/installation-assignment.component';
import { ChipsModule } from 'primeng/chips';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { CalendarComponent } from './calendar/calendar.component';
import { InstallationUpdateComponent } from './installation-update/installation-update.component';
import { UpdatesComponent } from './updates/updates.component';
import { SlideMenuModule } from 'primeng/slidemenu';
import { AccordionModule } from 'primeng/accordion';
import { ClientViewSearchComponent } from './client-view-search/client-view-search.component';
import { TreeTableModule } from 'primeng/treetable';
import { ListboxModule } from 'primeng/listbox';
import {PaginatorModule} from 'primeng/paginator';
import { StepsModule } from 'primeng/steps';
import {
  MatFormFieldModule,
  MatDatepickerModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatIconModule,
  MatInputModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatTooltipModule,
  MatToolbarModule,
} from '@angular/material';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { AngularDraggableModule } from 'angular2-draggable';
import { ClinicNewComponent } from './clinic-new/clinic-new.component';
import { MyInstallationsComponent } from './my-installations/my-installations.component';
import { SeeInstallationsComponent } from './see-installations/see-installations.component';
import { SeeTrainingsComponent } from './see-trainings/see-trainings.component';
import { MyTrainingsComponent } from './my-trainings/my-trainings.component';
import { InstallationsComponent } from './installations/installations.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { SeeOtherEventsComponent } from './see-other-events/see-other-events.component';
import { MyOtherEventsComponent } from './my-other-events/my-other-events.component';
import { ClientViewComponent } from './client-view/client-view.component';
import { ClinicViewComponent } from './clinic-view/clinic-view.component';
import { InterventionNewViewComponent } from './intervention-new-view/intervention-new-view.component';
import { InterventionsViewComponent } from './interventions-view/interventions-view.component';
import { NovigestTypeViewComponent } from './novigest-type-view/novigest-type-view.component';
import { AccessViewComponent } from './access-view/access-view.component';
import { ClinicStatusViewComponent } from './clinic-status-view/clinic-status-view.component';
import { ContractchangesViewComponent } from './contractchanges-view/contractchanges-view.component';
import { ClientStatusViewComponent } from './client-status-view/client-status-view.component';
import { HandleErrorComponent } from './handle-error/handle-error.component';
import { MonitorViewComponent } from './monitor-view/monitor-view.component';
import { NewUpgradeComponent } from './new-upgrade/new-upgrade.component';
import { UpgradesComponent } from './upgrades/upgrades.component';
import { UpgradesEditComponent } from './upgrades-edit/upgrades-edit.component';
import { ClinicsCardViewComponent } from './clinics-card-view/clinics-card-view.component';
import { EventResumeViewComponent } from './event-resume-view/event-resume-view.component';
import { PemActivationNewComponent } from './pem-activation-new/pem-activation-new.component';
import { PemActivationsViewComponent } from './pem-activations-view/pem-activations-view.component';
import { NovigestActivationsViewComponent } from './novigest-activations-view/novigest-activations-view.component';
import { EditInstallationComponent } from './edit-installation/edit-installation.component';
import { ViewHeaderComponent } from './view-header/view-header.component';
import { HeaderComponent } from './header/header.component';
import { SectionHeaderComponent } from './section-header/section-header.component';
import { NovigestActivationNewComponent } from './novigest-activation-new/novigest-activation-new.component';
import { DateTimeFormatPipe} from '../../util/DateTimeFormatPipe';
import { ClientNewFormComponent } from './client-new-form/client-new-form.component';
import { ContractNewFormComponent } from './contract-new-form/contract-new-form.component';
import { TrainingsViewComponent } from './trainings-view/trainings-view.component';
import { ContractsUnfinishedComponent } from './contracts-unfinished/contracts-unfinished.component';
import { ContractUnfinishedEditComponent } from './contract-unfinished-edit/contract-unfinished-edit.component';
import {FileUploadModule} from 'primeng/fileupload';
import { ClientDetailsV2Component } from './client-details-v2/client-details-v2.component';
import { ContractRenewEditComponent } from './contract-renew-edit/contract-renew-edit.component';
import { NewContractAssociationComponent } from './new-contract-association/new-contract-association.component';
import {RadioButtonModule} from 'primeng/radiobutton';
import { TrainingAssignmentFormComponent } from './training-assignment-form/training-assignment-form.component';
import { NovidashKpiGestComponent } from './novidash-kpi-gest/novidash-kpi-gest.component';
import { NovidashGraficosGestComponent } from './novidash-graficos-gest/novidash-graficos-gest.component';
import { AlertsSeeComponent } from './alerts-see/alerts-see.component';
import { ListagemCircularesSeeComponent } from './listagem-circulares-see/listagem-circulares-see.component';
import { ListagemClientesSeeComponent } from './listagem-clientes-see/listagem-clientes-see.component';
import { ListagemDividasSeeComponent } from './listagem-dividas-see/listagem-dividas-see.component';
import { AditamentoNewComponent } from './aditamento-new/aditamento-new.component';
import { AditamentoAssignmentComponent } from './aditamento-assignment/aditamento-assignment.component';
import { AditamentoUpdateComponent } from './aditamento-update/aditamento-update.component';
import { ListagemRenovationsSeeComponent } from './listagem-renovations-see/listagem-renovations-see.component';
import {TabViewModule} from 'primeng/tabview';
import { StatsDashboardComponent } from './statistics/stats-dashboard/stats-dashboard.component';
import { NumInterventionsUserComponent } from './statistics/graficos/num-interventions-user/num-interventions-user.component';
import { DurInterventionsUserComponent } from './statistics/graficos/dur-interventions-user/dur-interventions-user.component';
import { NumInterventionsActivityComponent } from './statistics/graficos/num-interventions-activity/num-interventions-activity.component';
import { DurInterventionsActivityComponent } from './statistics/graficos/dur-interventions-activity/dur-interventions-activity.component';
import { ChartModule } from 'primeng/chart';
import { IndicadoresComponent } from './statistics/indicadores/indicadores.component';
import { StatsView2DashboardComponent } from './statistics/stats-view2-dashboard/stats-view2-dashboard.component';
import { ContractAssociationComponent } from './contract-association/contract-association.component';
import { IndicadoresView2Component } from './statistics/indicadores-view2/indicadores-view2.component';
import { NumMonthInterventionsUserComponent } from './statistics/graficos/num-month-interventions-user/num-month-interventions-user.component';
import { NumMonthInterventionsTotalComponent } from './statistics/graficos/num-month-interventions-total/num-month-interventions-total.component';
import { NumDayInterventionsTotalComponent } from './statistics/graficos/num-day-interventions-total/num-day-interventions-total.component';
import { NumDayInterventionsUserComponent } from './statistics/graficos/num-day-interventions-user/num-day-interventions-user.component';
import { NumHourInterventionsTotalComponent } from './statistics/graficos/num-hour-interventions-total/num-hour-interventions-total.component';

@NgModule({
  declarations: [
    HomeCardsComponent,
    UserNewComponent,
    UserProfileComponent,
    ClientNewComponent,
    ContractNewComponent,
    ClientEditComponent,
    UsersComponent,
    InstallationAssignmentComponent,
    InstallationsComponent,
    CalendarComponent,
    InstallationUpdateComponent,
    UpdatesComponent,
    ClientViewSearchComponent,
    ClinicNewComponent,
    MyInstallationsComponent,
    SeeInstallationsComponent,
    SeeTrainingsComponent,
    MyTrainingsComponent,
    CreateEventComponent,
    SeeOtherEventsComponent,
    MyOtherEventsComponent,
    ClientViewComponent,
    ClinicViewComponent,
    InterventionNewViewComponent,
    InterventionsViewComponent,
    NovigestTypeViewComponent,
    AccessViewComponent,
    ClinicStatusViewComponent,
    ContractchangesViewComponent,
    ClientStatusViewComponent,
    HandleErrorComponent,
    MonitorViewComponent,
    NewUpgradeComponent,
    UpgradesComponent,
    UpgradesEditComponent,
    ClinicsCardViewComponent,
    EventResumeViewComponent,
    PemActivationNewComponent,
    PemActivationsViewComponent,
    NovigestActivationsViewComponent,
    EditInstallationComponent,
    HeaderComponent,
    ViewHeaderComponent,
    SectionHeaderComponent,
    NovigestActivationNewComponent,
    DateTimeFormatPipe,
    ClientNewFormComponent,
    ContractNewFormComponent,
    TrainingsViewComponent,
    ContractsUnfinishedComponent,
    ContractUnfinishedEditComponent,
    ClientDetailsV2Component,
    ContractRenewEditComponent,
    NewContractAssociationComponent,
    TrainingAssignmentFormComponent,
    NovidashKpiGestComponent,
    NovidashGraficosGestComponent,
    AlertsSeeComponent,
    ListagemCircularesSeeComponent,
    ListagemClientesSeeComponent,
    ListagemDividasSeeComponent,
    AditamentoNewComponent,
    AditamentoAssignmentComponent,
    AditamentoUpdateComponent,
    ListagemRenovationsSeeComponent,
    StatsDashboardComponent,
    NumInterventionsUserComponent,
    DurInterventionsUserComponent,
    NumInterventionsActivityComponent,
    DurInterventionsActivityComponent,
    IndicadoresComponent,
    StatsView2DashboardComponent,
    ContractAssociationComponent,
    IndicadoresView2Component,
    NumMonthInterventionsUserComponent,
    NumMonthInterventionsTotalComponent,
    NumDayInterventionsTotalComponent,
    NumDayInterventionsUserComponent,
    NumHourInterventionsTotalComponent
  ],
  imports: [
    CommonModule,
    HomeMenuRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    // routing,
    TableModule,
    ListboxModule,
    DropdownModule,
    EcoFabSpeedDialModule,
    AngularDraggableModule,
    TreeTableModule,
    MatButtonModule,
    MatIconModule,
    RadioButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatFormFieldModule,
    ChipsModule,
    MultiSelectModule,
    SlideMenuModule,
    TooltipModule,
    StepsModule,
    InputTextModule,
    TabViewModule,
    MatInputModule,
    MatRadioModule,
    PaginatorModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatToolbarModule,
    InputTextareaModule,
    FileUploadModule,
    CheckboxModule,
    ButtonModule,
    ConfirmDialogModule,
    MenubarModule,
    FormsModule,
    CommonModule,
    AccordionModule,
    DialogModule,
    MessagesModule,
    MessageModule,
    InputMaskModule,
    CalendarModule,
    FullCalendarModule,
    CardModule,
    ToastModule,
    ScrollPanelModule,
    PasswordModule,
    KeyFilterModule,
    DataViewModule,
    MatDatepickerModule,
    PanelModule,
    ProgressSpinnerModule,
    NgbModalModule,
  //  FontAwesomeModule,
    OwlDateTimeModule, 
         OwlNativeDateTimeModule,
    FlatpickrModule.forRoot(),
    ChartModule
  ],
  providers: [
    MessageService
  ]
})
export class HomeMenuModule { }
