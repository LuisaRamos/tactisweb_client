import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Clinic } from 'src/app/model/clinic';
import { Client } from 'src/app/model/client';
import { ClinicService } from 'src/app/services/clinic.service';

@Component({
  selector: 'app-clinic-view',
  templateUrl: './clinic-view.component.html',
  styleUrls: ['./clinic-view.component.css']
})
export class ClinicViewComponent implements OnInit {

  clinica_postcode: string;
  notesRed: boolean = false;
  noClient: boolean;

  clientReady: boolean = false;

  @Input() clinic: Clinic;
  client: Client;

  constructor(
    private clinicService: ClinicService
  ) { 
    
  }

  ngOnInit() {
    
  }

  checkClientNotes() {
    this.clinicService.getClientOfClinic(this.clinic.clinicId).subscribe(res => {
      this.client = <Client>res;
      if (this.client.notes != null && this.client.notes != undefined && this.client.notes.trim().length != 0) {
        this.notesRed = true;
      } else {
        this.notesRed = false;
      }
      this.clientReady = true;
    })

  }

  ngOnChanges(changes: SimpleChanges) {

    let newFocusedChallenge  = changes["clinic"].currentValue;
    this.clinica_postcode = this.clinic.address.postcode1 + " - " + this.clinic.address.postcode2;
    this.checkClientNotes();
  }

}
