import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ClinicService } from 'src/app/services/clinic.service';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { Contact } from 'src/app/model/contact';
import { Clinic } from 'src/app/model/clinic';
import { Address } from 'src/app/model/address';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MAT_DATE_FORMATS } from '@angular/material';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'L',
    monthYearA11yLabel:  'YYYY',
  },
};

@Component({
  selector: 'app-clinic-new',
  templateUrl: './clinic-new.component.html',
  styleUrls: ['./clinic-new.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'pt'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class ClinicNewComponent implements OnInit {

  @Input() clinic:Clinic;

  date = new FormControl(moment());
  isDefined:boolean = true;
  iconCLI: string = "pi pi-question";
  hexColorCLI: string = "grey";
  blockSpecial : RegExp = /^[^<>$%&(),*!^\s]+$/ 
  constructor(private clinicService:ClinicService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.clinic)
    if(this.clinic == undefined || this.clinic == null){
      this.isDefined =true ;
    } else {
      this.isDefined = false;
    }
  }

  // ---- check clinic name

  checkClinicName() {
    let name: string = this.clinic.name;
    if (name == undefined) {
      this.iconCLI = "pi pi-question";
      this.hexColorCLI = "grey";
      return;
    }
    if (name == "") {
      this.iconCLI = "pi pi-question";
      this.hexColorCLI = "grey";
      return;
    }
    if (name.trim().length == 0) {
      this.iconCLI = "pi pi-question";
      this.hexColorCLI = "grey";
      return;
    }
    this.clinicService.checkIfNameExists(name).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result == true) {

        this.iconCLI = "pi pi-times";
        this.hexColorCLI = "red";
      }
      else {
        this.iconCLI = "pi pi-check";
        this.hexColorCLI = "green";
      }
    })
  }

  clearBtnColor() {
    this.iconCLI = "pi pi-question";
    this.hexColorCLI = "grey";
  }

}
