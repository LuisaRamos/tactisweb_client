import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { EventAndUsers } from 'src/app/util/view/eventAndUsers';
import { ContractAndInstallation } from 'src/app/util/view/contractandinstallation';
import { NoviGestView } from 'src/app/model/novigestview';

@Component({
  selector: 'app-event-resume-view',
  templateUrl: './event-resume-view.component.html',
  styleUrls: ['./event-resume-view.component.css']
})
export class EventResumeViewComponent implements OnInit {

  nAtrasadas: number = 0;
  nHoje: number = 0;
  nAmanha: number = 0;
  afterCalculated:boolean = false;

  @Input() events:EventAndUsers[];
  @Input() installations:ContractAndInstallation[];

  constructor() { }

  ngOnInit() {
    
  }

  calculateResume() {
    this.nHoje = 0;
    this.nAmanha = 0;
    this.nAtrasadas = 0;
    let today = new Date();
    let eventDate:Date;

    if(this.events != undefined){
      this.events.forEach(element => {
        eventDate =  new Date(element.event.beginDate);
        this.calculate(today, eventDate);
      });
    } else if(this.installations != undefined){
      this.installations.forEach(element => {
        if(element.installation != undefined){
          eventDate =  new Date(element.installation.installationDate);
        } else {
          eventDate =  new Date(element.aditamento.data);
        }
        
        this.calculate(today, eventDate);
      });
    }
    this.afterCalculated = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.calculateResume();
  }

  private calculate(today:Date, date:Date){
    if (today.getUTCFullYear() == date.getUTCFullYear() && today.getMonth() == date.getMonth() && today.getDate() == date.getDate()) {
      this.nHoje += 1;
    } else if (today.getUTCFullYear() == date.getUTCFullYear() && today.getMonth() == date.getMonth() && today.getDate() == date.getDate() - 1) {
      this.nAmanha += 1;
    } else if (today > date) {
      this.nAtrasadas += 1;
    }
  }

}
