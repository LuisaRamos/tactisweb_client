import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventResumeViewComponent } from './event-resume-view.component';

describe('EventResumeViewComponent', () => {
  let component: EventResumeViewComponent;
  let fixture: ComponentFixture<EventResumeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventResumeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventResumeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
