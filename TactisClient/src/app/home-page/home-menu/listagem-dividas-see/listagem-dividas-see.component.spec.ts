import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemDividasSeeComponent } from './listagem-dividas-see.component';

describe('ListagemDividasSeeComponent', () => {
  let component: ListagemDividasSeeComponent;
  let fixture: ComponentFixture<ListagemDividasSeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemDividasSeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemDividasSeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
