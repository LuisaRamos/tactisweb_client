import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { TrainingView } from 'src/app/util/view/trainingview';
import { Clinic } from 'src/app/model/clinic';
import { TrainingService } from 'src/app/services/training.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { SortEvent } from 'primeng/api';

@Component({
  selector: 'app-trainings-view',
  templateUrl: './trainings-view.component.html',
  styleUrls: ['./trainings-view.component.css']
})
export class TrainingsViewComponent implements OnInit {

  cols: any[];
  trainings: TrainingView[] = [];
  dataLoaded: boolean = false;

  @Input() cid: number;

  @Input() redo:boolean;
  @Output() nrTrainings: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private trainingService: TrainingService,
    private errorService: HandleErrorComponent
  ) { }

  ngOnInit() {
    this.cols = [
      { field: 'training', header: 'Data', position: 1 },
      { field: 'users', header: 'Utilizador(es)', position: 2 }/*,*/
      //{ field: 'training', header: 'Notas', position: 3 }
    ];
    
  }

  ngOnChanges(changes:SimpleChanges){
    this.getTrainings();
  }

  getTrainings() {
    console.log("get trainings doing")
    this.trainingService.getTrainingOfClinic(this.cid).subscribe(res => {
      this.trainings = <TrainingView[]>res;
      this.dataLoaded = true;
      this.tellParent();
    }, error => {
      this.errorService.handleError("Não foi possível obter as formações", error);
    })
  }

  tellParent(){
    this.nrTrainings.emit(this.trainings.length);
  }

  onRowSelect(data){

  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null){
        result = -1;
      }else if (value1 != null && value2 == null){
        result = 1;
      }else if (value1 == null && value2 == null){
        result = 0;
      }else if (typeof value1 === 'string' && typeof value2 === 'string'){
        result = value1.localeCompare(value2);
      } else if (typeof value1 === 'object' && typeof value2 === 'object') {
        if(value1.trainingDate != null && value1.trainingId != undefined){
          result = value1.trainingDate.localeCompare(value2.trainingDate);
        }
      }else{
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      }
      return (event.order * result);
    });
  }
}
