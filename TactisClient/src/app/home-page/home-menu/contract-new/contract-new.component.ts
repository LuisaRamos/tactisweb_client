import { Component, OnInit, ViewChild } from '@angular/core';
import { Client } from 'src/app/model/client';
import { NoviGestView } from 'src/app/model/novigestview';
import { Address } from 'src/app/model/address';
import { Contact } from 'src/app/model/contact';
import { ClientService } from 'src/app/services/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Installation } from 'src/app/model/installation';
import { Clinic } from 'src/app/model/clinic';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Contract } from 'src/app/model/contract';
import { ContractService } from 'src/app/services/contract.service';
import { AuthService } from 'src/app/services/auth.service';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { ClinicService } from 'src/app/services/clinic.service';
import { ContractChanges } from 'src/app/model/contractchanges';
import { NewClientAndContract } from 'src/app/util/view/newclientandcontract';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ContractNewFormComponent } from '../contract-new-form/contract-new-form.component';

@Component({
  selector: 'app-contract-new',
  templateUrl: './contract-new.component.html',
  styleUrls: ['./contract-new.component.css'],
  providers: [ConfirmationService]
})
export class ContractNewComponent implements OnInit {

  openHelp:boolean = false;

  //necessary to contract
  clientToCopy: Client;
  client: Client;
  contract: Contract = new Contract(this.client, 0, 0, "", "", "", "", false, false, [], []);
  //clientSince: Date = new Date();
  //contractAte: Date;

  installations: Installation[] = [];
  routeHasClient: boolean;

  //necessary to installation - null if not necessary, create instance if yes
  novigest: NoviGestView;
  selectedInstallation: Installation;
  qtyInstallations: number;

  //necessary to clinic
  clinicName: string;
  notes: string;
  address: Address;
  contact: Contact;

  //quer associar contrato
  wantAssocContract: boolean;
  selectedContract: Contract;


  installation: boolean = true;
  loading: boolean = true;
  displayDialog: boolean;
  displayContactDialog: boolean;
  displayAddressDialog: boolean;

  showContact: boolean = false;
  showAddress: boolean = false;

  instColor: string = "grey";

  associatePlease: boolean = false;

  display: boolean = false;

  selectedClinic1: Clinic;
  noNeedToCopy: boolean = false;


  clinics: Clinic[];
  clinicsCopy: Clinic[] = [];
  apagar: [number, string][] = []; //index da clinicsCopy

  selectedClinic: Clinic;
  clinicLoading: boolean = true;
  previousChanges: Clinic;

  iconCLI: string = "pi pi-question";
  hexColorCLI: string = "grey";

  numbReg: RegExp = /[0-9]+$/;

  selectedType: CodeDescription;
  codes: CodeDescription[] = [];
  typesLoaded: boolean = false;

  @ViewChild("contForm") contform: ContractNewFormComponent;

  constructor(
    private clientService: ClientService,
    private clinicService: ClinicService,
    private contractService: ContractService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private auth: AuthService,
    private previousRouteService: PreviousRouteService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private errorService: HandleErrorComponent) { }

  ngOnInit() {
    this.loading = true;
    let id: number;
    this.route.params.subscribe(res => {
      id = <number>res.id;
    });
    if (id != null || id != undefined) {
      this.getClient(id);
    }
    else {
      this.routeHasClient = false;
      this.client = new Client("", "", "", new Contact("", "", "", "", ""), new Address("", "", "", ""), [], "","");
      this.loading = false;
      this.noNeedToCopy = true;
      // new client
    }
    this.getPotencialClinics();
    this.getCodes();
  }

  getCodes() {
    this.contractService.getContractCodes().subscribe(res => {
      this.codes = <CodeDescription[]>res;
      this.typesLoaded = true;
    }, error => {
      this.errorService.handleError("Não foi possível obter os códigos de contrato.", error);
    })
  }

  oneMoreContract() {
    this.contract.contractList.push(new Contract(null, 0, 0, "", "", "", "", true, false, [], []));
  }

  listenClientChosen(contract: Contract, client: Client) {
    contract.clientId = client;
  }

  getPotencialClinics() {
    // get potencial clinics
    this.clinicService.getPotencialClinics().subscribe(res => {
      this.clinics = <Clinic[]>res;
      this.clinicLoading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter os potenciais clientes.", error);
    });
    this.clinicService.getPotencialClinics().subscribe(res => {
      this.clinicsCopy = <Clinic[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os potenciais clientes.", error);
    });
  }

  showDialog() {
    this.display = true;
  }

  hearType(event) {
    this.selectedType = event;
  }

  selectClinic(clinic: Clinic) {
    this.selectedClinic1 = clinic;
    this.checkClinicName(this.selectedClinic1);
  }

  getClient(id: number) {

    //procura o cliente
    this.clientService.getClient(id).subscribe(res => {
      this.client = <Client>res;
      this.routeHasClient = true;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter o cliente.", error);
    });

    this.clientService.getClient(id).subscribe(res => {
      this.clientToCopy = <Client>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter o cliente.", error);
    });
  }

  addClinic(installation: Installation) {
    //coloca um id temporario, de acordo com o index da lista
    let c: Clinic = new Clinic("", /*this.clientSince*/ null, "", "", new Address("", "", "", ""), new Contact("", "", "", "", ""), 0, 0, new Date(), null);
    installation.clinicList.push(c);
  }

  removeClinic(installation: Installation, clinic: Clinic): boolean {

    //obter a instalaçao
    let instIndex: number = this.installations.findIndex(inst => (inst == installation));
    //obter o index da clinica alterada na lista da instalaçao
    let index: number = this.installations[instIndex].clinicList.findIndex(clinica => (clinica == clinic));
    // remover a clinica alterada da lista da instalaçao
    this.installations[instIndex].clinicList.splice(index, 1);

    let instElem: number[] = [];
    let apagarIx: number;
    for (let j = 0; j < this.apagar.length; j++) {
      if (this.apagar[j][1] == installation.installationName) {
        instElem.push(this.apagar[j][0]);
      }
    }

    let id = instElem[index];
    if (id != undefined) {
      let clinica = this.clinicsCopy[id];
      this.clinics.push(clinica);

      for (let i = 0; i < this.apagar.length; i++) {
        if (this.apagar[i][1] == installation.installationName &&
          this.apagar[i][0] == id) {
          apagarIx = i;
        }
      }
      this.apagar.splice(apagarIx, 1);
    }

    return true;
  }

  removeInstallation(installation: Installation) {
    this.confirmationService.confirm({
      message: 'Tem a certeza que quer apagar? As clínicas não serão guardadas.',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        let index: number = this.installations.findIndex(ins => (ins == installation));
        let ready: boolean = false;
        while (installation.clinicList.length != 0) {
          ready = this.removeClinic(installation, installation.clinicList[0]);
        }
        this.installations.splice(index, 1);
        this.selectedInstallation = undefined;

      }
    });
  }

  keepClientContact(clinic: Clinic) {
    clinic.contact = new Contact(this.client.contact.phone1, this.client.contact.phone2, this.client.contact.phone3, this.client.contact.email, this.client.contact.other);
    this.copiedDataToastMessage();
  }

  keepClientAddress(clinic: Clinic) {
    clinic.address = new Address(this.client.address.address, this.client.address.postcode1, this.client.address.postcode2, this.client.address.addressLocation);
    this.copiedDataToastMessage();
  }

  addContact(clinic: Clinic) {
    this.selectedClinic = clinic;
    if (this.selectedClinic.contact == null ||
      this.selectedClinic.contact == undefined) {
      this.selectedClinic.contact = new Contact("", "", "", "", ""/*, [], [clinic]*/);
    }

    this.displayContactDialog = true;
    this.showContact = true;
  }

  addAddress(clinic: Clinic) {
    this.selectedClinic = clinic;
    if (this.selectedClinic.address == null ||
      this.selectedClinic.address == undefined) {
      this.selectedClinic.address = new Address("", "", "", "");
    }

    this.displayAddressDialog = true;
    this.showAddress = true;
  }

  onContactDialogHide() {
    this.displayContactDialog = false;
  }

  onAddressDialogHide() {
    this.displayAddressDialog = false;
  }

  doShowContacts() {
    this.showContact = !this.showContact;
  }

  doShowAddress() {
    this.showAddress = !this.showAddress;
  }

  seeAddress(clinic: Clinic) {
    this.selectedClinic = clinic;
    if (this.selectedClinic.address == null || this.selectedClinic.address == undefined) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Ainda não tem morada',
        sticky: false,
        life: 2000
      });
      return;
    }
    this.displayAddressDialog = true;
    this.showAddress = true;
  }

  seeContact(clinic: Clinic) {
    this.selectedClinic = clinic;
    if (this.selectedClinic.contact == null || this.selectedClinic.contact == undefined) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Ainda não tem contactos',
        sticky: false,
        life: 2000
      });
      return;
    }
    this.displayContactDialog = true;
    this.showContact = true;
  }

  clearBtnColor() {
    this.iconCLI = "pi pi-question";
    this.hexColorCLI = "grey";
  }

  checkClinicName(clinic: Clinic) {
    let name: string = clinic.name;
    if (name == undefined) {
      this.iconCLI = "pi pi-question";
      this.hexColorCLI = "grey";
      return;
    }
    if (name == "") {
      this.iconCLI = "pi pi-question";
      this.hexColorCLI = "grey";
      return;
    }
    if (name.trim().length == 0) {
      this.iconCLI = "pi pi-question";
      this.hexColorCLI = "grey";
      return;
    }
    this.clinicService.checkIfNameExists(name).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result == true) {

        this.iconCLI = "pi pi-times";
        this.hexColorCLI = "red";
      }
      else {
        this.iconCLI = "pi pi-check";
        this.hexColorCLI = "green";
      }
    })

  }

  validField(field: string, mandatory: boolean): boolean {
    if (mandatory && this.nullOrUndefinedField(field)) {
      return false;
    } else if (!mandatory && this.nullOrUndefinedField(field)) {
      return true;
    } else {
      return field.trim().length > 0;
    }
  }

  private nullOrUndefinedField(field: string) {
    if (field == null || field == undefined) {
      return true;
    }
    return false;
  }

  checkClientFields(): boolean {
    if (!this.validField(this.client.name, true)) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher o nome',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return false;
    }

    if (!(this.validField(this.client.contact.email, true) || this.validField(this.client.contact.phone1, true))) {
      // mensagem preencher contacto ou email
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher um contacto ou email',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!(this.validField(this.client.address.address, true) || this.validField(this.client.address.addressLocation, true))) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher a morada ou localização',
        sticky: false,
        life: 2000
      });
      // mensagem preencher morada ou localização
      return false;
    }
    return true;
  }


  checkContractFields(): boolean {
    if (this.selectedType == undefined || this.selectedType == null) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve escolher um tipo de contrato',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (this.selectedType.descr == undefined) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Há algum problema sobre o tipo de contrato escolhido',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (this.contract.qtyAddStations < 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'O número de postos adicionais não pode ser negativo',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (this.contract.qtyMainStations < 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'O número de postos principais não pode ser negativo',
        sticky: false,
        life: 2000
      });
      return false;
    }


    return true;
  }

  checkInstallationFields(): boolean {
    if (this.installations.length == 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Tem de definir as instalações e clínicas',
        sticky: false,
        life: 2000
      });
      return false;
    }

    let listOK: boolean = true;
    let clinicOK: boolean = true;
    this.installations.forEach(element => {
      if (element.clinicList.length == 0) {
        listOK = false;
      } else {
        element.clinicList.forEach(element => {
          if (element.name.trim().length == 0) {
            clinicOK = false;
          }
          // if(!this.checkClinicName(element)){
          // clinicOK = false;
          // }
          if (element.mainPostos < 0) {
            clinicOK = false;
          }
          if (element.addPostos < 0) {
            clinicOK = false;
          }

        });
      }
    });

    if (!listOK) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Há pelo menos uma instalação sem clínicas definidas',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!clinicOK) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Há pelo menos uma clínica sem nome ou com postos negativos!',
        sticky: false,
        life: 2000
      });
      return false;
    }

    return true;
  }

  removeContractFromList(contract: Contract) {
    let index: number = this.contract.contractList.findIndex(c => (c == contract));
    this.contract.contractList.splice(index, 1);
  }

  checkChildContractsFields(): boolean {
    if (this.contract.contractList.length == 0) {
      return true;
    }

    this.contract.contractList.forEach(element => {

      element.contractList.forEach(element => {
        if (element.clientId == null || element.clientId == undefined) {
          this.messageService.add({
            severity: 'error',
            summary: 'Atenção',
            detail: 'Há pelo menos um contrato sem um cliente atribuído.',
            sticky: false,
            life: 2000
          });
          return false;
        }

        if (element.contractType == undefined || element.contractType == null) {
          this.messageService.add({
            severity: 'error',
            summary: 'Atenção',
            detail: 'Há algum problema sem o tipo de contrato',
            sticky: false,
            life: 2000
          });
          return false;
        }

      });
      return true;
    });
  }

  save() {
    if (!this.checkClientFields()) {
      return;
    }

    if (!this.checkContractFields()) {
      return;
    }

    if (!this.checkInstallationFields()) {
      return;
    }

    if (!this.checkChildContractsFields()) {

    }

    let toDelete: string[] = [];
    this.apagar.forEach(element => {
      toDelete.push(this.clinicsCopy[element[0]].name);
    })

    this.contract.installationList = this.installations;

    //set first stage = true
    this.contract.firstStage = true;
    this.contract.contractType = this.selectedType.descr;
    let cc = new NewClientAndContract(this.client, this.contract);
    this.contractService.createContract2(cc).subscribe(data => {
      let contract: Contract = <Contract>data;

      //let cc: ContractChanges = new ContractChanges(new Date(), this.selectedType.code, this.selectedType.descr, false, null, this.contractAte, data);
      //this.contractService.createContractChanges(contract.contractId, cc).subscribe(res => {

      //this.clinicService.deletePotencialClinics(toDelete).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'Contrato Registado!',
        sticky: false,
        life: 1000
      });
      setTimeout(() => {
        this.back();
      }, 1000);
      //}, error => {
      //  this.errorService.handleError("Não foi possível guardar o contrato.", error);
      // });
      //
      //    }, error => {
      //    this.errorService.handleError("Não foi possível guardar o contrato.", error);
      //});

    }, error => {
      this.errorService.handleError("Não foi possível guardar o contrato.", error);
    });
  }

  copiedDataToastMessage() {
    this.messageService.add({
      severity: 'success',
      summary: 'Sucesso',
      detail: 'Dados copiados do cliente!',
      sticky: false,
      life: 2000
    });
  }

  checkNInstallations() {
    if (this.qtyInstallations == 0) {
      this.instColor = "grey";
    }
    else if (this.qtyInstallations >= 1) {
      this.instColor = "green";
    }
  }

  createInstallations() {
    let now = new Date();
    let installation_name = "dummy_" + "fakecontract" /*this.contract.contractNumber*/;
    let last: number;
    if (this.installations.length >= 1) {
      last = +this.installations[this.installations.length - 1].installationName.split("_")[3];
    } else {
      last = -1;
    }

    let totalinst = this.contract.qtyMainStations - this.installations.length;
    for (let index = 0; index < totalinst; index++) {
      let num: number = last + 1 + index;
      let installation_name2 = installation_name + "_" + now.getTime() + "_" + num;
      let descr: string = "inst_" + new Date().getTime() + "_" + index;
      this.installations.push(new Installation(null, "", descr, [], [], [], installation_name2, "", false, false, false, false, false, false, false, false, false, false));
    }
  }

  copyToClinic() {
    let now = new Date();
    let installation_name = "dummy_" + "fakecontract" /*this.contract.contractNumber*/;
    let last: number;
    if (this.installations.length >= 1) {
      last = +this.installations[this.installations.length - 1].installationName.split("_")[3];
    } else {
      last = -1;
      
    let num: number = last + 1;
    let installation_name2 = installation_name + "_" + now.getTime() + "_" + num;
    let descr: string = "inst_" + new Date().getTime() + "_" + 0;
    this.installations.push(new Installation(null, "", descr, [], [], [], installation_name2, "", false, false, false, false, false, false, false, false, false, false));

    }

    this.addClinicWithClientData(this.installations[0]);
  }

  addClinicWithClientData(installation: Installation) {
    let c: Clinic = new Clinic(this.clientToCopy.name, null, this.clientToCopy.notes, "",
      new Address(this.clientToCopy.address.address, this.clientToCopy.address.postcode1, this.clientToCopy.address.postcode2, this.clientToCopy.address.addressLocation),
      new Contact(this.clientToCopy.contact.phone1, this.clientToCopy.contact.phone2, this.clientToCopy.contact.phone3, this.clientToCopy.contact.email, this.clientToCopy.contact.other), 0, 0, this.clientToCopy.criacaoFicha);
    this.client.name = "";
    this.client.notes = "";
    this.client.criacaoFicha = new Date();

    if (installation.clinicList == undefined) {
      installation.clinicList = [];
    }
    installation.clinicList.push(c);
    this.contract.qtyMainStations +=1;
  }

  associateClinicToInstallation(inst: Installation) {
    this.selectedInstallation = inst;
    this.associatePlease = true;
  }

  /**
   * Para guardar a seleção + edição do potencial selecionado
   */
  associate() {

    let contact: Contact = new Contact(this.selectedClinic.contact.phone1, this.selectedClinic.contact.phone2, this.selectedClinic.contact.phone3,
      this.selectedClinic.contact.email, this.selectedClinic.contact.other);

    let address: Address = new Address(this.selectedClinic.address.address, this.selectedClinic.address.postcode1, this.selectedClinic.address.postcode2,
      this.selectedClinic.address.addressLocation);

    this.selectedClinic.contact = contact;
    this.selectedClinic.address = address;
    this.selectedClinic.clientSince = /*new Date(this.clientSince)*/ null;

    this.selectedInstallation.clinicList.push(this.selectedClinic);
    let i = this.clinicsCopy.findIndex(c => c == this.previousChanges);
    this.apagar.push([i, this.selectedInstallation.installationName]);


    let index: number = this.clinics.findIndex(clinica => (clinica == this.selectedClinic));
    this.deleteIndexOfClinicsArray(index, this.clinics);

    this.selectedClinic = undefined;
    this.associatePlease = false;

  }

  back() {
    let route: string = this.previousRouteService.getPreviousUrl();
    let now: string = this.router.url;
    if (route == null || route == undefined) {
      this.router.navigate(['home']);
    } else if (route == now) {
      this.router.navigate(['home']);
    }
    else {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    }

  }

  event(event) {
    let index = this.clinicsCopy.findIndex(clinic => clinic.name == (<Clinic>event.value).name);
    if (index == -1) {
      //this.getPotencialClinics();

      //e apagar as que nao tem interesse
    }
    this.previousChanges = this.clinicsCopy[index];
  }

  deleteIndexOfClinicsArray(index: number, array: Clinic[]) {
    array.splice(index, 1);
  }
}
