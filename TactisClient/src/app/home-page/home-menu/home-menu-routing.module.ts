import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeCardsComponent } from './home-cards/home-cards.component';
import { UserNewComponent } from './user-new/user-new.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ClientNewComponent } from './client-new/client-new.component';
import { ContractNewComponent } from './contract-new/contract-new.component';
import { UsersComponent } from './users/users.component';
import { InstallationAssignmentComponent } from './installation-assignment/installation-assignment.component';
import { CalendarComponent } from './calendar/calendar.component';
import { InstallationUpdateComponent } from './installation-update/installation-update.component';
import { UpdatesComponent } from './updates/updates.component';
import { ClientViewSearchComponent } from './client-view-search/client-view-search.component';
import { MyInstallationsComponent } from './my-installations/my-installations.component';
import { SeeInstallationsComponent } from './see-installations/see-installations.component';
import { SeeTrainingsComponent } from './see-trainings/see-trainings.component';
import { MyTrainingsComponent } from './my-trainings/my-trainings.component';
import { InstallationsComponent } from './installations/installations.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { SeeOtherEventsComponent } from './see-other-events/see-other-events.component';
import { MyOtherEventsComponent } from './my-other-events/my-other-events.component';
import { NewUpgradeComponent } from './new-upgrade/new-upgrade.component';
import { UpgradesComponent } from './upgrades/upgrades.component';
import { NovigestActivationsViewComponent } from './novigest-activations-view/novigest-activations-view.component';
import { PemActivationsViewComponent } from './pem-activations-view/pem-activations-view.component';
import { ContractsUnfinishedComponent } from './contracts-unfinished/contracts-unfinished.component';
import { ClientDetailsV2Component } from './client-details-v2/client-details-v2.component';
import { NovidashKpiGestComponent } from './novidash-kpi-gest/novidash-kpi-gest.component';
import { NovidashGraficosGestComponent } from './novidash-graficos-gest/novidash-graficos-gest.component';
import { AditamentoNewComponent } from './aditamento-new/aditamento-new.component';
import { AditamentoAssignmentComponent } from './aditamento-assignment/aditamento-assignment.component';
import { AditamentoUpdateComponent } from './aditamento-update/aditamento-update.component';
import { ListagemRenovationsSeeComponent } from './listagem-renovations-see/listagem-renovations-see.component';
import { StatsDashboardComponent } from './statistics/stats-dashboard/stats-dashboard.component';
import { ListagemClientesSeeComponent } from './listagem-clientes-see/listagem-clientes-see.component';
import { ListagemCircularesSeeComponent } from './listagem-circulares-see/listagem-circulares-see.component';
import { DurInterventionsActivityComponent } from './statistics/graficos/dur-interventions-activity/dur-interventions-activity.component';
import { DurInterventionsUserComponent } from './statistics/graficos/dur-interventions-user/dur-interventions-user.component';
import { NumInterventionsActivityComponent } from './statistics/graficos/num-interventions-activity/num-interventions-activity.component';
import { NumInterventionsUserComponent } from './statistics/graficos/num-interventions-user/num-interventions-user.component';
import { IndicadoresComponent } from './statistics/indicadores/indicadores.component';
import { StatsView2DashboardComponent } from './statistics/stats-view2-dashboard/stats-view2-dashboard.component';
import { ContractAssociationComponent } from './contract-association/contract-association.component';
import { IndicadoresView2Component } from './statistics/indicadores-view2/indicadores-view2.component';
import { NumDayInterventionsUserComponent } from './statistics/graficos/num-day-interventions-user/num-day-interventions-user.component';
import { NumDayInterventionsTotalComponent } from './statistics/graficos/num-day-interventions-total/num-day-interventions-total.component';
import { NumMonthInterventionsUserComponent } from './statistics/graficos/num-month-interventions-user/num-month-interventions-user.component';
import { NumMonthInterventionsTotalComponent } from './statistics/graficos/num-month-interventions-total/num-month-interventions-total.component';
import { NumHourInterventionsTotalComponent } from './statistics/graficos/num-hour-interventions-total/num-hour-interventions-total.component';

const routes: Routes = [
  {
    path: '',
    component: HomeCardsComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'users',
    component: UsersComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'users/new',
    component: UserNewComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'clients/new',
    component: ClientNewComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'byclient',
    component: ClientViewSearchComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'byclient/page/:page',
    component: ClientViewSearchComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'byclient/:keyword',
    component: ClientViewSearchComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'clinic/:cid',
    // component: ClientDetailsComponent/*,
    component: ClientDetailsV2Component/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'clinic/:cid/access',
    //component: ClientDetailsComponent/*,
    component: ClientDetailsV2Component
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'clients/info/:id',
    //component: ClientDetailsComponent/*,
    component: ClientDetailsV2Component
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'clinic/:cid/intervention/new',
    //component: ClientDetailsComponent/*,
    component: ClientDetailsV2Component
    // canActivate: [AuthGuard]*/
  },
  {
    path: 'clients/:id/contract/new',
    component: ContractNewComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'clients/:id/aditamento/new',
    component: AditamentoNewComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'contract/new',
    component: ContractNewComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'contract/:id/association',
    component: ContractAssociationComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'schedules',
    component: InstallationsComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'schedule/aditamento/:id',
    component: AditamentoAssignmentComponent
    //component: SchedulesComponent/*,
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'schedule/:type/:id',
    component: InstallationAssignmentComponent
    //component: SchedulesComponent/*,
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'install/pp/:id',
    component: InstallationUpdateComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'install/pa/:id',
    component: AditamentoUpdateComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'event/new',
    component: CreateEventComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'novidash/kpi',
    component: NovidashKpiGestComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'novidash/grafico',
    component: NovidashGraficosGestComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'schedule/installations',
    component: MyInstallationsComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'schedule/trainings',
    component: MyTrainingsComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'schedule/others',
    component: MyOtherEventsComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'scheduled/installations',
    component: SeeInstallationsComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'scheduled/others',
    component: SeeOtherEventsComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'scheduled/trainings',
    component: SeeTrainingsComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'contracts/unfinished/see',
    component: ContractsUnfinishedComponent
    //canActivate: [AuthGuard]*/
  },
  {
    path: 'calendar',
    component: CalendarComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'myprofile',
    component: UserProfileComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'profile/:username',
    component: UserProfileComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'updates/:type',
    component: UpdatesComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'activations/ng',
    component: NovigestActivationsViewComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'activations/pem',
    component: PemActivationsViewComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'manage/updates/:type',
    component: UpgradesComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'manage/updates/new',
    component: NewUpgradeComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'listagem/renovacoes',
    component: ListagemRenovationsSeeComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'listagem/novosclientes',
    component: ListagemClientesSeeComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'listagem/circulares',
    component: ListagemCircularesSeeComponent/*,
    canActivate: [AuthGuard]*/
  },
  {
    path: 'dashboard',
    component: StatsDashboardComponent,
    /*canActivate: [AuthGuard]*/
    children: [
      {
        path: '',
        component: IndicadoresComponent/*,
        canActivate: [AuthGuard]*/
        // , outlet: 'graficos'
      },
      {
        path: 'nrIntervencoesUser',
        component: NumInterventionsUserComponent/*,
        canActivate: [AuthGuard]*/
        // , outlet: 'graficos'
      },
      {
        path: 'durIntervencoesUser',
        component: DurInterventionsUserComponent/*,
        canActivate: [AuthGuard]*/
        //, outlet: 'graficos'
      },
      {
        path: 'nrIntervencoesActivity',
        component: NumInterventionsActivityComponent/*,
        canActivate: [AuthGuard]*/
        //, outlet: 'graficos'
      },
      {
        path: 'durIntervencoesActivity',
        component: DurInterventionsActivityComponent/*,
        canActivate: [AuthGuard]*/
        // , outlet: 'graficos'
      }
    ]
  },
  {
    path: 'dashboarddsm',
    component: StatsView2DashboardComponent,
    /*canActivate: [AuthGuard]*/
    children: [
      {
        path: '',
        component: IndicadoresView2Component/*,
        canActivate: [AuthGuard]*/
        // , outlet: 'graficos'
      },
      {
        path: 'nrIntervencoesUserDia',
        component: NumDayInterventionsUserComponent/*,
        canActivate: [AuthGuard]*/
        // , outlet: 'graficos'
      },
      {
        path: 'nrIntervencoesTotalDia',
        component: NumDayInterventionsTotalComponent/*,
        canActivate: [AuthGuard]*/
        //, outlet: 'graficos'
      },
      {
        path: 'nrIntervencoesUserMes',
        component: NumMonthInterventionsUserComponent/*,
        canActivate: [AuthGuard]*/
        //, outlet: 'graficos'
      },
      {
        path: 'nrIntervencoesTotalMes',
        component: NumMonthInterventionsTotalComponent/*,
        canActivate: [AuthGuard]*/
        // , outlet: 'graficos'
      },
      {
        path: 'nrIntervencoesTotalHora',
        component: NumHourInterventionsTotalComponent/*,
        canActivate: [AuthGuard]*/
        // , outlet: 'graficos'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeMenuRoutingModule { }
