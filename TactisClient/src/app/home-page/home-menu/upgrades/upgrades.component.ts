import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Upgrade } from 'src/app/model/upgrade';
import { UpgradeService } from 'src/app/services/upgrade.service';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-upgrades',
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.css']
})
export class UpgradesComponent implements OnInit {

  upgrades: Upgrade[] = [];
  selectedVersion: Upgrade;
  displayNew: boolean = false;
  displayEdit: boolean = false;
  updateNG: boolean = false;
  updateTU: boolean = false;
  updatePEM: boolean = false;
  updateINF: boolean = false;
  selectedDB:string;
  titleDB:string;

  constructor(
    private router: Router,
    private upgradeService: UpgradeService
  ) { }

  ngOnInit() {
    if (this.router.url.match("ng") != null) {
      this.updateNG = true;
      this.selectedDB = "ng";
      this.titleDB = "Novigest";
    } else if (this.router.url.match("inf") != null) {
      this.updateINF = true;
      this.selectedDB = "inf";
      this.titleDB = "Infarmed";
    } else if (this.router.url.match("tu") != null) {
      this.updateTU = true;
      this.selectedDB = "tu";
      this.titleDB = "Tactis Updater";
    } else if (this.router.url.match("pem") != null) {
      this.updatePEM = true;
      this.selectedDB = "pem";
      this.titleDB = "Novipem";
    }
    this.getDBUpgrades();
  }

  getDBUpgrades() {
    this.upgradeService.getDBUpgrades(this.selectedDB).subscribe(res => {
      this.upgrades = res;
      console.log(this.upgrades);
    })
  }

  back() {
    this.router.navigate(['home']);
  }

  addUpgrade() {
    this.displayNew = true;
  }

  readCloseNew(event) {
    this.displayNew = event;
    this.upgrades = [];
    this.getDBUpgrades();
  }

  readCloseEdit(event) {
    this.displayEdit = event;
    this.upgrades = [];
    this.getDBUpgrades();
  }

  onRowSelect(event) {
    this.selectedVersion = event.data;
    this.displayEdit = true;
  }

}
