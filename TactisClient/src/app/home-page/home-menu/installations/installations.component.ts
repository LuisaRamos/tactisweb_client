import { Component, OnInit } from '@angular/core';
import { Installation } from 'src/app/model/installation';
import { Router } from '@angular/router';
import { InstallationService } from 'src/app/services/installation.service';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService, MenuItem } from 'primeng/api';
import { ContractAndInstallation } from 'src/app/util/view/contractandinstallation';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-installations',
  templateUrl: './installations.component.html',
  styleUrls: ['./installations.component.css']
})
export class InstallationsComponent implements OnInit {

  data: ContractAndInstallation[];
  loading:boolean = true;
  openHelp:boolean = false;

  constructor(
    private installationService: InstallationService,
    private router: Router,
    private errorService: HandleErrorComponent
  ) { }

  ngOnInit() {
    this.getInstallations();
  }

  getInstallations() {
    this.installationService.getInstallationsUnassigned().subscribe(data => {

      this.data = <ContractAndInstallation[]>data;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações.", error);
    });
  }

  schedule(d: ContractAndInstallation) {
    if (d.installation != undefined && d.installation != null) {
      console.log("quer agendar inst")
      this.router.navigate(["home/schedule/installation/" + d.installation.installationId]);
    } else {
      console.log("quer agendar adit")
      this.router.navigate(["home/schedule/aditamento/" + d.aditamento.aditamentoId]);
    }
  }

  scheduleNewEvent() {
    this.router.navigate(['home/event/new']);
  }

  back() {
    this.router.navigate(['home']);
  }
}
