import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { TactisUser } from '../../../model/tactisuser';
import { AuthService } from '../../../services/auth.service';
import { RoleService } from 'src/app/services/role.service';
import { UserRole } from 'src/app/model/userrole';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user: TactisUser;
  name: string;
  username: string;
  email: string;
  changePass: boolean = false;
  keepOrChangePass: string = "Alterar";
  updateForm: FormGroup;
  roles: UserRole[];
  selectedRole: UserRole;
  submitted = false;
  carregou: boolean = false;
  isAdmin: boolean;
  userGot: boolean = false;
  //regex
  blockSpace: RegExp = /[^\s]/;
  editOther: boolean = false;
  activeUser: TactisUser;

  iconUN: string = "pi pi-question";
  iconEM: string = "pi pi-question";
  hexColorUN: string = "grey";
  hexColorEM: string = "grey";

  constructor(
    private authSrv: AuthService,
    private userSrv: UserService,
    private roleSrv: RoleService,
    private auth: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private errorService:HandleErrorComponent) {

  }

  ngOnInit() {
    this.getUserRoles();
    this.getActiveUserToCheckAuthorization();
    this.updateForm = this.formBuilder.group({
      _name: [{ disabled: false }, [Validators.required, Validators.minLength(1)]],
      _selectedRole: ['', [Validators.required]],
      _username: ['', [Validators.required, Validators.minLength(1)]],
      _email: ['', [Validators.required, Validators.minLength(1)]],
      _password: ['', [Validators.minLength(6)]],
      _retypePassword: ['', [Validators.minLength(6)]]/*,
      _active: ['', [Validators.required]]*/
    });

    this.checkUrlAndGetUser();

  }

  getUserRoles(): void {
    this.roleSrv.getUserRoles().subscribe(data => {
      this.roles = <UserRole[]>data;
    }, error => {
      this.errorService.handleError("Aconteceu um erro, não foi possível obter os tipos de utilizador.", error);
    });
  }

  checkUrlAndGetUser() {

    if (this.router.url.match("myprofile") != null) {
      this.editOther = false;
      this.getActiveUser();
      return;

    } else {
      this.editOther = true;
      this.route.params.subscribe(res => {
        let username: string = <string>res.username;
        this.getUserByUsername(username);
      });
      return;
    }
  }

  putData() {
    this.f._selectedRole.setValue(this.user.userRoleId);
    this.f._name.setValue(this.user.name);
    this.f._username.setValue(this.user.username);
    this.f._email.setValue(this.user.email);
    this.f._password.setValue(this.user.password);
    this.f._retypePassword.setValue(this.user.password);
    this.carregou = true;
    this.isAdmin = this.user.userRoleId.userRole == 'ADMIN';
  }

  getActiveUserToCheckAuthorization() {
    let username: string = this.auth.getActiveUser();

    this.userSrv.getTactisUserByUsername(username).subscribe(data => {
      this.activeUser = <TactisUser>data;
    }, error => {
      this.errorService.handleError("Aconteceu um erro, não foi possível obter o utilizador.", error);
    });

  }


  getActiveUser(): TactisUser {
    let username: string = this.auth.getActiveUser();

    this.userSrv.getTactisUserByUsername(username).subscribe(data => {
      let tu: TactisUser = <TactisUser>data;
      this.user = tu;
      this.putData();
      return tu;
    }, error => {
      this.errorService.handleError("Aconteceu um erro, não foi possível obter o utilizador.", error);
    });
    return;
  }


  getUserByUsername(username: string): TactisUser {

    this.route.params.subscribe(res => {
      username = <string>res.username;

      if (username != null && username != undefined) {
        this.userSrv.getTactisUserByUsername(username).subscribe(data => {
          let tu: TactisUser = <TactisUser>data;
          this.user = tu;
          this.putData();
          return;
        }, error => {
          if (error.status == 401 /*&& error.error == "SessionExpired" or "Unauthorized"*/) {
            this.authSrv.setSessionExpired();
            this.messageService.add({
              severity: 'error',
              summary: 'Erro',
              detail: 'Sessão expirada. Faça novamente o login.',
              sticky: false,
              life: 1000
            });
            setTimeout(() => {
              this.authSrv.logout();
              this.router.navigate(["/login"]);
            }, 1000);
          } else {
            this.f._name.setValue("");
            this.f._username.setValue("");
            this.f._email.setValue("");
            this.messageService.add({
              severity: 'error',
              summary: 'Erro',
              detail: 'Não foi possivel obter as informações do utilizador.',
              sticky: false,
              life: 1000
            });
          }
        });
      }
    });
    return;
  }

  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  save() {
    this.submitted = true;
    let nameFormControl = this.f._name.value;
    let usernameFormControl = this.f._username.value;
    let emailFormControl = this.f._email.value;
    let passwordFormControl = this.f._password.value;
    let rpasswordFormControl = this.f._retypePassword.value;
    let roleFormControl = this.f._selectedRole.value;

    // stop here if form is invalid
    if (this.updateForm.invalid) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Verifique os dados preenchidos',
        sticky: false,
        life: 1000
      });
      return;
    }

    if (nameFormControl == null || nameFormControl.trim().length == 0) {
      this.messageToast('error', 'Atenção!', 'O nome não pode estar vazio');
      return;
    }
    if (usernameFormControl == null || usernameFormControl.trim().length == 0) {
      this.messageToast('error', 'Atenção!', 'O username não pode estar vazio');
      return;
    }
    if (emailFormControl == null || emailFormControl.trim().length == 0) {
      this.messageToast('error', 'Atenção!', 'O email não pode estar vazio');
      return;
    }
    if (passwordFormControl == null || passwordFormControl.trim().length == 0) {
      this.messageToast('error', 'Atenção!', 'A password não pode estar vazio');
      return;
    }
    if (roleFormControl == null) {
      this.messageToast('error', 'Atenção!', 'Deve escolher um tipo de utilizador');
      return;
    }
    if (passwordFormControl != rpasswordFormControl) {
      this.messageService.add({
        severity: 'error',
        summary: 'Erro',
        detail: 'As Passwords não coincidem!',
        sticky: false,
        life: 1000
      });
      return;
    }

    let updated: TactisUser = {
      tactisUserId: this.user.tactisUserId, name: nameFormControl, username: usernameFormControl,
      email: emailFormControl, password: passwordFormControl, userRoleId: roleFormControl, active: this.user.active
    };

    this.userSrv.updateTactisUser(this.user.tactisUserId, updated).subscribe(data => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'Alterado com sucesso. Volte a iniciar sessão.',
        sticky: false,
        life: 1000
      });
      setTimeout(() => {
        if (this.editOther) {
          this.router.navigate(['/home/users']);
        } else {
          this.authSrv.logout();
          this.router.navigate(['']);
        }

      }, 1000);

    }, error => {
      if (error.status == 400 && error.error == "DataIntegrityViolationException") {
        this.messageToast("error", "Erro", "O username ou email já estão em uso");
      }
      else {
        this.errorService.handleError("Aconteceu um erro. O username ou email poderão estar", error);
      }
    });
  }

  checkUsername(){
    if(this.f._username == undefined){
      return;
    }
    let usernameFormControl:string = this.f._username.value;
    if(usernameFormControl == undefined){
      return ;
    }
    if(usernameFormControl.trim().length == 0 ){
      return ;
    }
    this.userSrv.checkIfUsernameExists(usernameFormControl).subscribe(res => {
      console.log(res);
      let result: boolean = <boolean>res;
      console.log(result)
      if (result == true) {
        
        this.iconUN = "pi pi-times";
        this.hexColorUN = "red";
      }
      else {
        this.iconUN = "pi pi-check";
        this.hexColorUN = "green";
      }
    })
  }

  clearUsername(){
    this.hexColorUN = "grey";
    this.iconUN= "pi pi-question";
  }

  clearEmail(){
    this.hexColorEM = "grey";
    this.iconEM= "pi pi-question";
  }

  checkEmail(){
    if(this.f._email == undefined){
      return;
    }
    let emailFormControl = this.f._email.value;
    if(emailFormControl == undefined){
      return ;
    }
    if(emailFormControl.trim().length == 0 ){
      return ;
    }
    
    this.userSrv.checkIfEmailExists(emailFormControl).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result == true) {
        this.iconEM = "pi pi-times";
        this.hexColorEM = "red";
      }
      else {
        this.iconEM = "pi pi-check";
        this.hexColorEM = "green";
      }
    })
  }

  changePassword() {
    this.changePass = !this.changePass;
    if (this.changePass) {
      this.f._password.setValue("");
      this.f._retypePassword.setValue("");
      this.keepOrChangePass = "Manter";

    } else {
      this.f._password.setValue(this.user.password);
      this.f._retypePassword.setValue(this.user.password);

      this.keepOrChangePass = "Alterar";
    }
  }

  back() {
    if (this.editOther) {
      this.router.navigate(['/home/users']);
    } else {
      this.router.navigate(['home']);
    }

  }

  messageToast(sev: string, summary: string, message: string) {
    this.messageService.add({
      severity: sev,
      summary: summary,
      detail: message,
      sticky: false,
      life: 2000
    });
  }
}
