import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { registerLocaleData, formatDate } from '@angular/common';
import localePT from "@angular/common/locales/pt";
import { GetStatisticsService } from '../service/get-statistics.service';

@Component({
  selector: 'app-stats-dashboard',
  templateUrl: './stats-dashboard.component.html',
  styleUrls: ['./stats-dashboard.component.css']
})
export class StatsDashboardComponent implements OnInit {

  rangeDates: Date[] = [];
  selectedGrafico: any;
  pt: any;

  constructor(
    private router: Router,
    private getstatistics: GetStatisticsService
    // private messageService: MessageService
  ) { }

  ngOnInit() {
    registerLocaleData(localePT, "pt");
    this.pt = {
      firstDayOfWeek: 1,
      dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
      dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
      dayNamesMin: ["D", "2ª", "3ª", "4ª", "5ª", "6ª", "S"],
      monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      today: 'Hoje',
      clear: 'Limpar',
      dateFormat: 'dd/mm/yy',
      weekHeader: 'Sem'
    }
    this.rangeDates[0] = new Date();
    this.rangeDates[1] = new Date();
    const start = formatDate(this.rangeDates[0], 'dd/MM/yyyy', 'pt');
    const finish = formatDate(this.rangeDates[1], 'dd/MM/yyyy', 'pt');
    this.getstatistics.changeDatas([start, finish]);
  }

  back() {
    if (this.router.url.match('dashboard/')) {
      this.router.navigate(['home/dashboard']);
    }
    else {
      this.router.navigate(['home']);
    }
  }

  changeViewToDSM(){
    this.router.navigate(['home/dashboarddsm']);
  }

  dateChange() {
    if (this.rangeDates[0] != undefined && this.rangeDates[1] != undefined) {
      const start = formatDate(this.rangeDates[0], 'dd/MM/yyyy', 'pt');
      const finish = formatDate(this.rangeDates[1], 'dd/MM/yyyy', 'pt');
      this.getstatistics.changeDatas([start, finish]);
    }
  }
}
