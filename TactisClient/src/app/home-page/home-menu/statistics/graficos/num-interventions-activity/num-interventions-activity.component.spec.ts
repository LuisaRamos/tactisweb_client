import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumInterventionsActivityComponent } from './num-interventions-activity.component';

describe('NumInterventionsActivityComponent', () => {
  let component: NumInterventionsActivityComponent;
  let fixture: ComponentFixture<NumInterventionsActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumInterventionsActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumInterventionsActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
