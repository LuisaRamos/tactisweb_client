import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumMonthInterventionsUserComponent } from './num-month-interventions-user.component';

describe('NumMonthInterventionsUserComponent', () => {
  let component: NumMonthInterventionsUserComponent;
  let fixture: ComponentFixture<NumMonthInterventionsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumMonthInterventionsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumMonthInterventionsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
