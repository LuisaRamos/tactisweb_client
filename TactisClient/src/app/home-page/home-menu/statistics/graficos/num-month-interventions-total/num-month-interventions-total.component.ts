import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GetStatisticsService } from '../../service/get-statistics.service';
import { totalStrucutre } from '../../totalStructure';

@Component({
  selector: 'app-num-month-interventions-total',
  templateUrl: './num-month-interventions-total.component.html',
  styleUrls: ['./num-month-interventions-total.component.css']
})
export class NumMonthInterventionsTotalComponent implements OnInit, OnDestroy {

  dateSubscription: Subscription;
  data: any;
  res: totalStrucutre;
  options: any;
  dados: any;

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
  }

  constructor(private getstatistics: GetStatisticsService) { }

  ngOnInit() {
    this.dateSubscription = this.getstatistics.currentDate.subscribe(data => {
      if (data != undefined) {
        this.data = data;
        this.redraw(null);
      }
    });
  }

  redraw(event) {
    var ctx = document.getElementById("chart").getElementsByTagName("canvas")[0].getContext("2d");
    var blue_gradient = ctx.createLinearGradient(0, 0, 0, 500);
    blue_gradient.addColorStop(0, 'rgba(66,134,244,0.5)');
    blue_gradient.addColorStop(1, 'rgba(66,134,244,0)');

    this.getstatistics.getNumMonIntervencoesTotais(this.data).subscribe(res => {
      console.log(res);
      this.res = res;
      
      this.dados = {
         labels: this.res.periodos,
        datasets: [
          {
            label: 'Intervenções',
            data: this.res.valores,
            borderWidth: 2,
            backgroundColor: blue_gradient,
            borderColor: 'rgba(66, 134, 244, 1)',
            pointHoverRadius: 15
          }
        ]
      }

      this.options = {
        hover: {
          mode: 'nearest',
          intersect: false
        },
        tooltips: {
          intersect: false,
          callbacks: {
            label: function (tooltipItems, data) {
              return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.yLabel.toLocaleString("pt");
            }
          }
        },
        responsive: true,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              precision: 0,
              callback: function (value, index, values) {
                return value.toLocaleString("pt");
              }
            }
          }]
        }
      }
    });
  }
}
