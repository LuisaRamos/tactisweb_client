import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumMonthInterventionsTotalComponent } from './num-month-interventions-total.component';

describe('NumMonthInterventionsTotalComponent', () => {
  let component: NumMonthInterventionsTotalComponent;
  let fixture: ComponentFixture<NumMonthInterventionsTotalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumMonthInterventionsTotalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumMonthInterventionsTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
