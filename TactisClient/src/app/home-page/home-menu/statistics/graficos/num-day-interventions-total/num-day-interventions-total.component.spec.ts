import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumDayInterventionsTotalComponent } from './num-day-interventions-total.component';

describe('NumDayInterventionsTotalComponent', () => {
  let component: NumDayInterventionsTotalComponent;
  let fixture: ComponentFixture<NumDayInterventionsTotalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumDayInterventionsTotalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumDayInterventionsTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
