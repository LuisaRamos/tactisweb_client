import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GetStatisticsService } from '../../service/get-statistics.service';

@Component({
  selector: 'app-num-day-interventions-user',
  templateUrl: './num-day-interventions-user.component.html',
  styleUrls: ['./num-day-interventions-user.component.css']
})
export class NumDayInterventionsUserComponent implements OnInit, OnDestroy {

  dateSubscription: Subscription;
  data: any;
  dados: any;
  res: any;
  options: any;

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
  }

  constructor(private getstatistics: GetStatisticsService) { }

  ngOnInit() {
    this.dateSubscription = this.getstatistics.currentDate.subscribe(data => {
      if (data != undefined) {
        this.data = data;
        this.redraw(null);
      }
    });
  }

  redraw(event) {
    this.getstatistics.getNumDayIntervencoesUser(this.data).subscribe(res => {
      this.res = res;

      var ctx = document.getElementById("chart").getElementsByTagName("canvas")[0].getContext("2d");
      let dsets: any[] = [];
      this.res.userInts.forEach(element => {
        let color: any = '#' + ('00000' + (Math.random() * (1 << 24) | 0).toString(16)).slice(-6);

        let gradient: any = ctx.createLinearGradient(0, 0, 0, 500);
        gradient.addColorStop(0, color + 'af');
        gradient.addColorStop(1, color + '00');

        dsets = dsets.concat({
          label: element.nome,
          data: element.valores,
          borderWidth: 2,
          backgroundColor: gradient,
          borderColor: color+'ff',
          pointHoverRadius: 15
        });
      });

      this.dados = {
        labels: this.res.periodos,
        datasets: dsets
      }

      this.options = {
        hover: {
          mode: 'nearest',
          intersect: false
        },
        tooltips: {
          intersect: false,
          callbacks: {
            label: function (tooltipItems, data) {
              return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.yLabel.toLocaleString("pt");
            }
          }
        },
        responsive: true,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              precision: 0,
              callback: function (value, index, values) {
                return value.toLocaleString("pt");
              }
            }
          }]
        }
      }
    });
  }
}
