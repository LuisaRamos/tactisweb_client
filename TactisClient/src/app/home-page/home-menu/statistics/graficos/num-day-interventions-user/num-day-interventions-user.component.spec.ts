import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumDayInterventionsUserComponent } from './num-day-interventions-user.component';

describe('NumDayInterventionsUserComponent', () => {
  let component: NumDayInterventionsUserComponent;
  let fixture: ComponentFixture<NumDayInterventionsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumDayInterventionsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumDayInterventionsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
