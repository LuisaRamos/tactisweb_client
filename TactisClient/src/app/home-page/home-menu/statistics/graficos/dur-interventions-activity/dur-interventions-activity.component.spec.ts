import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurInterventionsActivityComponent } from './dur-interventions-activity.component';

describe('DurInterventionsActivityComponent', () => {
  let component: DurInterventionsActivityComponent;
  let fixture: ComponentFixture<DurInterventionsActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurInterventionsActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurInterventionsActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
