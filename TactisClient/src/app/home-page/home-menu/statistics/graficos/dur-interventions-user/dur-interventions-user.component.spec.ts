import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurInterventionsUserComponent } from './dur-interventions-user.component';

describe('DurInterventionsUserComponent', () => {
  let component: DurInterventionsUserComponent;
  let fixture: ComponentFixture<DurInterventionsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurInterventionsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurInterventionsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
