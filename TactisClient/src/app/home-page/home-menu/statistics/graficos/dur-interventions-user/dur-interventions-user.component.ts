import { Component, OnInit, OnDestroy } from '@angular/core';
import { GetStatisticsService } from '../../service/get-statistics.service';
import { Router } from '@angular/router';
import { barStructure } from '../../barStructure';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dur-interventions-user',
  templateUrl: './dur-interventions-user.component.html',
  styleUrls: ['./dur-interventions-user.component.css']
})
export class DurInterventionsUserComponent implements OnInit, OnDestroy {
  options: any;
  data: any;
  title: string;
  res: barStructure;
  level: number = 0;
  datas: string[];
  dateSubscription: Subscription;
  constructor(private router: Router, private getstatistics: GetStatisticsService) { }

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
  }

  ngOnInit() {
    this.dateSubscription = this.getstatistics.currentDates.subscribe(datas => {
      if (datas[0] != undefined && datas[1] != undefined) {
        this.level = 0;
        this.datas = datas;
        this.redraw(null);
      }
    });
  };

  back() {
    if (this.level) {
      this.title = undefined;
      this.level--;
      this.redraw(null);
    }
    else {
      this.router.navigate(['home/dashboard']);
    }
  }

  drilldown(event) {
    if (!this.level) {
      this.title = ' ' + this.res.names[event.element._index];
      this.level++;
      this.redraw(event);
    }
  }

  redraw(event) {
    if (!this.level) {
      this.getstatistics.getDurIntervencoesUser(this.datas[0], this.datas[1]).subscribe(res => {
        this.res = res;
        this.data = {
          labels: res.names,
          datasets: [
            {
              label: 'Intervenções',
              data: res.values,
              backgroundColor: 'rgb(255,154,0)',
              borderWidth: 2
            }
          ]
        };

        this.options = {
          hover: {
            mode: 'nearest',
            intersect: false,
            onHover: function (e) {
              var point = this.getElementAtEvent(e);
              if (point.length) e.target.style.cursor = 'pointer';
              else e.target.style.cursor = 'default';
            }
          },
          legend: {
            display: true
          },
          tooltips: {
            intersect: false,
            callbacks: {
              label: function (tooltipItems, data) {
                return data.datasets[tooltipItems.datasetIndex].label + ': ' + Math.floor(tooltipItems.xLabel / 60).toLocaleString("pt") + 'h ' + tooltipItems.xLabel % 60 + 'm';
              }
            }
          },
          responsive: true,
          maintainAspectRatio: res.names.length == 0,
          scales: {
            xAxes: [{
              ticks: {
                beginAtZero: true,
                precision: 0,
                callback: function (value, index, values) {
                  return value.toLocaleString("pt");
                }
              }
            }]
          }
        };

        if (res.names.length > 0) {
          document.getElementsByTagName("p-chart")[0].getElementsByTagName("div")[0].style.height = (res.names.length < 3 ? 180 : (res.names.length * 40 + 70)) + "px";
        }
      });
    }
    else {
      this.getstatistics.getDurIntervencoesByUserId(this.datas[0], this.datas[1], this.res.ids[event.element._index]).subscribe(res => {
        this.data = {
          labels: res.names,
          datasets: [
            {
              label: 'Intervenções',
              data: res.values,
              backgroundColor: 'rgb(255,154,0)',
              borderWidth: 2
            }
          ]
        };
        this.options = {
          hover: {
            mode: 'nearest',
            intersect: false,
            onHover: function (e) {
              e.target.style.cursor = 'default';
            }
          },
          legend: {
            display: true
          },
          tooltips: {
            intersect: false,
            callbacks: {
              label: function (tooltipItems, data) {
                return data.datasets[tooltipItems.datasetIndex].label + ': ' + Math.floor(tooltipItems.xLabel / 60).toLocaleString("pt") + 'h ' + tooltipItems.xLabel % 60 + 'm';
              }
            }
          },
          responsive: true,
          maintainAspectRatio: res.names.length == 0,
          scales: {
            xAxes: [{
              ticks: {
                beginAtZero: true,
                precision: 0,
                callback: function (value, index, values) {
                  return value.toLocaleString("pt");
                }
              }
            }]
          }
        };

        if (res.names.length > 0) {
          document.getElementsByTagName("p-chart")[0].getElementsByTagName("div")[0].style.height = (res.names.length < 3 ? 180 : (res.names.length * 40 + 70)) + "px";
        }
      });
    }

  }
}
