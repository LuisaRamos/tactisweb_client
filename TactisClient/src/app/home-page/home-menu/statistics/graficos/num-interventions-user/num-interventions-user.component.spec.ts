import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumInterventionsUserComponent } from './num-interventions-user.component';

describe('NumInterventionsUserComponent', () => {
  let component: NumInterventionsUserComponent;
  let fixture: ComponentFixture<NumInterventionsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumInterventionsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumInterventionsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
