import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumHourInterventionsTotalComponent } from './num-hour-interventions-total.component';

describe('NumHourInterventionsTotalComponent', () => {
  let component: NumHourInterventionsTotalComponent;
  let fixture: ComponentFixture<NumHourInterventionsTotalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumHourInterventionsTotalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumHourInterventionsTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
