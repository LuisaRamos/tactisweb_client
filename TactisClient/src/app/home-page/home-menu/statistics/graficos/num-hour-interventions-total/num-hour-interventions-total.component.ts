import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GetStatisticsService } from '../../service/get-statistics.service';
import { totalStrucutre } from '../../totalStructure';

@Component({
  selector: 'app-num-hour-interventions-total',
  templateUrl: './num-hour-interventions-total.component.html',
  styleUrls: ['./num-hour-interventions-total.component.css']
})
export class NumHourInterventionsTotalComponent implements OnInit, OnDestroy {

  dateSubscription: Subscription;
  data: any;
  res: totalStrucutre;
  options: any;
  dados: any;

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
  }

  constructor(private getstatistics: GetStatisticsService) { }

  ngOnInit() {
    this.dateSubscription = this.getstatistics.currentDate.subscribe(data => {
      if (data != undefined) {
        this.data = data;
        this.redraw(null);
      }
    });
  }


  redraw(event) {
    this.getstatistics.getNumHourIntervencoesTotais(this.data).subscribe(res => {
      this.res = res;

      this.dados = {
        labels: this.res.periodos,
        datasets: [
          {
            label: "Intervenções",
            data: this.res.valores,
            backgroundColor: 'rgba(66,134,244,1)'
          }
        ]
      }

      this.options = {
        hover: {
          mode: 'nearest',
          intersect: false
        },
        tooltips: {
          intersect: false,
          callbacks: {
            label: function (tooltipItems, data) {
              return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.yLabel.toLocaleString("pt");
            }
          }
        },
        responsive: true,
        scales: {
          xAxes: [{
            stacked: true
          }],
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
              precision: 0,
              callback: function (value, index, values) {
                return value.toLocaleString("pt");
              }
            }
          }]
        }
      }
    });
  }

}
