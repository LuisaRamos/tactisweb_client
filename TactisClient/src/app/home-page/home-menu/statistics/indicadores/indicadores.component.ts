import { Component, OnInit, OnDestroy } from '@angular/core';
import { GetStatisticsService } from '../service/get-statistics.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-indicadores',
  templateUrl: './indicadores.component.html',
  styleUrls: ['./indicadores.component.css']
})
export class IndicadoresComponent implements OnInit, OnDestroy {

  res: any;
  dateSubscription: Subscription;

  h1: number;
  h2: number;
  m1: number;
  m2: number;
  constructor(private router : Router, private getstatistics: GetStatisticsService) { }

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
  }

  ngOnInit() {
    this.dateSubscription = this.getstatistics.currentDates.subscribe(datas => {
      if (datas[0] != undefined && datas[1] != undefined) {
        this.getstatistics.getKpis(datas[0], datas[1]).subscribe(res => {
          this.res = res;
          this.h1 = Math.floor(this.res[1].valor / 60);
          this.m1 = this.res[1].valor % 60;
          this.h2 = Math.floor(this.res[3].valor / 60);
          this.m2 = this.res[3].valor % 60;
        });
      }
    });
  }

  back() {
      this.router.navigate(['home']);
  }

}
