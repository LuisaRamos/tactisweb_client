import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsView2DashboardComponent } from './stats-view2-dashboard.component';

describe('StatsView2DashboardComponent', () => {
  let component: StatsView2DashboardComponent;
  let fixture: ComponentFixture<StatsView2DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsView2DashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsView2DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
