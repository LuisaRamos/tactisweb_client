import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { registerLocaleData, formatDate } from '@angular/common';
import localePT from "@angular/common/locales/pt";
import { GetStatisticsService } from '../service/get-statistics.service';

@Component({
  selector: 'app-stats-view2-dashboard',
  templateUrl: './stats-view2-dashboard.component.html',
  styleUrls: ['./stats-view2-dashboard.component.css']
})
export class StatsView2DashboardComponent implements OnInit {

  pt: any;
  date:Date;
  constructor(private router:Router, private getstatistics:GetStatisticsService) { }

  ngOnInit() {
    registerLocaleData(localePT, "pt");
    this.pt = {
      firstDayOfWeek: 1,
      dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
      dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
      dayNamesMin: ["D", "2ª", "3ª", "4ª", "5ª", "6ª", "S"],
      monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      today: 'Hoje',
      clear: 'Limpar',
      dateFormat: 'dd/mm/yy',
      weekHeader: 'Sem'
    }
    this.date = new Date();
    const start = formatDate(this.date, 'dd/MM/yyyy', 'pt');
    this.getstatistics.changeData(start);
  }

  changeViewToRange(){
    this.router.navigate(['home/dashboard']);
  }

  back() {
    if (this.router.url.match('dashboarddsm/')) {
      this.router.navigate(['home/dashboarddsm']);
    }
    else {
      this.router.navigate(['home']);
    }
  }

  dateChange() {
    if (this.date != undefined) {
      const start = formatDate(this.date, 'dd/MM/yyyy', 'pt');
      this.getstatistics.changeData(start);
    }
  }
}
