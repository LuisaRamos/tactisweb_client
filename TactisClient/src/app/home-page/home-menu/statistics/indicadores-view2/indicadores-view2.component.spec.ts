import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadoresView2Component } from './indicadores-view2.component';

describe('IndicadoresView2Component', () => {
  let component: IndicadoresView2Component;
  let fixture: ComponentFixture<IndicadoresView2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadoresView2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadoresView2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
