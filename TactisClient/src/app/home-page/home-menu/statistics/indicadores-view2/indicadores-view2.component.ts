import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { GetStatisticsService } from '../service/get-statistics.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-indicadores-view2',
  templateUrl: './indicadores-view2.component.html',
  styleUrls: ['./indicadores-view2.component.css']
})
export class IndicadoresView2Component implements OnInit, OnDestroy {

  res: any;
  dateSubscription: Subscription;


  constructor(private router : Router, private getstatistics: GetStatisticsService) { }

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
  }

  ngOnInit() {
    this.dateSubscription = this.getstatistics.currentDate.subscribe(data => {
      if (data != undefined) {
        this.getstatistics.getKpisDsm(data).subscribe(res => {
          this.res = res;
        });
      }
    });
  }

  back() {
      this.router.navigate(['home']);
  }

}
