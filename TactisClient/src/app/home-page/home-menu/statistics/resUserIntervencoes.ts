import { userIntervencoes } from './userIntervencoes';

export interface resUserIntervencoes{
    periodos: string[];
    userInts: userIntervencoes[];
}