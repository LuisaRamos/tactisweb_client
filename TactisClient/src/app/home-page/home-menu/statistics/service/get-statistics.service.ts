import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { kpi } from '../kpi';
import { GenericService } from 'src/app/services/generic.service';
import { AuthService } from 'src/app/services/auth.service';
import { barStructure } from '../barStructure';
import { totalStrucutre } from '../totalStructure';

@Injectable({
  providedIn: 'root'
})
export class GetStatisticsService extends GenericService {

  // private start = new BehaviorSubject([]);
  // currentStart = this.start.asObservable();

  // private finish = new BehaviorSubject([]);
  // currentFinish = this.finish.asObservable();

  // changeDatas(s: string[],f: string[]) {
  //   this.start.next(s);
  //   this.finish.next(f);
  // }

  private dates = new BehaviorSubject([]);
  currentDates = this.dates.asObservable();

  changeDatas(startfinish: string[]) {
    this.dates.next(startfinish);
  }

  private date = new BehaviorSubject(undefined);
  currentDate = this.date.asObservable();

  changeData(novadata: string) {
    this.date.next(novadata);
  }

  constructor(httpClient: HttpClient, private http: HttpClient, auth: AuthService) {
    super("statistics/", httpClient, auth);
  }

  getNumIntervencoesUser(start: string, finish: string): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish);
    return this.http.get<barStructure>(this.url + "numIntervencoesUser", { params: params, headers: this.generateHeaders() });
  }

  getDurIntervencoesUser(start: string, finish: string): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish);
    return this.http.get<barStructure>(this.url + "durIntervencoesUser", { params: params, headers: this.generateHeaders() });
  }

  getNumIntervencoesActivity(start: string, finish: string): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish);
    return this.http.get<barStructure>(this.url + "numIntervencoesActivity", { params: params, headers: this.generateHeaders() });
  }

  getDurIntervencoesActivity(start: string, finish: string): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish);
    return this.http.get<barStructure>(this.url + "durIntervencoesActivity", { params: params, headers: this.generateHeaders() });
  }

  getKpis(start: string, finish: string): Observable<any> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish);
    return this.http.get<any>(this.url + "kpis", { params: params, headers: this.generateHeaders() });
  }

  getNumIntervencoesByUserId(start: string, finish: string, id: number): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish)
      .set('id', id.toString());
    return this.http.get<barStructure>(this.url + "numIntervencoesByUserId", { params: params, headers: this.generateHeaders() });
  }

  getDurIntervencoesByUserId(start: string, finish: string, id: number): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish)
      .set('id', id.toString());
    return this.http.get<barStructure>(this.url + "durIntervencoesByUserId", { params: params, headers: this.generateHeaders() });
  }

  getNumIntervencoesByActivityId(start: string, finish: string, id: number): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish)
      .set('id', id.toString());
    return this.http.get<barStructure>(this.url + "numIntervencoesByActivityId", { params: params, headers: this.generateHeaders() });
  }

  getDurIntervencoesByActivityId(start: string, finish: string, id: number): Observable<barStructure> {
    let params = new HttpParams()
      .set('start', start)
      .set('finish', finish)
      .set('id', id.toString());
    return this.http.get<barStructure>(this.url + "durIntervencoesByActivityId", { params: params, headers: this.generateHeaders() });
  }

  // =================== VIEW 2 ===================

  getKpisDsm(start: string): Observable<any> {
    let params = new HttpParams()
      .set('start', start);
    return this.http.get<any>(this.url + "kpisdsm", { params: params, headers: this.generateHeaders() });
  }

  getNumDayIntervencoesTotais(start: string): Observable<totalStrucutre> {
    let params = new HttpParams()
      .set('start', start);
    return this.http.get<totalStrucutre>(this.url + "numDayIntervencoesTotais", { params: params, headers: this.generateHeaders() });
  }

  getNumDayIntervencoesUser(start: string): Observable<any> {
    let params = new HttpParams()
      .set('start', start);
    return this.http.get<any>(this.url + "numDayIntervencoesUser", { params: params, headers: this.generateHeaders() });
  }

  getNumMonIntervencoesTotais(start: string): Observable<totalStrucutre> {
    let params = new HttpParams()
      .set('start', start);
    return this.http.get<totalStrucutre>(this.url + "numMonIntervencoesTotais", { params: params, headers: this.generateHeaders() });
  }

  getNumMonIntervencoesUser(start: string): Observable<any> {
    let params = new HttpParams()
      .set('start', start);
    return this.http.get<any>(this.url + "numMonIntervencoesUser", { params: params, headers: this.generateHeaders() });
  }

  getNumHourIntervencoesTotais(start: string): Observable<any> {
    let params = new HttpParams()
      .set('start', start);
    return this.http.get<any>(this.url + "numHourIntervencoesTotais", { params: params, headers: this.generateHeaders() });
  }
}
