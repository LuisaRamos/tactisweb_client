import { TestBed } from '@angular/core/testing';

import { GetStatisticsService } from './get-statistics.service';

describe('GetStatisticsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetStatisticsService = TestBed.get(GetStatisticsService);
    expect(service).toBeTruthy();
  });
});
