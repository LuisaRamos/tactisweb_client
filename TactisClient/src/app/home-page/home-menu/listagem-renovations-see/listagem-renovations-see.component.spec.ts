import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemRenovationsSeeComponent } from './listagem-renovations-see.component';

describe('ListagemRenovationsSeeComponent', () => {
  let component: ListagemRenovationsSeeComponent;
  let fixture: ComponentFixture<ListagemRenovationsSeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemRenovationsSeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemRenovationsSeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
