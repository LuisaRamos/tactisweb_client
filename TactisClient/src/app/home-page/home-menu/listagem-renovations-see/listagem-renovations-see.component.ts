import { Component, OnInit } from '@angular/core';
import { RenovationView } from 'src/app/util/view/RenovationView';
import { ContractService } from 'src/app/services/contract.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { DateTimeFormatPipe } from 'src/app/util/DateTimeFormatPipe';
import { Router } from '@angular/router';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-listagem-renovations-see',
  templateUrl: './listagem-renovations-see.component.html',
  styleUrls: ['./listagem-renovations-see.component.css']
})
export class ListagemRenovationsSeeComponent implements OnInit {

  renovacoesAluguer: RenovationView[] = [];
  renovacoesClientInit: RenovationView[] = [];
  renovacoesClientRenFidRein: RenovationView[] = [];

  aluguerLoading: boolean = true;
  outrosLoading: boolean = true;
  initLoading: boolean = true;
  cols: any[];
  colsren: any[];
  openHelp: boolean = false;

  constructor(
    private contractService: ContractService,
    private errorService: HandleErrorComponent,
    private router: Router) { }

  ngOnInit() {
    this.cols = [
      { field: 'client', header: 'Cliente' },
      { field: 'contractNumber', header: 'Nº Contrato' },
      { field: 'changeCode', header: 'Código' },
      { field: 'dateAte', header: 'Até' }
    ];
    this.colsren = [
      { field: 'client', header: 'Cliente' },
      { field: 'local', header: 'Local' },
      { field: 'contractNumber', header: 'Nº Contrato' },
      { field: 'changeCode', header: 'Código' },
      { field: 'dateAte', header: 'Até' }
    ];
    this.getRenovacoesAluguer();
    this.getRenovacoesClientInit();
    this.getRenovacoesOutros();
  }

  getRenovacoesAluguer() {
    this.contractService.getListRenovationsAluguer().subscribe(res => {
      this.renovacoesAluguer = <any[]>res;
      this.aluguerLoading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter a listagem de renovações de alugueres", error);
    })
  }

  getRenovacoesClientInit() {
    this.contractService.getListRenovationsClienteInicial().subscribe(res => {
      this.renovacoesClientInit = <RenovationView[]>res;
      this.initLoading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter a listagem de renoações de clientes iniciais", error);
    })
  }

  getRenovacoesOutros() {
    this.contractService.getListRenovationsclientFidRenReint().subscribe(res => {
      this.renovacoesClientRenFidRein = <RenovationView[]>res;
      this.outrosLoading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter a listagem de renovações de clientes renovados, fidelizados e reintegrados.", error);
    })
  }

  // -------- FICHEIROS PDF

  aluguerToPDFArray(): any[] {
    let rows = [];
    this.renovacoesAluguer.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.dateAte, "YYYY-MM-DD");
      rows.push([element.client.name, element.local, element.contractNumber, element.changeCode, date]);
    });
    return rows;
  }

  iniciaisToPDFArray(): any[] {
    let rows = [];
    this.renovacoesClientInit.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.dateAte, "YYYY-MM-DD");
      rows.push([element.client.name, element.local, element.contractNumber, element.changeCode, date]);
    });
    return rows;
  }

  outrosToPDFArray(): any[] {
    let rows = [];
    this.renovacoesClientRenFidRein.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.dateAte, "YYYY-MM-DD");
      rows.push([element.client.name, element.local, element.contractNumber, element.changeCode, date]);
    });
    return rows;
  }

  exportPdfAluguer() {
    let rows = this.aluguerToPDFArray();
    let doc = new jsPDF('l', 'pt');
    doc.autoTable(this.colsren, rows);
    doc.save('renovacoes_aluguer.pdf');
  }

  exportPdfIniciais() {
    let rows = this.iniciaisToPDFArray();
    let doc = new jsPDF('l', 'pt');
    doc.autoTable(this.colsren, rows);
    doc.save('renovacoes_iniciais.pdf');
  }

  exportPdfOutros() {
    let rows = this.outrosToPDFArray();
    let doc = new jsPDF('l', 'pt');
    doc.autoTable(this.colsren, rows);
    doc.save('renovacoes_outros.pdf');
  }


  // -------- FICHEIROS EXCEL

  iniciaisToExcelFormat(): any[] {
    let rows = [];
    this.renovacoesClientInit.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.dateAte, "YYYY-MM-DD");
      rows.push({ Nome: element.client.name, 'Local':element.local, 'Nº Contrato': element.contractNumber, 'Status': element.changeCode, 'Até': date });
    });
    return rows;
  }

  aluguerToExcelFormat(): any[] {
    let rows = [];
    this.renovacoesAluguer.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.dateAte, "YYYY-MM-DD");
      rows.push({ Nome: element.client.name, 'Local':element.local, 'Nº Contrato': element.contractNumber, 'Status': element.changeCode, 'Até': date });
    });
    return rows;
  }

  OUTROSToExcelFormat(): any[] {
    let rows = [];
    this.renovacoesClientRenFidRein.forEach(element => {
      let date = new DateTimeFormatPipe().transform(element.dateAte, "YYYY-MM-DD");
      rows.push({ Nome: element.client.name, 'Local':element.local, 'Nº Contrato': element.contractNumber, 'Status': element.changeCode, 'Até': date });
    });
    return rows;
  }


  exportExcel() {

    const worksheet = XLSX.utils.json_to_sheet(this.aluguerToExcelFormat());
    const worksheet2 = XLSX.utils.json_to_sheet(this.iniciaisToExcelFormat());
    const worksheet3 = XLSX.utils.json_to_sheet(this.OUTROSToExcelFormat());
    const workbook = { Sheets: { 'Aluguer': worksheet, 'Iniciais': worksheet2, 'Renovacoes': worksheet3 }, SheetNames: ['Aluguer', 'Iniciais', 'Renovacoes'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, "Renovacoes");

  }

  saveAsExcelFile(buffer: any, fileName: string): void {

    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_' + new Date().getTime() + EXCEL_EXTENSION);

  }

  back() {
    this.router.navigate(["home"]);
  }

}
