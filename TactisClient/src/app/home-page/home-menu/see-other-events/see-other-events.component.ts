import { Component, OnInit } from '@angular/core';
import {Event} from 'src/app/model/event';
import { TactisUser } from 'src/app/model/tactisuser';
import { EventAndUsers } from 'src/app/util/view/eventAndUsers';
import { TrainingService } from 'src/app/services/training.service';
import { UserService } from 'src/app/services/user.service';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MessageService } from 'primeng/api';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-see-other-events',
  templateUrl: './see-other-events.component.html',
  styleUrls: ['./see-other-events.component.css']
})
export class SeeOtherEventsComponent implements OnInit {

  users: TactisUser[] = [];
  data: EventAndUsers[];
  activeUser: TactisUser;
  display: boolean = false;
  selectedEvent: Event;
  newInstDate: Date = new Date();
  values: TactisUser[] = [];

  loading: boolean = true;
  dataLoaded:boolean = false;

  constructor(
    private userService: UserService,
    private calendarService: CalendarEventsService,
    private router: Router,
    private auth: AuthService,
    private messageService: MessageService,
    private errorService:HandleErrorComponent
  ) {
  }

  ngOnInit() {
    this.getAllNotDoneEvents();
    this.getActiveUsers();
  }

  getAllNotDoneEvents() {
    this.calendarService.getAllOtherEventsAssignedAndNotDone().subscribe(data => {

      this.data = <EventAndUsers[]>data;
      this.loading = false;
      this.dataLoaded = true;
    }, error => {
      this.errorService.handleError("Não foi possível obter os eventos.", error);
    });
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.users = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores.", error);
    });
  }

  confirmEditedTraining() {

    if(this.values.length < 1){
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção!',
        detail: 'Tem de atribuir a pelo menos uma pessoa.',
        sticky: false,
        life: 1000
      });
      return;
    }

    this.selectedEvent.beginDate = this.newInstDate;
    let end2: Date = new Date(this.selectedEvent.beginDate);
    end2.setHours(end2.getHours() + 1);

    let trainingEndDate: Date = new Date(end2);
    this.selectedEvent.endDate = trainingEndDate;

    let eau:EventAndUsers = new EventAndUsers(this.selectedEvent, this.values);

    this.calendarService.updateOtherEvent(this.selectedEvent.eventId, eau).subscribe(res => {
        this.messageService.add({
        severity: 'success',
          summary: 'Sucesso',
          detail: 'Dados do evento alterados',
          sticky: false,
          life: 1000
      })
      this.display = false;
      this.getAllNotDoneEvents();
    })
  }
  
  editSchedule(data:EventAndUsers) {
    this.values = [];
  
      data.users.forEach(element1 => {
        this.users.forEach(element2 => {
          if (element1.username == element2.username) {
            this.values.push(element2);
            return;
          }
        });
      })

    this.display = true;
    this.selectedEvent = data.event;
    this.newInstDate = new Date(data.event.beginDate);
  }


  back() {
    this.router.navigate(['home']);
  }
}
