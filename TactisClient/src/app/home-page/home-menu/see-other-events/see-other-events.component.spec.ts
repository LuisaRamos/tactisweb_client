import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeOtherEventsComponent } from './see-other-events.component';

describe('SeeOtherEventsComponent', () => {
  let component: SeeOtherEventsComponent;
  let fixture: ComponentFixture<SeeOtherEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeOtherEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeOtherEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
