import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradesEditComponent } from './upgrades-edit.component';

describe('UpgradesEditComponent', () => {
  let component: UpgradesEditComponent;
  let fixture: ComponentFixture<UpgradesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
