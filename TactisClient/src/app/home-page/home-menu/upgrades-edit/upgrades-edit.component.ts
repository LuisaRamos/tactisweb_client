import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Upgrade } from 'src/app/model/upgrade';
import { UpgradeService } from 'src/app/services/upgrade.service';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-upgrades-edit',
  templateUrl: './upgrades-edit.component.html',
  styleUrls: ['./upgrades-edit.component.css'],
  providers: [ConfirmationService]
})
export class UpgradesEditComponent implements OnInit {

  @Input() up:Upgrade;
  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  constructor(
    private upgradeService:UpgradeService,
    private confirmationService: ConfirmationService,
    private messageService:MessageService
    ) { }

  ngOnInit() {
  }

  confirmDelete() {
    

    this.confirmationService.confirm({
      message: 'Tem a certeza que pretende apagar esta versão?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.upgradeService.deleteUpgrade(this.up.upgradeId).subscribe(res => {
          console.log(res);
          this.tellParent();
        }, error => {
          this.tellParent();
        });
        
      },
      reject: () => {
        
      }
    });
  }

  save(){
    if (this.up.appTag.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher o nome',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }

    if (this.up.versao.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher a versão',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }

    if (this.up.checksum.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher o checksum',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }

    // if (this.url_db.trim().length < 1) {
    //   this.messageService.add({
    //     severity: 'warn',
    //     summary: 'Atenção',
    //     detail: 'Deve preencher o URL DB',
    //     sticky: false,
    //     life: 2000
    //   });
    //   //mensagem preencher nome
    //   return;
    // }

    if (this.up.urlApp.trim().length < 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Deve preencher o URL App',
        sticky: false,
        life: 2000
      });
      //mensagem preencher nome
      return;
    }
    
    this.upgradeService.updateUpgrade(this.up).subscribe(res => {
      this.tellParent();
    })
  }

  tellParent(){
    this.close.emit(false);
  }
}
