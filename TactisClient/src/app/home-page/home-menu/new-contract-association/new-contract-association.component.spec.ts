import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewContractAssociationComponent } from './new-contract-association.component';

describe('NewContractAssociationComponent', () => {
  let component: NewContractAssociationComponent;
  let fixture: ComponentFixture<NewContractAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewContractAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewContractAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
