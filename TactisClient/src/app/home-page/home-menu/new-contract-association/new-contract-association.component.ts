import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { Contract } from 'src/app/model/contract';
import { ContractService } from 'src/app/services/contract.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ClientService } from 'src/app/services/client.service';
import { Client } from 'src/app/model/client';

@Component({
  selector: 'app-new-contract-association',
  templateUrl: './new-contract-association.component.html',
  styleUrls: ['./new-contract-association.component.css']
})
export class NewContractAssociationComponent implements OnInit {
  clients:Client[];
  codes: CodeDescription[] = [];
  typesLoaded: boolean = false;
  clientsLoaded:boolean = false;
  selectedClient:Client;
 
  selectedType:CodeDescription;

  numbReg: RegExp = /[0-9]+$/;

  @Input() contract:Contract;
  @Output() cli: EventEmitter<Client> = new EventEmitter<Client>();

  constructor(
    private contractService:ContractService,
    private errorService:HandleErrorComponent,
    private clientService:ClientService
  ) { }

  ngOnInit() {
    this.getCodes();
    this.getClients();
  }

  getCodes() {
    this.contractService.getContractCodes().subscribe(res => {
      this.codes = <CodeDescription[]>res;
      this.typesLoaded = true;
    }, error => {
      this.errorService.handleError("Não foi possível obter os códigos de contrato.", error);
    })
  }

  getClients(){
    this.clientService.getClients().subscribe(res => {
      this.clients = <Client[]> res;
      this.clientsLoaded = true;
    })
  }

  tellParent(){
    this.cli.emit(this.selectedClient);
  }

  putIt(){
    this.contract.contractType = this.selectedType.descr;
  }

}