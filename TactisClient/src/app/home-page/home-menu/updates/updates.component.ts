import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService, TreeNode, SelectItem } from 'primeng/api';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { Clinic } from 'src/app/model/clinic';
import { InstallationService } from 'src/app/services/installation.service';
import { InstallationClinics } from 'src/app/util/view/installationClinics';
import { ContractService } from 'src/app/services/contract.service';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { Installation } from 'src/app/model/installation';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Client } from 'src/app/model/client';
import { ClinicService } from 'src/app/services/clinic.service';
import { ContractChanges } from 'src/app/model/contractchanges';
import { TreeTable } from 'primeng/treetable';
import { UpdateService } from 'src/app/services/update.service';
import { InstallationLastContractChange } from 'src/app/util/view/installationLastContractChange';

@Component({
  selector: 'app-updates',
  templateUrl: './updates.component.html',
  styleUrls: ['./updates.component.css']
})
export class UpdatesComponent implements OnInit {

  selectedNodes: InstallationLastContractChange[] = [];
  allInst: TreeNode[] = [];
  cols: any;
  knowsIt: boolean = false;
  installations: TreeNode[];
  codes: SelectItem[] = [];
  toSearch: string;
  updateNG: boolean = false;
  updatePEM: boolean = false;
  updateTU: boolean = false;
  updateINF: boolean = false;
  selectedQty: number;
  selectedDB: string;
  titleDB: string;
  selectedClinic: Clinic;
  selectedClient: Client;
  selectedContract: ContractChanges;
  displayClinica: boolean = false;
  displayContract: boolean = false;
  loading: boolean = true;
  randomNR: number = 2;
  selectedInstallation: Installation;
  dividas: SelectItem[] = [
    { label: 'Não importa', value: null },
    { label: 'Em dívida', value: 'false' },
    { label: 'Não tem dívida', value: 'true' }];

  filtrados: SelectItem[] = [
    { label: 'Não', value: false },
    { label: 'Sim', value: true }
  ];
  perm: SelectItem[] = [
    { label: 'Não importa', value: null },
    { label: 'Não permite atualizações', value: false },
    { label: 'Permite atualizações', value: true }
  ];

  selectedFilt: boolean = false;
  allSel: boolean = false;
  permField: string = "";

  @ViewChild('tt') treeTable: TreeTable;

  constructor(
    private router: Router,
    private installationService: InstallationService,
    private messageService: MessageService,
    private contractService: ContractService,
    private errorService: HandleErrorComponent,
    private clinicService: ClinicService,
    private updateService: UpdateService) { }

  ngOnInit() {
    this.randomNR = this.randomInt(0, 4);
    if (this.router.url.match("ng") != null) {
      this.updateNG = true;
      this.selectedDB = "ng";
      this.titleDB = "Novigest";
      this.permField = "installation.ngAllowed";
    } else if (this.router.url.match("pem") != null) {
      this.updatePEM = true;
      this.selectedDB = "pem";
      this.titleDB = "Novipem";
      this.permField = "installation.pemAllowed";
    } else if (this.router.url.match("tu") != null) {
      this.updateTU = true;
      this.selectedDB = "tu";
      this.titleDB = "Tactis Updater";
      this.permField = "installation.tuAllowed";
    } else if (this.router.url.match("inf") != null) {
      this.updateINF = true;
      this.selectedDB = "inf";
      this.titleDB = "Infarmed";
      this.permField = "installation.bdinfarmedAllowed";
    }

    this.cols = [
      { field: 'name', header: 'Nome' },
      { field: 'version', header: 'Versão' },
      { field: 'contract', header: 'Contrato' },
      { field: 'divida', header: 'Dívida' }
    ];
    setTimeout(res => {
      this.getInstallations(this.selectedDB);
    }, 500);
    this.getContractCodes();
  }

  /**
   * Credit: https://gist.github.com/ValeryToda/fbf1de017f91c0ec3da04116c5ccf8b5
   * @param min 
   * @param max 
   */
  randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  getInstallations(bd: string) {
    this.installationService.getAllInstallationsAndItsClinics(bd).subscribe(res => {
      this.installations = <TreeNode[]>res;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações", error);
    });
    this.installationService.getAllInstallationsAndItsClinics(bd).subscribe(res => {
      this.allInst = <TreeNode[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações", error);
    });
  }

  getContractCodes() {
    this.contractService.getContractAllCodes().subscribe(res => {
      let codesDescr: CodeDescription[] = <CodeDescription[]>res;
      codesDescr.forEach(element => {
        if (!element.code.match("DOC") && !element.code.match("UPG")) {
          this.codes.push({ label: element.code, value: element.code });
        }

      })
      // this.codes.push({ label: "Sem dados", value: '' });
    }, error => {
      this.errorService.handleError("Não foi possível obter os tipos de contrato", error);
    });
  }

  search() {
    this.installationService.getInstallationsAndItsClinicsWithKeyword(this.toSearch, this.selectedDB).subscribe(res => {
      this.installations = [];
      this.installations = <TreeNode[]>res;

    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações", error);
    });
  }

  refresh() {
    if (this.toSearch != null || this.toSearch != undefined) {
      this.getInstallations(this.selectedDB);
      return;
    } else {
      this.selectedNodes = [];
    }
  }

  back() {
    this.router.navigate(['home']);
  }

  all() {
    this.installations = this.allInst;
  }

  selectChange(event, rowNode) {
    if (event) {
      this.select(rowNode, 0);
    }
    else {
      this.unselect(rowNode, 0);
    }
  }

  selectAll(event) {
    if (event) {

      if (this.treeTable.filteredNodes == undefined) {
        this.selectedNodes = [];
        this.installations.forEach(element => {
          element.data.isSelected = true;
          this.select(element, 1);
        })

      } else {
        this.treeTable.filteredNodes.forEach(element => {
          let index = this.installations.findIndex(inst => (inst.data == element.data));
          this.installations[index].data.isSelected = true;
          this.select(this.installations[index], 1);
        });
      }

    } else {

      if (this.treeTable.filteredNodes == undefined) {
        this.selectedNodes = [];
        this.installations.forEach(element => {
          element.data.isSelected = false;
          this.unselect(element, 1);
        })

      } else {
        this.treeTable.filteredNodes.forEach(element => {
          let index = this.installations.findIndex(inst => (inst.data == element.data));
          this.installations[index].data.isSelected = false;
          this.unselect(this.installations[index], 1);
        });
      }
    }

    this.checkBigCheckBox();

  }

  checkBigCheckBox() {
    if (this.treeTable.filteredNodes != undefined) {
      if (this.selectedNodes.length == this.treeTable.filteredNodes.length) {
        this.allSel = true;
      } else {
        this.allSel = false;
      }
    } else {
      if (this.selectedNodes.length == this.installations.length) {
        this.allSel = true;
      } else {
        this.allSel = false;
      }
    }

  }

  select(row, n: number) {
    if (n == 0) {
      this.selectedNodes.push(row.node.data);
    } else {
      this.selectedNodes.push(row.data);
    }

    this.checkBigCheckBox();
  }

  unselect(row, n: number) {
    if (n == 0) {
      let index: number = this.selectedNodes.findIndex(inst => (inst == row.node.data));
      this.selectedNodes.splice(index, 1);
    } else {
      let index: number = this.selectedNodes.findIndex(inst => (inst == row.data));
      this.selectedNodes.splice(index, 1);
    }

    this.checkBigCheckBox();

  }

  save() {
    let selected: Installation[] = [];
    this.selectedNodes.forEach(element => {
      selected.push(element.installation);
    });
    this.updateService.changeAllowUpdate(this.selectedDB, selected).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'As permissões foram atualizadas',
        sticky: false,
        life: 2000
      });

      this.getInstallations(this.selectedDB);
      this.selectedNodes = [];

    }, error => {
      this.errorService.handleError("Não foi possivel atualizar as permissões", error);
    })
  }

  openClinicInfo(rowData, rowNode) {
    this.selectedClinic = rowData;
    this.clinicService.getClientOfClinic(this.selectedClinic.clinicId).subscribe(res => {
      this.selectedClient = <Client>res;
      this.displayClinica = true;
    });
  }

  openInstallationInfo(rowData) {
    this.selectedContract = rowData.lastChange;
    this.selectedInstallation = rowData.installation;
    this.selectedClinic = rowData.installation.clinicList[0];
    this.displayContract = true;
  }

}
