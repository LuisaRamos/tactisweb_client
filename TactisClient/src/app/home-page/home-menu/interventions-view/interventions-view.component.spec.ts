import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterventionsViewComponent } from './interventions-view.component';

describe('InterventionsViewComponent', () => {
  let component: InterventionsViewComponent;
  let fixture: ComponentFixture<InterventionsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterventionsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterventionsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
