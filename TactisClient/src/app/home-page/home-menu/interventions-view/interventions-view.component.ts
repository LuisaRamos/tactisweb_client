import { Component, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { InterventionService } from 'src/app/services/intervention.service';
import { Intervention } from 'src/app/model/intervention';
import { AuthService } from 'src/app/services/auth.service';
import { Route, Router } from '@angular/router';
import { MessageService, SortEvent } from 'primeng/api';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-interventions-view',
  templateUrl: './interventions-view.component.html',
  styleUrls: ['./interventions-view.component.css']
})
export class InterventionsViewComponent implements OnInit {

  interventions:Intervention[];
  cols: any[];
  selectedColumns: any[];
  display: boolean = false;
  selectedIntervention: Intervention;
  screenWidth: any;
  showButton: boolean = true;


  @Input() cid:number;

  @Output() nrInterventions: EventEmitter<number> = new EventEmitter<number>();
  
  constructor(
    private interventionService:InterventionService,
    private errorService:HandleErrorComponent) {
      this.getScreenSize();
     }

  ngOnInit() {
    this.cols = [
      { field: 'productActivity', header: 'Prod-Act' },
      { field: 'interventionType', header: 'Tipo' },
      { field: 'tactisUserId', header: 'Utilizador' },
      { field: 'interventionDate', header: 'Data' },
      { field: 'interlocutor', header: 'Interlocutor' },
      { field: 'notes', header: 'Notas' }
    ];
    this.selectedColumns = this.cols;
    this.getInterventionsOfClinic();
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenWidth = window.innerWidth;
    if (this.screenWidth > 1000) {
      this.showButton = false;
    } else {
      this.showButton = true;
    }
  }

  getInterventionsOfClinic() {
    if (this.cid != -1) {
      this.interventionService.getInterventionOfClinic(this.cid).subscribe(res => {
        this.interventions = <Intervention[]>res;
        this.tellParent();
      }, error => {
        this.errorService.handleError("Não foi possível obter as intervenções.", error);
      });
    }
    else {
      this.interventions = [];
      this.tellParent();
    }
  }

  tellParent(){
    this.nrInterventions.emit(this.interventions.length);
  }

  onRowSelect(event) {
    this.display = true;
  }

  showDialog(index) {
    this.selectedIntervention = index;
    this.display = true;
  }

  customSort(event: SortEvent) {

    event.data.sort((data1, data2) => {

      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else if (typeof value1 === 'object' && typeof value2 === 'object') {
        if (value1.tactisUserId != null && value1.tactisUserId != undefined) {
          result = value1.username.localeCompare(value2.username);
        } else if (value1.interventionTypeId != null && value1.interventionTypeId != undefined) {
          result = value1.description.localeCompare(value2.description);
        } else if (value1.productActivityId != null && value1.productActivityId != undefined) {
          let prod1 = value1.product + " - " + value1.activity;
          let prod2 = value2.product + " - " + value2.activity;
          result = prod1.localeCompare(prod2);
        }
      } else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

      return (event.order * result);
    });
  }
}
