import { Component, OnInit, Input } from '@angular/core';
import { NovigestSliderDb } from 'src/app/model/novigestSliderDb';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-novigest-activation-new',
  templateUrl: './novigest-activation-new.component.html',
  styleUrls: ['./novigest-activation-new.component.css']
})
export class NovigestActivationNewComponent implements OnInit {

  anamnese: SelectItem[] = [
    { label: 'Sim', value: true },
    { label: 'Não', value: false },
    { label: 'Sem dados', value: null}
  ];

  aluguerMensal: SelectItem[] = [
    { label: 'Sim', value: true },
    { label: 'Não', value: false },
    { label: 'Sem dados', value: null}
  ];

  aluguerAnual: SelectItem[] = [
    { label: 'Sim', value: true },
    { label: 'Não', value: false },
    { label: 'Sem dados', value: null}
  ];

  venda: SelectItem[] = [
    { label: 'Sim', value: true },
    { label: 'Não', value: false },
    { label: 'Sem dados', value: null}
  ];

  usarMac: SelectItem[] = [
    { label: 'Sim', value: true },
    { label: 'Não', value: false },
    { label: 'Sem dados', value: null}
  ];

  ativar: SelectItem[] = [
    { label: 'Sim', value: true },
    { label: 'Não', value: false },
    { label: 'Sem dados', value: null}
  ];

  @Input() viewOnly: boolean;
  @Input() ng: NovigestSliderDb;

  constructor() { }

  ngOnInit() {
    console.log(this.ng)
  }

  changeMensal(event){
    if(event.value == true){
      this.ng.aluguerAnual = false;
      this.ng.venda = false;
    }
  }
  changeAnual(event){
    if(event.value == true){
      this.ng.aluguerMensal = false;
      this.ng.venda = false;
    }
  }
  changeVenda(event){
    if(event.value == true){
      this.ng.aluguerMensal = false;
      this.ng.aluguerAnual = false;
    }
  }

}
