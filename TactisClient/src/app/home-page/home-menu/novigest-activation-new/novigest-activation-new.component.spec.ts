import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovigestActivationNewComponent } from './novigest-activation-new.component';

describe('NovigestActivationNewComponent', () => {
  let component: NovigestActivationNewComponent;
  let fixture: ComponentFixture<NovigestActivationNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovigestActivationNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovigestActivationNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
