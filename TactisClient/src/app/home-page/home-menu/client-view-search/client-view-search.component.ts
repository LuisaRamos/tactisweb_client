import { Component, OnInit, ɵConsole, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ClientService } from 'src/app/services/client.service';
import { ClientClinics } from 'src/app/util/view/clientClinics';
import { Clinic } from 'src/app/model/clinic';
import { MessageService, TreeNode, ConfirmationService } from 'primeng/api';
import { Client } from 'src/app/model/client';
import { AuthService } from 'src/app/services/auth.service';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { TreeTable } from 'primeng/treetable';
import { Paginator } from 'primeng/paginator';

@Component({
  selector: 'app-client-view-search',
  templateUrl: './client-view-search.component.html',
  styleUrls: ['./client-view-search.component.css']
})
export class ClientViewSearchComponent implements OnInit {

  clientClinics: ClientClinics[] = [];
  all: ClientClinics[];
  selectedClinic: Clinic;
  selectedNode: TreeNode;
  cols: any;
  ttlOcorr: number;
  toSearch: string;
  wasSearched: boolean = false;
  userSubscription: Subscription;
  loading: boolean = true;
  actual_role: string;

  //auths
  canCreateNewClient: boolean;
  canDoContract: boolean;
  canDeletePotencial:boolean;


  editOnlyClient: boolean = false;
  selectedClient: Client;
  editOnlyClinic: boolean = false;

  @ViewChild('tt') tt:TreeTable;
  @ViewChild('p') paginator: Paginator;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private clientService: ClientService,
    private auth: AuthService,
    private previousRouteService: PreviousRouteService,
    private confirmationService:ConfirmationService,
    private errorService: HandleErrorComponent
  ) {
  }

  ngOnInit() {
    this.actual_role = this.auth.getRole();
    this.canCreateNewClient = this.actual_role == 'ADMINISTRATIVE' || this.actual_role == 'COMERCIAL' || this.actual_role == 'ADMIN';
    this.canDoContract = this.actual_role == 'ADMINISTRATIVE' || this.actual_role == 'COMERCIAL' || this.actual_role == 'ADMIN';
    this.canDeletePotencial = this.actual_role == 'ADMINISTRATIVE' || this.actual_role == 'COMERCIAL' || this.actual_role == 'ADMIN';
    this.findWithOrWithoutKey();
  }

  doButton(event){
    // console.log("paginator");
    // console.log(this.paginator);
    this.tt.reset();
  }

  findWithOrWithoutKey() {
    
    this.clientClinics = [];
    let keyword: string;

    this.route.params.subscribe((params: Params) => {
      keyword = <string>params.keyword;
      //se for uma route limpa
      if (keyword == undefined || keyword == null) {
        this.getClientsAndClinics();
        this.createColumns();
        this.loading = false;
        return;
      }
      if (keyword.length == 0) {
        this.getClientsAndClinics();
        this.createColumns();
        this.loading = false;
        return;
      }
      this.findClientsWithKeyword(keyword);

      this.createColumns();
      this.wasSearched = true;
      this.loading = false;
    });
  }

  createColumns() {
    this.cols = [
      { field: 'name', header: 'Nome', width: '50%' },
      { field: 'address.addressLocation', header: 'Localização', class: 'priority-2' },
      { field: 'contact.phone1', header: 'TEL1', class: 'priority-2' },
      { field: 'contact.phone2', header: 'TEL2', class: 'priority-3' },
      { field: 'contact.phone3', header: 'TEL3', class: 'priority-3' }
    ];
  }

  //------------ BTN
  private _fixed = true;

  public open = true;
  public spin = true;
  public direction = 'down';
  public animationMode = 'fling';

  get fixed(): boolean {
    return this._fixed;
  }

  set fixed(fixed: boolean) {
    this._fixed = fixed;
    if (this._fixed) {
      this.open = true;
    }
  }

  public stopPropagation(event: Event): void {
    // Prevent the click to propagate to document and trigger
    // the FAB to be closed automatically right before we toggle it ourselves
    event.stopPropagation();
  }

  public doAction(event: any) {
    console.log(event);
  }

  isClient(): boolean {
    return this.selectedNode.data.clientId != undefined /*|| this.selectedNode.data.clinic == undefined*/;
  }

  isPotencial(): boolean {
    return this.selectedNode.children.length == 0 /*|| this.selectedNode.data.clinic == undefined*/;
  }

  isClinic(): boolean {
    return this.selectedNode.data.clientId == undefined /*|| this.selectedNode.data.clinic.clinicId != undefined*/;
  }

  do(event) {
    console.log(event)
    if(event.node.data.address != null){
      event.node.expanded = !event.node.expanded;
      this.clientClinics = [...this.clientClinics];
    }
    this.open = true;
    // (<TreeNode> event.node).droppable = true ;

  }

  onRowDblclick(event){
    console.log('double click')
  }

  do2(event) {
    console.log(event)
  }

  public newIntervention() {
    let clinic: number = this.selectedNode.data.clinic.clinicId;
    this.router.navigate(['home/clinic/' + clinic + '/intervention/new']);
  }

  public seeDetails() {
    //se só tiver selecionado cliente e tiver na mesma clinicas, so quero mostrar o cliente
    if (this.isClient()) {
      let client: number = this.selectedNode.data.clientId;
      this.router.navigate(['home/clients/info/' + client]);
      return;
    } else if (this.isClinic()) {
      let clinic: number = this.selectedNode.data.clinic.clinicId;
      this.router.navigate(['home/clinic/' + clinic]);
      return;
    }
  }

  public access() {
    if (this.isClinic()) {
      let clinic: number = this.selectedNode.data.clinic.clinicId;
      this.router.navigate(['home/clinic/' + clinic + '/access']);
      return;
    }
  }

  public newContract() {
    let client: number = this.selectedNode.data.clientId;
    this.router.navigate(['home/clients/' + client + '/contract/new'])
  }

  newEmptyContract() {
    this.router.navigate(['home/contract/new'])
  }

  public newClient() {
    this.router.navigate(['home/clients/new']);
  }

  newAditamento(){
    let client: number = this.selectedNode.data.clientId;
    this.router.navigate(['home/clients/' + client + '/aditamento/new'])
  }

  // -----------------


  getClientsAndClinics() {
    this.clientService.getClientandItsClinics().subscribe(data => {
      let got: ClientClinics[] = <ClientClinics[]>data;
      this.clientClinics = [];
      got.forEach(element => {
        this.clientClinics.push(new ClientClinics(element.data, element.children, element.expanded));
      })
      this.all = this.clientClinics;
    }, error => {
      this.errorService.handleError("Não foi possível obter os clientes/clínicas", error);
    });
  }

  findClientsWithKeyword(words: string) {
    this.clientService.getClientsWithKeyword(words).subscribe(res => {
      let result: ClientClinics[] = <ClientClinics[]>res;
      //this.searched = [];
      this.clientClinics = [];
      result.forEach(element => {
        this.clientClinics.push(new ClientClinics(element.data, element.children));
      })
    }, error => {
      this.errorService.handleError("Não foi possível obter os clientes/clínicas", error);
    });
  }

  deletePotencial(){
    this.confirmationService.confirm({
      message: 'Tem a certeza que quer apagar este potencial cliente?',
      header: 'Confirmação',
      key: 'deletepot',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.clientService.delete(this.selectedNode.data.clientId).subscribe(res => {
          this.messageService.add({
            severity: 'success',
            summary: 'Sucesso',
            detail: 'O potencial cliente foi apagado.',
            sticky: false,
            life: 2000
          });
          this.getClientsAndClinics();
        }, error => {
          this.errorService.handleError("Não foi possível apagar o potencial cliente", error);
        })
      }
    });
  }

  clientHasDebt(rowData): number {
    let hasDebt: number = 0;
    if (rowData.node.children == null || rowData.node.children.length == 0) {
      hasDebt = -1;
      return hasDebt;
    }
    rowData.node.children.forEach(element => {
      if (element.data.divida == undefined || element.data.divida == null) {
        hasDebt = -1;
        return;
      }
      if (element.data.divida == "Sim") {
        hasDebt = 1;
        return;
      } else if (element.data.divida == "Não") {
        hasDebt = 0;
        return;
      }
    });
    return hasDebt;
  }

  editClient() {
    if (this.isClient()) {
      let client: Client = this.selectedNode.data;
      this.selectedClient = client;
      this.editOnlyClient = true;
      return;
    } else if (this.isClinic()) {
      let clinic: Clinic = this.selectedNode.data.clinic;
      this.selectedClinic = clinic;
      this.editOnlyClinic = true;
      return;
    }
  }

  /**
   * check error https://github.com/primefaces/primeng/issues/8044
   * @param event 
   */
  closeEditDialogs(event) {
    this.editOnlyClinic = false;
    this.editOnlyClient = false;
    this.findWithOrWithoutKey();
  }

  back() {
    if (this.wasSearched) {
      this.router.navigate(['home/byclient']);
      // } 
      // else if (this.previousRouteService.getPreviousUrl() != undefined){

    } else {
      this.router.navigate(['home']);
    }

  }
}
