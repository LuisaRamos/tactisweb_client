import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientViewSearchComponent } from './client-view-search.component';

describe('ClientViewSearchComponent', () => {
  let component: ClientViewSearchComponent;
  let fixture: ComponentFixture<ClientViewSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientViewSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientViewSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
