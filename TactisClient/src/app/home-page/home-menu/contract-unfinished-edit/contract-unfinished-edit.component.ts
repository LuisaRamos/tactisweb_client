import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ContractService } from 'src/app/services/contract.service';
import { Contract } from 'src/app/model/contract';
import { ContractUnfinishedView } from 'src/app/util/view/ContractUnfinishedView';
import { SelectItem, MessageService } from 'primeng/api';

@Component({
  selector: 'app-contract-unfinished-edit',
  templateUrl: './contract-unfinished-edit.component.html',
  styleUrls: ['./contract-unfinished-edit.component.css']
})
export class ContractUnfinishedEditComponent implements OnInit {

  isCAL: boolean = false;
  isCM: boolean = false;
  timesCM: SelectItem[];
  timesCAL: SelectItem[];

  codes: CodeDescription[] = [];
  typesLoaded: boolean = false;
  icon: string = "pi pi-question";
  hexColor: string = "grey";
  selectedTime: string;
  selectedType: CodeDescription;

  @Input() contractAte: Date;
  @Input() clientSince: Date;
  @Input() contract: ContractUnfinishedView;

  @Output() ca: EventEmitter<Date> = new EventEmitter<Date>();
  @Output() cs: EventEmitter<Date> = new EventEmitter<Date>();


  constructor(
    private errorService: HandleErrorComponent,
    private contractService: ContractService,
    private messageService:MessageService
  ) { }

  ngOnInit() {
    this.getCodes();
    this.timesCM = [
      { label: '', value: null },
      { label: '1 mês', value: "1M" },
      { label: '6 meses', value: "6M" },
      { label: '1 ano', value: "1A" }];
    this.timesCAL = [
      { label: '', value: null },
      { label: '1 mês', value: "1M" },
      { label: '6 meses', value: "6M" },
      { label: '1 ano', value: "1A" }];
     
  }

  getCodes() {
    this.contractService.getContractCodes().subscribe(res => {
      this.codes = <CodeDescription[]>res;
      let index: number = this.codes.findIndex((code) => code.descr == this.contract.contract.contractType);
      this.selectedType = this.codes[index];
      this.typesLoaded = true;
      if (this.selectedType.code.match("RENT")) {
        this.isCAL = true;
        this.isCM = false;
      } else {
        this.isCAL = false;
        this.isCM = true;
      }
    }, error => {
      this.errorService.handleError("Não foi possível obter os códigos de contrato.", error);
    })
  }

  calculateTime(){
    console.log("calculate time")
    if(this.clientSince!= null){
      this.contractAte = new Date(this.clientSince);
      if(this.selectedTime == "1M"){
        this.contractAte.setMonth(this.clientSince.getMonth() + 1);
      } else if ( this.selectedTime == "6M"){
        this.contractAte.setMonth(this.clientSince.getMonth() + 6);
      } else if (this.selectedTime == "1A"){
        this.contractAte.setMonth(this.clientSince.getMonth() + 12);
      }
    }
    else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Para calcular automaticamente o periodo, deve preencher o "Cliente Desde".',
        sticky: false,
        life: 3000
      });
    }
  }

  tellParentCA() {
    this.ca.emit(this.contractAte);
  }

  tellParentCS() {
    this.cs.emit(this.clientSince);
  }

  sugestNextCode() {
    this.contractService.getLastContractNumber(this.selectedType.code).subscribe(res => {
      let t: string = res;
      let tcut;
      let numbers: number;
      let code: string;
      if (this.selectedType.code.match("RENT")) {
        tcut = t.slice(3, 7);
        code = "CAL0";
        this.isCAL = true;
        this.isCM = false;
      } else {
        code = "CM0";
        this.isCAL = false;
        this.isCM = true;
        if (t.length > 5) {
          tcut = t.slice(2, 6);
        } else {
          tcut = t.slice(2, 5);
        }
      }
      numbers = +tcut;
      numbers += 1;
      code = code + numbers;
      this.contract.contract.contractNumber = code + "/0/00";
    });

  }


  clearCNbutton() {
    this.icon = "pi pi-question";
    this.hexColor = "grey"
  }


  clearContractNumber(event) {
    this.selectedType = event.value;
    this.contract.contract.contractNumber = "";
    this.icon = "pi pi-question";
    this.hexColor = "grey"
    this.isCAL = false;
    this.isCM = false;
  }

  checkValidAndExistsContractNumber(): boolean {
    if (!this.checkValidContractNumber()) {
      this.icon = "pi pi-times";
      this.hexColor = "red";
      return false;
    }

    let contractnr = this.contract.contract.contractNumber.split('/')[0];
    this.contractService.checkIfContractNumberExists(contractnr).subscribe(res => {
      let result: boolean = <boolean>res;
      if (result) {
        this.icon = "pi pi-times";
        this.hexColor = "red";
        return false;
      }
      else {
        this.icon = "pi pi-check";
        this.hexColor = "green";
        return true;
      }
    })
  }

  checkValidContractNumber(): boolean {
    let tcut;
    let codeCut;
    let t = this.contract.contract.contractNumber;
    if (this.selectedType.code.match("RENT")) {
      codeCut = t.slice(0, 3);
      tcut = t.slice(4, this.contract.contract.contractNumber.length);
      let num: number = <number>tcut.length;
      if (codeCut == "CAL") {
        if (num > 0) {
          return true;
        }
      }
      return false;

    } else {
      codeCut = t.slice(0, 2);
      tcut = t.slice(3, this.contract.contract.contractNumber.length);
      let num: number = <number>tcut.length;
      if (codeCut == "CM") {
        if (num > 0) {
          return true;
        }
      }
      return false;
    }
  }


}
