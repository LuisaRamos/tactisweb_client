import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractUnfinishedEditComponent } from './contract-unfinished-edit.component';

describe('ContractUnfinishedEditComponent', () => {
  let component: ContractUnfinishedEditComponent;
  let fixture: ComponentFixture<ContractUnfinishedEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractUnfinishedEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractUnfinishedEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
