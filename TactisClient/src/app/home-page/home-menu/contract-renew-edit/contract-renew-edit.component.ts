import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ContractService } from 'src/app/services/contract.service';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { ClinicService } from 'src/app/services/clinic.service';
import { InstallationService } from 'src/app/services/installation.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ContractChanges } from 'src/app/model/contractchanges';
import { CodeDescription } from 'src/app/util/view/codeAndDescription';
import { Contract } from 'src/app/model/contract';

@Component({
  selector: 'app-contract-renew-edit',
  templateUrl: './contract-renew-edit.component.html',
  styleUrls: ['./contract-renew-edit.component.css']
})
export class ContractRenewEditComponent implements OnInit {


  newContract: boolean = false;
  editDebt: boolean = false;
  changecolsA: any[];
  changecolsB: any[];
  changes: ContractChanges[];

  contractDate: Date = new Date();
  ateDate: Date = new Date();
  newContractChange: ContractChanges;
  codes: CodeDescription[] = [];
  totaladded: number = 0;
  typesLoaded: boolean = false;
  changeDescr: string;
  changeCode: string;

  @Input() contract: Contract;
  @Input() toRenew: boolean;

  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() askContract: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private router: Router,
    private contractService: ContractService,
    private previousRouteService: PreviousRouteService,
    private messageService: MessageService,
    private errorService: HandleErrorComponent,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    console.log(this.toRenew)
    if (this.toRenew == true) {
      this.newContract = true;
      this.editDebt = false;
      this.totaladded++;
      this.getCodes();
    }
    else {
      this.newContract = false;
      this.getCodes();
      this.editDebt = true;
    }

    this.changecolsA = [
      { field: 'changeDatadoc', header: 'Data' },
      { field: 'changeCode', header: 'Código' },
      { field: 'changeDescr', header: 'Descrição' },
      { field: 'changeDate', header: 'Até' },
      // { field: 'changeNormalized', header: 'Dívida' }
    ];

    this.changecolsB = [
      { field: 'changeDatadoc', header: 'Data' },
      { field: 'changeCode', header: 'Código' },
      { field: 'changeDescr', header: 'Descrição' },
      { field: 'changeDate', header: 'Até' }
    ];

    this.getContractChanges();
  }

  getCodes() {
    this.contractService.getContractAllCodes().subscribe(res => {
      this.codes = <CodeDescription[]>res;
      this.typesLoaded = true;
    })
  }

  renovarContract() {
    this.newContractChange = new ContractChanges(null, "", "", false, null, new Date(), this.contract);
    this.changes.reverse();
    this.changes.push(this.newContractChange);
    this.changes.reverse();
    this.totaladded++;
  }

  changeCodeMethod(event, rowData, i) {

    rowData.changeDescr = event.value;
    this.codes.forEach(element => {
      if (rowData.changeDescr != null) {
        if (rowData.changeDescr.code == null) {
          if (event.value.code == undefined) {
          } else {
            let descr: String = event.value.descr;
            rowData.changeDescr = descr;
            if (descr.match(element.descr)) {
              rowData.changeCode = element.code;
            }
          }

        } else {
          rowData.changeCode = (<CodeDescription>event.value).code;
          rowData.changeDescr = (<CodeDescription>event.value).descr;
        }

      }
    });
  }

  getContractChanges() {
    this.contractService.getChangesOfContract(this.contract.contractId).subscribe(res => {
      this.changes = [];
      this.changes = <ContractChanges[]>res;
      console.log(this.changes)
      if (this.newContract) {
        this.newContractChange = new ContractChanges(null, "", "", false, null, new Date(), this.contract);
        // this.newContractChange = new ContractChanges(null, "", "", false, null, new Date(), this.changes[0].contractId);
        this.changes.reverse();
        this.changes.push(this.newContractChange);
        this.changes.reverse();

        //sugestao
        //var b = [...this.changes].reverse();
        //this.changes.reverse();
      }
    }, error => {

      this.errorService.handleError("Não foi possível obter as mudanças do contrato.", error);
    });
  }

  value(rowData, a: string): boolean {
    if (a == 'input') {
      if (rowData.changeNormalized) {
        return false;
      }
      else {
        return true;
      }
    }
    if (a == 'output') {
      if (rowData.changeNormalized) {
        //rowData.changeNormalized = false;
        return false;
      }
      else {
        // rowData.changeNormalized = true;
        return true;
      }
    }
  }

  value2(normalized: boolean, rowData) {
    if (normalized) {
      rowData.changeNormalized = false;
      rowData.normalizedDate = new Date();
    } else {
      rowData.changeNormalized = true;
      rowData.normalizedDate = new Date();
    }
  }

  deleteContractChange(rowData) {

    this.confirmationService.confirm({
      message: 'Tem a certeza que quer apagar? O passo é irreversível.',
      header: 'Confirmação',
      key: 'deletecc',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        if(rowData.contractChangeId != undefined){
          this.contractService.deleteContractChange(rowData.contractChangeId).subscribe(res => {
            this.getContractChanges();
            this.messageService.add({
              severity: 'success',
              summary: 'Sucesso',
              detail: 'A linha foi removida.',
              sticky: false,
              life: 1200
            });
          }, error => {
            this.errorService.handleError("Não foi possível eliminar esta linha", error);
          })
          this.tellParent(true);
        } else {
          let index: number = this.changes.findIndex(change => (change == rowData));
          this.changes.splice(index, 1);
        }
        
      },
      reject: () => {
        this.messageService.add({
          severity: 'info',
          summary: 'Atenção',
          detail: 'A operação foi cancelada.',
          sticky: false,
          life: 1200
        });
      }
    })
  }

  saveStatusChanges() {

    if (!this.checkNewChangeFields()) {
      console.log("fez isto")
      return;
    }

    this.contractService.updateContract(this.contract.contractId, this.contract).subscribe(res => {

    }, error => {
      this.errorService.handleError("Não foi possível guardar as alterações", error);
    })

    this.contractService.updateContractChanges(this.changes[0].contractId.contractId, this.changes).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'As alterações foram gravadas.',
        sticky: false,
        life: 1200
      });

      console.log("going to tell papa")
      this.tellParent(true);

    }, error => {

      this.errorService.handleError("Não foi possível guardar as alterações", error);
    });

  }

  revertChanges() {
    this.changes = [];
    this.getContractChanges();
    this.askContract.emit(true);
    this.close.emit(false);
  }

  checkNewChangeFields(): boolean {
    let ret: boolean = true;
    this.changes.forEach(element => {
      if (element.changeDate == null || element.changeDate == undefined) {
        this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Deve preencher a Data Até.', sticky: false, life: 1200 });
        console.log("vai return false 1")
        ret = false;
      }
      if (element.changeDatadoc == null || element.changeDatadoc == undefined) {
        this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Deve preencher a Data.', sticky: false, life: 1200 });
        console.log("vai return false 2")
        ret = false;
        return false;
      }
      if (element.changeDescr == null || element.changeDescr == undefined) {
        this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Deve escolher uma descrição.', sticky: false, life: 1200 });
        console.log("vai return false 3")
        ret = false;
        return false;
      }
      if (element.changeDescr.trim().length < 1) {
        this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Deve escolher uma descrição.', sticky: false, life: 1200 });
        console.log("vai return false 4")
        ret = false;
        return false;
      }
      if (element.changeCode == null || element.changeCode == undefined) {
        this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Deve escolher uma descrição.', sticky: false, life: 1200 });
        console.log("vai return false 5")
        ret = false;
        return false;
      }
      if (element.changeCode.trim().length < 1) {
        this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Deve escolher uma descrição.', sticky: false, life: 1200 });
        console.log("vai return false 6")
        ret = false;
        return false;
      }
    });
    if (this.contract.contractNumber.trim().length < 1) {
      this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Deve preencher o número de contrato.', sticky: false, life: 1200 });
      console.log("vai return false 7")
      ret = false;
      return false;
    }
    if(ret){
      return true;
    } else {
      return false;
    }
  }

  back() {
    let prevUrl = this.previousRouteService.getPreviousUrl();
    if (prevUrl == null || prevUrl == undefined) {
      this.router.navigate(["home/byclient"]);
      return;
    }
    if (prevUrl == this.router.url) {
      this.router.navigate(["home/byclient"]);
      return;
    }
    this.router.navigate([prevUrl]);
  }

  tellParent(value) {
    this.close.emit(value);
  }

  something() {

  }

}
