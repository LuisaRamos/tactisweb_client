import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractRenewEditComponent } from './contract-renew-edit.component';

describe('ContractRenewEditComponent', () => {
  let component: ContractRenewEditComponent;
  let fixture: ComponentFixture<ContractRenewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractRenewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractRenewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
