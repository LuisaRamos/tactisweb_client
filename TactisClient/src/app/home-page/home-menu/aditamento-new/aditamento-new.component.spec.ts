import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AditamentoNewComponent } from './aditamento-new.component';

describe('AditamentoNewComponent', () => {
  let component: AditamentoNewComponent;
  let fixture: ComponentFixture<AditamentoNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AditamentoNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AditamentoNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
