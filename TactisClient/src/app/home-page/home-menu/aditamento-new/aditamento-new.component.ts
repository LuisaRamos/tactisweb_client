import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/model/client';
import { ActivatedRoute, Router } from '@angular/router';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { ClinicService } from 'src/app/services/clinic.service';
import { Clinic } from 'src/app/model/clinic';
import { ClientService } from 'src/app/services/client.service';
import { Contract } from 'src/app/model/contract';
import { Aditamento } from 'src/app/model/aditamento';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';
import { ContractService } from 'src/app/services/contract.service';
import { Contact } from 'src/app/model/contact';
import { Address } from 'src/app/model/address';
import { MessageService, ConfirmationService } from 'primeng/api';
import { AditamentoService } from 'src/app/services/aditamento.service';
import { of } from 'rxjs';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { AditamentoCreate } from 'src/app/util/view/aditamentocreate';

@Component({
  selector: 'app-aditamento-new',
  templateUrl: './aditamento-new.component.html',
  styleUrls: ['./aditamento-new.component.css']
})
export class AditamentoNewComponent implements OnInit {

  client: Client;
  loading: boolean = true;
  resumo: any[][] = []; //,0 - nivel ; 1- tipo ; 2 - nome ; 3- PP; 4- PA

  clinics: Clinic[];
  clinicLoading: boolean = true;
  clinicsCopy: Clinic[] = [];
  apagar: [number, string][] = []; //index da clinicsCopy
  selectedClinic1: Clinic;
  selectedClinic: Clinic;

  contractsLoaded: boolean = false;
  contracts: Contract[] = []; //contratos do cliente
  selectedContract: Contract;

  aditamento: Aditamento;
  aditamentos: Aditamento[] = []; //um aditamento por clinica, se for 0,0 deve ser ignorado

  installationsLoaded:boolean;
  installations: Installation[] = [];
  selectedInstallation: Installation;

  associatePlease: boolean = false;
  previousChanges: Clinic;

  confirmPlease: boolean = false;

  displayContactDialog: boolean;
  displayAddressDialog: boolean;
  showContact: boolean = false;
  showAddress: boolean = false;

  numbReg: RegExp = /[0-9]+$/;
  openHelp: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private errorService: HandleErrorComponent,
    private clinicService: ClinicService,
    private clientService: ClientService,
    private contractService: ContractService,
    private router: Router,
    private installationService: InstallationService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private aditamentoService: AditamentoService,
    private previousRouteService: PreviousRouteService) { }

  ngOnInit() {
    this.loading = true;
    this.aditamento = new Aditamento(0, 0, null, null, "", "", "",false, false, false, false, 0, 0, null);
    let id: number;
    this.route.params.subscribe(res => {
      id = <number>res.id;
    });
    if (id != null || id != undefined) {
      this.getClient(id);
    }

    this.getPotencialClinics();
  }

  getPotencialClinics() {
    // get potencial clinics
    this.clinicService.getPotencialClinics().subscribe(res => {
      this.clinics = <Clinic[]>res;
      this.clinicLoading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter os potenciais clientes.", error);
    });
    this.clinicService.getPotencialClinics().subscribe(res => {
      this.clinicsCopy = <Clinic[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os potenciais clientes.", error);
    });
  }

  getClient(id: number) {
    this.clientService.getClient(id).subscribe(res => {
      this.client = <Client>res;
      this.loading = false;
      this.clientService.getClientContracts(id).subscribe(res => {
        this.contracts = <Contract[]>res;
        this.contractsLoaded = true;
        if (this.contracts.length == 1) {
          this.selectedContract = this.contracts[0];
          this.findInstallationsOfSelectedContract();
        }
      }, error => {
        this.errorService.handleError("Não foi possível obter os contratos do cliente.", error);
      })
    }, error => {
      this.errorService.handleError("Não foi possível obter o cliente.", error);
    });
  }

  findInstallationsOfSelectedContract() {
    this.contractService.getContractInstallations(this.selectedContract.contractId).subscribe(res => {
      this.installations = <Installation[]>res;
      this.installations.forEach(installation => {
        installation.clinicList.forEach(clinic => {
          clinic.mainPostos = 0;
          clinic.addPostos = 0;
          //isto é feito apenas nas clinicas existentes para caso se verifique que têm estes números alterados, juntar ao aditamento
        });
      });
      this.installationsLoaded = true;
      if (this.installations.length == 1) {
        this.selectedInstallation = this.installations[0];
      }
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações do contrato.", error);
    })
  }

  removeInstallation() {
    this.confirmationService.confirm({
      message: 'Tem a certeza que quer apagar? As clínicas não serão guardadas.',
      header: 'Confirmação',
      key: 'deleteinst',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        let index: number = this.installations.findIndex(ins => (ins == this.selectedInstallation));
        let ready: boolean = false;
        while (this.selectedInstallation.clinicList.length != 0) {
          ready = this.removeClinic(this.selectedInstallation.clinicList[0]);
        }
        this.installations.splice(index, 1);
        this.selectedInstallation = undefined;
        this.installations = [...this.installations];
      }
    });
  }

  addClinic() {
    //coloca um id temporario, de acordo com o index da lista
    let c: Clinic = new Clinic("", /*this.clientSince*/ null, "", "", new Address("", "", "", ""), new Contact("", "", "", "", ""), 0, 0, new Date(), null);
    this.selectedInstallation.clinicList.push(c);
  }

  createInstallations() {
    let now = new Date();
    let installation_name = "dummy_" + "fakecontract" /*this.contract.contractNumber*/;
    let last: number;
    if (this.installations.length >= 1) {
      if (this.installations[this.installations.length - 1].installationName.split("_")[3] != undefined && this.installations[this.installations.length - 1].installationName.split("_")[3] != null) {
        console.log("split 3 != undefined")
        console.log(this.installations[this.installations.length - 1].installationName.split("_")[3])
        last = +this.installations[this.installations.length - 1].installationName.split("_")[3];
      }
      else {
        last = -1;
      }
    } else {
      last = -1;
    }

    for (let index = 0; index < this.aditamento.mainPostos; index++) {
      let num: number = last + 1 + index;
      let installation_name2 = installation_name + "_" + now.getTime() + "_" + num;
      let descr: string = "inst_" + new Date().getTime() + "_" + index;
      this.installations.push(new Installation(null, "", descr, [], [], [], installation_name2, "", false, false, false, false, false, false, false, false, false, false));
    }

    this.installations = [...this.installations];
    console.log(this.installations)
    this.selectedInstallation = this.installations[this.installations.length-1]
  }

  selectClinic(clinic: Clinic) {
    this.selectedClinic1 = clinic;
  }

  associateClinicToInstallation() {
    this.associatePlease = true;
  }

  /**
   * Para guardar a seleção + edição do potencial selecionado
   */
  associate() {

    let contact: Contact = new Contact(this.selectedClinic.contact.phone1, this.selectedClinic.contact.phone2, this.selectedClinic.contact.phone3,
      this.selectedClinic.contact.email, this.selectedClinic.contact.other);

    let address: Address = new Address(this.selectedClinic.address.address, this.selectedClinic.address.postcode1, this.selectedClinic.address.postcode2,
      this.selectedClinic.address.addressLocation);

    this.selectedClinic.contact = contact;
    this.selectedClinic.address = address;
    this.selectedClinic.clientSince = /*new Date(this.clientSince)*/ null;

    this.selectedInstallation.clinicList.push(this.selectedClinic);
    let i = this.clinicsCopy.findIndex(c => c == this.previousChanges);
    this.apagar.push([i, this.selectedInstallation.installationName]);


    let index: number = this.clinics.findIndex(clinica => (clinica == this.selectedClinic));
    this.deleteIndexOfClinicsArray(index, this.clinics);

    this.selectedClinic = undefined;
    this.associatePlease = false;

  }

  event(event) {
    let index = this.clinicsCopy.findIndex(clinic => clinic.name == (<Clinic>event.value).name);
    if (index == -1) {
      //this.getPotencialClinics();

      //e apagar as que nao tem interesse
    }
    this.previousChanges = this.clinicsCopy[index];
  }

  deleteIndexOfClinicsArray(index: number, array: Clinic[]) {
    array.splice(index, 1);
  }

  save() {
    if (this.seeResumo() == true) {
      this.confirmPlease = true;
    }
  }

  async confirmSave() {
    let lastInstallation: Installation;
    for await (let line of this.resumo) {
      if (line[0] == 1) { //is installation
        lastInstallation = line[1];
      }
      if (line[0] == 2) { //is clinic
        let descr: string = "adit_" + new Date().getTime();
        let aditamento: Aditamento = new Aditamento(line[1].mainPostos, line[1].addPostos, line[1], this.selectedContract, this.aditamento.proposta, "", descr, false, false, false, false, line[1].mainPostos + line[1].addPostos, line[1].mainPostos + line[1].addPostos,null);
        // check if main Postos > 1 
        // if(line[1].mainPostos > 0){
        //   this.installations.forEach(element => {
        //     if(element.installationName.includes("dummy")){
        //       let index = element.installationName.split("_")[3]
        //       let descr: string = "inst_" + new Date().getTime() + "_" + index;
        //       element.description = descr;
        //     }
        //   });
        // }
        if(lastInstallation.installationId != undefined){
          if (line[1].clinicId == undefined) {
            let createAdit:AditamentoCreate = new AditamentoCreate(aditamento, lastInstallation);
            await this.aditamentoService.createAditamento(createAdit).then(res => {
              console.log("guardou um");
            }, error => {
              this.errorService.handleError("Não foi possível registar o aditamento.", error);
            })
          } else {
            let createAdit:AditamentoCreate = new AditamentoCreate(aditamento);
            await this.aditamentoService.createAditamento(createAdit).then(res => {
              console.log("guardou um");
            }, error => {
              this.errorService.handleError("Não foi possível registar o aditamento.", error);
            })
          }
        } else {
          // NAO CRIA ADITAMENTO, ADICIONA INSTALACAO AO CONTRATO
          this.contractService.addInstallationToContract(this.selectedContract.contractId, lastInstallation).subscribe(res => {

          })
        }

        
      }
    }
    this.confirmPlease = false;
    this.messageService.add({
      severity: 'success',
      summary: 'Sucesso',
      detail: 'O registo de novo aditamento foi guardado. O processo passou para o Helpdesk.',
      sticky: false,
      life: 3000
    });
    setTimeout(res => {
      
      this.back();
      
    },2000)

  }

  seeResumo() {
    if (this.validarPostos() == false) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'O número de postos totais não coincide com o total atribuído por cada clínica.',
        sticky: false,
        life: 5000
      });
      return false;
    }

    if (this.tem0Postos() == true) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'O número de postos deve ser superior a 0.',
        sticky: false,
        life: 5000
      });
      return false;
    }

    let vaiadicionar: boolean = false;
    this.installations.forEach(installation => {

      installation.clinicList.forEach(clinic => {
        if (clinic.mainPostos != 0 || clinic.addPostos != 0) {
          this.resumo.push([2, clinic]);
          vaiadicionar = true;
        }
      });
      if (vaiadicionar == true) {
        this.resumo.push([1, installation]);
      }
      vaiadicionar = false;
    });
    this.resumo.push([0, "Contrato " + this.selectedContract.contractNumber + " | TOTAL POSTOS: " + this.aditamento.mainPostos + " PP + " + this.aditamento.addPostos + " PA"]);

    this.resumo.reverse();
    return true;
  }

  validarPostos(): boolean {
    let totalMain: number = this.aditamento.mainPostos;
    let totalAdd: number = this.aditamento.addPostos;
    let sumMain: number = 0;
    let sumAdd: number = 0;

    this.installations.forEach(installation => {
      installation.clinicList.forEach(clinic => {
        sumMain += clinic.mainPostos;
        sumAdd += clinic.addPostos;
      });
    });

    return totalMain == sumMain && totalAdd == sumAdd;
  }

  tem0Postos(): boolean {
    return this.aditamento.mainPostos <= 0 && this.aditamento.addPostos <= 0;
  }

  removeClinic(clinic: Clinic): boolean {


    //obter o index da clinica alterada na lista da instalaçao
    let index: number = this.selectedInstallation.clinicList.findIndex(clinica => (clinica == clinic));
    console.log("index da clinica a apagar " + index)
    // remover a clinica alterada da lista da instalaçao
    this.selectedInstallation.clinicList.splice(index, 1);

    let instElem: number[] = [];
    let apagarIx: number;
    for (let j = 0; j < this.apagar.length; j++) {
      if (this.apagar[j][1] == this.selectedInstallation.installationName) {
        instElem.push(this.apagar[j][0]);
      }
    }

    let id = instElem[index];
    if (id != undefined) {
      let clinica = this.clinicsCopy[id];
      this.clinics.push(clinica);

      for (let i = 0; i < this.apagar.length; i++) {
        if (this.apagar[i][1] == this.selectedInstallation.installationName &&
          this.apagar[i][0] == id) {
          apagarIx = i;
        }
      }
      this.apagar.splice(apagarIx, 1);
    }

    return true;
  }

  addContact(clinic: Clinic) {
    this.selectedClinic = clinic;
    if (this.selectedClinic.contact == null ||
      this.selectedClinic.contact == undefined) {
      this.selectedClinic.contact = new Contact("", "", "", "", ""/*, [], [clinic]*/);
    }

    this.displayContactDialog = true;
    this.showContact = true;
  }

  addAddress(clinic: Clinic) {
    this.selectedClinic = clinic;
    if (this.selectedClinic.address == null ||
      this.selectedClinic.address == undefined) {
      this.selectedClinic.address = new Address("", "", "", "");
    }

    this.displayAddressDialog = true;
    this.showAddress = true;
  }

  onContactDialogHide() {
    this.displayContactDialog = false;
  }

  onAddressDialogHide() {
    this.displayAddressDialog = false;
  }

  keepClientContact(clinic: Clinic) {
    clinic.contact = new Contact(this.client.contact.phone1, this.client.contact.phone2, this.client.contact.phone3, this.client.contact.email, this.client.contact.other);
    this.copiedDataToastMessage();
  }

  keepClientAddress(clinic: Clinic) {
    clinic.address = new Address(this.client.address.address, this.client.address.postcode1, this.client.address.postcode2, this.client.address.addressLocation);
    this.copiedDataToastMessage();
  }

  copiedDataToastMessage() {
    this.messageService.add({
      severity: 'success',
      summary: 'Sucesso',
      detail: 'Dados copiados do cliente!',
      sticky: false,
      life: 2000
    });
  }

  back() {
    let route: string = this.previousRouteService.getPreviousUrl();
    let now: string = this.router.url;
    if (route == null || route == undefined) {
      this.router.navigate(['home']);
    } else if (route == now) {
      this.router.navigate(['home']);
    }
    else {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    }

  }
}
