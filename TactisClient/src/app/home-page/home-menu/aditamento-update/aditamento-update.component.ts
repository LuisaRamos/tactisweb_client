import { Component, OnInit } from '@angular/core';
import { Aditamento } from 'src/app/model/aditamento';
import { MenuItem, MessageService, ConfirmationService } from 'primeng/api';
import { Client } from 'src/app/model/client';
import { InstallationService } from 'src/app/services/installation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { AuthService } from 'src/app/services/auth.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Installation } from 'src/app/model/installation';
import { Contract } from 'src/app/model/contract';
import { NovigestSliderDb } from 'src/app/model/novigestSliderDb';
import { AditamentoService } from 'src/app/services/aditamento.service';
import { ContractService } from 'src/app/services/contract.service';
import { ClinicService } from 'src/app/services/clinic.service';

@Component({
  selector: 'app-aditamento-update',
  templateUrl: './aditamento-update.component.html',
  styleUrls: ['./aditamento-update.component.css']
})
export class AditamentoUpdateComponent implements OnInit {

  aditamento: Aditamento;
  loading: boolean = true;
  items: MenuItem[];
  activeIndex: number = 0;
  id: number;
  cli: Client;
  clientLoaded: boolean = false;
  selectedPrograms: string[] = [];
  installation: Installation;
  oldName: string;
  event: Event;
  pendente: number = 0;
  openDialog: boolean = false;
  openHelp:boolean = false;
  numbReg: RegExp = /[0-9]+$/;

  constructor(
    private installationService: InstallationService,
    private route: ActivatedRoute,
    private calendarService: CalendarEventsService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private authSrv: AuthService,
    private errorService: HandleErrorComponent,
    private aditamentoService: AditamentoService,
    private contractService: ContractService,
    private clinicService: ClinicService) { }

  ngOnInit() {
    this.loading = true;
    this.items = [
      {
        label: 'Cliente',
        command: (event: any) => {
          this.activeIndex = 0;
        }
      },
      {
        label: 'Clínica',
        command: (event: any) => {
          this.activeIndex = 1;
        }
      },
      {
        label: 'Instalação',
        command: (event: any) => {
          this.activeIndex = 2;
        }
      }];
    this.getAditamentoInformation();
  }


  getAditamentoInformation() {
    this.route.params.subscribe(res => {
      this.id = <number>res.id;
    });
    this.aditamentoService.getById(this.id).subscribe(res => {
      this.aditamento = <Aditamento>res;
      this.getAditamentoClient();
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter a instalação", error);
    });
  }

  getAditamentoClient() {
    this.clinicService.getClientOfClinic(this.aditamento.clinicId.clinicId).subscribe(res => {
      this.cli = <Client>res;
      this.clientLoaded = true;
    });

    this.installationService.getInstallationOfClinic(this.aditamento.clinicId.clinicId).subscribe(res => {
      this.installation = <Installation>res;
    });
  }

  back() {
    this.router.navigate(["home/schedule/installations"]);
  }

  checkInstallationThings(): boolean {
    if (this.installation.installationName.trim().length == 0) {
      this.showInputError("Tem de escrever um nome para a instalação");
      return false;
    }

    if (this.installation.password.trim().length == 0) {
      this.showInputError("Tem de definir uma password para a instalação");
      return false;
    }

    // if(!this.checkInstallationName()){
    //   this.showInputError("O nome de instalação já existe. Deve escolher outro.");
    //   return false;
    // }
  }

  finishAD() {

    if(this.pendente < 0){
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: 'Não podem sobrar postos "negativos" para instalar...',
        sticky: false,
        life: 3000
      })
      return;
    }

    if(this.pendente > 0){
      let total = this.aditamento.mainPostos + this.aditamento.addPostos;
      if(this.pendente == total){
        this.save();
        return;
      } else if(this.pendente > total){
        this.messageService.add({
          severity: 'warn',
          summary: 'Atenção',
          detail: 'Não podem faltar mais postos dos que os previstos...',
          sticky: false,
          life: 3000
        })
        return;
      }
    }

    this.aditamento.pendente = this.pendente;
    this.aditamento.instalado = true;
    this.aditamento.conclusionDate = new Date();
    this.installationService.updateInstallation(this.installation.installationId, this.installation).subscribe(res => {

    }, error => {
      this.errorService.handleError("Não foi possível dar a instalação como concluída", error);
    })

    this.aditamentoService.updateAditamento(this.aditamento.aditamentoId, this.aditamento).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'A instalação foi concluída.',
        sticky: false,
        life: 2000
      })
      setTimeout(res => {
        this.back();
      }, 2000)

    }, error => {
      this.errorService.handleError("Não foi possível dar a instalação como concluída", error);
    })
  }

  save() {
    this.installationService.updateInstallation(this.installation.installationId, this.installation).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'A instalação foi guardada.',
        sticky: false,
        life: 2000
      })
      setTimeout(res => {
        this.back();
      }, 2000)

    }, error => {
      this.errorService.handleError("Não foi possível dar a instalação como concluída", error);
    })
  }


  showInputError(mensagem: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Atenção',
      detail: mensagem,
      sticky: false,
      life: 1500
    });
  }

}
