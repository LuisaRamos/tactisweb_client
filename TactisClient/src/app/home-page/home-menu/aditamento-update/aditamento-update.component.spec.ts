import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AditamentoUpdateComponent } from './aditamento-update.component';

describe('AditamentoUpdateComponent', () => {
  let component: AditamentoUpdateComponent;
  let fixture: ComponentFixture<AditamentoUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AditamentoUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AditamentoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
