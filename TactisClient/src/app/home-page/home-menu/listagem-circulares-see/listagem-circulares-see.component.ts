import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ContractService } from 'src/app/services/contract.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-listagem-circulares-see',
  templateUrl: './listagem-circulares-see.component.html',
  styleUrls: ['./listagem-circulares-see.component.css']
})
export class ListagemCircularesSeeComponent implements OnInit {

  openHelp:boolean = false;
  selectedCirc:string;
  cm:boolean;
  cminicial:boolean;
  aluguerano:boolean;
  aluguermes:boolean;
  potenciais:boolean;
  desiste:boolean;
  scm:boolean;
  fechou:boolean;
  loading:boolean;
  listagem:string[] = [];

  
  constructor(
    private router:Router,
    private messageService:MessageService,
    private contractService:ContractService,
    private errorService:HandleErrorComponent
  ) { }

  ngOnInit() {
  }

  back() {
    this.router.navigate(["home"]);
  }

  getListagem(){
    this.loading = true;

    this.contractService.getListagemCirculares(this.cm,this.cminicial,this.aluguerano,this.aluguermes,this.potenciais,this.desiste,this.scm, this.fechou).subscribe(res => {
      this.listagem = <string[]> res;
      this.loading = false;

    }, error => {
      this.errorService.handleError("Não foi possível obter a listagem", error);
    })
  }

  

}
