import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemCircularesSeeComponent } from './listagem-circulares-see.component';

describe('ListagemCircularesSeeComponent', () => {
  let component: ListagemCircularesSeeComponent;
  let fixture: ComponentFixture<ListagemCircularesSeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemCircularesSeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemCircularesSeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
