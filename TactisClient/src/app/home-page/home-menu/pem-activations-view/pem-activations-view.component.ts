import { Component, OnInit, ViewChild } from '@angular/core';
import { InstallationAndPemSlider } from 'src/app/util/view/InstallationAndPemSlider';
import { InstallationService } from 'src/app/services/installation.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { TreeNode, MessageService, ConfirmationService } from 'primeng/api';
import { PemSliderDb } from 'src/app/model/pemSliderDb';
import { TreeTable } from 'primeng/treetable';
import { Installation } from 'src/app/model/installation';
import { ActivatedRoute, Router } from '@angular/router';
import { PreviousRouteService } from 'src/app/services/previous-route.service';

@Component({
  selector: 'app-pem-activations-view',
  templateUrl: './pem-activations-view.component.html',
  styleUrls: ['./pem-activations-view.component.css'],
  providers: [ConfirmationService]
})
export class PemActivationsViewComponent implements OnInit {

  selectedNodes: PemSliderDb[] = [];
  data: TreeNode[] = [];
  loading: boolean = true;
  allPem: TreeNode[] = [];
  allCodPresc: number;
  selectedParentNode: Installation;
  selectedChildNode: PemSliderDb;
  toNew:boolean=false;

  displaySeeInstallation: boolean;
  displayEditInstallation: boolean;
  displaySeePem: boolean;
  displayAddNewPem1: boolean;
  displayAddNewPem2: boolean;
  displayEditPem: boolean;
  newPem: PemSliderDb;

  selectedInstallationToAdd: Installation;
  installations: Installation[];

  @ViewChild('tt') treeTable: TreeTable;

  // --- btn 
  private _fixed = false;

  public open = false;
  public spin = true;
  public direction = 'down';
  public animationMode = 'fling';

  get fixed(): boolean {
    return this._fixed;
  }

  set fixed(fixed: boolean) {
    this._fixed = fixed;
    if (this._fixed) {
      this.open = true;
    }
  }

  public stopPropagation(event: Event): void {
    // Prevent the click to propagate to document and trigger
    // the FAB to be closed automatically right before we toggle it ourselves
    event.stopPropagation();
  }

  // --- btn 

  constructor(
    private installationService: InstallationService,
    private errorService: HandleErrorComponent,
    private route: ActivatedRoute,
    private previousRouteService: PreviousRouteService,
    private router: Router,
    private messageService: MessageService,
    private confirmationService:ConfirmationService
  ) { }

  ngOnInit() {
    this.getData();
    this.getUnactivatedInstallations();
  }

  getData() {
    this.installationService.getPemSliders().subscribe(res => {
      this.data = <TreeNode[]>res;
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as ativações", error);
    })
    this.installationService.getPemSliders().subscribe(res => {
      this.allPem = <TreeNode[]>res;
      let array = <InstallationAndPemSlider[]>res;
      this.calculateTotalChildren(array);
    }, error => {
      this.errorService.handleError("Não foi possível obter as ativações", error);
    })
  }

  getUnactivatedInstallations() {
    this.installations = [];
    this.installationService.getInstallationsWithoutNovipemSlider().subscribe(res => {
      this.installations = <Installation[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações", error);
    });
  }

  doSelectParentNode(e) {
    this.selectedParentNode = e;
    this.selectedChildNode = undefined;
  }

  doSelectChildNode(e, node) {
    this.selectedChildNode = e;
    this.selectedParentNode = node.parent.data;
  }

  calculateTotalChildren(pems: InstallationAndPemSlider[]) {
    let count: number = 0;
    pems.forEach(element => {
      console.log(element.children.length);
      count += element.children.length;
    })
    this.allCodPresc = count;
  }

  select(event) {
    console.log(event);
    let ev = event.node;
    let p: PemSliderDb = new PemSliderDb(ev.maxusers, ev.codreceitas, ev.localprescricao, ev.activacaoinicio, ev.activacaofim, ev.activa, ev.id);
    this.selectedNodes.push(p);
  }

  unselect(event) {
    let index: number = this.selectedNodes.findIndex(pem => (pem == event.node));
    this.selectedNodes.splice(index, 1);
  }

  seeInstallation() {
    this.displaySeeInstallation = true;
  }
  seePem() {
    this.displaySeePem = true;
  }

  addInstallationPemActivation2() {
    this.displayAddNewPem2 = true;
    this.newPem = new PemSliderDb(0, "", "", null, null, false);
  }

  addInstallationPemActivation1() {
    this.displayAddNewPem1 = true;
    this.newPem = new PemSliderDb(0, "", "", null, null, false);
  }

  editInstallation() {
    this.displayEditInstallation = true;
  }
  editPemSlider() {
    this.displayEditPem = true;
  }

  saveEditInstallation() {
    this.installationService.updateInstallation(this.selectedParentNode.installationId, this.selectedParentNode).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "Os dados da instalação foram guardados.",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.displayEditInstallation = false;
        this.selectedParentNode = undefined;
        this.selectedChildNode = undefined;
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível guardar as alterações da instalação", error);
    })
  }

  saveEditPem() {
    this.installationService.addPemToInstallation(this.selectedParentNode.installationId, this.selectedChildNode).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "A ativação foi editada",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.displayEditPem = false;
        this.selectedChildNode = undefined;
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível guardar a edição da ativação", error);
    })
  }

  deletePemSlider() {
    this.confirmationService.confirm({
      message: 'Tem a certeza que quer apagar?',
      header: 'Confirmação',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.forceDeletePem();
      }
    });
  }

  forceDeletePem(){
    this.installationService.deletePemActivation(this.selectedParentNode.installationId, this.selectedChildNode.id).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "A ativação foi apagada.",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.selectedChildNode = undefined;
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível eliminar a ativação", error);
    })
  }

  back() {
    let route: string = this.previousRouteService.getPreviousUrl();
    let now: string = this.router.url;
    if (route == null || route == undefined) {
      this.router.navigate(['home']);
    } else if (route == now) {
      this.router.navigate(['home']);
    }
    else {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    }
  }

  saveNewPemActivation2() {

    this.installationService.addPemToInstallation(this.selectedParentNode.installationId, this.newPem).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "Foi adicionada uma ativação à instalação selecionada",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.displayAddNewPem2 = false;
        this.selectedChildNode = undefined;
        this.selectedParentNode = undefined;
        this.newPem = new PemSliderDb(0, "", "", null, null, false);
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível adicionar uma nova ativação", error);
    })
  }

  doAtivarAlterations(){
    this.messageService.add({
      severity: 'warn',
      summary: 'Atenção',
      detail: "Ups. ainda não está implementado. é suposto alterar o estado da ativação dos selecionados (ativado -> desativado ou desativado-> ativado)",
      sticky: false,
      life: 4000
    });
  }

  saveNewPemActivation1() {

    if(this.selectedInstallationToAdd  == null || this.selectedInstallationToAdd == undefined){
      this.messageService.add({
        severity: 'warn',
        summary: 'Atenção',
        detail: "Tem de selecionar uma instalação",
        sticky: false,
        life: 2000
      });
      return;
    }
    this.installationService.addPemToInstallation(this.selectedParentNode.installationId, this.newPem).subscribe(res => {
      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: "Foi adicionada uma ativação à instalação selecionada",
        sticky: false,
        life: 2000
      });
      setTimeout(res => {
        this.displayAddNewPem1 = false;
        this.newPem = new PemSliderDb(0, "", "", null, null, false);
        this.selectedInstallationToAdd = undefined;
        this.getData();
      }, 1500);

    }, error => {
      this.errorService.handleError("Não foi possível adicionar uma nova ativação", error);
    })
  }
}
