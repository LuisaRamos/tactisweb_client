import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PemActivationsViewComponent } from './pem-activations-view.component';

describe('PemActivationsViewComponent', () => {
  let component: PemActivationsViewComponent;
  let fixture: ComponentFixture<PemActivationsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PemActivationsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PemActivationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
