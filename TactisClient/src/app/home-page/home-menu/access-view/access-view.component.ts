import { Component, OnInit, Input } from '@angular/core';
import { Installation } from 'src/app/model/installation';
import { Clinic } from 'src/app/model/clinic';
import { InstallationService } from 'src/app/services/installation.service';
import { Client } from 'src/app/model/client';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-access-view',
  templateUrl: './access-view.component.html',
  styleUrls: ['./access-view.component.css']
})
export class AccessViewComponent implements OnInit {

  @Input() installation: Installation;
  @Input() clinic:Clinic;

  @Input() client:Client; //ver  quem usa isto para apagar

  constructor(
  ) { }

  ngOnInit() {
  }

}
