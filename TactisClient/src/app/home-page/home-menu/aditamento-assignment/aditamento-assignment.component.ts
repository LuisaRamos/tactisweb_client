import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { TactisUser } from 'src/app/model/tactisuser';
import { Aditamento } from 'src/app/model/aditamento';
import { Client } from 'src/app/model/client';
import { Event } from 'src/app/model/event';
import { MenuItem, MessageService } from 'primeng/api';
import { Training } from 'src/app/model/training';
import { ClientViewComponent } from '../client-view/client-view.component';
import { ClinicsCardViewComponent } from '../clinics-card-view/clinics-card-view.component';
import { TrainingAssignmentFormComponent } from '../training-assignment-form/training-assignment-form.component';
import { UserService } from 'src/app/services/user.service';
import { InstallationService } from 'src/app/services/installation.service';
import { CalendarEventsService } from 'src/app/services/calendar-events.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ClinicService } from 'src/app/services/clinic.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { Installation } from 'src/app/model/installation';
import { EventTactisUser } from 'src/app/model/eventTactisUser';
import { AditamentoService } from 'src/app/services/aditamento.service';

@Component({
  selector: 'app-aditamento-assignment',
  templateUrl: './aditamento-assignment.component.html',
  styleUrls: ['./aditamento-assignment.component.css']
})
export class AditamentoAssignmentComponent implements OnInit {

  users: TactisUser[] = [];

  loading: boolean = true;
  aditamento: Aditamento;
  pt: any;
  installationDate: Date = new Date();

  id: number;
  instReady: boolean = false;

  cli: Client;
  clientLoaded: boolean = false;

  values: TactisUser[] = [];
  items: MenuItem[];

  activeIndex: number = 0;

  allpushed: boolean = false;
  openHelp:boolean = false;

  // --- formaçao
  trainOpt: string = "val1";
  trainings: Training[] = [];
  trainingDate: Date = new Date();
  trainingNotes: string = "";
  valuesT: TactisUser[];
  trainingUsers: TactisUser[];

  @ViewChild("client") client: ClientViewComponent;
  @ViewChild("clinicList") clinicList: ClinicsCardViewComponent;
  @ViewChildren(TrainingAssignmentFormComponent) tabs: QueryList<TrainingAssignmentFormComponent>;

  constructor(
    private userService: UserService,
    private installationService: InstallationService,
    private calendarService: CalendarEventsService,
    private route: ActivatedRoute,
    private clinicService: ClinicService,
    private messageService: MessageService,
    private router: Router,
    private errorService: HandleErrorComponent,
    private aditamentoService: AditamentoService) { }

  ngOnInit() {
    this.items = [{
      label: 'Cliente',
      command: (event: any) => {
        this.activeIndex = 0;
      }
    },
    {
      label: 'Clínicas',
      command: (event: any) => {
        this.activeIndex = 1;
      }
    },
    {
      label: 'Instalação',
      command: (event: any) => {
        this.activeIndex = 2;
      }
    },
    {
      label: 'Formação',
      command: (event: any) => {
        this.activeIndex = 3;
      }
    },
    {
      label: 'Contrato',
      command: (event: any) => {
        this.activeIndex = 4;
      }
    }
    ];
    this.getActiveUsers();
    this.getInstallationInformation();


    this.pt = {
      firstDayOfWeek: 0,
      dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
      dayNamesShort: ["Dom", "2ª", "3ª", "4ª", "5ª", "6ª", "Sáb"],
      dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S"],
      monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      today: 'Hoje',
      clear: 'Clear',
      dateFormat: 'yy-mm-dd'
    };
  }

  getActiveUsers() {
    this.userService.getActiveUsers().subscribe(res => {
      this.users = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores", error);
    });
    this.userService.getActiveUsers().subscribe(res => {
      this.trainingUsers = <TactisUser[]>res;
    }, error => {
      this.errorService.handleError("Não foi possível obter os utilizadores", error);
    });
  }

  getInstallationInformation() {
    this.route.params.subscribe(res => {
      this.id = <number>res.id;
    });
    this.aditamentoService.getAditamento(this.id).subscribe(res => {
      this.aditamento = <Aditamento>res;
      this.getAditamentoClient();
      this.loading = false;
    }, error => {
      this.errorService.handleError("Não foi possível obter as instalações", error);
    });
  }

  getAditamentoClient() {
    this.aditamentoService.getAditamentoClient(this.aditamento.aditamentoId).subscribe(res => {
      this.cli = <Client>res;
      this.clientLoaded = true;
    }, error => {
      this.errorService.handleError("Não foi possível obter o cliente", error);
    })
  }

  allSame() {

    setTimeout(res => {
      let trainingName: string = "train_" + new Date().getTime();
      let training: Training = new Training(new Date(), trainingName, "", this.aditamento.clinicId, false, true);
      this.trainings.push(training);
    }, 20);
    setTimeout(res => {
      this.allpushed = true;
    }, 100);

    // let training: Training = new Training(this.trainingDate, trainingName, this.trainingNotes, this.selectedClinicTraining, false, true);
  }

  back() {
    this.router.navigate(["home/schedules"]);
  }

  save2() {
    this.doSaveTraining();
  }

  save() {
    if (this.values.length < 1) {
      this.messageService.add({
        severity: 'error',
        summary: 'Erro',
        detail: 'Tem de escolher utilizadores para realizar a instalação. Pode alterá-los no futuro.',
        sticky: false,
        life: 2000
      });
      return;
    }

    this.aditamento.data = this.installationDate;
    this.aditamento.scheduled = true;

    let end: Date = new Date(this.installationDate);
    end.setHours(end.getHours() + 1);

    let endDate: Date = new Date(end);

    this.aditamentoService.updateAditamento(this.aditamento.aditamentoId, this.aditamento).subscribe(res => {
      this.instReady = true;

      //fez update, entao criar os eventos
      let userlist: EventTactisUser[] = [];
      this.values.forEach(element => {
        userlist.push(new EventTactisUser(element));
      })

      let myevent: Event = new Event("Aditamento", this.aditamento.description, this.aditamento.notas, "", false, this.installationDate, endDate, null, this.aditamento.clinicId, userlist);
      this.calendarService.createEvent(myevent).subscribe(res => {
        if (this.trainOpt == 'val1') {
          this.messageService.add({
            severity: 'success',
            summary: 'Sucesso',
            detail: 'Agendamento de instalação guardado!',
            sticky: false,
            life: 2000
          });
          setTimeout(() => {
            this.router.navigate(["/home"]);
          }, 1000);
          return;
        }
        // ----------------- TRAINING ---------------------
        this.doSaveTraining();

      }, error => {
        this.errorService.handleError("Não foi possível criar o evento de instalação", error);
      });

    }, error => {
      this.errorService.handleError("Não foi possível guardar a instalação", error);
    });
  }

  doSaveTraining() {
    console.log(this.trainings.length);
    for (let i = 0; i < this.trainings.length; i++) {
      let element = this.trainings[i];

      setTimeout(res => {
        // se for igual para todas, é preciso atualizar cada training com os valores gerais
        if (this.trainOpt == "val2") {
          element.trainingDate = this.trainingDate;
          element.notes = this.trainingNotes;
        }

        let end2: Date = new Date(element.trainingDate);
        end2.setHours(end2.getHours() + 1);
        let trainingEndDate: Date = new Date(end2);

        if (this.trainOpt == "val3" && this.aditamento.clinicId != undefined || this.trainOpt == "val2") {
          this.clinicService.addTrainingToClinic(element.clinic.clinicId, element).subscribe(res => {

            let userlist2: EventTactisUser[] = [];
            //se for diferente, tem de ir buscar os values de cada tab
            if (this.trainOpt == "val3") {
              let valuesTr: TactisUser[] = [];
              console.log("i = " + i);
              valuesTr = this.tabs.toArray()[i].valuesT;
              valuesTr.forEach(element => {
                userlist2.push(new EventTactisUser(element));
              })
            } else if (this.trainOpt == "val2") {
              //se for igual para todos, tem de ir buscar o valueT geral
              this.valuesT.forEach(element => {
                userlist2.push(new EventTactisUser(element));
              })
            }

            let myevent2: Event = new Event("Formação", element.description, element.notes, "", false, element.trainingDate, trainingEndDate, null, element.clinic, userlist2);

            this.calendarService.createEvent(myevent2).subscribe(res => {
              if (i == this.trainings.length - 1) {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Sucesso',
                  detail: 'Agendamentos guardados!',
                  sticky: false,
                  life: 2000
                });
              }
              setTimeout(() => {
                this.router.navigate(["/home"]);
              }, 1000);
            }, error => {
              this.errorService.handleError("Não foi possível criar o evento", error);
            });

          }, error => {
            this.errorService.handleError("Não foi possível criar a formação", error);
          });
        }


      }, 20)
    }
  }

  next() {
    if (this.activeIndex < 3) {
      this.activeIndex += 1;
      return;
    }
  }

  previous() {
    if (this.activeIndex > 0) {
      this.activeIndex -= 1;
      return;
    }
  }
}
