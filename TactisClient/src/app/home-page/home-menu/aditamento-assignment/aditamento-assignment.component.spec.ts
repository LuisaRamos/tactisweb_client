import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AditamentoAssignmentComponent } from './aditamento-assignment.component';

describe('AditamentoAssignmentComponent', () => {
  let component: AditamentoAssignmentComponent;
  let fixture: ComponentFixture<AditamentoAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AditamentoAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AditamentoAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
