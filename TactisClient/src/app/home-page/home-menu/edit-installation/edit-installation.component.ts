import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Installation } from 'src/app/model/installation';
import { InstallationService } from 'src/app/services/installation.service';

@Component({
  selector: 'app-edit-installation',
  templateUrl: './edit-installation.component.html',
  styleUrls: ['./edit-installation.component.css']
})
export class EditInstallationComponent implements OnInit {

  iconNAME: string = "pi pi-question";
  iconNIF: string = "pi pi-question";
  hexColorNAME: string = "grey";
  hexColorNIF: string = "grey";


  @Input() installation: Installation;
  @Input() viewOnly: boolean;
  @Input() editAccess: boolean;
  constructor(
    private installationService: InstallationService
  ) { }

  ngOnInit() {
  }

  checkName() {
    this.installationService.checkInstallationNameExists(this.installation.installationName, this.installation.installationId).subscribe(res => {
      if (res == true) {
        this.iconNAME = "pi pi-times";
        this.hexColorNAME = "red";
      }
      else {
        this.iconNAME = "pi pi-check";
        this.hexColorNAME = "green";
      }
    })
  }

  clearName() {
    this.hexColorNAME = "grey";
    this.iconNAME = "pi pi-question";
  }

}
