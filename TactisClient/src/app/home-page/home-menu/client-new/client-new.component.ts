import { Component, OnInit, ɵConsole } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService } from 'src/app/services/client.service';
import { MessageService } from 'primeng/api';
import { Address } from 'src/app/model/address';
import { Contact } from 'src/app/model/contact';
import { Client } from 'src/app/model/client';
import { AuthService } from 'src/app/services/auth.service';
import { PreviousRouteService } from 'src/app/services/previous-route.service';
import { HandleErrorComponent } from '../handle-error/handle-error.component';

@Component({
  selector: 'app-client-new',
  templateUrl: './client-new.component.html',
  styleUrls: ['./client-new.component.css']
})
export class ClientNewComponent implements OnInit {


  client: Client = new Client("", "", "", new Contact("", "", "", "", ""), new Address("", "", "", ""),[], "","");

  constructor(
    private router: Router,
    private clientService: ClientService,
    private messageService: MessageService,
    private auth: AuthService,
    private previousRouteService: PreviousRouteService,
    private errorService: HandleErrorComponent
  ) { }

  ngOnInit() {
  }

  back() {
    let route: string = this.previousRouteService.getPreviousUrl();
    let now: string = this.router.url;
    if (route == null || route == undefined) {
      this.router.navigate(['home']);
    } else if (route == now) {
      this.router.navigate(['home']);
    }
    else {
      this.router.navigate([this.previousRouteService.getPreviousUrl()]);
    }
  }

  checkClientFields() {
    if (!this.validField(this.client.name, true)) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher o nome do cliente',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!(this.validField(this.client.contact.email, true) || this.validField(this.client.contact.phone1, true))) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher um contacto ou email do cliente',
        sticky: false,
        life: 2000
      });
      return false;
    }

    if (!(this.validField(this.client.address.address, true) || this.validField(this.client.address.addressLocation, true))) {
      this.messageService.add({
        severity: 'error',
        summary: 'Atenção',
        detail: 'Deve preencher a morada ou localização do cliente',
        sticky: false,
        life: 2000
      });
      return false;
    }
    return true;
  }

  submit() {
    if (!this.checkClientFields()) {
      return;
    }

    this.clientService.createClient(this.client).subscribe(data => {

      this.messageService.add({
        severity: 'success',
        summary: 'Sucesso',
        detail: 'Cliente Registado!',
        sticky: false,
        life: 2000
      });

      setTimeout(() => {
        this.router.navigate(["/home"]);
      }, 2000);

    }, error => {
      if (error.status == 400 && error.error == "DataIntegrityViolationException") {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Nome do Cliente já utilizado.',
          sticky: false,
          life: 1000
        });
      } else {
        this.errorService.handleError("Não foi possível guardar o cliente", error);
      }
    });
  }

  validField(field: string, mandatory: boolean): boolean {
    if (mandatory && this.nullOrUndefinedField(field)) {
      //exemplo '', true
      return false;
    } else if (!mandatory && this.nullOrUndefinedField(field)) {
      //exemplo '', false
      return true;
    } else {
      return field.trim().length > 0;
    }
  }

  private nullOrUndefinedField(field: string) {
    if (field == null || field == undefined) {
      return true;
    }
    return false;
  }
}
