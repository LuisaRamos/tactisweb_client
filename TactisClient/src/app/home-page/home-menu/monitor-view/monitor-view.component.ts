import { Component, OnInit, Input } from '@angular/core';
import { Clinic } from 'src/app/model/clinic';
import { Client } from 'src/app/model/client';
import { Installation } from 'src/app/model/installation';
import { HandleErrorComponent } from '../handle-error/handle-error.component';
import { InstallationService } from 'src/app/services/installation.service';
import { MonitorView } from 'src/app/util/view/monitorView';

@Component({
  selector: 'app-monitor-view',
  templateUrl: './monitor-view.component.html',
  styleUrls: ['./monitor-view.component.css']
})
export class MonitorViewComponent implements OnInit {

  monitor: MonitorView;
  hasNG: boolean = false;
  hasPEM: boolean = false;
  hasTU: boolean = false;
  instReady: boolean = false;

  @Input() clinic: Clinic;
  @Input() client: Client;
  @Input() installation: Installation;

  constructor(
    private errorService: HandleErrorComponent,
    private installationService: InstallationService
  ) { }

  ngOnInit() {
    this.getInstallation();
  }

  getInstallation() {
    // this.installationService.getInstallationBetweenClientClinic(this.client.clientId, this.clinic.clinicId).subscribe(res => {
    //   this.installation = <Installation>res;
    //   if(this.installation.installationName != null && this.installation.installationName != undefined){
    this.installationService.readInstallationMonitor(this.installation.installationName).subscribe(res => {
      this.monitor = <MonitorView>res;
      if (this.monitor.ngmonitor != undefined && this.monitor.ngmonitor != null) {
        this.hasNG = true;

      }
      if (this.monitor.pemmonitor != undefined && this.monitor.pemmonitor != null) {
        this.hasPEM = true;
      }
      if (this.monitor.tumonitor != undefined && this.monitor.tumonitor != null) {
        this.hasTU = true;
      }
      this.instReady = true;

    }, error => {
      this.errorService.handleError("Não foi possível obter a monitorização.", error);
    });
  }

}
