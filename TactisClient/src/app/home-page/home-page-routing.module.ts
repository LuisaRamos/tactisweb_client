import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeMenuComponent } from './home-menu/home-menu.component';


const homepageRoutes: Routes = [

  { 
    path: '', 
    loadChildren: './home-menu/home-menu.module#HomeMenuModule'/*, canActivate: [AuthGuard]*/, 
    component: HomeMenuComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(homepageRoutes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule { }

