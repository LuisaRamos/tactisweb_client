import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';


const appRoutes: Routes = [
  //{ path: '', loadChildren: './main-page/main-page.module#MainPageModule' , canActivate: [AuthGuard]},
  { path: '', loadChildren: './main-page/main-page.module#MainPageModule' , canActivate: [AuthGuard]},
  { path: 'home', loadChildren: './home-page/home-page.module#HomePageModule', canActivate: [AuthGuard]},
  //{ path: 'login', component: LoginComponent, canActivate: [AuthGuard]},
  //{ path: 'home', component: HomePageComponent, canActivate: [AuthGuard] },
  //{ path: 'users/new', component: UserNewComponent, canActivate: [AuthGuard]},
  //{ path: 'myprofile', component: UserProfileComponent, canActivate: [AuthGuard] },
  //{ path: 'clients/new', component: ClientNewComponent, canActivate: [AuthGuard] },

  // otherwise redirect to home
  //{ path: '**', redirectTo: '' }
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(appRoutes)
  ]
})
export class AppRoutingModule { }

//export const routing = RouterModule.forRoot(appRoutes);
