import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  _username: string = "";

  _password: string = "";

  loginForm: FormGroup;
  loading = false;
  submitted = false;

  returnUrl: string;

  //regex
  blockSpace: RegExp = /[^\s]/;

  expiredSession :boolean= false;

  constructor(
    private formBuilder: FormBuilder,
    private authSrv: AuthService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
      this.expiredSession= this.authSrv.sessionExpired();
    }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      _username: ['', Validators.required],
      _password: ['', Validators.required]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.authSrv.login(this._username, this._password).subscribe(data => {
      this.router.navigate(['/home']);
    }, error => {
      if (error.status == 404) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Erro',
          detail: 'Utilizador não encontrado.',
          sticky: false,
          life: 2000
        });
      }
      else if (error.status == 400) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Erro',
          detail: 'Combinação de utilizador/palavra-passe falhada.',
          sticky: false,
          life: 2000
        });
      }else if (error.status == 500) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Erro',
            detail: 'Não foi possível comunicar com o servidor',
            sticky: false,
            life: 2000
          });
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Erro desconhecido',
          sticky: false,
          life: 2000
        });
      }
    }
    );
  }

  back(){
    this.router.navigate(["/"]);
  }
}
