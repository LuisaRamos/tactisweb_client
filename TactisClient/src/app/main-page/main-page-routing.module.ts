import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { AuthGuard } from '../guards/auth.guard';
import { LoginComponent } from './login/login.component';

const mainPageRoutes: Routes = [
  {
    path:'',
    component: MainPageComponent
  },
  {
        path:'login',
        component: LoginComponent
      }
];

@NgModule({
  imports: [RouterModule.forChild(mainPageRoutes)],
  exports: [RouterModule]
})
export class MainPageRoutingModule { }
