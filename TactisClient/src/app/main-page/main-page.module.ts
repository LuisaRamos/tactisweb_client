import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageRoutingModule } from './main-page-routing.module';
import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//import { routing } from '../app-routing.module';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MenubarModule } from 'primeng/menubar';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { InputMaskModule } from 'primeng/inputmask';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PasswordModule } from 'primeng/password';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { JwtModule } from '@auth0/angular-jwt';
import { MessageService } from 'primeng/api';

@NgModule({
  imports: [
    CommonModule,
    MainPageRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
   // routing,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    InputTextModule,
    ButtonModule,
    ConfirmDialogModule,
    MenubarModule,
    FormsModule,
    CommonModule,
    DialogModule,
    MessagesModule,
    MessageModule,
    InputMaskModule,
    CalendarModule,
    CardModule,
    ToastModule,
    ScrollPanelModule,
    PasswordModule,
    KeyFilterModule,
    ProgressSpinnerModule
    
  ],
  declarations: [
    LoginComponent,
    MainPageComponent
  ],
  providers: [
    MessageService
],
})
export class MainPageModule { }
