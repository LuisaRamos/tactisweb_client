import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
        private router: Router,
        private authenticationService: AuthService
    ) { }

    /**
     * Se tiver um token no local storage e for válido, então é aceite o can Activate
     * @param route 
     * @param state 
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      console.log("url:" + state.url);

        //se estiver no main e não tiver o login, vai para a main page onde pode fazer login
        if((state.url == "/" || state.url == "/login") && this.authenticationService.isTokenExpired()){
            return true;
        } 
        //se estiver no main e tiver o login, é redirecionado para a home
        else if((state.url == "/" || state.url == "/login") && !this.authenticationService.isTokenExpired()){
            this.router.navigate(['/home']);
            return false;
        //se estiver no login e houver um login ligado, é redirecionado para a home
          //se estiver no login e não houver login ligado, entao pode ir

        }else if (!this.authenticationService.isTokenExpired()) {
            return true;
        }
        
        console.log("não tem autorização. efetue login! É necessário fazer um componente de mensages")
        // not logged in so redirect to login page with the return url
        this.router.navigate(['']/*, { queryParams: { returnUrl: state.url } }*/);
        return false;
    }
  
}
