import { Client } from './client';
import { Clinic } from './clinic';
import { EventTactisUser } from './eventTactisUser';

export class Event {
    eventId:number;
    name:string;
    description:string;
    notes:string;
    eventStatus:string;
    done:boolean;
    beginDate:Date;
    endDate:Date;
    clientId:Client;
    clinicId:Clinic;
    eventTactisUserList:EventTactisUser[];

    constructor(name:string, description:string, notes:string, eventStatus:string,
        done:boolean, beginDate:Date, endDate:Date, clientId:Client, clinicId:Clinic,
        eventTactisUserList:EventTactisUser[], eventId?:number){
            this.name = name;
            this.description = description;
            this.notes = notes;
            this.eventStatus = eventStatus;
            this.done = done;
            this.beginDate = beginDate;
            this.endDate = endDate;
            this.clientId = clientId;
            this.clinicId = clinicId;
            this.eventTactisUserList = eventTactisUserList;
            this.eventId = eventId;
        }
}