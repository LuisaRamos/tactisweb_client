export class Grafico {
    id:number;
    titulo:string;
    tipo:string;
    options:string;
    categoriaBase:string;
    codigo:number;

    constructor(titulo:string, tipo:string, options:string, categoriaBase:string, codigo:number, id?:number){
        this.tipo = tipo;
        this.titulo = titulo;
        this.options = options;
        this.id = id;
        this.categoriaBase = categoriaBase;
        this.codigo = codigo;
    }
}