export class NovigestSliderDb {
    id: number;
    nomea: string;
    nomeb: string;
    nomec: string;
    nomed: string;
    nifa: string;
    nifb: string;
    nifc: string;
    nifd: string;
    contrato: string;
    postos: number;
    validade: number;
    datainicio: Date;
    produto: string;
    dataactivacao: Date;
    sites: number;
    servidores: number;
    anamneseMobile: boolean;
    aluguerMensal: boolean;
    aluguerAnual: boolean;
    usarmac: boolean;
    venda: boolean;
    ativar: boolean;
    mac: string;

    constructor(
        nomea: string, nomeb: string, nomec: string, nomed: string,
        nifa: string, nifb: string, nifc: string, nifd: string,
        contrato: string, postos: number,
        validade: number, datainicio: Date, produto: string,
        dataactivacao: Date,
        sites: number, servidores: number,
        anamneseMobile: boolean, aluguerMensal: boolean, aluguerAnual: boolean, usarmac: boolean, venda: boolean,
        ativar: boolean, mac: string, id?: number,
    ) {
        this.id = id;
        this.nomea = nomea;
        this.nomeb = nomeb;
        this.nomec = nomec;
        this.nomed = nomed;
        this.nifa = nifa;
        this.nifb = nifb;
        this.nifc = nifc;
        this.nifd = nifd;
        this.contrato = contrato;
        this.postos = postos;
        this.validade = validade;
        this.datainicio = datainicio;
        this.produto = produto;
        this.dataactivacao = dataactivacao;
        this.sites = sites;
        this.servidores = servidores;
        this.anamneseMobile = anamneseMobile;
        this.aluguerMensal = aluguerMensal;
        this.aluguerAnual = aluguerAnual;
        this.usarmac = usarmac;
        this.venda = venda;
        this.ativar = ativar;
        this.mac = mac;
    }

}