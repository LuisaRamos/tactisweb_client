import { Address } from './address';
import { Contact } from './contact';
import { Contract } from './contract';
import { getTreeControlFunctionsMissingError } from '@angular/cdk/tree';

export class Client {
    clientId:number;
    name:string;
    nif:string;
    notes:string;
    address:Address;
    contact:Contact;
    eventList:Event[];
    contractList:Contract[];
    criacaoFicha:Date;
    motivo:string;
    recomendadoPor:string;
    
    constructor(name:string, nif:string, notes:string,  contact:Contact, 
        address:Address, eventList:Event[], motivo:string, recomendadoPor:string,clientId?:number){
        this.name =name;
        this.nif = nif;
        this.notes = notes;
        this.address = address;
        this.contact = contact;
        this.clientId = clientId;
        this.eventList = eventList;
        this.motivo = motivo;
        this.recomendadoPor = recomendadoPor;
    }
}