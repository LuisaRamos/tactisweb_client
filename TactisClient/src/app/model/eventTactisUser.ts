import { TactisUser } from './tactisuser';
import { Event } from './event';
export class EventTactisUser {
    eventUserId:number;
    eventId:Event;
    tactisUserId:TactisUser;

    constructor(tactisUserId:TactisUser,eventId?:Event, eventUserId?:number){
        this.tactisUserId = tactisUserId;
        this.eventId = eventId;
        this.eventUserId = eventUserId;
    }
}