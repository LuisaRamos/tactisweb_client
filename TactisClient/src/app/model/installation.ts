import { NoviGestView } from './novigestview';
import { Clinic } from './clinic';
import { Contract } from './contract';
import { PemSliderDb } from './pemSliderDb';
import { NovigestSliderDb } from './novigestSliderDb';

export class Installation {
    installationId:number;
    installationDate: Date;
    notes: string;
    accessData:string;
    description:string;
    contract:Contract;
    installationName:string;
    clinicList:Clinic[];

    done:boolean;
    scheduled:boolean;
    ready:boolean;

    ngAllowed:boolean;
    tuAllowed:boolean;
    pemAllowed:boolean;
    bdinfarmedAllowed:boolean;

    installng:boolean;
    installtu:boolean;
    installpem:boolean;

    pemSliderDbList:PemSliderDb[];
    novigestSliderDbList:NovigestSliderDb[];
    password:string;   

    conclusionDate:Date;

    constructor(installationDate: Date, notes:string, description:string,
       clinicList:Clinic[],  pemSliderDbList:PemSliderDb[], novigestSliderDbList:NovigestSliderDb[], installationName:string, password:string, 
       done:boolean, schedule:boolean, ready:boolean,
       installng:boolean, installtu:boolean, installpem:boolean, 
        ngAllowed?:boolean, tuAllowed?:boolean, pemAllowed?:boolean, bdinfarmedAllowed?:boolean,  conclusionDate?:Date,
        installationId?:number){
        this.installationDate = installationDate;
        this.description = description;
        this.clinicList = clinicList;
        this.installationId = installationId;
        this.installationName = installationName;
        this.notes = notes;
        this.done = done;
        this.scheduled = schedule;
        this.ngAllowed = ngAllowed;
        this.tuAllowed = tuAllowed;
        this.pemAllowed = pemAllowed;
        this.pemSliderDbList = pemSliderDbList;
        this.bdinfarmedAllowed = bdinfarmedAllowed;
        this.novigestSliderDbList = novigestSliderDbList;
        this.password = password;
        this.installng = installng;
        this.installpem = installpem;
        this.installtu = installtu;
        this.ready = ready;
        this.conclusionDate = conclusionDate;
    }
}