export class InterventionType{
    interventionTypeId:number;
    description:string;

    constructor(description:string, interventionTypeId?:number){
        this.description = description;
        this.interventionTypeId = interventionTypeId;
    }
}