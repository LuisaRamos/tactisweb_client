import { Contract } from './contract';
import { Clinic } from './clinic';

export class Aditamento {
    aditamentoId: number;
    mainPostos: number;
    addPostos: number;
    data: Date;
    notas: string;
    proposta: string;
    instalado: boolean;
    faturado: boolean;
    dividido: boolean;
    pendente: number;
    fat_pendente:number;
    clinicId: Clinic;
    contractId: Contract;
    description: string;
    scheduled: boolean;
    conclusionDate: Date; // ainda nao está a ser usado

    constructor(
        mainPostos: number, addPostos: number,
        clinicId: Clinic, contractId: Contract,
        proposta: string, notas: string, description:string,
        instalado: boolean, faturado: boolean, dividido: boolean, scheduled: boolean,
        pendente: number, fat_pendente:number, data: Date,
        aditamentoId?: number) {
        this.addPostos = addPostos;
        this.mainPostos = mainPostos;
        this.aditamentoId = aditamentoId;
        this.clinicId = clinicId;
        this.contractId = contractId;
        this.proposta = proposta;
        this.description = description;
        this.notas = notas;
        this.instalado = instalado;
        this.faturado = faturado;
        this.dividido = dividido;
        this.pendente = pendente;
        this.scheduled = scheduled;
        this.data = data;
        this.fat_pendente = this.fat_pendente;
    }
}