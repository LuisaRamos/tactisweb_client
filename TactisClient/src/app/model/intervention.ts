import { InterventionType } from './interventionType';
import { ProductActivity } from './productActivity';
import { Clinic } from './clinic';
import { TactisUser } from './tactisuser';

export class Intervention{
    interventionId:number;
    interventionDate: Date;
    notes: string;
    duration:number;
    interlocutor:string;
    interventionType:InterventionType;
    productActivity:ProductActivity;
    tactisUserId:TactisUser;
    clinic:Clinic;

    constructor(interventionDate: Date, notes: string, duration:number, 
        interlocutor:string, interventionType:InterventionType,
        tactisUserId:TactisUser, productActivity:ProductActivity, clinic:Clinic,
        interventionId?:number){
            this.interventionId=interventionId;
        this.interventionDate = interventionDate;
        this.notes = notes;
        this.duration = duration;
        this.interlocutor = interlocutor;
        this.interventionType = interventionType;
        this.productActivity = productActivity;
        this.tactisUserId = tactisUserId;
        this.clinic =clinic;
    }
}