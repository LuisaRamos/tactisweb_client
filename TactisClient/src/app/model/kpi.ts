export class Kpi {
    titulo:string;
    icon:string;
    routerlink:string;
    categoriaBase:string;
    id:number;
    codigo:number;
    sufixo:string;

    constructor(titulo:string, icon:string, routerlink:string,categoriaBase:string,codigo:number, sufixo:string,id?:number ){
        this.titulo = titulo;
        this.routerlink = routerlink;
        this.icon= icon;
        this.categoriaBase = categoriaBase;
        this.codigo=codigo;
        this.sufixo = sufixo;
        this.id = id;
    }
}