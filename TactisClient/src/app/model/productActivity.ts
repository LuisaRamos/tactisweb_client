export class ProductActivity {
    productActivityId:number;
    activity:string;
    product:string;

    constructor(product:string, activity:string, productActivityId?:number){
        this.product =product;
        this.activity = activity;
        this.productActivityId = productActivityId;
    }
}