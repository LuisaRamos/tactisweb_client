import { UserRole } from './userrole';

export class TactisUser {
    tactisUserId: number;
    name:string;
    username: string;
    email: string;
    password:string;
    userRoleId: UserRole;
    active: boolean;

    constructor(name:string, username:string, email:string, password, userRoleId:UserRole, active:boolean, tactisUserId?:number){
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.userRoleId = userRoleId;
        this.active = active;
        this.tactisUserId = tactisUserId;
    }
}