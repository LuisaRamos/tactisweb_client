import { Contract } from './contract';

export class ContractChanges {
    contractChangeId:number;
    changeDate:Date;
    changeDescr:string;
    changeCode:string;
    changeNormalized:boolean;
    normalizedDate:Date;
    changeDatadoc:Date;
    contractId:Contract;

    constructor( changeDate:Date, changeCode:string, changeDescr:string,
        changeNormalized:boolean,normalizedDate:Date,changeDatadoc:Date, contractId:Contract, contractChangeId?:number){
            this.contractChangeId=contractChangeId;
            this.changeDate= changeDate;
            this.changeDescr = changeDescr;
            this.changeCode=changeCode;
            this.changeNormalized = changeNormalized;
            this.normalizedDate = normalizedDate;
            this.changeDatadoc = changeDatadoc;
            this.contractId = contractId;
        }
}