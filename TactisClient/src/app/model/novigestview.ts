import { Installation } from './installation';

export class NoviGestView {

    novigestId:number;
    novigestType:string;
    purchasedIn:string;
    versionNg:string;
    installationList:Installation[];

    constructor(novigestType:string, purchasedIn:string,
        versionNg:string, installationList:Installation[], novigestId?:number ){
            this.novigestId = novigestId;
            this.novigestType = novigestType;
            this.purchasedIn = purchasedIn;
            this.versionNg = versionNg;
            this.installationList = installationList;
        
    }
}