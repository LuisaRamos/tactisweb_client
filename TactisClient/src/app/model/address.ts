
export class Address {
    addressId:number;
    address:string;
    addressLocation:string;
    addressNumber:string;
    postcode1:string;
    postcode2:string;
    floor:string;
    room:string;

    constructor(address:string, postcode1:string, postcode2:string, location:string, addressId?:number){
        this.address = address;
        this.postcode1 = postcode1;
        this.postcode2 = postcode2;
        this.addressLocation = location;
        this.addressId = addressId;
    }
}