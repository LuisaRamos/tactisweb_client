export class Upgrade {
    upgradeId: number;
    appTag: string;
    ativo: boolean;
    versao: string;
    checksum: string;
    urlDb: string;
    urlApp: string;
    password: string;

    constructor(appTag: string, ativo: boolean, versao: string, checksum: string, urlDb: string,
        urlApp: string, password: string, upgradeId?: number) {
        this.appTag = appTag;
        this.ativo = ativo;
        this.versao = versao;
        this.checksum = checksum;
        this.urlDb = urlDb;
        this.urlApp = urlApp;
        this.password = password;
        this.upgradeId = upgradeId;
    }
}