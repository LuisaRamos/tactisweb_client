
export class Contact {
    contactId:number;
    phone1:string;
    phone2:string;
    phone3:string;
    email:string;
    other:string;

    constructor(phone1:string, phone2:string, phone3:string, email:string, other:string, contactId?:number){
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.phone3 = phone3;
        this.email = email;
        this.other = other;
        this.contactId = contactId;
    }
}