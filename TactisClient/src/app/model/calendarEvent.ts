import { toJSDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-calendar';

export class CalendarEvent {
    title:string;
    start:Date;
    end:Date;
    constructor(title:string, start:Date, end:Date){
        this.title=title;
        this.start=start;
        this.end=end;
    }
}