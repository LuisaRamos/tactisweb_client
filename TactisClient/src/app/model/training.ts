import { Clinic } from './clinic';

export class Training {
    trainingId:number;
    trainingDate:Date;
    description:string;
    notes:string;
    clinic:Clinic;
    done:boolean;
    schedule:boolean;

    constructor(trainingDate:Date, description:string, notes:string, clinic:Clinic, done:boolean, schedule:boolean, trainingId?:number){
        this.trainingDate = trainingDate;
        this.description = description;
        this.notes = notes;
        this.clinic = clinic;
        this.trainingId = trainingId;
        this.done = done;
        this.schedule = schedule;
    }
}