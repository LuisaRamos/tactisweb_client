import { Address } from './address';
import { Contact } from './contact';
import { Installation } from './installation';

export class Clinic {
    clinicId: number;
    name: string;
    clientSince: Date;
    notes: string;
    address: Address;
    contact: Contact;
    installation: Installation;
    accessData: string;
    addPostos: number;
    mainPostos: number;
    criacaoFicha:Date;

    constructor(name: string, clientSince: Date, 
        notes: string, accessData: string, 
        address: Address, contact: Contact, 
        addPostos: number, mainPostos: number, criacaoFicha?:Date,
        installation?: Installation,  clinicId?: number) {
        this.name = name;
        this.clientSince = clientSince;
        this.notes = notes;
        this.accessData = accessData;
        this.contact = contact;
        this.address = address;
        this.installation = installation;
        this.clinicId = clinicId;
        this.addPostos = addPostos;
        this.mainPostos = mainPostos;
        this.criacaoFicha = criacaoFicha;
    }
}