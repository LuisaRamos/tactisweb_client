export class PemSliderDb {
    id:number;
    maxusers:number;
    codreceitas:string;
    localprescricao:string;
    activacaoinicio:Date;
    activacaofim:Date;
    activa:boolean;

    constructor(maxusers:number,codreceitas:string,localprescricao:string,
        activacaoinicio:Date, activacaofim:Date, activa:boolean, id?:number   ){

            this.maxusers = maxusers;
            this.codreceitas = codreceitas;
            this.localprescricao = localprescricao;
            this.activacaoinicio = activacaoinicio;
            this.activacaofim = activacaofim
            this.activa = activa;
            this.id = id;
    }
}