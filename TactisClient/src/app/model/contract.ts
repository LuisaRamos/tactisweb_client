import { Client } from "./client";
import { Installation } from './installation';

export class Contract{
    
    contractId:number;
    qtyMainStations:number;
    qtyAddStations:number;
    contractNumber:string;
    contractType:string;
    firstStage:boolean;
    secondStage:boolean;
    clientId:Client;
    notes:string;
    installationList:Installation[];
    contractList:Contract[];
    proposta:string;
    

    constructor(client:Client, qtyMainStations:number,qtyAddStations:number, contractNumber:string, contractType:string,
         notes:string, proposta:string, firstStage:boolean, secondStage:boolean, installationList:Installation[], contractList:Contract[], contractId?:number){
        this.clientId = client;
        this.qtyMainStations = qtyMainStations;
        this.qtyAddStations = qtyAddStations;
        this.contractNumber = contractNumber;
        this.contractId = contractId;
        this.notes=notes;
        this.proposta = proposta;
        this.installationList = installationList;
        this.firstStage = firstStage;
        this.secondStage = secondStage;
        this.contractType = contractType;
        this.contractList = contractList;
    }
}