import { NgModule, InjectionToken } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MenubarModule } from 'primeng/menubar';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { InputMaskModule } from 'primeng/inputmask';
import { CalendarModule } from 'primeng/calendar';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PasswordModule } from 'primeng/password';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import {AccordionModule} from 'primeng/accordion';
import {TreeTableModule} from 'primeng/treetable';
import { TooltipModule } from 'primeng/tooltip';
import { MatNativeDateModule } from '@angular/material';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MatchHeightDirective } from './match-height.directive';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        TableModule,
        DropdownModule,
        MultiSelectModule,
        InputTextModule,
        ButtonModule,
        TooltipModule,
        ConfirmDialogModule,
        MenubarModule,
        MatNativeDateModule,
        FormsModule,
        CommonModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        InputMaskModule,
        CalendarModule,
        FullCalendarModule,
        CardModule,
        ToastModule,
        ScrollPanelModule,
        PasswordModule,
        TreeTableModule,
        KeyFilterModule,
        AccordionModule,
        ProgressSpinnerModule,
        
    ],
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        MatchHeightDirective
    ],
    providers: [
        MessageService
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
    constructor(router: Router) {
 }
}
