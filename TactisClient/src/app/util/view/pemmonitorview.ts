export class PemMonitorView {
    idclinica:string ;
    version:string ;
    pemVersion:string ;
    infarmedVersion:string ;
    dataping:Date;

    constructor(idclinica:string,version:string , pemVersion:string ,infarmedVersion:string,dataping:Date){
        this.idclinica = idclinica;
        this.version = version;
        this.pemVersion = pemVersion;
        this.infarmedVersion = infarmedVersion;
        this.dataping = dataping;
    }
}