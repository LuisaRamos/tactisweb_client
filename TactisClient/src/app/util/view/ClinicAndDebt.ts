import { Clinic } from '../../model/clinic';
import { ContractChanges } from '../../model/contractchanges';

export class ClinicAndDebt {
    clinic:Clinic;
    lastChange:ContractChanges;

    constructor(clinic:Clinic, lastChange:ContractChanges){
        this.clinic = clinic;
        this.lastChange = lastChange;
    }
}