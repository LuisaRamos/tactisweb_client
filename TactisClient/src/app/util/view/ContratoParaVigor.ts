import { Contract } from 'src/app/model/contract';
import { Clinic } from 'src/app/model/clinic';

export class ContractoParaVigor {
    contract: Contract;
    clinics: Clinic[];
    clientSince: Date;
    contractDate: Date;
    contractAte: Date;

    constructor(contract: Contract,clinics: Clinic[], clientSince: Date, contractDate: Date, contractAte: Date ){
        this.contract = contract;
        this.clinics = clinics;
        this.clientSince = clientSince;
        this.contractDate = contractDate;
        this.contractAte = contractAte;
    }

}