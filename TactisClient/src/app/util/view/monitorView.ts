import { PemMonitorView } from './pemmonitorview';
import { TactisApplicationsMonitorView } from './tumonitorview';
import { NovigestMonitorView } from './novigestmonitor';

export class MonitorView{
    ngmonitor:NovigestMonitorView;
    pemmonitor:PemMonitorView;
    tumonitor:TactisApplicationsMonitorView;

    constructor(ngmonitor:NovigestMonitorView, pemmonitor:PemMonitorView,
        tumonitor:TactisApplicationsMonitorView){
        this.ngmonitor = ngmonitor;
        this.pemmonitor= pemmonitor;
        this.tumonitor = tumonitor;
    }
}