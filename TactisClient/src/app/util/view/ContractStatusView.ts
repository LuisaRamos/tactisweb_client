export class ContractStatusView {
    code:string;
    codenr:number;
    divida:string;
    debttime:string;
    pagamentodivida:boolean;
    cmdev:string;
    avldev:string;
    cmdocdev:string;
    avldocdev:string;
    cmdocate:Date;
    fechou:boolean;
}