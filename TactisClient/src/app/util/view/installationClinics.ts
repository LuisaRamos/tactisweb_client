import { Clinic } from '../../model/clinic';
import { InstallationLastContractChange } from './installationLastContractChange';

export class InstallationClinics {
    data:InstallationLastContractChange;
    children:Clinic[];
    expanded:boolean;
   

    constructor(data:InstallationLastContractChange, expanded:boolean, children:Clinic[]){
        this.data = data;
        this.expanded = expanded;
        this.children = children;
    } 
}