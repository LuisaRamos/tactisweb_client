import { Client } from 'src/app/model/client';
import { Contract } from 'src/app/model/contract';
import { ContractStatusView } from './ContractStatusView';

export class ClientContractDividaView {
    client:Client;
    contract:Contract;
    divida:ContractStatusView;

    constructor(client:Client){
        this.client = client;
    }
}