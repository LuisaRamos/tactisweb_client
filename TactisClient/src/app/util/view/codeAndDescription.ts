export class CodeDescription {
    code:string;
    descr:string;

    constructor(code?:string, descr?:string){
        this.code = code;
        this.descr = descr;
    }
}