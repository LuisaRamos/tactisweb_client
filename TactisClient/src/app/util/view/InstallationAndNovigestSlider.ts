import { Installation } from 'src/app/model/installation';
import { NovigestSliderDb } from 'src/app/model/novigestSliderDb';

export class InstallationAndNovigestSlider {
    installation:Installation;
    novigest:NovigestSliderDb;

    constructor(installation:Installation, novigest:NovigestSliderDb){
        this.installation = installation;
        this.novigest = novigest;
    }
}