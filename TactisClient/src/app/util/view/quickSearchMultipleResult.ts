import { Clinic } from '../../model/clinic';
import { Client } from '../../model/client';
import { Contract } from '../../model/contract';
import { ContractChanges } from '../../model/contractchanges';

export class QuickSearchMultipleResult{
    clinicId:Clinic;
    clientId:Client;
    contractChange:ContractChanges;

    constructor(clinicId:Clinic, clientId:Client, contractChange:ContractChanges){
        this.clientId=clientId;
        this.clinicId=clinicId;
        this.contractChange=contractChange;
    }
}