import { Contract } from '../../model/contract';
import { Installation } from '../../model/installation';
import { Aditamento } from 'src/app/model/aditamento';

export class ContractAndInstallation {
    contract:Contract;
    installation:Installation;
    aditamento:Aditamento;

    constructor(contract?:Contract,installation?:Installation, aditamento?:Aditamento ){
        this.contract = contract;
        this.installation = installation;
        this.aditamento = aditamento;
    }

    static fromJSON(data: any) {
        return Object.assign(new Array, data);
    }
}