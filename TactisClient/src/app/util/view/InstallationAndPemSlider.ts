import { Installation } from 'src/app/model/installation';
import { PemSliderDb } from 'src/app/model/pemSliderDb';

export class InstallationAndPemSlider {
    data:Installation;
    children:PemSliderDb[];

    constructor(data:Installation, children:PemSliderDb[]){
        this.data = data;
        this.children = children;
    }
}