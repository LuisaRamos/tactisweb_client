import { Client } from 'src/app/model/client';
import { ContractData } from './ContractData';

export class ClientContracts {
    parent:Client;
    children:ContractData[];
}