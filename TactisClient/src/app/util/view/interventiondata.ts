import { Intervention } from 'src/app/model/intervention';

export class InterventionData {
    intervention:Intervention;
    produto:string;
    status:string;

    constructor(intervention:Intervention, produto:string, status:string){
        this.intervention = intervention;
        this.produto = produto;
        this.status = status;
    }
}