import { Clinic } from 'src/app/model/clinic';
import { ContractChanges } from 'src/app/model/contractchanges';
import { Contract } from 'src/app/model/contract';

export class ClinicChange {
    clinic:Clinic;
    divida:string;
}