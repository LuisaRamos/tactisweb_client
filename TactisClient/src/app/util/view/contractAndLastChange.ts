import { Contract } from 'src/app/model/contract';
import { ContractChanges } from 'src/app/model/contractchanges';

export class ContractAndLastChange {
    contract:Contract;
    lastChange:ContractChanges;
}