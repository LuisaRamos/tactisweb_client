import { ContractAndLastChange } from './contractAndLastChange';
import { InstallationData } from './InstallationData';

export class ContractData {
    data:ContractAndLastChange;
    children:InstallationData[];
}