import { Clinic } from 'src/app/model/clinic';
import { Installation } from 'src/app/model/installation';
import { ClientContractDividaView } from './ClientContractDividaView';
import { Client } from 'src/app/model/client';

export class ClinicDetailsView {
    clinic:Clinic;
    clientcontract:ClientContractDividaView[];
    installation:Installation;
    hasProduct:boolean;

    constructor(client:Client){
        this.clientcontract = [];
        this.clientcontract.push(new ClientContractDividaView(client));
    }
}