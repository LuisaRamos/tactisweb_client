export class NovigestMonitorView {
    idclinica:string;
    version:string;
    dataping:Date;

    constructor(idclinica:string,version:string, dataping:Date ){
        this.idclinica = idclinica;
        this.version = version;
        this.dataping = dataping;
    }
}