import { Contract } from 'src/app/model/contract';
import { Client } from 'src/app/model/client';
import { Clinic } from 'src/app/model/clinic';
import { Aditamento } from 'src/app/model/aditamento';
import { Installation } from 'src/app/model/installation';

export class ContractUnfinishedView {
    contract:Contract;
    clinics:Clinic[];
    client:Client;
    aditamento:Aditamento;
    installation:Installation;

    constructor(contract:Contract,clinics:Clinic[], client:Client, aditamento:Aditamento ){
        this.contract = contract;
        this.client = client;
        this.clinics = clinics;
        this.aditamento = aditamento;
    }
}