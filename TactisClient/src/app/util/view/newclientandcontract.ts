import { Client } from '../../model/client';
import { Contract } from '../../model/contract';

export class NewClientAndContract{
    client:Client;
    contract:Contract;

    constructor(client:Client, contract:Contract){
        this.client = client;
        this.contract = contract;
    }
}