import { TactisUser } from '../../model/tactisuser';
import { Event } from '../../model/event';

export class EventAndUsers {
    event:Event;
    users:TactisUser[];

    constructor(event?:Event, users?:TactisUser[]){
        this.event = event;
        this.users = users;
    }

    static fromJSON(data: any) {
        return Object.assign(new this, data);
    }
}