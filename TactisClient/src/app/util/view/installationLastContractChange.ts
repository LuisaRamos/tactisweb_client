import { Installation } from '../../model/installation';
import { ContractChanges } from '../../model/contractchanges';
import { PemMonitorView } from './pemmonitorview';
import { TactisApplicationsMonitorView } from './tumonitorview';
import { NovigestMonitorView } from './novigestmonitor';

export class InstallationLastContractChange {
    installation:Installation;
    lastChange:ContractChanges;
    novigest:NovigestMonitorView;
    novipem:PemMonitorView;
    updater:TactisApplicationsMonitorView;
    isSelected:boolean;

    constructor(installation:Installation, lastChange:ContractChanges, isSelected:boolean,
        novigest?:NovigestMonitorView,
        novipem?:PemMonitorView,
        updater?:TactisApplicationsMonitorView){
        this.installation = installation;
        this.lastChange = lastChange;
        this.novigest = novigest;
        this.novipem = novipem;
        this.updater = updater;
        this.isSelected = isSelected;
    } 
}