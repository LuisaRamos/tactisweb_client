import { Installation } from 'src/app/model/installation';
import { Aditamento } from 'src/app/model/aditamento';

export class AditamentoCreate {
    aditamento:Aditamento;
    installation:Installation;

    constructor(aditamento:Aditamento,installation?:Installation){
        this.aditamento = aditamento;
        this.installation = installation;
    }
}