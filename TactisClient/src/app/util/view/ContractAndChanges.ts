import { Contract } from '../../model/contract';
import { ContractChanges } from '../../model/contractchanges';

export class ContractAndChanges {
    contract: Contract;
    changes: ContractChanges[];
    status: string;
    codenr: number;
    aluguer: boolean;
    cmPagamentoDivida: string;
    cmPagamentoData: Date;
    cmDocDevolvido: string;
    cmDocData: Date;
    avlDocDevolvido: string;
    avlDocData: Date;


    constructor(contract: Contract, changes: ContractChanges[], status: string, codenr: number, aluguer: boolean, cmPagamentoDivida: string,
        cmPagamentoData: Date,cmDocDevolvido: string,cmDocData: Date,avlDocDevolvido: string, avlDocData: Date) {
        this.changes = changes;
        this.contract = contract;
        this.status = status;
        this.codenr = codenr;
        this.aluguer = aluguer;
        this.cmPagamentoDivida = cmPagamentoDivida;
        this.cmPagamentoData = cmPagamentoData;
        this.cmDocDevolvido = cmDocDevolvido;
        this.cmDocData = cmDocData;
        this.avlDocDevolvido = avlDocDevolvido;
        this.avlDocData = avlDocData;
    }
}