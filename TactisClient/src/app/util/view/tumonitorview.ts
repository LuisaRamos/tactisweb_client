export class TactisApplicationsMonitorView {
    idclinica:string;
    version:string;
    dataping:Date;
    apacheDaemon:boolean;

    constructor(idclinica:string,version:string,dataping:Date, apacheDaemon:boolean){
        this.idclinica = idclinica;
        this.version = version;
        this.dataping = dataping;
        this.apacheDaemon = apacheDaemon;
    }
}