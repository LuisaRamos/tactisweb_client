import { Training } from 'src/app/model/training';

export class TrainingView {
    users:string;
    training:Training;

    constructor(users:string, training:Training){
        this.users = users;
        this.training = training;
    }
}