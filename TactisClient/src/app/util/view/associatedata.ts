import { Installation } from 'src/app/model/installation';
import { Clinic } from 'src/app/model/clinic';

export class AssociateData {
    installation:Installation;
    clinicsToAdd:Clinic[];

    constructor(installation:Installation, clinicsToAdd:Clinic[]){
        this.installation = installation;
        this.clinicsToAdd = clinicsToAdd;
    }
}