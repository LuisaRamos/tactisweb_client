import { Client } from '../../model/client';
import { Clinic } from '../../model/clinic';
import { ClinicAndContractChange } from './CilnicAndContractChange';

export class ClientClinics {
    data:Client;
    children:ClinicAndContractChange[] = [];
    expanded:boolean;

    constructor(data:Client,children:ClinicAndContractChange[],expanded?:boolean){
        this.data=data;
        this.children = children;
        this.expanded = expanded;
    }
}