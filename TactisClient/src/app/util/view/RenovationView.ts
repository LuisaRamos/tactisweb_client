import { Client } from 'src/app/model/client';

export class RenovationView {
    client:Client;
    contractNumber:string;
    changeCode:string;
    dateAte:Date;
    local:string;
}