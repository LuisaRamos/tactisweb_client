export class ClinicInstallationAccess {
    clinicAccess:string;
    installationAccess:string;

    constructor(clinic:string, inst:string){
        this.clinicAccess = clinic;
        this.installationAccess = inst;
    }
}